#!/usr/bin/python3
"""Convert CMOS-style SBF references to 青い花 page references."""

import argparse
import math
import re
import sys

# Control verbosity of output. Set to True for debugging.
VERBOSE = False


def get_page_ref():
    """Get CMOS-style page reference string from command line argument."""

    parser = argparse.ArgumentParser()
    parser.add_argument(
        'page_ref',
        nargs='?',
        default='',
        help='SBF page reference')
    parser.add_argument(
        '--test',
        action='store_true',
        help="run conversion tests",
    )

    # Either --test or a page reference must be present.
    args = parser.parse_args()
    if not args.test and args.page_ref == '':
        parser.print_usage()
        sys.exit('You must specify either a page reference or --test')

    return (args.test, args.page_ref)


def get_page_range(ref):
    """Given a CMOS page reference, return volume plus page range."""

    assert isinstance(ref, str) and ref != ''

    # Page references are of the form x:yyy or x:yyy-zzz where x is
    # the volume number, yyy is the first page number in the
    # reference, '-' is a dash of some sort (see below), and zzz is
    # the end of the page number range (in CMOS format).

    # NOTE: To ensure maximum matching of page references, we allow
    # the "dash" to be one or more of the following: a hyphen-minus
    # ('-', U+002d), an en dash ('–', U+2013), an em dash ('—',
    # U+2014), or a horizontal bar ('―', U+2015). To conform to proper
    # regular expression syntax, these are listed in reverse order in
    # the character class, with the hyphen-minus last.

    # References may also have a preceding or succeeding whitespace or
    # underscore, and may be followed by additional punctuation or
    # other material we can ignore.
    ref_re = '^ *_?([1-9][0-9]*):([1-9][0-9]*)([―—–-]+)?([0-9]+)?_?[,; ]?.*$'

    # Look for a page reference.
    ref_m = re.match(ref_re, ref)
    if VERBOSE:
        print(f"get_page_range: {ref_m}")
    if ref_m is None:
        return (0, 0, 0)

    # Pull out the volume, starting page, and ending page (CMOS format).
    volume = int(ref_m.group(1))
    if ref_m.group(2) is None:  # Shouldn't happen, but...
        return (0, 0, 0)
    if ref_m.group(4) is None:  # Single page
        first = int(ref_m.group(2))
        cmos_last = first
    else:  # Range of pages
        first = int(ref_m.group(2))
        cmos_last = int(ref_m.group(4))

    if VERBOSE:
        print(f"get_page_range: {volume}, {first}, {cmos_last}")

    return (volume, first, cmos_last)


def get_last_page(first, cmos_last):
    """Convert CMOS-style page range to first and last page numbers."""

    assert isinstance(first, int) and first > 0
    assert isinstance(cmos_last, int) and cmos_last > 0

    if cmos_last >= first:  # Second part is the actual page number
        last = cmos_last
    else:  # Second part contains only changed digits from first part
        # Compute the number of digits in the second part.
        n_digits = math.ceil(math.log10(cmos_last))

        # Compute base digits of first part, ignoring last n digits.
        first_base = math.floor(first / 10**n_digits)

        # The last page number is that value plus the changed digits.
        last = first_base * 10**n_digits + cmos_last

    if VERBOSE:
        print(f"get_last_page: {last}")

    return last


def en_to_jp_range(volume, first, last, pages):
    """Convert SBG volume and page numbers to 青い花 equivalents."""

    # Determine the first and last page for each part of this volume.
    first_1 = pages['part1_first'][volume]
    last_1 = pages['part1_last'][volume]
    first_2 = pages['part2_first'][volume]
    last_2 = pages['part2_last'][volume]

    if VERBOSE:
        print(f"en_to_jp_range: {first_1}, {last_1}, {first_2}, {last_2}")

    # We have three cases:
    #
    #   1) The first and last pages are in the first part of the volume,
    #      in which case the page numbers map exactly and only the volume
    #      number changes.
    #   2) The first and last pages are in the second part of the volume,
    #      in which case the page numbers must be adjusted based on the
    #      page number of SBF on which the second part starts.
    #   3) Otherwise this is not a valid page reference.
    if first_1 <= first <= last_1 and first_1 <= last <= last_1:
        return (2 * volume - 1, first - first_1 + 1, last - first_1 + 1)
    if first_2 <= first <= last_2 and first_2 <= last <= last_2:
        return (2 * volume, first - first_2 + 1, last - first_2 + 1)
    return (0, 0, 0)


def get_jp_page_range(volume, first, last):
    """Convert Sweet Blue Flowers page range to 青い花 page range."""

    assert isinstance(volume, int) and volume > 0
    assert isinstance(first, int) and first > 0
    assert isinstance(last, int) and last > 0

    # Check that the original page range is valid.
    if volume > 4 or first > last:
        return (0, 0, 0)

    # Each omnibus volume of Sweet Blue Flowers has two parts, each of
    # which corresponds to a separate tankōbon volume of 青い花.
    #
    # For each SBF volume, keep track of the page number of the first
    # and last page of the first part of the volume and the page
    # numbers of the first and last pages of the second part.
    #
    # NOTE: We put 0 as the first entry of the lists so that the lists
    # can be indexed directly using the volume number.
    #
    # NOTE 2: Page 1 of volume 5 of the Japanese edition actually
    # corresponds to page 3:3 of the English edition. Similarly, Page
    # 1 of volume 7 of the Japanese edition actually corresponds to
    # page 4:3 of the English edition.
    pages = {
        'part1_first': [0, 1, 1, 3, 3],
        'part1_last': [0, 193, 177, 177, 176],
        'part2_first': [0, 197,181, 181, 181],
        'part2_last': [0, 381, 357, 360, 371],
    }

    return en_to_jp_range(volume, first, last, pages)


def get_jp_page_ref(volume, first, last):
    """Return page reference string for 青い花 page range."""

    assert isinstance(volume, int) and volume > 0
    assert isinstance(first, int) and 0 < first <= last
    assert isinstance(last, int) and 0 < last < 100000

    # If the first and last pages are the same, simply return
    # a page reference to the first page.
    if first == last:
        jp_page_ref = f"({volume}) p. {first}"
        if VERBOSE:
            print(f"get_jp_page_ref: {jp_page_ref}")
        return jp_page_ref

    # We want to find the last set of digits where the first page
    # number and last page number are different.

    # Convert the page numbers into equal-length strings.
    first_str = f"{first:>5}"
    last_str = f"{last:>5}"

    # Iterate over both strings and create a string of digits once
    # they are different between the two strings.
    different_digits = ''
    differ = False
    for first_c, last_c in zip(first_str, last_str):
        if first_c != last_c:
            differ = True
        if differ:
            different_digits = different_digits + last_c

    # Return the page reference. Note that the separator between
    # the two page numbers is an en dash (U+2013), not a hyphen.
    return f"({volume}) p. {first}–{different_digits}"


def en_to_jp_ref(page_ref):
    """Given an SBF page reference string, return a 青い花 reference."""

    # Parse the SBF reference into volume number and two page numbers.
    (volume, first, cmos_last) = get_page_range(page_ref)

    # The SBF reference was not valid.
    if volume == 0:
        return ''

    # Compute the actual last page based on the CMOS algorithm.
    last = get_last_page(first, cmos_last)
    if VERBOSE:
        print(f"en_to_jp_ref: {volume}, {first}, {last}")

    # Convert the SBF page numbers into 青い花 page numbers.
    (jp_volume, jp_first, jp_last) = get_jp_page_range(
        volume,
        first,
        last,
    )
    if VERBOSE:
        print(f"en_to_jp_ref: {jp_volume}, {jp_first}, {jp_last}")

    # Check to make sure the conversion worked.
    if jp_volume <= 0:
        return ''

    # Use the 青い花 page numbers to create a page reference.
    return get_jp_page_ref(jp_volume, jp_first, jp_last)


def test_start_end():
    """Basic tests for start and end of various parts."""

    assert en_to_jp_ref('1:1') == '(1) p. 1'
    assert en_to_jp_ref('1:193') == '(1) p. 193'
    assert en_to_jp_ref('1:197') == '(2) p. 1'
    assert en_to_jp_ref('1:381') == '(2) p. 185'
    assert en_to_jp_ref('2:1') == '(3) p. 1'
    assert en_to_jp_ref('2:177') == '(3) p. 177'
    assert en_to_jp_ref('2:181') == '(4) p. 1'
    assert en_to_jp_ref('2:357') == '(4) p. 177'
    assert en_to_jp_ref('3:3') == '(5) p. 1'
    assert en_to_jp_ref('3:177') == '(5) p. 175'
    assert en_to_jp_ref('3:181') == '(6) p. 1'
    assert en_to_jp_ref('3:360') == '(6) p. 180'
    assert en_to_jp_ref('4:3') == '(7) p. 1'
    assert en_to_jp_ref('4:176') == '(7) p. 174'
    assert en_to_jp_ref('4:181') == '(8) p. 1'
    assert en_to_jp_ref('4:371') == '(8) p. 191'


def test_beyond_boundaries():
    """Test page references beyond boundaries of parts."""

    assert en_to_jp_ref('1:194') == ''
    assert en_to_jp_ref('1:196') == ''
    assert en_to_jp_ref('2:178') == ''
    assert en_to_jp_ref('2:180') == ''
    assert en_to_jp_ref('2:358') == ''
    assert en_to_jp_ref('3:2') == ''
    assert en_to_jp_ref('3:178') == ''
    assert en_to_jp_ref('3:180') == ''
    assert en_to_jp_ref('3:361') == ''
    assert en_to_jp_ref('4:2') == ''
    assert en_to_jp_ref('4:177') == ''
    assert en_to_jp_ref('4:180') == ''
    assert en_to_jp_ref('4:372') == ''


def test_max_ranges():
    """Test page ranges with maximum extents."""

    assert en_to_jp_ref('1:1–193') == '(1) p. 1–193'
    assert en_to_jp_ref('1:197–381') == '(2) p. 1–185'
    assert en_to_jp_ref('2:1–177') == '(3) p. 1–177'
    assert en_to_jp_ref('2:181–357') == '(4) p. 1–177'
    assert en_to_jp_ref('3:3–177') == '(5) p. 1–175'
    assert en_to_jp_ref('3:181–360') == '(6) p. 1–180'
    assert en_to_jp_ref('4:3–176') == '(7) p. 1–174'
    assert en_to_jp_ref('4:181–371') == '(8) p. 1–191'


def test_too_large_ranges():
    """Test page ranges beyond boundaries of parts."""

    assert en_to_jp_ref('1:2–194') == ''
    assert en_to_jp_ref('1:198–382') == ''
    assert en_to_jp_ref('2:2–178') == ''
    assert en_to_jp_ref('2:182–358') == ''
    assert en_to_jp_ref('3:2–178') == ''
    assert en_to_jp_ref('3:182–361') == ''
    assert en_to_jp_ref('4:2–177') == ''
    assert en_to_jp_ref('4:182–372') == ''


def test_cmos_ranges():
    """Test page ranges with CMOS-style numbering."""

    assert en_to_jp_ref('1:3–10') == '(1) p. 3–10'
    assert en_to_jp_ref('1:71–72') == '(1) p. 71–2'
    assert en_to_jp_ref('2:96–117') == '(3) p. 96–117'
    assert en_to_jp_ref('2:100–104') == '(3) p. 100–4'
    assert en_to_jp_ref('3:103–9') == '(5) p. 101–7'
    assert en_to_jp_ref('3:123–28') == '(5) p. 121–6'
    assert en_to_jp_ref('4:144–58') == '(7) p. 142–56'


def test():
    """Test correctness of page reference conversions."""

    print('running tests')
    test_start_end()
    test_beyond_boundaries()
    test_max_ranges()
    test_too_large_ranges()
    test_cmos_ranges()
    print('all tests succeeded')


def main():
    """Convert SBF CMOS-style references to 青い花 page references."""

    (run_test, page_ref) = get_page_ref()
    if run_test:
        test()
    elif page_ref != '':
        jp_page_ref = en_to_jp_ref(page_ref)
        if not jp_page_ref:
            sys.exit(f"{page_ref} is not a valid SBF page reference")
        print(jp_page_ref)


if __name__ == "__main__":
    main()
