---
title: "Appendix 1: Character Index"
---

## Appendix 1: Character Index

This index is for readers trying to keep track of the characters in
*Sweet Blue Flowers*, many of whom are unnamed or not given full names
upon their first appearance in the manga. It lists characters under
both their given names and family names where known, and where
appropriate also by their roles in relation to other characters
(e.g., “Yasuko’s mother”).[^07-01-01]
{:.first}

[^07-01-01]: One ambiguous case is regarding Fumi’s cousin Chizu. Since Chizu’s family name is not given until volume 3, well after she is married early in volume 1, I have assumed that Hanashiro is her husband’s family name and that Chizu took it upon marrying him.

The index lists pages on which a particular character is depicted or
(for minor characters) mentioned, omitting incidental appearances by
major characters.[^07-01-02] The index also lists significant events
in the characters’ lives, for example, for readers wishing to consult
the parts of the manga dealing with Fumi’s relationship with
Yasuko. Events are listed in chronological order (as best I can
determine such order) rather than by their location in the manga.

[^07-01-02]: The book’s source repository (see the colophon) contains an unabridged character index that lists all appearances by all characters.

*Characters with names in boldface are those included in the profiles
at the beginnings of the original Japanese volumes. Page numbers in
italics refer to depictions of characters on cover pages, tables of
contents, or chapter title pages, or in character profiles or
afterwords.*

- Akihiko. *See* Ikumi, Akihiko
- Akira. *See* Okudaira, Akira
- Akira’s aunt. *See* Keiko (aunt of Akira, sister of Sakiko)
- Akira’s brother. *See* Okudaira, Shinobu
- Akira’s childhood friend. *See* Kurata
- Akira’s father. *See* Okudaira (father of Akira)
- Akira’s friends at Fujigaya. *See* Ikumi, Kyoko; Ueda, Ryoko
- Akira’s mother. *See* Okudaira, Sakiko
- Arai, Asuka (Tomoka Arai in the Japanese edition),
  4:76--77 (?);
  plays D’Artagnan in *The Three Musketeers*,
  4:119--23,
  *4:147*,
  4:168
- &nbsp;
- boy who likes Fumi. *See* Tanaka, Atsushi
- &nbsp;
- Chie. *See* Sugimoto, Chie
- Chie’s friend (and admirer?). *See* Sonoko
- Chizu. *See* Hanashiro, Chizu
- Chizu’s husband. *See* Hanashiro (husband of Chizu)
- Chizu’s mother,
  3:126;
  calls young Chizu “very rebellious” and “never obedient,”
  3:125
- Chizu’s uncle. *See* Yoshio (uncle of Chizu)
- &nbsp;
- flight attendant:
  assists Akira when she falls ill,
  4:263
- Fumi. *See* Manjome, Fumi
- Fumi’s cousin. *See* Hanashiro, Chizu
- Fumi’s father. *See* Manjome (father of Fumi)
- Fumi’s friends at Matsuoka. *See* Honatsugi, Yoko (Pon); Motegi, Miwa (Mogi); Yasuda, Misako (Yassan)
- Fumi’s grandmother,
  1:53,
  1:66,
  4:320--22
- Fumi’s male admirer. *See* Tanaka, Atsushi
- Fumi’s mother. *See* Manjome (mother of Fumi)
- Fumi (servant of Sugimoto family),
  1:302,
  1:305,
  1:317--19,
  2:86--87
- Fujigaya drama club president (Akira’s first year),
  1:83,
  1:111--12,
  1:150,
  1:159,
  1:166,
  1:173,
  1:264,
  1:336--37;
  tells Yasuko that she wants her to play Heathcliff,
  1:83;
  tells Mr. Kagami that they invited Yasuko to perform in *Wuthering Heights*,
  1:111--12;
  scolds Mr. Kagami for not showing up for rehearsals,
  1:248--49
- Fujigaya drama club president (Akira’s second year),
  2:254,
  2:263,
  2:282,
  2:293;
  envisions Fumi for a part in *Rokumeikan*,
  2:271--72,
  2:274--75;
  concedes that it was her fault to invite Fumi to audition for *Rokumeikan*,
  2:292,
  2:308;
  accompanies Akira to Matsuoka to lobby for that drama club,
  3:271--72
- Fujigaya elementary school student (star of *The Little Prince*),
  1:225--27,
  1:235
- Fujigaya elementary school students:
  greet Akira on her first day at Fujigaya,
  1:19;
  perform in *The Bamboo Cutter*,
  3:63,
  3:70
- Fujigaya junior high school students:
  perform in *The Izu Dancer*,
  3:74--75;
  perform in *The Diary of Anne Frank*,
  4:161--62
- Fujigaya newspaper editor-in-chief (admirer of Hinako):
  interviews Hinako about her personal life,
  4:213--16;
  confesses to Hinako,
  4:230--32;
  spreads rumors about Hinako,
  2:204--5,
  4:234--36;
  is helped by Hinako while on the school trip to England,
  4:237--38,
  4:247--49;
  asks Hinako to pin on her graduation corsage,
  4:310--11
- Fujigaya nun(s),
  1:19,
  1:288--89;
  scolds Akira for running on the stairs,
  1:281,
  1:283;
  are mentioned as disapproving of *The Three Musketeers* production,
  4:67
- Fujigaya principal (?),
  1:285
- Fujigaya student (costume mistress in drama club),
  3:16,
  3:43,
  3:74,
  3:85--86,
  3:97,
  3:108
- Fujigaya student (lesbian). *See* Maeda
- Fujigaya student (member of newspaper club),
  4:120--24,
  4:138--39
- Fujigaya students (Hinako’s admirers). *See* Fujigaya newspaper editor-in-chief; Ito; Kawakubo
- Fujigaya students (Kyoko’s admirers),
  2:319,
  4:127--29
- Fujigaya teacher (of home economics),
  3:294
- Fujigaya teacher (Orie’s former teacher),
  2:144,
  3:45
- &nbsp;
- Guide on Matsuoka school trip to Nagasaki. *See* Kohinata, Ayumi
- &nbsp;
- Hanae (aunt of Ko, sister of Ko’s mother),
  3:156;
  hosts Kyoko and her friends at Ko’s family’s summer home,
  2:29,
  2:45,
  2:47--49,
  2:52,
  2:61;
  cares for Akira and Fumi while they’re sick,
  2:40--44
- Hanae (friend of Fumi). *See* Mori, Hanae
- Hanae’s daughters (cousins of Ko),
  2:14,
  2:42,
  2:45--47
- **Hanashiro, Chizu** (cousin of Fumi),
  *3:5*,
  *3:183*,
  *4:5*,
  *4:183*;
  visits Fumi while Fumi is in second grade,
  3:124--26;
  visits Fumi while Fumi is in fifth grade and marvels at her height,
  3:127--28;
  visits Fumi while Fumi is in junior high and has a sleepover,
  3:129--32;
  claims that she might stay single forever,
  3:131;
  asks Fumi if she likes anyone,
  3:131--32;
  has sex with Fumi,
  1:65,
  2:337;
  visits Fumi before her wedding,
  1:38--41;
  visits Fumi with her husband,
  1:59;
  visits Fumi and asks whom Fumi likes,
  3:133--34;
  calls Fumi “*that* kind of girl,”
  3:135;
  expresses regrets about ending her relationship with Fumi and tells her that she herself can’t be “*that* kind of girl,”
  3:136--38
- Hanashiro (husband of Chizu),
  1:59,
  1:61
- Haruka. *See* Ono, Haruka
- Haruka’s sister. *See* Ono, Orie
- Hinako. *See* Yamashina, Hinako
- Hinako’s admirers. *See* Fujigaya newspaper editor-in-chief; Ito; Kawakubo
- Hinako’s aunt. *See* Shimozuka
- Hinako’s partner. *See* Ono, Orie
- **Honatsugi, Yoko (Pon)** (friend of Fumi),
  *1:3*,
  1:342--44,
  1:353,
  *2:182*,
  2:191--92,
  2:199,
  2:237,
  2:309--12,
  *3:5*,
  3:110,
  3:145--47,
  *3:183*,
  *3:185*,
  3:199--202,
  3:220--21,
  3:273,
  3:304--6,
  3:310--11,
  *4:5*,
  4:14,
  4:16--17,
  4:92--94,
  4:118,
  4:132,
  *4:183*,
  4:189--90,
  4:255,
  4:283--84,
  4:306--7,
  4:313--14,
  4:316,
  4:337--40,
  4:352--53;
  participates in drama club in junior high,
  4:192--93;
  meets Fumi and tries to recruit her to the Matsuoka drama club,
  1:21--25,
  1:56,
  1:58;
  visits Fujigaya with Yasuko, Fumi, Yassan, and Mogi,
  1:77--78,
  1:81,
  1:85;
  interrupts Fumi’s conversation with Yasuko,
  1:91;
  accompanies Yasuko on a visit to Fujigaya,
  1:147,
  1:150--52,
  1:154;
  helps out at and attends the *Wuthering Heights* performance,
  1:222--24,
  1:232,
  1:234,
  1:241;
  witnesses Akira’s fight with Yasuko,
  1:342--44;
  goes with Fumi and her other friends on a trip to Ko’s family’s summer home,
  2:8,
  2:10--12,
  2:16,
  2:18,
  2:20,
  2:26,
  2:28--30,
  2:32--33,
  2:39--40,
  2:46--48,
  2:61;
  with Yassan and Kyoko, teases Mogi about liking Shinobu,
  2:62--63;
  visits Akira’s house for a sleepover,
  2:132--34;
  performs in a minor role in *Rokumeikan*,
  3:71,
  3:73;
  asks Fumi if she’s “that type of girl,”
  3:307--9;
  shows Fumi a script (*Heavenly Creatures*) that she wrote about Fumi,
  4:102--6;
  rehearses *Heavenly Creatures* with the Matsuoka drama club,
  4:245;
  performs in *Heavenly Creatures* before judges,
  4:285
- &nbsp;
- Ikumi, Akihiko (father of Kyoko),
  2:241--42 (?),
  4:60--62
- Ikumi, Kayoko (mother of Kyoko),
  2:299--300,
  4:36,
  4:44,
  4:63,
  4:67--68;
  goes on a marriage interview with Akihiko,
  4:58--61;
  marries Akihiko,
  4:62;
  loses Kyoko in the woods and is scolded by Akihiko for it,
  4:64--65;
  laments the growing distance between herself and her husband and daughter,
  4:66--68;
  asks Kyoko if she’s lonely,
  3:100;
  tries and fails to get Kyoko to go out with her,
  3:101;
  is complimented by Ko on her appearance,
  1:94;
  claims that it’s her fault that Ko doesn’t visit anymore,
  4:48--50;
  is visited by Ko after a long absence,
  4:69--72
- **Ikumi, Kyoko** (friend of Akira),
  *1:3*,
  1:108,
  *1:117*,
  1:135,
  1:150,
  *1:198*,
  1:232,
  *1:282*,
  *2:38*,
  2:126,
  2:128,
  2:133,
  2:144--45,
  *2:181*,
  *2:183*,
  2:185--87,
  2:201,
  *2:229*,
  2:231--32,
  2:266,
  2:283,
  2:296,
  2:300
  2:342,
  *3:4*,
  3:25--27,
  3:86,
  3:88--89,
  3:104--5,
  3:109,
  3:111,
  3:140--42,
  3:146--47,
  *3:182*,
  3:267--71,
  3:287,
  *4:4*,
  4:8--9,
  4:21,
  4:23,
  *4:27*,
  4:46,
  4:48--50,
  4:52,
  4:68,
  4:70--72,
  4:74,
  4:78--80,
  4:84--86,
  *4:99*,
  4:138--40,
  *4:147*,
  *4:182*,
  4:219,
  4:224,
  4:239--42,
  4:262 (?),
  4:294,
  4:302,
  4:306,
  4:309,
  4:313--14,
  4:316,
  4:319
  - childhood:
    is praised for her looks,
    4:28--29;
    gets lost in the woods and is found by Ko,
    2:37,
    2:67--68,
    4:24--25,
    4:64--66;
    tells Ko that she’s glad that they’ll get to marry,
    4:30--32;
    tells Ko again how much she likes him,
    4:33--35;
    is lonely as a child, and discovers her mother and others pray to a different God,
    3:100;
  - junior high:
    thinks to herself that she hates her mother and father and that everyone in her family is worthless,
    3:101,
    3:103;
    deals with her mother’s illness,
    4:35--38;
    has sex with Ko and calls herself “ugly,”
    4:39--44;
    joins the Fujigaya art club and meets Yasuko,
    4:45;
    imitates Yasuko by cutting her hair and contemplating joining the Fujigaya drama club,
    2:115--16
  - high school, year 1:
    introduces herself to Akira,
    1:26;
    is asked by Akira to join the Fujigaya drama club,
    1:55;
    gets a love letter from a junior high girl,
    1:78--79;
    meets Yasuko again and is seen crying by Fumi afterward,
    1:83--84,
    1:87;
    refuses an offer of a lift from Ko,
    1:95--96;
    invites Akira to a singles party and tells her that “the person I like rejected me,”
    1:97--98;
    introduces Akira to Ko, and tells her that Ko took a liking to her,
    1:103,
    1:129--31;
    meets Mr. Kagami and asks about Yasuko,
    1:132--34,
    1:136--37;
    talks to Akira about liking girls,
    1:145--46;
    asks Fumi if she likes Yasuko,
    1:170--72;
    breaks down in front of Akira while talking about Fumi and Yasuko,
    1:174--75;
    is comforted by Akira regarding other girls’ interest in Yasuko, and tells herself that she’s different from them,
    1:208--12;
    talks with Yasuko about their non-relationship,
    1:212--15;
    talks with Yasuko backstage at *Wuthering Heights*,
    1:244--45;
    tells Ko (and Akira) that she thinks of him like a big brother,
    1:261--62;
    learns about Yasuko’s breakdown on seeing Mr. Kagami,
    1:263--64;
    meets Akira’s family,
    1:265;
    teases Akira about her having to go to the bathroom and getting scolded by a nun,
    1:282,
    1:284;
    tells Akira about her time at Fujigaya and studying with Kazusa,
    1:285--90;
    accuses Mr. Kagami of lying about not knowing Yasuko,
    1:336--40;
    invites Akira to visit Ko’s family with Fumi and Fumi’s friends,
    1:357--58;
    after learning that Fumi and Yasuko have broken up, approaches Yasuko again unsuccessfully,
    1:358--59,
    1:361,
    1:363--67;
    bonds with Fumi over their respective relationships with Yasuko,
    1:374--76;
    goes with her friends to visit Ko’s family’s summer home,
    2:6--11,
    2:15,
    2:17--20,
    2:28--30,
    2:34,
    2:39--42,
    2:45,
    2:46 (?),
    2:47--49,
    2:61,
    2:63;
    tells her friends about getting lost as a child and rescued by Ko,
    2:24--26;
    talks with Yasuko before Yasuko leaves for England,
    2:145--48
  - high school, year 2:
    teases Mogi about her liking Shinobu,
    2:62;
    attends Mr. Kagami’s wedding and introduces Ko to Yasuko,
    2:70--71,
    2:80--82,
    2:92,
    2:94,
    2:98;
    begins second year at Fujigaya and meets Haruka,
    2:193--98,
    2:224--26;
    is encouraged by Akira and others to audition for *Rokumeikan*,
    2:235--36,
    2:241--42;
    auditions for a part in *Rokumeikan*,
    2:281;
    has an awkward conversation with Ko,
    2:297--99;
    reflects that her mother “has her own special god,”
    2:301,
    2:324;
    receives another love letter from an admirer,
    2:320--23;
    thinks to herself how she was ashamed of her mother,
    2:324;
    talks with Kazusa and Shinako about Mr. Kagami and Yasuko,
    3:10--14;
    learns that Ko wants to end their engagement,
    3:25--27;
    receives flowers from Ko,
    3:46--47;
    asks Ko if they can stay engaged,
    3:90--91;
    plays Asako in *Rokumeikan*,
    3:15,
    3:59,
    3:69,
    3:80--81,
    3:87,
    3:91--92,
    3:95--99,
    3:102--3,
    3:105;
    is praised for her performance in *Rokumeikan*,
    3:109;
    tells Ko’s aunt that Ko broke up with her and that she won’t be visiting,
    3:156--57;
    denies to Kasuza that she still likes Yasuko,
    3:160--61;
    visits Yasuko and tells her she doesn’t know if she still has a crush on her,
    3:186--91;
    talks with Akira about Akira dating Fumi,
    3:267--70
  - high school, year 3:
    breaks down in tears after Ko tells her that he likes her,
    4:25--26;
    tells Ueda her worries about her mother,
    4:53--55;
    tries to reconnect to Ko,
    4:56--57;
    talks with Akira about choosing the drama club play,
    4:88--89;
    is stalked by younger students,
    4:127--29;
    writes to Yasuko in England,
    4:129--30
  - after high school:
    prepares to wed Ko,
    4:332--33;
    marries Ko,
    4:349
- Ito (Fujigaya year 2 student):
  asks Hinako how she’ll be spending the holidays,
  3:322--26
- &nbsp;
- Kagami, Shiho (child of Kazusa and Mr. Kagami),
  4:335--36
- Kagami (husband of Kazusa),
  1:332,
  1:348,
  2:87--89,
  3:108,
  3:192--95,
  4:19--20,
  4:73--74,
  4:84--86,
  4:88;
  meets Yasuko and encourages her to join the Fujigaya drama club,
  2:111,
  2:113--14;
  calls Yasuko a “library maiden,”
  1:162;
  rejects Yasuko’s advances,
  2:116;
  apologizes for not supervising the Fujigaya drama club, and asks about Yasuko and her involvement in *Wuthering Heights*,
  1:111--12;
  is criticized for not showing up at drama club meetings,
  1:131--32;
  asks again about Yasuko being invited to perform in *Wuthering Heights*, but evades Kyoko’s questions about her,
  1:132--34,
  1:136--37;
  meets Yasuko again,
  1:155--57,
  1:163;
  congratulates Yasuko on her performance in *Wuthering Heights*,
  1:248--50;
  reads a story about his marriage in the school newspaper,
  1:335--36;
  is reproached by Kyoko for lying about his not knowing Yasuko,
  1:337--40;
  talks with Yasuko (who interprets it as flirting) before his wedding with Kazusa,
  2:87--89;
  weds Kazusa,
  2:93--94,
  2:98;
  recalls the *Rokumeikan* line about “a helpless child inside” and Yasuko’s using that line in her letter to him,
  3:107;
  congratulates Ueda on her performance in *Rokumeikan*,
  3:108;
  finds out that the Fujigaya students know about Kazusa’s pregnancy,
  4:18--20;
  shows up unexpectedly at a Fujigaya drama club meeting,
  4:84--86,
  4:88
- Kanako (sister of Ko). *See* Sawanoi, Kanako
- Kanako (adult lesbian):
  regrets mentioning to a girl student that it’s Valentine’s Day,
  3:353--55;
  tells a man at a marriage interview, “I like girls,”
  3:356
- Kaoruko (admirer of Shinako):
  in high school, confesses to Shinako, goes out with her, and then breaks up with her,
  2:347--53;
  as an adult, has dinner with Shinako and regrets that her love for Shinako is one-sided,
    2:354
- Kawakubo (student admirer of Hinako):
  has a crush on her kindergarten teacher and her elementary school classmate Kusakabe,
  4:173;
  confesses to Hinako but is rejected by her,
  2:165--67,
  2:170--73,
  4:174--75;
  confides to Hinako that her parents think that she’s sick because she likes girls,
  2:169;
  finds a girlfriend as an adult,
  4:176
- Kawasaki (classmate and then roommate of Yasuko),
  4:130,
  4:224,
  4:265;
  complains about *Wuthering Heights* posters,
  1:166;
  performs in *Wuthering Heights* with Yasuko,
  1:168,
  1:242--44;
  plans to room with Yasuko in England,
  2:142--43;
  shops with Yasuko for a present for Kazusa’s baby,
  4:22--23;
  meets Akira and others in England,
  4:241--42,
  4:246;
  is sick with appendicitis and is tended to by her boyfriend,
  4:336,
  4:338
- Kawasaki’s boyfriend,
  4:338
- Kayoko. *See* Ikumi, Kayoko
- Kazusa. *See* Sugimoto, Kazusa
- Keiko (aunt of Akira, sister of Sakiko),
  1:371,
  4:149,
  4:151;
  tells Akira that she didn’t expect her to go to Fujigaya, and that she’ll come watch her if she performs in a play,
  1:272--73;
  claims that she’s “not expecting much” from Akira’s performance in *Rokumeikan*,
  3:79;
  is visited by Shinobu and Mogi,
  3:239;
  wants the Fujigaya drama club to “do something sexy,”
  4:148;
  is reported as being “in a huff” about the performance of *The Three Musketeers*,
  4:169
- Ko. *See* Sawanoi, Ko
- Ko’s aunt. *See* Hanae (aunt of Ko)
- Ko’s cousins. *See* Hanae’s daughters (cousins of Ko)
- Ko’s father. *See* Sawanoi (father of Ko)
- Ko’s mother. *See* Sawanoi (mother of Ko)
- Ko’s sister. *See* Sawanoi, Kanako
- Kohinata, Ayumi (guide on Nagasaki trip),
  4:118,
  4:131--32
- Komako (admirer of Kuri),
  2:2--3,
  2:176--77
- Kubota (Fujigaya teacher, advisor to the student newspaper?),
  4:230
- Kudo (Fujigaya student),
  1:282,
  1:284--86,
  1:288--90
- Kurata (childhood friend of Akira),
  2:137--38
- Kuri. *See* Sugimoto, Kuri
- Kuri’s admirer. *See* Komako
- Kusakabe (elementary student admired by Kawakubo),
  4:173
- Kyoko. *See* Ikumi, Kyoko
- Kyoko’s admirers. *See* Fujigaya students (Kyoko’s admirers)
- Kyoko’s father. *See* Ikumi, Akihiko
- Kyoko’s mother. *See* Ikumi, Kayoko
- &nbsp;
- Maeda (Fujigaya student):
  admits to her friend that she’s a lesbian,
  3:357--60
- Maeda’s friend. *See* Nakajima
- **Manjome, Fumi**,
  *1:1--2*,
  *1:4*,
  *1:7*,
  1:33,
  *1:45*,
  *1:93*,
  1:173--74,
  *1:198*,
  *1:202*,
  1:353,
  *1:354*,
  *2:1*,
  *2:4*,
  *2:5*,
  2:43--44,
  2:53--54,
  2:59,
  2:61,
  2:63,
  2:65--67,
  2:75--77,
  2:79,
  2:103--7,
  2:118--20,
  2:126--27,
  2:130,
  2:134--37,
  *2:183*,
  2:185--86,
  *2:189*,
  2:199--200,
  2:213,
  2:215--16,
  2:226--27,
  2:237,
  2:245,
  2:248--50,
  2:267--70
  2:287--95,
  *2:302*,
  2:325--27,
  2:339--42,
  2:344,
  *2:357*,
  *3:5*,
  3:21,
  3:30,
  3:40--41,
  3:60,
  3:73,
  3:93--94,
  *3:115*,
  3:150--51,
  *3:183*,
  3:198--99,
  3:202--3,
  3:210,
  3:212--13,
  3:220,
  3:222--23,
  *3:260*,
  3:265--66,
  3:276,
  3:283--85,
  *3:303*,
  3:304--6,
  3:313--17,
  3:320,
  3:330--37,
  *4:1*,
  *4:5*,
  4:14--15,
  4:81,
  4:90,
  4:93--98,
  4:102--4,
  4:108--10,
  4:113,
  4:118,
  4:123,
  4:125,
  4:132--33,
  4:145--46,
  4:157,
  4:159--60,
  4:162--64,
  4:168--70,
  *4:179*,
  *4:180*,
  *4:183*,
  4:185,
  *4:186*,
  4:200,
  4:219--22,
  4:225--29,
  4:255--56,
  *4:257*,
  4:268,
  *4:281*,
  4:282--85,
  4:289,
  4:295--96,
  4:314--18,
  4:323--24,
  *4:330*,
  4:360--61,
  *4:370--71*
  - childhood:
    is called a crybaby by Akira,
    1:25--26;
    urinates on herself at school and is helped by Akira,
    1:29--31;
    visits her grandmother along with Akira,
    1:53;
    is comforted by Akira when sick,
    1:71;
    has a sleepover with Akira in elementary school,
    3:241--42;
    is helped by Akira in a Christmas performance,
    3:68,
    3:76;
    says goodbye to Akira and promises to write her,
    1:32,
    3:318;
    enters second grade in a new school and makes new friends,
    3:116--23;
    is visited by Chizu while in second grade,
    3:124--26;
    is visited by Chizu while in fifth grade,
    3:127--28;
  - junior high:
    is visited by Chizu as Chizu enters university,
    3:129--30;
    talks with Chizu about not liking boys,
    3:131--32;
    has sex with Chizu,
    1:65,
    2:337
  - high school, year 1:
    1:14,
    1:23--24,
    1:33,
    1:42--44;
    meets Akira again without recognizing her,
    1:12,
    1:14;
    is rescued from a groper by Akira,
    1:16--17;
    enters Matsuoka Girls’ High School and meets Pon,
    1:21--22;
    meets Mogi and Yassan,
    1:23--25;
    has a reunion with Akira,
    1:34--37;
    meets Chizu again and learns that she’s getting married,
    1:38--40;
    renews her friendship with Akira,
    1:42--44,
    1:46--53,
    1:66--67;
    tries to join the Matsuoka literature club and meets Yasuko,
    1:56--58,
    1:68;
    is consoled by Akira after Chizu and her husband visit,
    1:59--60,
    1:62--64,
    1:66--67;
    pretends to be sick to avoid attending Chizu’s wedding, and goes out with Akira instead,
    1:70--76;
    tells her friends about volunteering at Fujigaya,
    1:77--78;
    visits Fujigaya with Mogi, Pon, Yassan, and Yasuko,
    1:81--83;
    sees Kyoko break down after talking to Yasuko,
    1:85--87;
    is confessed to by Yasuko,
    1:88--92;
    has a first date with Yasuko,
    1:99--101,
    1:104--8;
    kisses Yasuko in the Matsuoka library,
    1:113--16,
    1:119--22;
    agrees to walk to school with Yasuko but regrets it,
    1:118--19,
    1:122--23,
    1:125--28;
    tells Akira about dating Yasuko, and asks her not to think it “gross,”
    1:135--40,
    1:143--44;
    tells Yasuko that she came out to Akira,
    1:147--49;
    visits Fujigaya with her friends and asks Akira to “just be normal” regarding her dating Yasuko,
    1:149--54;
    wonders about the “library maiden” and why Yasuko left Fujigaya,
    1:156,
    1:158--61,
    1:163;
    tells Yasuko that she’s jealous of her popularity,
    1:167--68;
    tells Akira that she thinks Yasuko likes someone else,
    1:169;
    is asked by Kyoko if she likes Yasuko,
    1:170--72;
    talks to Yasuko about Kyoko,
    1:176--81;
    talks to Akira and recalls Akira being her first love,
    1:182--85;
    compares her feelings for Akira to “a very small flower,”
    1:186--87;
    tells Yasuko about her discomfort with Kyoko,
    1:205--7;
    is visited by Yasuko at home,
    1:216--18;
    apologizes to Akira for not telling Yasuko to be less popular,
    1:218--20;
    helps out at and attends the performance of *Wuthering Heights*,
    1:223--24,
    1:228--32,
    1:234,
    1:241,
    1:246--47;
    witnesses Yasuko’s crying at meeting Mr. Kagami,
    1:251--52;
    talks with Yasuko about Yasuko liking someone else,
    1:252--55;
    considers telling Yasuko about Chizu but rejects it as “too sordid,”
    1:255--56;
    promises Yasuko that she’ll watch her play basketball,
    1:267;
    skips seeing Yasuko at school to spend the day with Akira,
    1:270--73,
    1:275--78;
    has a sleepover with Akira and tells Akira her concerns about her relationship with Yasuko,
    1:278--80;
    is invited by Yasuko to visit the Sugimoto home,
    1:291--95;
    visits the Sugimoto home,
    1:296--300,
    1:315--18,
    1:327--31;
    meets the other Sugimoto sisters and their mother,
    1:301,
    1:304--5;
    is outed by Yasuko,
    1:306--8;
    is questioned about her sexual orientation by Shinako,
    1:309;
    tells Yasuko about her relationship with Chizu,
    1:310--13;
    is dumped by Yasuko,
    1:320;
    realizes that Yasuko had a crush on Mr. Kagami,
    1:332;
    learns about Mr. Kagami getting engaged,
    1:323--26;
    tells Akira about her breaking up with Yasuko, and is comforted by her,
    1:333--35,
    1:350--52;
    sees Yasuko again at the literature club,
    1:355--56;
    is invited by Akira to join her on the visit to Ko’s family’s summer home,
    1:370--74;
    commiserates with Kyoko about Yasuko,
    1:375--76;
    goes with Akira and her other friends on a trip to Ko’s family’s summer home,
    2:6--11,
    2:13,
    2:15,
    2:18,
    2:20,
    2:25--30,
    2:32;
    stays up late with Akira and catches a cold,
    2:34--36;
    sees Akira talking outside with Ko,
    2:60;
    decides on the spur of the moment to visit Enoshima,
    2:89--91,
    2:99--100;
    confronts Yasuko about her behavior while on the trip to Enoshima,
    2:108--9,
    2:121--23;
    speculates that she has lingering feelings for Yasuko,
    2:128--29;
    tells Akira that she was her first love,
    2:138--41;
    is jealous of both Ko and Akira’s childhood friend Kurata,
    2:141,
    2:154--56
  - high school, year 2:
    begins her second year at Matsuoka,
    2:189--93;
    hears Akira’s comments about tall people (like Ueda) being cool, and thinks that she’s talking about Ko,
    2:214;
    reads *Rokumeikan* in the library and recalls her kiss with Yasuko,
    2:238--40;
    is encouraged by Akira and others to audition for *Rokumeikan*,
    2:245,
    2:271--77;
    auditions for a part in *Rokumeikan*,
    2:279--86;
    removes herself from consideration for a part in *Rokumeikan*,
    2:308--10,
    2:313--16;
    is taken aback when Haruka tells her that Orie may be in love with a girl,
    2:328--31, 2:333--34;
    confesses to Akira and tells her she wants to have sex with Akira like she did with Chizu,
    2:335--38,
    3:8;
    tells Haruka that she likes a girl,
    3:48--50;
    apologizes to Akira for her “gross” behavior in confessing to her,
    3:35--37;
    tells Akira that she won’t do anything to make Akira uncomfortable,
    3:37--39;
    meets Hinako and Orie and envies their relationship,
    3:52--54;
    reassures Akira before Akira’s performance in *Rokumeikan*,
    3:64--70,
    3:76;
    learns that Chizu will be visiting again,
    3:112--13;
    tells Chizu that she’s dating a girl,
    3:133--37;
    tells herself that her love for Chizu was real,
    3:138;
    goes on the trip to Haruka’s grandfather’s inn,
    3:153--55;
    sees Akira naked in the bath and is seen by her in turn,
    3:204--7;
    passes out in the bath and is found by Hinako and Orie,
    3:213--17;
    confides in Hinako regarding her love for Akira,
    3:218,
    3:224--27,
    3:243--44;
    sleeps next to Akira and holds her hand,
    3:229--32;
    is visited by Akira, whom their mothers encourage to sleep over,
    3:234--37,
    3:239--42;
    feels guilty recalling Akira naked, and wants to have a relationship like Hinako and Orie have,
    3:245--46;
    thinking Akira is asleep, tells her that she likes her, and then apologizes for doing so,
    3:247--49,
    3:251--54;
    reacts to Akira suggesting that they date,
    3:255--56;
    imagines going on a date with Akira,
    3:259--64;
    joins the Matsuoka drama club,
    3:273--75;
    is asked by Akira to go on a date,
    3:277--78;
    asks Kyoko (unsuccessfully) to accompany her on her date with Akira,
    3:286--88;
    goes on a date with Akira,
    3:289--93,
    3:297--301;
    kisses Akira,
    3:302;
    tells Mogi, Pon, and Yassan that she’s “that type of girl,”
    3:307--11;
    worries that her love of Akira is unrequited,
    3:311--12;
    gets drunk on Christmas day and makes advances toward Akira,
    3:338--50
  - high school, year 3:
    asks to kiss Akira again,
    4:82--84;
    reads Pon’s script and recalls scenes and lines from it,
    4:105--7,
    4:111--12,
    4:114;
    worries about scaring Akira off with her desires,
    4:135,
    4:137;
    hears and reflects on Akira’s doubts about their relationship,
    4:201--8;
    receives and rejects a confession from Atsushi Tanaka,
    4:270--77;
    reflects after her breakup with Akira,
    4:277--80;
    is asked about *Heavenly Creatures* by another girl who sees herself in it,
    4:286--88;
    has a role-reversal dream about Akira and her as children, with Akira urinating on herself,
    4:290--93;
    graduates from Matsuoka,
    4:306--8
  - after high school:
    tells Akira that she’ll always love her,
    4:319--20;
    has a dream about her grandmother telling her to give up on Akira,
    4:320--22;
    is seen by Akira talking with Hanae,
    4:325--28;
    talks with Hanae about marriage and tells her that there’s someone she likes,
    4:331,
    4:333;
    cries after Akira confesses to her,
    4:358--59;
    has a sleepover with Akira and talks to her all night,
    4:362--68
- Manjome (father of Fumi),
  1:40,
  1:70,
  2:76,
  2:79,
  3:131,
  3:331,
  3:350,
  4:228
- Manjome (mother of Fumi),
  1:100,
  1:118,
  1:176,
  1:350,
  2:79,
  3:40--41,
  3:128,
  3:251,
  3:350,
  4:157,
  4:228;
  worries about Fumi starting a new school, and urges her to write to Akira,
  3:117,
  3:120--22;
  hosts Chizu and her mother for a visit,
  3:124--26;
  tells Chizu she can help her find a husband,
  3:131;
  has a reunion with Akira’s mother,
  1:33--34,
  1:36;
  hosts Chizu and celebrates her upcoming wedding,
  1:39--40;
  has Akira over for a visit, when Chizu and her husband stop by,
  1:49,
  1:60--61;
  criticizes Fumi for missing Chizu’s wedding,
  1:67,
  1:70,
  1:76;
  is surprised when Yasuko shows up to see Fumi,
  1:216--17;
  criticizes Fumi for not studying on Sundays,
  2:75--77;
  asks Fumi if she’s going on a date to Enoshima,
  2:90--91;
  learns about Fumi auditioning for a role in *Rokumeikan*,
  2:303,
  2:306--7;
  tells Fumi that Chizu is coming to visit,
  3:112--13;
  wonders why Akira is concerned about sleeping over with Fumi,
  3:236--37,
  3:240;
  tells Fumi to stay at home on Christmas day with Akira,
  3:330--31
- Matsuoka student (friend of Yasuko):
  kids Yasuko about her popularity and Kyoko’s crush on her,
  1:360--63
- Matsuoka student (literature club member),
  1:68,
  1:274,
  1:355--56;
  tells Fumi to “watch out” around Yasuko,
  1:113
- Matsuoka students (new drama club members),
  4:15--18,
  4:101-02;
  promote the idea of a culture festival,
  4:187--89
- Matsuoka teachers,
  3:272,
  4:116,
  4:188
- Mogi. *See* Motegi, Miwa
- Mori, Hanae (friend of Fumi in college),
  4:324--26,
  *4:330*,
  4:331;
  is seen by Akira talking with Fumi,
  4:354;
  talks with Fumi about marriage and whom Fumi likes,
  4:333--34
- **Motegi, Miwa (Mogi)** (friend of Fumi),
  1:3,
  1:24--25,
  1:342--44,
  1:353,
  2:39--40,
  2:46,
  2:49,
  2:60--61,
  2:132,
  *2:182*,
  2:191,
  2:198--200,
  2:311--13,
  *2:356--57*,
  *3:1*,
  *3:5*,
  3:110,
  3:145--47,
  *3:183*,
  *3:185*,
  3:196,
  3:199--202,
  3:206,
  3:211--12,
  3:221,
  3:273,
  3:305--11,
  3:337--39,
  *4:5*,
  4:14,
  4:16--17,
  *4:75*,
  4:102,
  4:132--33,
  *4:183*,
  4:189,
  4:244--45,
  4:255,
  4:293--94,
  4:306--7,
  4:313--14;
  participates in the drama club in junior high,
  4:192--94;
  meets Fumi,
  1:23--25;
  complains about there being only three Matsuoka drama club members,
  1:23;
  visits the Fujigaya drama club with Yasuko, Fumi, Pon, and Yassan,
  1:78,
  1:81,
  1:85,
  1:88;
  sees Fumi’s conversation with Yasuko,
  1:91;
  accompanies Yasuko on a visit to Fujigaya,
  1:147,
  1:150--52,
  1:154;
  helps out at and attends the *Wuthering Heights* performance,
  1:222--24,
  1:232,
  1:234;
  witnesses Akira’s fight with Yasuko,
  1:342--44;
  buys coffee for Shinobu,
  2:8--9;
  tells her friends about her interest in Shinobu and is teased for it,
  2:62--63;
  confesses to Shinobu while at a sleepover at Akira’s house,
  2:133--35;
  is teased about her relationship with Shinobu,
  2:192--93;
  performs in a minor role in *Rokumeikan*,
  3:71--73;
  thinks that Shinobu might kiss her,
  3:72;
  visits Aunt Keiko with Shinobu,
  3:239;
  is teased again about her relationship with Shinobu,
  4:92--94;
  performs in *Heavenly Creatures* before judges,
  4:285;
  is asked about marrying Shinobu,
  4:352--53
- Motegi (mother of Mogi),
  4:94
- Mr. Kagami. *See* Kagami
- Ms. Kanako. *See* Kanako (adult lesbian)
- &nbsp;
- Nakajima (friend of Maeda):
  gets angry when Maeda is called a lesbian,
  3:357--60
- Nishinomiya (friend of Fujigaya newspaper editor-in-chief),
  2:204--5 (?),
  4:234--35 (?),
  4:247
- &nbsp;
- Ogino (servant of Sugimoto family),
  1:297--99,
  1:327
- **Okudaira, Akira**,
  *1:1--2*,
  *1:6*,
  1:33,
  1:42--44,
  1:123,
  *1:142*,
  1:156,
  1:158,
  1:185,
  *1:199--200*,
  1:263--64,
  1:291,
  *2:1*,
  *2:5*,
  2:99--101,
  2:126--30,
  2:139--41,
  2:153,
  2:156,
  *2:182*,
  *2:184*,
  2:185,
  2:190,
  2:211--16,
  2:226--27,
  2:236,
  2:264,
  2:269--71,
  2:273--74,
  2:283--84,
  2:295,
  2:318,
  2:334,
  2:343--44,
  *2:356*,
  *3:2*,
  *3:4*,
  3:16--21,
  *3:31*,
  3:32--34,
  3:38--39,
  3:41,
  3:74,
  3:110--11,
  *3:139*,
  3:141--42,
  3:146--48,
  3:152--55,
  *3:173*,
  *3:177*,
  *3:182*,
  3:196,
  3:200,
  3:212,
  3:221--24,
  3:230--32,
  *3:233*,
  3:237--41,
  3:245 3:261,
  3:264--66,
  3:274--75,
  *3:279*,
  *3:303*,
  3:313--17,
  3:322,
  *3:327*,
  3:330--36,
  3:279,
  3:284--85,
  3:294--97,
  *4:1*,
  *4:4*,
  4:19,
  4:73--74,
  4:81,
  4:84--86,
  4:88--91,
  4:98--100,
  *4:99*,
  4:106--8,
  4:112--14,
  *4:115*,
  4:120--22,
  4:125--26,
  4:136,
  4:149,
  4:151,
  4:158,
  4:169,
  *4:179*,
  *4:182*,
  *4:186*,
  4:219--22,
  4:228,
  4:234--35,
  4:257,
  4:268,
  4:282--84,
  4:289--91,
  4:293--95,
  *4:297*,
  4:313--16,
  4:339--40,
  4:353,
  *4:370--71*
  - childhood:
    calls Fumi a crybaby,
    1:25--26;
    helps Fumi when Fumi urinates on herself,
    1:30--31;
    with Fumi, visits Fumi’s grandmother,
    1:53;
    comforts Fumi when she’s sick,
    1:71;
    accompanies Fumi to the bathroom,
    2:35;
    has a sleepover at Fumi’s house,
    3:241--42;
    says Fumi’s lines for her in a Christmas performance,
    3:68,
    3:76;
    says goodbye when Fumi moves,
    1:32
  - junior high school:
    decides to apply to Fujigaya Women’s Academy,
    4:87
  - high school, year 1:
    is awakened on the first day of high school,
    1:5,
    1:8--9;
    hits Shinobu with her bag and warns him to stay out of her bed,
    1:10--11;
    meets Fumi again without recognizing her,
    1:12,
    1:16;
    is groped by a man on the train,
    1:13--15;
    rescues Fumi from the groper,
    1:17;
    enters Fujigaya Women’s Academy,
    1:19--20;
    meets Kyoko and describes her as “kinda sexy,”
    1:26--27;
    has a reunion with Fumi,
    1:28,
    1:34--37;
    renews her friendship with Fumi,
    1:42--43,
    1:46--49,
    1:51--53,
    1:66--67;
    decides to join the Fujigaya drama club,
    1:54--55;
    consoles Fumi after Chizu and her husband visit,
    1:59--64;
    tells Fumi about the Fujigaya Theater Festival and her joining the drama club,
    1:72--75;
    marvels at Kyoko receiving a love letter, and wonders whether Fumi had feelings toward Chizu,
    1:78--80;
    meets Yasuko and sees how Fumi reacts to her,
    1:82,
    1:85,
    1:88,
    1:91--92;
    attends Ko’s singles party,
    1:97--99,
    1:102--3;
    yells at Shinobu for following her to the singles party, and tells him that he’ll never have a girlfriend,
    1:109--10;
    meets Yasuko while going to school with Fumi,
    1:124--26;
    hears from Kyoko that Ko wants to see her again,
    1:129--31;
    learns about Fumi dating Yasuko,
    1:135--40;
    wonders about liking a girl, and asks Kyoko about it,
    1:141,
    1:143--46;
    asks Fumi how she can support her,
    1:151--54;
    talks with Fumi about Yasuko and Kyoko,
    1:169,
    1:182--83;
    talks with Kyoko about Yasuko,
    1:174--75;
    tries to reassure Kyoko regarding Yasuko,
    1:203--5,
    1:209--11;
    advises Fumi to tell Yasuko “not to be so popular,”
    1:218--19;
    helps with *Wuthering Heights* production,
    1:222--24,
    1:229,
    does Yasuko’s hair and thinks she’s a “lady-killer,”
    1:232--34;
    meets Ko Sawanoi,
    1:257--62;
    rides home with Kyoko and Ko, but rushes to the bathroom before introducing them to her family,
    1:265--66;
    visits Aunt Keiko with Fumi,
    1:268--73;
    tries to comfort Fumi about her relationship with Yasuko,
    1:275--81;
    rushes back to class after going to the bathroom and is scolded by a nun,
    1:281,
    1:283--84;
    talks to Kyoko about Fujigaya and the Sugimoto sisters,
    1:285--90;
    goes with Fumi to see a movie and learns about Fumi’s breakup,
    1:321;
    1:323--25,
    1:332--35;
    confronts Yasuko regarding her behavior toward Fumi,
    1:341--49;
    consoles Fumi about her breakup with Yasuko,
    1:350--52;
    is invited by Kyoko to visit Ko’s family’s summer home,
    1:357--58;
    tells Kyoko that Fumi and Yasuko broke up,
    1:358--59,
    1:364;
    invites Fumi on the trip to Ko’s family’s summer home,
    1:370--75;
    goes with Fumi and her other friends on a trip to Ko’s family’s summer home,
    2:6--7,
    2:9--10,
    2:13,
    2:16,
    2:18,
    2:20,
    2:25--33,
    2:61--63;
    wakes up to go the bathroom and asks Fumi to accompany her, then stays up late with Fumi and catches a cold,
    2:34--36;
    protests Shinobu hanging around her and Fumi while they’re sick, and tells Fumi that he’s “super creepy,”
    2:39,
    2:41,
    2:43--44;
    eavesdrops on Ko’s mother’s comments about Kyoko,
    2:51--56;
    talks with Ko about Kyoko’s family situation and Kyoko’s feelings towards him,
    2:57--60,
    2:64--65;
    wonders if she’ll ever like someone like Fumi does Yasuko,
    2:66--67;
    attends the wedding of Kazusa and Mr. Kagami,
    2:71--72,
    2:82,
    2:92--94;
    fights with Shinobu during her trip to Enoshima with Fumi,
    2:105--7,
    2:118--21;
    witnesses Fumi’s breakup with Yasuko,
    2:108,
    2:123;
    has all her friends over for a sleepover,
    2:130--32,
    tells Fumi that she’s never experienced a first love,
    2:135--38;
    helps Ko shop for a Christmas present for Kyoko,
    2:149--52;
    recalls a conversation with Fumi about sex,
    3:35--9
  - high school, year 2:
    begins her second year at Fujigaya and encounters Haruka,
    2:193--97;
    meets Ueda,
    2:201--3;
    is upset by student gossip about Hinako and her girlfriend,
    2:205--8;
    starts to ask Ueda about her situation with Fumi, but doesn’t,
    2:217--20;
    meets Haruka again at the Fujigaya drama club,
    2:221--24;
    thinks about asking Fumi about whether Fumi has a crush on her, but doesn’t,
    2:228;
    reads *Rokumeikan* and imagines a scene from it,
    2:230--35;
    is encouraged by Ueda to audition for *Rokumeikan*,
    2:265--68;
    auditions for a part in *Rokumeikan*,
    2:281;
    reads *Rokumeikan*,
    2:317,
    2:332--33;
    is confessed to by Fumi, and learns about Fumi wanting to have sex with her,
    2:335--36,
    2:338,
    2:341;
    talks to Fumi about Fumi’s liking her, and is frightened by talk of sex,
    3:35--37;
    wonders what “stuff” Fumi wants to do with her,
    3:8--9;
    meets Fumi for the first time since Fumi confessed to her,
    3:28--30;
    is reassured by Fumi before Akira’s performance in *Rokumeikan*,
    3:64--70,
    3:76;
    plays Akiko in *Rokumeikan*,
    3:31,
    3:84--85,
    3:87,
    3:92--95;
    invites Fumi on the trip to Haruka’s grandfather’s inn,
    3:149--50;
    thinks to herself that she and Fumi no longer have sleepovers,
    3:151;
    is embarrassed after staring at Fumi naked in the bath,
    3:203--6,
    3:210;
    thinks about the fact that a girl likes her,
    3:229;
    has a sleepover at Fumi’s house and overhears Fumi saying that she likes her,
    3:240--41,
    3:248--49,
    3:251;
    tells Fumi that she doesn’t feel the same way toward her,
    3:252--54;
    suggests to Fumi that they date,
    3:255--56,
    3:259;
    tells Kyoko about her relationship with Fumi, and is overheard by Ueda and others,
    3:267--70;
    is the (unnamed) subject of gossip in the Fujigaya newspaper,
    3:270--71;
    asks Fumi out on a date,
    3:277--78;
    goes on a date with Fumi,
    3:280--82,
    3:289--93,
    3:297--300;
    kisses Fumi,
    3:302,
    3:320--21;
    wakes up crying after a dream about her and Fumi as children, and remembers their kiss,
    3:318--21;
    visits Fumi on Christmas day, exchanges presents with her, and asks Fumi if she fantasizes about her,
    3:330--50
  - high school, year 3:
    becomes the president of the Fujigaya drama club,
    4:21--22;
    is thwarted in asking Kyoko about kissing,
    4:78--80;
    tells Fumi that she can kiss her anytime,
    4:82--83;
    recalls her decision to attend Fujigaya,
    4:87;
    thinks about Shinobu’s marriage,
    4:95--96;
    kisses Fumi, and asks about going beyond kissing,
    4:109--11;
    becomes angry when asked about her relationship with Fumi,
    4:123--24;
    begins to ask Hinako a question but backs off,
    4:140;
    recalls Fumi touching her,
    4:141--44;
    recalls waking up in bed with Fumi and being unsure how to act,
    4:145--46;
    asks Shinobu why he doesn’t get in her bed anymore,
    4:150;
    finds herself uncomfortable around Fumi,
    4:170;
    reads the play based on Fumi’s life,
    4:201;
    expresses regrets to Fumi about starting their relationship,
    4:202--6;
    dreams about having to go to the bathroom while in England,
    4:224--25;
    fondles Fumi while Fumi is asleep,
    4:225--27;
    goes on the Fujigaya school trip to England,
    4:239--43;
    tells Yasuko that she’s dating Fumi,
    4:250--54,
    4:266--67;
    decides that she needs to break up with Fumi if she can’t return Fumi’s feelings,
    4:269;
    gets sick on the flight back from England,
    4:262--65;
    breaks up with Fumi,
    4:277--78,
    4:284;
    graduates from Fujigaya,
    4:298--305,
    4:308--9
  - after high school:
    asks Fumi if she still likes her,
    4:317--19;
    tells Fumi that she needs time to think,
    4:323--24;
    gets jealous after seeing Fumi with another girl,
    4:325--28,
    4:354--56;
    recalls her rejection of Fumi,
    4:334;
    attends Kyoko’s wedding,
    4:339--48;
    meets Fumi again after a long separation,
    4:341--42;
    asks about Fumi’s friendship with Hanae,
    4:343--44;
    confesses to Fumi,
    4:345--48,
    4:357--59;
    recalls her jealousy regarding Fumi,
    4:355--56;
    has a sleepover with Fumi and talks to her all night,
    4:362--68
- Okudaira, Sakiko (mother of Akira),
  1:9,
  1:99,
  1:110,
  1:272,
  1:275,
  2:71,
  2:153,
  2:267,
  3:41,
  3:32--34,
  3:238,
  4:95,
  4:148,
  4:151,
  4:264,
  4:363;
  tries to persuade Akira to take ballet as a child,
  4:86;
  encourages Akira to apply to Fujigaya,
  4:87;
  anticipates Akira entering Fujigaya,
  4:298--300;
  wakes Akira on her first day of high school,
  1:5,
  1:8--9;
  kicks Shinobu out of Akira’s bed,
  1:8;
  speculates that Akira will soon have a girlfriend,
  1:27;
  learns that the Manjome family is back in town,
  1:27--28;
  has a reunion with Fumi’s mother,
  1:34,
  1:36;
  tells Shinobu and Akira to stop arguing about his over-protectiveness,
  1:110;
  attends the performance of *Wuthering Heights*,
  1:222--24,
  1:229,
  1:235,
  1:241,
  1:243;
  meets Kyoko and Ko,
  1:265--66;
  pushes Akira to visit Aunt Keiko,
  1:268--70;
  is called the “family rebel” by Aunt Keiko,
  1:272;
  hosts Akira’s friends for a sleepover,
  2:130--34;
  is surprised by Akira getting a part in *Rokumeikan*,
  2:267;
  tells Akira’s father and Shinobu not to have “the talk” in front of Akira,
  3:34;
  is nervous about Akira’s performance as Akiko in *Rokumeikan*,
  3:79,
  3:84,
  3:94;
  visits the Manjome family and encourages Akira to sleep over,
  3:236--37,
  3:240;
  apologizes for Akira getting sick on the plane back from England,
  4:264
- **Okudaira, Shinobu** (brother of Akira),
  1:27--28,
  1:98,
  1:219,
  1:266,
  2:63,
  2:72,
  2:101,
  2:104--6,
  2:109,
  2:118--21,
  2:130--33,
  *2:182*,
  2:268,
  *2:356*,
  *3:1*,
  *3:5*,
  3:33--34,
  *3:75*,
  3:84,
  *3:183*,
  3:313,
  *4:5*,
  4:75,
  4:87,
  4:92--95,
  4:148--49,
  *4:183*,
  4:264,
  4:299--300;
  is kicked out of Akira’s bed by his mother,
  1:8;
  drives Akira to school and warns her about gropers,
  1:9--12;
  stalks Akira at Ko’s singles party, and tells her that Ko might be interested in her,
  1:102,
  1:110;
  tells Akira that “all I need is you,”
  1:109;
  attends the performance of *Wuthering Heights*,
  1:222--24,
  1:229,
  1:235,
  1:241,
  1:243;
  criticizes Akira’s “dippiness,”
  1:266;
  accompanies Akira and her friends on their visit to Ko’s family’s summer home,
  2:6--7,
  2:10,
  2:30,
  2:32--33;
  meets Mogi,
  2:8--9;
  meets Ko and talks with him about Akira,
  2:16--17,
  2:22--24;
  is persuaded by Hanae to stop bothering Akira and Fumi and accompany the other girls while horseback riding,
  2:39--41;
  attends the wedding of Kazusa and Mr. Kagami,
  2:82,
  2:92--94;
  talks with Yasuko about Akira during the Enoshima trip,
  2:107;
  is confessed to by Mogi,
  2:134--35;
  is overheard by Akira while on the phone with Mogi,
  2:252--53;
  asks Mogi to help Akira out in *Rokumeikan*,
  3:71--73;
  visits Aunt Keiko with Mogi,
  3:239;
  deflects Akira’s question about why he doesn’t get in bed with her any more,
  4:150
- Okudaira (father of Akira),
  1:102,
  1:266,
  2:153,
  3:32--33,
  3:41,
  3:238,
  4:148,
  4:151,
  4:264,
  4:298;
  attends the performance of *Wuthering Heights*,
  1:222,
  1:229,
  1:235,
  1:241,
  1:243;
  offers to have “the talk” with Shinobu,
  3:34;
  attends the performance of *Rokumeikan*,
  3:79,
  3:84,
  3:94
- **Ono, Haruka** (sister of Orie),
  *2:182--83*,
  2:254--58,
  2:274,
  *2:278*,
  2:283,
  2:285,
  2:293--94,
  2:296,
  2:304--6,
  2:311,
  2:318,
  2:339,
  2:343--44,
  *2:357*,
  *3:4*,
  3:19--21,
  3:28--30,
  3:37--38,
  3:46--47,
  3:51--52,
  3:60,
  3:109--10,
  3:140--41,
  3:146,
  3:153--55,
  3:162,
  *3:182*,
  3:197--98,
  3:206--7,
  3:211,
  3:219,
  3:221--22,
  3:328--29,
  *4:1*,
  *4:4*,
  4:9--10,
  4:21,
  4:76--78,
  4:100,
  4:121,
  4:152--53,
  4:160--61,
  *4:182*,
  4:218,
  4:309,
  4:352 (?);
  enters Fujigaya Women’s Academy and meets Akira and Kyoko,
  2:194--98;
  joins the Fujigaya drama club and meets Akira and Kyoko again,
  2:221--25;
  bumps into Hinako at Fujigaya and is rebuked for addressing her as “Hina,”
  2:243--44;
  recruits Ueda for a part in *Rokumeikan*,
  2:259--60,
  2:262,
  2:266;
  meets Fumi and asks about the girl who played Heathcliff (Yasuko),
  2:288--92;
  apologizes to Fumi for pushing her to try out for a part in *Rokumeikan*,
  2:312--16;
  passes on to Kyoko a love letter from one of Kyoko’s admirers,
  3:320--21,
  3:322;
  finds a love letter to Orie from Hinako,
  2:322,
  3:22--24;
  invites Fumi to visit and tells Fumi her suspicions about Orie,
  2:326--31;
  apologizes to Fumi for asking if she liked someone,
  3:48--50;
  introduces Fumi to her sister Orie,
  3:53;
  meets Hinako and speculates that she’s Orie’s girlfriend,
  3:61--63;
  offers to host her friends at her grandfather’s inn,
  3:142--43;
  starts year 2 at Fujigaya,
  4:10;
  sees Hinako and recalls Orie coming out to their parents,
  4:11--13;
  is disappointed about not getting the part of D’Artagnan,
  4:121;
  asks Hinako to help get the *Three Musketeers* costumes that she left at home,
  4:154--56;
  asks Hinako if she and Orie want to marry,
  4:350--52
- Ono, Orie (sister of Haruka, partner of Hinako),
  2:174,
  3:44,
  3:54,
  *3:180*,
  3:197--98,
  *3:209*,
  3:214--15,
  3:219,
  3:227,
  3:246,
  3:328,
  *4:1*;
  talks with Hinako about liking Shinako, only for Hinako to confess to her,
  1:379--81;
  cries after Hinako predicts that she’ll forget about Shinako after graduation,
  2:159--60,
  2:171;
  confesses to Hinako,
  2:163--64;
  kisses Hinako and promises that they’ll stay together,
  2:168--69;
  graduates from Fujigaya high school with Hinako,
  2:160;
  is asked by her former teacher why she’s not married,
  3:45;
  is introduced to Fumi,
  3:53;
  invites Hinako to accompany her on the visit to her grandfather’s inn,
  3:143--45;
  prepares for a Christmas date with Hinako,
  3:328--29;
  helps Haruka by bringing *The Three Musketeers* costumes that Haruka forgot,
  4:156,
  4:160--61;
  commiserates with Hinako about their parents,
  4:210--11;
  imagines a wedding to Hinako,
  4:350--51
- Ono (grandfather of Orie and Haruka),
  3:198--99
- Ono (mother of Orie and Haruka),
  2:326,
  3:23--24,
  3:142--43,
  4:9;
  wonders if Orie will ever marry,
  2:329,
  3:328--29;
  tells Haruka that Orie and Hinako used to be good friends,
  3:22
- Orie. *See* Ono, Orie
- Orie’s partner. *See* Yamashina, Hinako
- Orie’s sister. *See* Ono, Haruka
- &nbsp;
- Pon. *See* Honatsugi, Yoko
- &nbsp;
- Sakiko. *See* Okudaira, Sakiko
- Sawanoi, Kanako (sister of Ko),
  4:30--32
- **Sawanoi, Ko** (fiancé of Kyoko),
  *2:38*,
  2:185--87,
  *2:357*,
  *3:5*,
  3:88--90,
  *3:183*,
  *4:5*,
  *4:7*,
  4:8--9,
  *4:27*,
  4:29--32,
  4:37--40,
  4:56,
  *4:183*;
  rescues Kyoko as a child when she is lost,
  2:37,
  2:67--68,
  4:65;
  avoids visiting Kyoko while in junior high,
  4:33--35;
  has sex with Kyoko while in high school,
  4:41--42;
  tells Kyoko that he wishes she’d let him do things for her,
  1:94--95;
  plans a singles party with Kyoko,
  1:96;
  hosts Akira at a singles party, and tells Kyoko that Akira is a “fun girl,”
  1:102--3,
  1:108;
  grabs Akira after the performance of *Wuthering Heights*, and (with Kyoko) drives her home,
  1:257--62,
  1:265;
  hosts Kyoko and her friends at his family’s summer home,
  2:14--18,
  2:33,
  2:42,
  2:45;
  talks with Shinobu about Akira,
  2:21--24;
  talks to his mother about Kyoko, and mentions that both his father and Kyoko’s have been absent for many years,
  2:49--52;
  talks to Akira about Kyoko’s family situation and asks Akira to be a good friend to her,
  2:56--60;
  confirms with Akira that Kyoko likes someone else, and wishes that he could be the one that Kyoko likes,
  2:64--65;
  with Kyoko, attends Kazusa’s wedding to Mr. Kagami,
  2:70--71,
  2:80--81,
  2:92,
  2:98;
  introduces himself to Yasuko as Kyoko’s “fiancé in name only,”
  2:81;
  takes Akira with him to buy a Christmas present for Kyoko,
  2:149--52;
  has an awkward conversation with Kyoko,
  2:297--99;
  tells Kyoko that he wants to break off their engagement,
  3:25--27;
  sends Kyoko flowers before her performance in *Rokumeikan*,
  3:46--47;
  complains to Kyoko that she’s trying to use him to escape her family,
  3:91;
  reiterates to Kyoko his love for her,
  4:23--25;
  visits Kayoko and reconnects with Kyoko,
  4:69--70,
  4:72;
  prepares to wed Kyoko,
  4:332--33;
  marries Kyoko,
  4:349
- Sawanoi (father of Ko),
  2:50
- Sawanoi (mother of Ko),
  2:45,
  2:48;
  worries about Kyoko marrying into the Sawanoi family, and refers to Kyoko’s mother as “sick,”
  2:49--51;
  discovers Akira eavesdropping on her comments about Kyoko,
  2:52,
  2:56
- Shiho. *See* Kagami, Shiho
- Shimozuka (aunt of Hinako):
  wants to introduce Hinako to a marriage prospect,
  4:209
- Shinobu. *See* Okudaira, Shinobu
- Shinobu’s girlfriend. *See* Motegi, Miwa
- Shinako. *See* Sugimoto, Shinako
- Shinako’s admirer. *See* Kaoruko
- Sonoko (friend of Yasuko’s mother),
  3:165--72
- student (unknown name or school):
  tells Fumi that she sees herself in *Heavenly Creatures*,
  4:286--88
- Sugimoto, Chie (mother of Yasuko),
  1:305,
  4:336;
  meets Fumi and learns about her relationship with Yasuko,
  1:305--6,
  1:309;
  speculates on Yasuko and Fumi’s sexual activity,
  1:318--19;
  recalls her school friend Sonoko,
  3:165--72
- Sugimoto, Kazusa (sister of Yasuko, wife of Mr. Kagami),
  *1:197*,
  1:287,
  1:290,
  1:332,
  1:348,
  2:114,
  3:12--14,
  3:160--61,
  4:336;
  joins Fujigaya as a teacher and meets Hinako,
  2:161--63;
  is characterized by Yasuko as having a “tough personality,”
  2:112;
  meets Fumi when Fumi comes to visit,
  1:304--6,
  1:309,
  1:317;
  reproaches Kuri for her comments about Yasuko’s relationship with Fumi,
  1:310,
  1:319;
  talks with Yasuko about Kyoko,
  1:367--69;
  tells Yasuko a secret about Kuri and Mr. Kagami,
  2:72--74,
  3:192--95;
  apologizes to Kuri for taking Mr. Kagami,
  2:86;
  weds Mr. Kagami,
  2:87,
  2:93--94,
  2:97--98;
  urges Kyoko to write Yasuko,
  3:10--11;
  asks Kyoko if she still likes Yasuko,
  3:160--61;
  talks with Mr. Kagami about her pregnancy,
  4:20
- Sugimoto, Kuri (sister of Yasuko),
  *1:197*,
  2:83,
  2:87,
  3:159,
  3:171,
  4:336;
  meets Fumi,
  1:304--5;
  questions Yasuko about her sexuality,
  1:306--8;
  claims Yasuko is spoiled and rebellious,
  1:309--10,
  1:318;
  jokes about Yasuko’s and Fumi’s relationship,
  1:319;
  drives Fumi home after her breakup with Yasuko,
  1:327--31;
  talks with her admirer Komako,
  2:2--3,
  2:176--77;
  is teased by Yasuko about her past crush on Mr. Kagami,
  2:84--86,
  2:95--97;
  bickers with her sisters and mother about not getting married,
  4:336
- Sugimoto, Shinako (sister of Yasuko),
  *1:197*,
  1:317--9,
  *1:378*,
  2:83,
  2:163,
  *2:356*,
  3:12--14,
  3:194--95,
  4:335--36;
  in high school, is confessed to by Kaoruko and goes out with her,
  2:348--53;
  meets Fumi,
  1:301--2;
  asks Yasuko if she’s a lesbian,
  1:306--8;
  asks Fumi if she’s a lesbian or bisexual,
  1:309;
  criticizes Yasuko’s behavior toward Fumi,
  1:314--16;
  accompanies Fumi home after Fumi’s breakup with Yasuko,
  1:327--31;
  jokes about Kazusa, Kuri, and Yasuko all falling for Mr. Kagami,
  2:85,
  2:96--97;
  is characterized by Kuri as only liking *young* guys,
  2:87;
  tells Kaoruko that they have a “rotten relationship,”
  2:354;
  asks Kyoko if she’s in love with Yasuko,
  3:11;
  encourages Yasuko and Kyoko to write each other,
  3:191
- **Sugimoto, Yasuko**,
  *1:3*,
  1:76,
  *1:69*,
  1:150,
  1:160--61,
  *1:165*,
  1:166,
  1:170,
  1:173,
  *1:192*,
  *1:197--98*,
  1:208--9,
  *1:221*,
  1:337,
  1:359--61,
  1:368--69,
  *1:322*,
  1:373,
  *2:69*,
  2:72--73,
  2:82--83,
  2:96--98,
  2:223,
  *3:5*,
  *3:183*,
  3:192--95,
  *4:5*,
  4:45,
  4:130,
  *4:183*,
  4:224,
  *4:233*,
  4:265,
  4:335--37
  - childhood:
    is treated “like a prince,”
    2:110--11;
  - high school, year 2:
    meets Mr. Kagami and develops a crush on him,
    1:162--63,
    2:111,
    2:113--14;
    cuts her hair short to emulate Kazusa’s “tough personality,”
    2:112;
    is imitated by Kyoko,
    2:115--16;
    is rejected by Mr. Kagami and sends a letter to him upon leaving Fujigaya,
    1:164,
    2:116--17
  - high school, year 3:
    meets Fumi,
    1:57--58;
    tells Fumi about the Fujigaya Theater Festival,
    1:76--77;
    takes Fumi and her friends to visit the Fujigaya drama club,
    1:81--84;
    tells Fumi that she likes her,
    1:88--92;
    asks Fumi on a date,
    1:101;
    goes on a date with Fumi,
    1:104--6;
    tells Fumi about the Fujigaya “library maiden,”
    1:113--15;
    kisses Fumi and tells her that she’s “cute,”
    1:115--16,
    1:119--22;
    goes to school with Fumi (and Akira),
    1:122,
    1:124--28;
    congratulates Fumi on her coming out to Akira,
    1:147--49;
    meets Mr. Kagami again,
    1:155--58;
    asks Fumi if she’s jealous of her popularity,
    1:167--68;
    calls Fumi and asks her why she’s been “acting weird,”
    1:176--81;
    asks Fumi to help at the performance of *Wuthering Heights*,
    1:205--7,
    1:216--18;
    talks to Kyoko about their non-relationship,
    1:212--15;
    calls her character Heathcliff a “troubled man,”
    1:225--27;
    meets Fumi before the performance of *Wuthering Heights*,
    1:229--31;
    talks to Akira while Akira does her hair,
    1:232--34;
    appears in *Wuthering Heights*,
    1:240,
    1:243;
    talks with Kyoko while backstage,
    1:244--46;
    breaks down at the cast party upon seeing Mr. Kagami,
    1:249--51,
    1:332;
    tells Fumi that she had an unrequited crush on someone else,
    1:252--55;
    asks Fumi to come watch her play basketball,
    1:267;
    discovers that Fumi has skipped school without telling her,
    1:274;
    invites Fumi to visit her at home,
    1:293--95;
    hosts Fumi on a visit to the Sugimoto home,
    1:297--304;
    tells her sisters and mother that she and Fumi are dating,
    1:306--8;
    talks with Fumi about their relationship,
    1:310--15;
    tells Fumi that they should stop seeing each other,
    1:320;
    tells Akira about her crush on Mr. Kagami,
    1:342--49;
    hangs out in the literature clubroom to see Fumi,
    1:355--56;
    rejects Kyoko again,
    1:362--67;
    is introduced to Ko by Kyoko,
    2:80--81;
    chides Kuri for also having had a crush on Mr. Kagami,
    2:84--85,
    2:95;
    meets Mr. Kagami at his wedding to Kazusa and tells him that he’s not “cool” and “thinks like an old guy,”
    2:87--89;
    intrudes on Fumi’s trip to Enoshima with Akira,
    2:100--101,
    2:104,
    2:110,
    2:118--19;
    is rebuked by Fumi for her interference,
    2:108--9,
    2:121--23;
    apologizes to Fumi for her behavior,
    2:123--24;
    talks about her popularity with Kyoko,
    2:146--48;
    takes the Matsuoka basketball team to nationals,
    2:187
  - after high school:
    leaves for England,
    2:188;
    calls to say that she’ll be coming home for a ten-day visit,
    3:159--61;
    invites Kyoko to visit, asks her if she still has a crush on her, and asks her to write her in England,
    3:187--97;
    with Kawasaki, shops for a baby present for Kazusa,
    4:22--23;
    meets Akira, Kyoko, and Ueda in England,
    4:239--43;
    counsels Akira about her relationship with Fumi,
    4:266--67;
    tells Shiho not to fall in love with a teacher,
    4:335;
    protests that she did not leave Ueda (?) in England,
    4:336--37
- Sugimoto (father of Yasuko),
  2:94
- Sugimoto family’s servants. *See* Fumi (servant of Sugimoto family); Ogino (servant of Sugimoto family)
- Suzuhara (Fujigaya student admired by Sonoko),
  3:167--70
- &nbsp;
- Tanaka, Atsushi (male admirer of Fumi) (Atsushi Taguchi in the Japanese edition),
  4:270--77
- &nbsp;
- **Ueda, Ryoko** (friend of Akira),
  *2:183*,
  2:217--18,
  2:230,
  *2:261*,
  2:275--76,
  2:283,
  2:294,
  *2:357*,
  *3:4*,
  3:17--18,
  *3:58*,
  3:154--55,
  *3:182*,
  3:294--96,
  3:321--22,
  3:294--96,
  *4:1*,
  *4:4*,
  4:46,
  4:52--55,
  *4:182*,
  4:239,
  4:241,
  4:337,
  4:340;
  meets Akira and takes her to the infirmary when Akira gets upset,
  2:202--3,
  2:205--8;
  calls Akira “cute” and says that she’d like to “use \[Akira\] as a key chain,”
  2:211--12;
  reads *Rokumeikan*,
  2:246--47;
  is overheard by Haruka while reading *Rokumeikan*,
  2:260;
  is asked to audition for *Rokumeikan*,
  2:263--67;
  auditions for a part in *Rokumeikan*,
  2:279--80;
  is (partially) confided in by Akira,
  3:9;
  plays Kiyaharo in *Rokumeikan*,
  3:17--18,
  3:58--59,
  3:69,
  3:98--99,
  3:102--3;
  is praised for her performance in *Rokumeikan*,
  3:108;
  overhears Akira’s conversation with Kyoko regarding Fumi,
  3:269--70;
  talks about Kawasaki’s condition and life in England,
  4:338--39
- &nbsp;
- **Yamashina, Hinako** (partner of Orie),
  1:380,
  2:174,
  *2:182*,
  2:207,
  *3:4*,
  3:44--45,
  3:52,
  3:54,
  3:61--63,
  3:143--45,
  3:158,
  *3:180*,
  *3:182*,
  *3:209*,
  3:227,
  *4:1*,
  *4:4*,
  4:11,
  4:13,
  *4:182*,
  *4:209*,
  4:218,
  4:230,
  4:237--38,
  4:264 (?);
  confesses to Orie (who likes Shinako),
  1:380--81;
  tells Orie that Orie will forget about Shinako after graduation,
  2:159--60,
  2:171;
  is confessed to and kissed by Orie,
  2:163--65;
  promises to stay together with Orie,
  2:168--69;
  graduates from Fujigaya high school with Orie,
  2:160;
  meets Kazusa at Fujigaya and reminisces about Shinako,
  2:161--63;
  listens to Kawakubo and tells her that she too liked a girl,
  2:169--70;
  rejects Kawakubo’s advances, but tells her that she won’t forget her,
  2:165--67,
  2:171--73,
  4:175;
  tells Orie that Kazusa is getting married,
  2:173--74;
  is interviewed by the Fujigaya newspaper editor-in-chief about her personal life,
  4:213--16;
  receives and rejects a confession from the Fujigaya newspaper editor-in-chief,
  4:231--32;
  becomes Akira’s and Ueda’s homeroom teacher,
  2:204;
  is the subject of gossip spread by the Fujigaya newspaper editor-in-chief,
  2:204--5,
  4:212,
  4:217,
  4:234--36;
  chastises Haruka for addressing her as “Hina” while at Fujigaya,
  2:243--44;
  talks with Orie before the performance of *Rokumeikan* and meets Fumi,
  3:44--45;
  meets Orie’s grandfather,
  3:198--99;
  assists Fumi after Fumi collapses in the bath, and talks with her about Fumi’s relationship with Akira,
  3:214--18,
  3:224--26,
  3:243--44;
  tells a student admirer (Ito) that she’ll be introducing “someone special” to her family on Christmas day,
  3:322--26;
  is tentatively approached by Akira,
  4:138--40;
  suggests staging a gender-swapped version of *The Three Musketeers*,
  4:165--66;
  helps Haruka by enlisting Orie to retrieve the *Three Musketeers* costumes,
  4:154--56;
  complains to her mother about not wanting to meet a man,
  4:209--10;
  commiserates with Orie about their parents,
  4:210--11;
  tries to converse with the Fujigaya newspaper editor-in-chief while on the school trip to England,
  4:247--49;
  pins a graduation corsage on the Fujigaya newspaper editor-in-chief,
  4:310--11;
  imagines a wedding to Orie,
  4:350--51
- Yamashina (father of Hinako):
  tells Hinako that she’ll never be a bride,
  4:210
- Yamashina (mother of Hinako):
  tries to set up a marriage interview for Hinako,
  4:209--10
- Yassan. *See* Yasuda, Misako
- **Yasuda, Misako (Yassan)** (friend of Fumi),
  *1:3*,
  1:151--52,
  1:342--44,
  1:353,
  2:126,
  2:131,
  *2:182*,
  2:191,
  2:199--200,
  2:237,
  2:309,
  2:311--13,
  *3:5*,
  3:110,
  3:145--46,
  *3:182*,
  *3:185*,
  3:196,
  3:198--99,
  3:201--3,
  3:206,
  3:211,
  3:220--21,
  3:228,
  3:273,
  3:304--10,
  *4:1*,
  *4:5*,
  4:13--14,
  4:92--94,
  4:102--3,
  4:105--6,
  4:118,
  4:132--33,
  4:135 (?),
  4:137,
  *4:183*,
  4:189--90,
  4:200,
  4:244--45,
  4:256--57,
  4:282--84,
  4:293--94,
  4:306--7,
  4:313--14,
  4:316,
  4:319,
  4:337--40,
  4:352--53;
  participates in the drama club in junior high,
  4:191--98;
  meets Fumi,
  1:23--25;
  complains about the lack of Matsuoka drama club members,
  1:23,
  2:310;
  visits the Fujigaya drama club with Yasuko, Fumi, Mogi, and Pon,
  1:78,
  1:81,
  1:88;
  sees Fumi’s conversation with Yasuko,
  1:91;
  accompanies Yasuko on a visit to Fujigaya,
  1:147,
  1:150--52,
  1:154;
  helps out at and attends the *Wuthering Heights* performance,
  1:222--24,
  1:232,
  1:234;
  witnesses Akira’s fight with Yasuko,
  1:342--44;
  goes with her friends to visit Ko’s family’s summer home,
  2:6--11,
  2:16,
  2:18--20,
  2:25--30,
  2:32--34,
  2:39,
  2:46--48,
  2:59--61;
  with Pon and Kyoko, teases Mogi about her liking Shinobu,
  2:62--63;
  urges Mogi to confess to Shinobu during their sleepover at Akira’s house,
  2:133--34;
  teases Mogi for being a “lovebird,”
  2:192--93;
  performs in a minor role in *Rokumeikan*,
  3:71,
  3:73,
  3:85;
  laments the fact that the Matsuoka drama club has only three members,
  3:147;
  thanks Akira for lobbying the Matsuoka faculty on behalf of the drama club,
  3:272;
  welcomes new recruits to the Matsuoka drama club,
  4:16--17;
  performs in *Heavenly Creatures* before judges,
  4:285
- Yasuko. *See* Sugimoto, Yasuko
- Yasuko’s father. *See* Sugimoto (father of Yasuko)
- Yasuko’s mother. *See* Sugimoto, Chie
- Yasuko’s sisters. *See* Sugimoto, Kazusa; Sugimoto, Kuri; Sugimoto, Shinako
- Yoshio (uncle of Chizu),
  3:126
{:.reference-index}
