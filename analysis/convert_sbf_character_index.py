#!/usr/bin/python3
"""Convert SBF character index to 青い花 character index."""

import argparse
import math
import re


# Control verbosity of output. Set to True for debugging.
VERBOSE = False


def get_index_args():
    """Get input and output pathnames from command line arguments."""

    parser = argparse.ArgumentParser()
    parser.add_argument(
        'sbf_index',
        help='SBF character index',
    )
    parser.add_argument(
        'ah_index',
        help='青い花 character index',
    )

    args = parser.parse_args()
    return (args.sbf_index, args.ah_index)


def get_character_name(index_entry):
    """Extract character name from index entry."""
    #
    # We have three possible cases, with a regular expression for each.
    # 1. The character has a full name in the format family, given.
    full_re = r'^- +\**([A-Za-z]+), +([A-Za-z]+).*$'
    #
    # 2. The character has a single name (which may be either their
    # given name or family name) followed by a comment in parentheses.
    single_re = r'^- +([A-Za-z]+)  *\(([^)]+)\).*$'
    #
    # 3. The character is identified in some other way.
    other_re = r'^- +(.+)[,:].*$'
    #
    # We attempt to match the line against all three possibilities.
    full_m = re.match(full_re, index_entry)
    single_m = re.match(single_re, index_entry)
    other_m = re.match(other_re, index_entry)
    #
    # Check for each case and set the character name accordingly.
    if full_m is not None:  # Family name, given name
        if full_m.group(2) is None:  # Shouldn't happen, but..
            character = full_m.group(1)
        else:
            character = f"{full_m.group(2)} {full_m.group(1)}"
    elif single_m is not None:  # Single name with comment
        if single_m.group(2) is None:  # Shouldn't happen, but...
            character = single_m.group(1)
        else:
            character = f"{single_m.group(1)} ({single_m.group(2)})"
    elif other_m is not None:  # Some other format
        character = other_m.group(1)
    else:  # Unknown format
        character = ''
    #
    # Return the name to be used for this character.
    return character


def get_page_range(ref):
    """Given a CMOS page reference, return volume plus page range."""

    assert isinstance(ref, str) and ref != ''

    # Page references are of the form x:yyy or x:yyy-zzz where x is
    # the volume number, yyy is the first page number in the
    # reference, '-' is a dash of some sort (see below), and zzz is
    # the end of the page number range (in CMOS format).

    # NOTE: To ensure maximum matching of page references, we allow
    # the "dash" to be one or more of the following: a hyphen-minus
    # ('-', U+002d), an en dash ('–', U+2013), an em dash ('—',
    # U+2014), or a horizontal bar ('―', U+2015). To conform to proper
    # regular expression syntax, these are listed in reverse order in
    # the character class, with the hyphen-minus last.

    # References may also have a preceding or succeeding whitespace or
    # underscore, and may be followed by additional punctuation or
    # other material we can ignore.
    ref_re = '^( *[*_]?)([1-9][0-9]*):([1-9][0-9]*)([―—–-]+)?([0-9]+)?([*_]?)([,;])?.*$'

    # Look for a page reference.
    ref_m = re.match(ref_re, ref)
    if VERBOSE:
        print(f"get_page_range: {ref_m}")
    if ref_m is None:
        return ('', 0, 0, 0, '', '')

    # Pull out the volume, starting page, and ending page (CMOS format).
    # Save and return any surrounding whitepace and punctuation.
    if ref_m.group(1) is None:
        prefix = ''
    else:
        prefix = ref_m.group(1)
    volume = int(ref_m.group(2))
    if ref_m.group(3) is None:  # Shouldn't happen, but...
        return ('', 0, 0, 0, '', '')
    if ref_m.group(4) is None:  # Single page
        first = int(ref_m.group(3))
        cmos_last = first
    else:  # Range of pages
        first = int(ref_m.group(3))
        cmos_last = int(ref_m.group(5))
    if ref_m.group(6) is None:
        suffix = ''
    else:
        suffix = ref_m.group(6)
    if ref_m.group(7) is None:
        punct = ''
    else:
        punct = ref_m.group(7)

    if VERBOSE:
        print(f"get_page_range: {volume}, {first}, {cmos_last}")

    return (prefix, volume, first, cmos_last, suffix, punct)


def get_last_page(first, cmos_last):
    """Convert CMOS-style page range to first and last page numbers."""

    assert isinstance(first, int) and first > 0
    assert isinstance(cmos_last, int) and cmos_last > 0

    if cmos_last >= first:  # Second part is the actual page number
        last = cmos_last
    else:  # Second part contains only changed digits from first part
        # Compute the number of digits in the second part.
        n_digits = math.ceil(math.log10(cmos_last))

        # Compute base digits of first part, ignoring last n digits.
        first_base = math.floor(first / 10**n_digits)

        # The last page number is that value plus the changed digits.
        last = first_base * 10**n_digits + cmos_last

    if VERBOSE:
        print(f"get_last_page: {last}")

    return last


def sbf_to_ah_range(volume, first, last, pages):
    """Convert SBG volume and page numbers to 青い花 equivalents."""

    # Determine the first and last page for each part of this volume.
    first_1 = pages['part1_first'][volume]
    last_1 = pages['part1_last'][volume]
    first_2 = pages['part2_first'][volume]
    last_2 = pages['part2_last'][volume]

    if VERBOSE:
        print(f"sbf_to_ah_range: {first_1}, {last_1}, {first_2}, {last_2}")

    # We have three cases:
    #
    #   1) The first and last pages are in the first part of the volume,
    #      in which case the page numbers map exactly and only the volume
    #      number changes.
    #   2) The first and last pages are in the second part of the volume,
    #      in which case the page numbers must be adjusted based on the
    #      page number of SBF on which the second part starts.
    #   3) Otherwise this is not a valid page reference.
    if first_1 <= first <= last_1 and first_1 <= last <= last_1:
        return (2 * volume - 1, first - first_1 + 1, last - first_1 + 1)
    if first_2 <= first <= last_2 and first_2 <= last <= last_2:
        return (2 * volume, first - first_2 + 1, last - first_2 + 1)
    return (0, 0, 0)


def get_ah_page_range(volume, first, last):
    """Convert Sweet Blue Flowers page range to 青い花 page range."""

    assert isinstance(volume, int) and volume > 0
    assert isinstance(first, int) and first > 0
    assert isinstance(last, int) and last > 0

    # Check that the original page range is valid.
    if volume > 4 or first > last:
        return (0, 0, 0)

    # Each omnibus volume of Sweet Blue Flowers has two parts, each of
    # which corresponds to a separate tankōbon volume of 青い花.
    #
    # For each SBF volume, keep track of the page number of the first
    # and last page of the first part of the volume and the page
    # numbers of the first and last pages of the second part.
    #
    # NOTE: We put 0 as the first entry of the lists so that the lists
    # can be indexed directly using the volume number.
    #
    # NOTE 2: Page 1 of volume 5 of the Japanese edition actually
    # corresponds to page 3:3 of the English edition. Similarly, Page
    # 1 of volume 7 of the Japanese edition actually corresponds to
    # page 4:3 of the English edition.
    pages = {
        'part1_first': [0, 1, 1, 3, 3],
        'part1_last': [0, 193, 177, 177, 176],
        'part2_first': [0, 197,181, 181, 181],
        'part2_last': [0, 381, 357, 360, 371],
    }

    return sbf_to_ah_range(volume, first, last, pages)


def get_ah_page_ref(volume, first, last):
    """Return page reference string for 青い花 page range."""

    assert isinstance(volume, int) and volume > 0
    assert isinstance(first, int) and 0 < first <= last
    assert isinstance(last, int) and 0 < last < 100000

    # If the first and last pages are the same, simply return
    # a page reference to the first page.
    if first == last:
        jp_page_ref = f"({volume}) p. {first}"
        if VERBOSE:
            print(f"get_ah_page_ref: {jp_page_ref}")
        return jp_page_ref

    # We want to find the last set of digits where the first page
    # number and last page number are different.

    # Convert the page numbers into equal-length strings.
    first_str = f"{first:>5}"
    last_str = f"{last:>5}"

    # Iterate over both strings and create a string of digits once
    # they are different between the two strings.
    different_digits = ''
    differ = False
    for first_c, last_c in zip(first_str, last_str):
        if first_c != last_c:
            differ = True
        if differ:
            different_digits = different_digits + last_c

    # Return the page reference. Note that the separator between
    # the two page numbers is an en dash (U+2013), not a hyphen.
    return f"({volume}) pp. {first}–{different_digits}"


def en_to_jp_ref(volume, first, cmos_last):
    """Given an SBF page reference string, return a 青い花 reference."""

    # Parse the SBF reference into volume number and two page numbers.
    # (volume, first, cmos_last) = get_page_range(page_ref)

    # The SBF reference was not valid.
    if volume == 0:
        return ''

    # Compute the actual last page based on the CMOS algorithm.
    last = get_last_page(first, cmos_last)
    if VERBOSE:
        print(f"en_to_jp_ref: {volume}, {first}, {last}")

    # Convert the SBF page numbers into 青い花 page numbers.
    (jp_volume, jp_first, jp_last) = get_ah_page_range(
        volume,
        first,
        last,
    )
    if VERBOSE:
        print(f"en_to_jp_ref: {jp_volume}, {jp_first}, {jp_last}")

    # Check to make sure the conversion worked.
    if jp_volume <= 0:
        return ''

    # Use the 青い花 page numbers to create a page reference.
    return get_ah_page_ref(jp_volume, jp_first, jp_last)


def main():
    """Convert SBF CMOS-style references to 青い花 page references."""

    (sbf_index, ah_index) = get_index_args()
    with open(sbf_index, 'r', encoding='UTF-8') as sbf_index_f:
        lines = sbf_index_f.readlines()
    with open(ah_index, 'w', encoding='UTF-8') as ah_index_f:
        character = ''

        # Look for characters and record their appearances.
        for line in lines:
            # Look for character entries.
            if line.startswith('- '):
                if line.startswith('- &nbsp;') or '*See*' in line:
                    ah_index_f.write(line)
                    continue
                character = get_character_name(line)
                ah_index_f.write(line)
            elif character == '':  # Haven't see a character entry yet
                ah_index_f.write(line)
                continue
            else:
                prefix, volume, first, lst, suffix, punct = get_page_range(line)
                if volume <= 0:
                    ah_index_f.write(line)
                    continue
                jp_page_ref = en_to_jp_ref(volume, first, lst)
                ah_index_f.write(prefix)
                ah_index_f.write(jp_page_ref)
                ah_index_f.write(suffix)
                ah_index_f.write(punct)
                ah_index_f.write("\n")


if __name__ == "__main__":
    main()
