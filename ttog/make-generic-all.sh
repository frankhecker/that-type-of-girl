#!/bin/sh
# Make all EN generic versions.

# NOTE: Run install-dependencies.sh before running this script.
./convert-images.sh
./make-generic-screen.sh
./make-generic-epub.sh
./make-generic-print.sh
./make-generic-web.sh
