---
title: "The Melancholy of Kyoko Ikumi"
---

## The Melancholy of Kyoko Ikumi

In another story, Kyoko Ikumi would be the main character. Her life
thus far has followed a trajectory associated with many Class S and
early yuri stories: apparently destined for an arranged marriage with
an older man for whom she seems to have little if any real feeling,
seeking refuge in an unrequited crush on an older girl, then meeting
clear and unequivocal rejection from the object of her affections. If
this were a traditional story, the only suspense would be whether her
remaining life would be short and unhappy or long and unhappy.
{:.first}

But Kyoko is not the main character of *Sweet Blue Flowers*, which
tells us something about what kind of story this is: “S for a new
generation,” as Erica Friedman put it in her review.[^02-08-01] That
new generation will presumably identify more closely with Akira and
(especially) Fumi, whom Friedman called in another context a “Heisei
girl,” born after the Shōwa period ended in 1989 with Emperor
Hirohito’s death.[^02-08-02]

[^02-08-01]: Erica Friedman, review of *Sweet Blue Flowers*, vol. 1, by Takako Shimura, *Okazu* (blog), October 4, 2017, [https://&#x200B;okazu&#x200B;.yuricon&#x200B;.com&#x200B;/2017&#x200B;/10&#x200B;/04&#x200B;/yuri&#x200B;-manga&#x200B;-sweet&#x200B;-blue&#x200B;-flowers&#x200B;-volume&#x200B;-1&#x200B;-english](https://okazu.yuricon.com/2017/10/04/yuri-manga-sweet-blue-flowers-volume-1-english).

[^02-08-02]: Erica Friedman, review of *Sweet Blue Flowers*, disc 1, *Okazu* (blog), May 6, 2013, [https://&#x200B;okazu&#x200B;.yuricon&#x200B;.com&#x200B;/2013&#x200B;/05&#x200B;/06&#x200B;/yuri&#x200B;-anime&#x200B;-sweet&#x200B;-blue&#x200B;-flowers&#x200B;-aoi&#x200B;-hana&#x200B;-disk&#x200B;-1&#x200B;-english](https://okazu.yuricon.com/2013/05/06/yuri-anime-sweet-blue-flowers-aoi-hana-disk-1-english).

So, why is Kyoko featured so prominently in *Sweet Blue Flowers*
(almost as prominently as Yasuko Sugimoto in this first volume)? For
one, she serves as a friend and classmate to Akira, a guide to her as
she navigates the new world of Fujigaya Women’s Academy. As Kyoko
tells Akira, the women in her family have always attended Fujigaya,
from elementary school on (*SBF*, 1:287).

Kyoko is also both a (would-be) rival to Fumi for Yasuko’s affections
and eventually a fellow sufferer (*SBF*, 1:375--76).  I’ll have more
to say about Yasuko later, but clearly Kyoko’s transparent neediness
and Yasuko’s own problems poisoned the possibility of any real
relationship between them.

Where will Kyoko go from here? Thematically she seems destined to
represent the past, as Fumi represents the future. Personally she
still has Ko Sawanoi waiting in the wings, and perhaps their
relationship (strained as it is) is her own best hope for escaping the
unhappy fate of many a Class S protagonist.
