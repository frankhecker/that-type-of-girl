---
title: "“Such an Old-Fashioned Woman”"
---

## “Such an Old-Fashioned Woman”

Asako Kageyama is the tragic heroine of *Rokumeikan*. Asako was a
geisha when she met her former lover Einosuke Kiyohara and later was
elevated to the aristocracy by her marriage to Count Kageyama. Such
marriages were not uncommon in the Meiji era, as leading politicians
looking to host social gatherings sought out geisha used to dealing
with men in a social context, making them their mistresses or (as in
the case of Prime Minister Hirobumi Itō) their wives.
{:.first}

If this was Count Kageyama’s intention in marrying Asako, it was
thwarted. Asako proved to be a retiring sort, apparently never
venturing outside the Kageyama estate, and certainly not to the
Rokumeikan. As she says, “I’m such an old-fashioned woman, I can’t
possibly go to such a fashionable place.”[^04-06-01]

[^04-06-01]: Mishima, *Rokumeikan*, 8.

But she does go to the Rokumeikan to try to save the life of her son
Hisao, previously bent on the assassination of Einosuke Kiyohara, his
father and Asako’s former lover. Her action comes to naught: Hisao
dies, shot by his father. It is strongly implied that Kiyohara dies as
well, killed by Count Kageyama’s henchman Tobita.  Asako herself is
left to live out her life with Count Kageyama, in a marriage from
which all illusions of love and tenderness have been stripped, with
only raw power and resentment remaining.

Asako, more than Kiyohara, Hisao, or Akiko, is thus the great tragic
figure of Rokumeikan. Akiko is young and still has the possibility of
finding happiness. Hisao and Kiyohara are beyond all feelings. But
Asako has only a life without hope stretching ahead of her.

*Sweet Blue Flowers* is not a tragedy, but if anyone in the manga can
be said to be a tragic figure, it is Kyoko Ikumi. It’s therefore
fitting that Shimura selected her to play Asako. She does not look the
part---her short brown hair totally unlike Asako’s long black
hair---but otherwise she fits the role to a T, with her reticence,
old-fashioned air, unhappy past, troubled present, and uncertain
future. As I wrote of Kyoko in a previous chapter, “If this were a
traditional [Class S] story, the only suspense would be whether her
remaining life would be short and unhappy or long and unhappy.”

In her dissertation on *Rokumeikan*, Mami Harano rhetorically asks why
Japanese audiences would continue to flock to a play in which power
wins and the heroine loses. Harano answers that audiences see in Asako
someone who breaks the bonds of convention and dares to love: to
engage in romantic love with her illicit lover Kiyohara, to show
maternal love toward her illegitimate son Hisao, to choose affection
(*ninjō*) over duty (*giri*).[^04-06-02]

[^04-06-02]: Harano, “Anatomy of Mishima’s Most Successful Play,” 2--3.

People often speak of “the power of love.” In *Rokumeikan* love has no
power, at least in terms of the outworking of the plot. But it still
has the power to move us. As Harano writes, “Observing all of the
contradictions in her life and seeing all the unfairness and
inequality in the world, audiences feel empathy with Asako
....”[^04-06-03] I think the same can be said of Kyoko in *Sweet Blue
Flowers*. Her life is messed up, her mother ill and dependent, her
hoped-for relationship with Yasuko thwarted, and her ongoing
relationship with Ko in trouble, but she at least has her friendship
with Akira and our sympathy as readers.

[^04-06-03]: Harano, “Anatomy of Mishima’s Most Successful Play,” 45.

Sometimes a secondary character will break out from the pack and
achieve a special place in the audience’s heart (for example, Nanami
in *Revolutionary Girl Utena*). Kyoko is such a character for
me. Though she is not the star of *Sweet Blue Flowers* as a whole, she
is undoubtedly the star of this section of it.
