---
title: "Patriarchy by the Book"
---

## Patriarchy by the Book

In commenting on Fumi’s visit to Yasuko’s family, I noted the absence
of Yasuko’s father, the head of the Sugimoto household: “unseen,
unnamed, and unmentioned.” Now, at last, we catch a glimpse of him,
albeit an exceedingly brief one, in a single panel in the chapter set
at Kazusa’s wedding (*SBF*, 2:94). However, though seen, he remains
unnamed and unmentioned.
{:.first}

Determining the intent of an author is always tricky. Still, this
omission may be deliberate on Takako Shimura’s part, her version of
the “curious incident of the dog in the night-time”---significant not
for what is said or shown but rather for what is not. The creators of
the *Sweet Blue Flowers* anime adaptation added an extra scene to
humanize Yasuko’s father: before the wedding, he sits on his back
porch, sighs, and looks to the heavens as if he were a father in an
Ozu film, mourning the loss of his daughter.[^03-05-01]

[^03-05-01]: *Sweet Blue Flowers*, episode 10, “The Happy Prince,” 00:17.

I believe this violates the intent of the manga. In the context of
everything else we know about the Sugimoto family and the evolving
themes of the manga, this particular father is not supposed to be a
character with emotions and an inner life. The Sugimoto sisters have a
father, here he is, but he is not integral in any way to the plot, and
hence we need know nothing more about him.

He is simply embodying a role, that of the family patriarch. More
specifically, he is an entry in a book, the person listed as the
“first registrant,” the de facto head of the Sugimoto family, in the
household register (*koseki*) maintained by the Japanese state. The
*koseki* lists the other family members after him, in a representation
of the family’s age-based hierarchy: his wife Chie, “eldest daughter”
Shinako, “second daughter” Kazusa, “third daughter” Kuri, and “fourth
daughter” Yasuko.

Although precursors to the *koseki* system existed for many centuries,
the *koseki* in its present form originated in the desire of Meiji-era
governments to bring the entire Japanese population under their purview.
The *koseki* system, as established in 1871 and revised and elaborated
in 1898, enforced as normative an ideal patrilineal family. In this
ideal family, a male head of household presided over a
multi-generational family consisting of his wife, his unmarried
daughters, his sons and their wives, their children in turn, and so
on---replicating in miniature the Japanese “family nation” ruled over
by the emperor.[^03-05-02]

[^03-05-02]: David Chapman, “Geographies of Self and Other: Mapping Japan through the *Koseki*,” *Asia-Pacific Journal: Japan Focus* 9, no. 29 (July 19, 2011), 6--7, [https://&#x200B;apjjf&#x200B;.org&#x200B;/&#x200B;-David&#x200B;-Chapman&#x200B;/3565&#x200B;/article&#x200B;.pdf](https://apjjf.org/-David-Chapman/3565/article.pdf).

The postwar reforms imposed by the occupation authorities included a
constitution that ostensibly treats the Japanese people as
individuals. To accompany that change, the *koseki* system was
reformed to eliminate its patrilineal character, at least in
theory. The postwar period also saw several administrative and legal
changes made to accommodate evolving family structures and practices,
including divorces, intermarriage between Japanese nationals and
foreign nationals, and the use of assisted reproductive
technologies.[^03-05-03]

[^03-05-03]: For an overview of how the *koseki* system has operated from the immediate postwar period to the present, see Vera Mackie, “Birth Registration and the Right to Have Rights: The Changing Family and the Unchanging *Koseki*,” in *Japan’s Household Registration System and Citizenship:* Koseki, *Identification, and Documentation*, ed. David Chapman and Karl Jakob Krogness (London: Routledge, 2014), 203--17.

However, in practice, the “reformed” *koseki* still reflects the
patriarchal and heteronormative ideal of a nuclear family headed by a
husband with a wife and one or more legitimate biological
children.

For example, consider the (only) context in which we see Sugimoto
*père*: he leads Kazusa down the aisle before her being wed to
Mr. Kagami. In Anglo-American parlance, he is “giving the bride away,”
a phrase that implies that men own women and that in marriage that
ownership is transferred to other men. Under the Meiji-era *koseki*
system, this transfer would have been made literal by Kazusa’s name
being removed from the Sugimoto register and added to that of the
Kagami household, headed by Mr. Kagami or his father or grandfather
(if one of them were still alive).

The postwar *koseki* system is ostensibly more egalitarian and less
patriarchal. Instead of Kazusa being transferred from the Sugimoto
register to the Kagami register, she and Mr. Kagami would together
leave their previous household registers and enter themselves into a
new register corresponding to their newly-formed nuclear
family. However, one of them would still be required to be the “first
registrant,” whose family name would become the name on the
*koseki*. If they followed typical practice, this would be
Mr. Kagami.[^03-05-04]

[^03-05-04]: At the time that *Aoi hana* was being serialized, wives adopted their husbands’ names in over 96 percent of Japanese marriages. Linda E. White, “Challenging the Heteronormative Family in the *Koseki*: Surname, Legitimacy, and Unmarried Mothers,”  in Chapman and Krogness, *Japan’s Household Registration System and Citizenship*, 253n16.

If Kazusa wished to continue to use the Sugimoto family name, she
would encounter a significant obstacle: which name she uses is not
simply a matter between her as an individual and the Japanese
state. The logic of the *koseki* dictates that she must enter herself
into some register, she and her husband must register together as a
household unit, and the household registration must be made under only
one family name. She could retain her original family name by not
registering her marriage with Mr. Kagami (in effect, entering into a
common-law marriage). However, their children might then not be
considered legitimate. One couple endured a lengthy legal battle over
precisely this issue.[^03-05-05]

[^03-05-05]: White, “Challenging the Heteronormative Family in the *Koseki*,” 239--40, 249--51.

The Japanese state forces everyone into the Procrustean bed of the
*koseki* and has been willing to adjust its dimensions only slightly,
slowly, and grudgingly. Likewise, Japanese society has come to
identify the household as defined in the *koseki* with the concept of
family itself: to be in a family means being registered together in
the *koseki* and not being registered together means not being a
family. We can see this especially in the case of LGBTQ individuals.

Consider, for example, Shuichi Nitori, the transgender protagonist of
Shimura’s *Wandering Son*. If she were to transition in accordance
with Japanese law, she would be forced off the Nitori *koseki* and
required to register herself separately---in effect, severing her from
her family of birth.[^03-05-06]

[^03-05-06]: Shūhei Ninomiya, “The *Koseki* and Legal Gender Change,” trans. Karl Jakob Krogness, in Chapman and Krogness, *Japan’s Household Registration System and Citizenship*, 169--71.

Some have justified this according to the logic of the *koseki*. If
Shuichi’s sister Maho were younger than Shuichi (instead of one year
older), after Shuichi’s transition there would be two “eldest
daughters” on the Nitori *koseki*. The claim is that the state needs
“to prevent such confusing *koseki* entries.”  However, this logic is
not applied in other cases more aligned with patriarchal norms---for
example, the case of a man who marries and has a daughter with one
woman, divorces her and remarries, and then has another daughter with
his second wife, all the while acting as the “head” of his
register. Here the *koseki* also contains two “eldest daughter”
entries, but the state does not care.[^03-05-07]

[^03-05-07]: Ninomiya, “The *Koseki* and Legal Gender Change,” 177--78.

Consider also Fumi and Akira, should their relationship ever progress
to becoming life partners. Japanese law does not explicitly prohibit
two women from marrying each other, but the logic of the *koseki* is
again deployed against them: when two people marry and start their own
register, one must designate as the husband and one as the wife. In
combination with ambiguous wording in the Japanese Constitution and
other aspects of Japanese law relating to gender, this fact has
heretofore provided the justification for Japanese judges to reject
marriage equality.[^03-05-08]

[^03-05-08]: Claire Maree, “Sexual Citizenship at the Intersections of Patriarchy and Heteronormativity: Same-Sex Partnerships and the *Koseki*,” in Chapman and Krogness, *Japan’s Household Registration System and Citizenship*, 190--91.

One way to work around this restriction is for the older person to
adopt the younger, as Nobuko Yoshiya did her partner Chiyo Monma. This
allows them to access some of the benefits usually denied to unmarried
couples, such as access to insurance and the ability to make medical
decisions for each other. However, such arrangements can be contested
by the partners’ families.[^03-05-09] They also promote the idea of an
age-based hierarchy in relationships, an idea that *Sweet Blue
Flowers* implicitly (and, at times, explicitly) opposes.

[^03-05-09]: Maree, “Sexual Citizenship at the Intersections of Patriarchy and Heteronormativity,” 194--96.

It’s true that men also can be harmed by the *koseki* system. Witness
the case of the man rejected by his biological father, who refused to
acknowledge the existence of his son in the *koseki*. Living outside
the system and unable even to attend public school, the man was
released from legal limbo only at age 36 upon his father’s death and
the successful conclusion of a subsequent legal proceeding.[^03-05-10]

[^03-05-10]: Kana Yamada, “Now with a Legal Father, Saitama Man, 36, Ready to Start Own Life,” *Asahi Shimbun*, February 21, 2018, [https://&#x200B;web&#x200B;.archive&#x200B;.org&#x200B;/web&#x200B;/20180222034739&#x200B;/http:&#x200B;/&#x200B;/www&#x200B;.asahi&#x200B;.com&#x200B;/ajw&#x200B;/articles&#x200B;/AJ201802210043&#x200B;.html](https://web.archive.org/web/20180222034739/http://www.asahi.com/ajw/articles/AJ201802210043.html). The man’s father was an aide to a member of the Japanese Diet, the legislative body responsible for the continued existence of the *koseki* in its present form.

But it’s clear that the system in which Kazusa is embedded (along with
all the other women of *Sweet Blue Flowers*) is ultimately of men, by
men, and for men. (In the story mentioned above, the man’s mother and
her household register were irrelevant in establishing his legal
existence.) In entering into a relationship, Fumi and Akira would be
living outside the law, or more precisely trapped by rules and
regulations that, for the most part, refuse to recognize their lives
except in the context of their relationships to men.

The typical “schoolgirl yuri” work slides over this reality in its
quest to portray a fantasy world of female relationships in which men
are invisible. Though *Sweet Blue Flowers* has the outward form of
schoolgirl yuri, like a Jane Austen novel it hints at a deeper and
darker reality beneath its allegedly frivolous surface.

I previously likened Yasuko’s father to another absent father, Sir
Thomas Bertram of *Mansfield Park*, who spends much of the story away
at his plantation on the Caribbean island of Antigua. In that novel,
after the demure heroine Fanny Price is reproved by others for being
“too silent in the evening circle,” she says of Sir Thomas, “Did not
you hear me ask him about the slave-trade last night?”[^03-05-11]

[^03-05-11]: Austen, *Mansfield Park*, chap. 21.

It is but one sentence in a very long novel. Still, in combination
with a reader’s knowledge of early nineteenth-century British society,
it speaks volumes about the socioeconomic system that supports the
aristocracy into which Fanny will ultimately marry. Whether Shimura
intended this or not, to someone like myself with at least a
superficial knowledge of contemporary Japanese society, *Sweet Blue
Flowers* similarly says a great deal with a single image.
