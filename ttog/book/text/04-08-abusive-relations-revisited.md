---
title: "Abusive Relations Revisited"
---

## Abusive Relations Revisited

*Content note: This chapter discusses child sexual abuse.*

Now that the Fujigaya production of *Rokumeikan* has ended, the story
of *Sweet Blue Flowers* moves on to other matters. After the
congratulations to the actors and reviews of the performances, the
chapter ends with Fumi Manjome being surprised by the news that her
cousin Chizu has a new baby and is coming to visit (*SBF*,
3:112--13). The next chapter (“After the Banquet”) opens with an image
of a young and vulnerable Fumi and is devoted to (re)telling the
story of Fumi’s childhood relationship with Chizu.
{:.first}

I’ve already had my say about the abusive nature of this relationship.
Does this chapter add anything to our understanding of Fumi and Chizu?
Here are my thoughts on this question---which, like all my thoughts on
*Sweet Blue Flowers*, are open to correction.

First, Chizu now has a family name, “Hanashiro,” as given in the
character introductions for volume 3 (*SBF*, 3:5). In Japanese, the
first part of her family name is the character for “flower,” the same
character used in the Japanese title of the manga and the title of
Nobuko Yoshiya’s *Hana monogatari* Class S series. The second part of
the name is the character for “castle,” but it shares the same
pronunciation as the character for the color “white.”

“White” in combination with “flower” evokes the white lily that
symbolized passionate but pure romance in Class S culture. Its
Japanese name subsequently became the name of the yuri
genre. Considering Chizu’s history with Fumi, this seems a somewhat
ironic pun on Shimura’s part.

Second, a brief comment about the title of the chapter. The
translator’s notes for the VIZ Media edition are silent as to its
origin and meaning, but other sources speculate that it references
Yukio Mishima’s 1960 novel *After the Banquet* (*Utage no ato*). This
is appropriate if true since the last few chapters focused on another
Mishima work. The novel portrays an ill-fated marriage between an
elderly politician and a middle-aged restaurant owner, who first meet
at a banquet held at her restaurant. The *New York Times* review
summarizes the novel’s conclusion as “Love is strong, but too weak to
hold disparate natures together.”[^04-08-01]

[^04-08-01]: Faubion Bowers, “Politics and Love in Japan,” review of *After the Banquet* by Yukio Mishima, *New York Times*, April 14, 1963, [https://&#x200B;archive&#x200B;.nytimes&#x200B;.com&#x200B;/www&#x200B;.nytimes&#x200B;.com&#x200B;/books&#x200B;/98&#x200B;/10&#x200B;/25&#x200B;/specials&#x200B;/mishima&#x200B;-banquet&#x200B;.html](https://archive.nytimes.com/www.nytimes.com/books/98/10/25/specials/mishima-banquet.html).

The theme of “disparate natures” in an equally ill-fated relationship
marked by an age gap is certainly apropos here. The purpose of the
chapter seems to be to explore the nuances of Chizu’s and Fumi’s
relationship. It also (perhaps deliberately?) contains clues to just
how wide the gap in their ages was.

The story starts with Fumi beginning second grade, having moved away
from Akira, whom she met in first grade. (See the chapter “Ten(?)
years after” for more on the chronology.) Fumi would thus now be
seven years old. We see Fumi’s introduction to her new class, her
feelings of loneliness and thoughts of Akira, her unwillingness to go
back to school after the first day, and (after her return) the
beginning of what appears to be a budding friendship with two girls in
her class (*SBF*, 3:116--23). And then Chizu comes to play.

At this point in the story, Chizu appears to be on the cusp of being a
teenager (perhaps twelve or thirteen years old?), “very rebellious,”
according to her mother, and not “obedient” like Fumi. Fumi clearly
looks up to Chizu, looks forward to her visits, and is disappointed
that she can’t sleep over (*SBF*, 3:124--26).

Then the story skips between pages to a time when Fumi is in
fifth grade (*SBF*, 3:126--27), and is thus at least ten years old.
Chizu’s age is not referenced, but if we take her uniform as that of a
high schooler, she is now at least fifteen years old and perhaps as
old as seventeen. However, her remark to Fumi that “You’re taller than
me!” and her comment about “Girls these days!”  seem to imply that
Chizu herself sees the age gap between herself and Fumi as smaller
than the five or more years it actually is (3:127--28).

Soon after, Chizu’s family moves closer to Fumi’s, implying that the
two girls will see each other much more frequently (*SBF*, 3:128). We
then time skip again between pages, with Chizu now headed off to
university, and thus at least seventeen or eighteen years old, with
Fumi therefore around twelve or thirteen years old, or perhaps
slightly younger (3:128--29). The subsequent conversation over dinner
and in bed highlights the pressure Chizu feels to marry and Fumi’s
lack of interest in boys (3:130--32).

Though it’s not spelled out in so many words, I sense that the sexual
activity between the two begins shortly after this. How frequent it
was and long it continued are not clear. However, it ended before the
time that Fumi’s family moved back to Kamakura, Fumi began attending
Matsuoka (at the age of fifteen), had her reunion with Akira, and was
then surprised and shocked by Chizu’s marriage (*SBF*, 1:20--25,
1:33-40). If we assume that the age gap between Fumi and Chizu is five
years, then at the time of her marriage Chizu would have been twenty
years old, now officially an adult, and would have completed at least
two years of university.

The chapter features one last time skip, as we come back to the
present day with an image of Chizu lost in rueful thought (*SBF*,
3:132--33). She’s interrupted by Fumi bringing two cups of tea and a
piece of cake---a hark back to earlier meetings between the two
(3:124, 3:130). Chizu enthuses about the possibility of moving to
Kamakura near Fumi, stops and thinks better of it, and then apologizes
to Fumi: “Just kidding.” Fumi stares at her, drawn in a full-body
profile view that echoes but reverses the image of a young and
vulnerable Fumi with which the chapter begins (3:133--34, 3:115).

The parallels to earlier scenes continue as Chizu again asks Fumi if
she likes anyone. This time Fumi answers, “Yes.” When Chizu questions
her further regarding the object of her affections, Fumi replies, “A
girl.” Chizu apologizes again (“I’m the one who made you that way.”),
but Fumi resists this interpretation (“Don’t say it like that.”)
(*SBF*, 3:134--35).

As the conversation continues, Chizu is overcome with regret and more
than a hint of jealousy (“Do you like her more than me?”). She muses
on her daughter’s resemblance to Fumi and finally breaks down in
tears in contemplation of the path her own life has taken compared to
Fumi’s (“I can’t be *that* kind of girl.”) (*SBF*, 3:136--37).

The last (unspoken) words are Fumi’s. She thinks to herself, “My love
for Chizu was real,” before drawing a line under the whole affair:
“And that’s the truth” (*SBF*, 3:138).

But we as readers can’t help wondering, what really is the truth here?
Clearly, Chizu thinks of herself, and by implication exonerates
herself, as a victim of circumstances beyond her control: that she was
pressured into marrying, and that social prejudices and ties of blood
(“We’re girls ... and cousins.”) kept her from having the relationship
she wanted to have with Fumi (*SBF*, 3:137).

What is less clear to me is whether Takako Shimura wants us to think
of Chizu as a victim. Chizu was certainly hemmed in by the
expectations of her family and society, expectations that limited whom
she could love and have as a life partner. On the other hand, Fumi was
just past childhood, and Chizu was nearly an adult. If Chizu was drawn
to women, she could have---and should have---sought out someone closer
to her own age, whether at high school or university. Instead, in Fumi
she found and exploited a young girl for her own purposes, a girl who
was predisposed to look up to her and follow her lead.

As for Fumi, objectively speaking, she was a victim of abuse, but I
don’t think she regards herself as such. I do not believe this is
because she was manipulated into this view, but rather because this is
consistent with her overall personality as portrayed in the manga.

To paraphrase what I previously wrote, Fumi is a person with a “steel
core,” a deeply emotional person who ultimately does not let her
emotions distract her from who she is and what she wants. Whether
Chizu “made [her] that way” is irrelevant to Fumi. As she tells her
friends later in volume 3, she *is* “that type of girl,” and that’s
all there is to it.

The final image of the chapter again echoes and reverses the chapter’s
initial image of a young and vulnerable Fumi: eyes no longer cast
down, she looks straight ahead, her glasses and her ponytail (a change
from her typical more childish pigtails) marking the maturity that she
is well on the way to achieving (*SBF*, 3:138).

In the end, I’ll repeat what I previously wrote about Chizu: “*Sweet
Blue Flowers* seems to valorize relationships between equals and
implicitly criticize unequal relationships based on age or other
hierarchies. ... From this point of view, Chizu’s relationship with
Fumi offers the reader another example of the potential harms inherent
in and inseparable from some classic yuri tropes.”

Although Chizu is featured in character introductions later in volume
3 and in volume 4, this is the last time she plays a part in the
events of the manga. What happened between Fumi and Chizu is now in
the past. What will happen between Fumi and Akira is now the most
important question.
