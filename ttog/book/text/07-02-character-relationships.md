---
title: "Appendix 2: Character Relationships"
---

## Appendix 2: Character Relationships

{% include image file="sbf-social-graph.png" alt="Graph showing the most prominent characters in Sweet Blue Flowers and their relationships." %}

In addition to serving as a guide to the characters who appear in
*Sweet Blue Flowers*, the character index also provides data helpful
in analyzing other aspects of the manga. Examples of such analyses
include determining how prominently a given character is featured in
the manga, how that character relates to other characters, and how
prominently such relationships are featured. The character index
included in Appendix 1 is abridged for readability, but I’ve used a
complete and unabridged character index to do an initial analysis of
these questions.[^07-02-01]
{:.first}

[^07-02-01]: Frank Hecker, “Relative Prominence of Characters and Their Relationships in Takako Shimura’s *Sweet Blue Flowers*,” RPubs.com, March 7, 2022, [https://&#x200B;rpubs&#x200B;.com&#x200B;/frankhecker&#x200B;/874648](https://rpubs.com/frankhecker/874648). The unabridged character index is available in this book’s public source repository; see the colophon for more information.

The first question that might come to mind is: how prominently is each
character featured in *Sweet Blue Flowers*? For example, does Fumi
appear more often than Akira, or vice versa? And what about Kyoko? How
frequently does she appear relative to Fumi and Akira? We can answer
these questions by counting the number of pages on which each
character appears and then seeing which are featured on the most
pages.[^07-02-02]

[^07-02-02]: The analysis omits the cover pages for each volume and the two parts of each volume, and the pages at or near the front of each chapter used for character portraits not related to the narrative. The analysis also omits pages in the character profiles and afterwords included in each volume.

Such an analysis shows that Fumi and Akira are almost equally
prominent in the manga, with Fumi having a slight edge. Each of the
two girls appears on around half of the manga’s pages. Kyoko is the
next most prominent character, appearing on about a quarter of all
pages, half as many as Fumi or Akira. Yasuko appears on about one
sixth of all pages. All other characters appear on no more than one in
ten pages. (A few characters appear only on one or two
pages.)[^07-02-03]

[^07-02-03]: Hecker, “Relative Prominence of Characters.” Fumi appears on 52.1 percent of the manga’s pages, Akira on 49.4 percent, Kyoko on 23.1 percent, and Yasuko on 16.5 percent. The “median character” appears on only seven pages or about 0.5 percent of the manga.

We can also use the character index to analyze relationships between
characters. In particular, if two characters appear on the same page,
I consider them to have a connection to each other.

This criterion is not foolproof---for example, a given page may have
one or two panels with one group of characters and then transition to
other panels with other characters. However, if those instances are
relatively few (and I believe they are), then the “relationships”
discovered using this method will map relatively closely to the actual
relationships in the manga.

There are eighty-two “characters” in the character index. (Some
entries correspond to groups of people, such as Fujigaya
elementary-school students.) If each character appeared on at least
one page with every other character, there would be over three
thousand “relationships” depicted in the manga. In actuality, most
characters in the manga appear together on a page with only a few
other characters. There are less than four hundred unique instances of
pairs of characters appearing together on one or more pages, so only
about one in ten of the possible relationships is depicted.[^07-02-04]

[^07-02-04]: Hecker, “Relative Prominence of Characters.” In theory, each of the eighty-two characters in the character index could appear on a page with any of the other eighty-one characters. The product of these two numbers is 6,642. However, we must divide this number by two to avoid double counting, giving 3,321 possible “relationships.” According to the character index, there are 350 unique instances of characters appearing together on at least one page. Thus only 11 percent of all possible character relationships are realized in the manga.

In addition to knowing which characters have a relationship with
others, I would also like to know how prominently those relationships
are featured in the manga. I use the number of pages on which two
characters appear together as a proxy measure of the prominence of
their relationship.

As one would expect, the relationship between Fumi and Akira is the
most prominent one. They appear together on almost a third of the
manga’s pages. No other relationship is featured on more than a tenth
of the manga’s pages, with the relationships between Akira and Kyoko
and Fumi and Yasuko being the next most prominent.[^07-02-05]

[^07-02-05]: Hecker, “Relative Prominence of Characters.” Fumi and Akira appear together on 30.3 percent of all pages, Akira and Kyoko on 9.7 percent, and Fumi and Yasuko on 8.8 percent.

Finally, the relationships between characters in Sweet Blue Flowers
can be represented graphically, again using joint appearances on a
page as a proxy for characters having some connection to each
other. There are many possible ways of constructing such a graph. The
graph above shows one such way.[^07-02-06]

[^07-02-06]: Hecker, “Relative Prominence of Characters.” This particular graph was laid out using the the force-directed algorithm of Fruchterman and Reingold, with the prominence values for nodes and edges calculated using the logarithms of the page counts for the characters and character pairs respectively. Thomas M. J. Fruchterman and Edward M. Reingold, “Graph Drawing by Force-Directed Placement,” *Software: Practice and Experience* 21, no. 11 (November 1991), 1129--64, [https://&#x200B;doi&#x200B;.org&#x200B;/10&#x200B;.1002&#x200B;/spe&#x200B;.4380211102](https://doi.org/10.1002/spe.4380211102).

The complete “social graph” of *Sweet Blue Flowers* is too cluttered
to be readable, even without all possible relationships being
represented in the manga. Therefore, the graph above shows only the
relationships between the top sixteen characters ranked by their
prominence in the manga (i.e., the number of pages on which they
appear). The size of the label for each character is related to each
character’s prominence, with Akira and Fumi appearing most
prominently, as expected.[^07-02-07]

[^07-02-07]: The difference in prominence between characters is even more pronounced than the sizes of the labels imply since the code that created the graph forces labels to be larger than a specific minimum size to improve readability.

The links between each pair of characters reflect the prominence of
their relationship, with character pairs with more
prominently-featured relationships positioned more closely
together. Since Fumi and Akira’s relationship is featured most
prominently, they are shown close together on the graph.

Solid lines between characters represent the top twenty percent of
relationships ranked by prominence, dashed lines the other eighty
percent. Lines corresponding to more prominently-featured
relationships are also thicker.

In addition to Akira and Fumi, there are also clusters of other people
whose relationships with each other are more prominently
featured. These clusters include Akira and her mother and brother;
Fumi and her mother; Kyoko and Ko; Mogi, Pon, and Yassan; and Yasuko,
Kyoko, Fumi, and Akira. There’s also a less prominently-featured set
of relationships among Yasuko, Kazusa, and Mr. Kagami.

These clusters could be intuited by anyone reading the manga, but it’s
helpful to have them confirmed by a more formal analysis. Such an
analysis could also uncover other clusters of characters not
necessarily apparent at first glance and discover other things
relating to *Sweet Blue Flowers*---for example, the chains of
relationships connecting characters not directly connected. However, I
leave further analyses to others who can reuse or adapt the code I
used to do this one.
