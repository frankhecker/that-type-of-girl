---
title: "Ten(?) Years After"
---

## Ten(?) Years After

What is the chronology of *Sweet Blue Flowers*? How old are Fumi and
Akira? When did they first know each other? And why were they
separated for such a long time? Attempting to answer these questions
can help us explore how *Sweet Blue Flowers* works as a narrative and
why Takako Shimura made the choices she did.
{:.first}

In chapter 1, Fumi Manjome and Akira Okudaira meet again for the first
time in several years (*SBF*, 1:34). To be precise, they meet knowing
who the other person is---when they met at the train station on their
first day in high school, they were effectively strangers to one
another (1:12). How long had it been since they had last been
together?

After their reunion, Fumi thinks to herself about “the ten years we’d
spent apart” (*SBF*, 1:44). Is this consistent with the rest of the manga? In
volume 1, Fumi and Akira are beginning their first year of (senior)
high school. In the Japanese educational system, students spend six
years in elementary school, three years in junior high school (lower
secondary school), and three years in senior high school (upper
secondary school).[^02-04-01]

[^02-04-01]: Ministry of Education, Culture, Sports, Science, and Technology, “Overview,” MEXT website, accessed January 2, 2022, [https://&#x200B;www&#x200B;.mext&#x200B;.go&#x200B;.jp&#x200B;/en&#x200B;/policy&#x200B;/education&#x200B;/overview&#x200B;/index&#x200B;.htm](https://www.mext.go.jp/en/policy/education/overview/index.htm).

In the flashbacks to Fumi and Akira’s childhood together, their class
year is given as “year 1,” with Fumi and Akira in the “Cherry Blossom”
and “Plum Blossom” classes, respectively (*SBF*, 1:29--30). If these
are elementary school classes, then nine years (not ten) would have
elapsed between their entering year 1 of elementary school and year 1
of senior high school.

If this were the case, the time since Fumi and Akira last saw each
other would be even less: they also appeared in a Christmas
performance together (*SBF*, 3:68), so they would have been in school
together almost an entire year. (The school year presumably started on
the standard date of April 1.) Assuming that Fumi’s parents moved away
sometime after that, their time apart would be closer to eight years
than nine.

The alternative explanation is that the manga depicts young Akira and
Fumi in kindergarten rather than elementary school. I don’t know
enough to judge whether this is a reasonable explanation. But I will
note that since Akira and Fumi must be fifteen years old to be
entering high school, a ten-year separation would put them at five
years old when they separated and four years old when they first
met. These ages seem a bit young given the characters’ appearances in
the manga and make it a bit implausible that they would even have
memories of that time.

Also, in a flashback in volume 3, Fumi is depicted as having started
at a new school in what appears to be a typical elementary school
classroom, numbered “2-2” (*SBF*, 3:116, 3:123). Therefore the most
reasonable conclusion is that this scene depicts Fumi entering the
second grade of elementary school relatively soon after her separation
from Akira at or near the end of first grade.

On balance, I believe that Fumi and Akira met in elementary school and
that the “ten years apart” was poetic license on the part of Fumi---or
perhaps just her not doing the math correctly.

The more interesting question is, why did Shimura choose the plot
device of girls separated in childhood and reunited in high school? I
think the most straightforward explanation flows from two key elements
of the manga: the contrast between Fujigaya and Matsuoka and the
relationship between Funi and Chizu.

As discussed in the last chapter, I think Shimura thought it essential
to the manga’s themes that Fumi and Akira go to different
schools. Having Fumi and Akira both go to Fujigaya would have made
*Sweet Blue Flowers* just another conventional “schoolgirl yuri”
story, while having them both go to Matsuoka would have denied Shimura
an opportunity to comment on Class S tropes and attitudes.

But if Fumi and Akira were to attend different high schools, that
raises the question of how they would have met in the first place. One
obvious solution is for their respective families to have known each
other in the past, either because they were related in some way or
because they lived near each other and had daughters going to the same
school.

There are already two incestual or potentially incestual relationships
in *Sweet Blue Flowers* (Fumi and her cousin and Akira and her
brother), neither of which the story portrays in a positive
light. Thus having Fumi and Akira be related would be a non-starter.
Shimura chose a better solution: the two families previously lived
near each other, Fumi and Akira went to school together, and then
Fumi’s family moved away, presumably due to the demands of her
father’s work.

But then the story also has to account for the relationship between
Fumi and Chizu. This relationship occurred before Fumi entered high
school, presumably while attending junior high school. In this case,
it does make sense for Chizu to be related to Fumi, providing an
excuse for them to know each other well enough to have sleepovers. And
since they were related, they likely spent time together well before
then.

It wouldn’t make sense for Fumi to be going to school with Akira
during the period in which Fumi was having sex with Chizu. Among other
things, this would eliminate the conceit that Akira was Fumi’s “first
love.” So, that pushes the timeframe for Fumi’s and Akira’s time
together past junior high into elementary school and likely into early
elementary school.

Having Fumi and Akira meet in the first grade supports the plot device
of Fumi coming to recall her “first love.” It provides enough time for
Fumi and Akira to have forgotten what each other looked like---and
even each other’s names---while still making them old enough to have
retained some detailed memories of the time they spent together.
