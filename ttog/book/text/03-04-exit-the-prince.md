---
title: "Exit the Prince"
---

## Exit the Prince

Having discussed Fumi Manjome’s side of the question, let’s turn back
to Yasuko Sugimoto. I earlier noted Yasuko as an example of the “girl
prince” archetype who was nonetheless “as messed-up as any teenaged
girl.” Volume 2 of *Sweet Blue Flowers* continues that picture and
(presumably) concludes Yasuko’s time as a significant character in the
story.
{:.first}

We already know the basic outlines of Yasuko’s story from volume 1.
While a student at Fujigaya, she had a crush on Mr. Kagami, a teacher
who was her older sister’s boyfriend and eventual husband (*SBF*,
1:162--63). After being rebuffed by him, she subsequently left
Fujigaya to attend Matsuoka, where she met Fumi (1:164,
1:57--58). After a brief relationship, they broke up, apparently
because of Yasuko’s lack of honesty with Fumi about her continued
feelings towards Mr. Kagami and Fumi’s consequent jealousy and anger
(1:320).

Volume 2 adds two new elements to the fallout from Mr. Kagami’s
wedding to Kazusa: a (presumably) definitive rejection by Fumi of
Yasuko’s attempt to get back together (discussed in the last chapter),
and a view into Yasuko’s past and inner feelings, apparently
occasioned by Yasuko’s self-reflection after being stung by Fumi’s
words.

The flashback starts with Yasuko’s remembrance of being “treated ...
like a prince” and growing used to that role, meeting her sister’s
boyfriend Mr. Kagami, and cutting her hair short to imitate her
sister’s “tough personality,” finding herself becoming popular with
other girls, and becoming “pretty stuck-up” (*SBF*, 2:110--13). The
whole sequence is consistent with what we know from volume 1, so what
if anything new do we learn in volume 2?

I don’t think it’s so much new information as a deeper look into
things only hinted at before. Most notably, the backstory explains
very well the fraught relationship between Yasuko and Kyoko Ikumi:
Kyoko reminds Yasuko of what she herself is, a needy person obsessed
with another person who will not return her affections. Yasuko’s
brusque and at times cruel behavior toward Kyoko resembles Kagami’s
treatment of Yasuko: “He laughed when I confessed my feelings” (*SBF*,
2:115--16).

Seeing Kyoko cutting her hair and otherwise mimicking her, Yasuko
considers it “cute but annoying” and concludes, “She never thinks
about what’s right for her” (*SBF*, 2:116). But this is often just as
true of Yasuko herself, who veers between recognizing that her own
behavior is destructive and then going ahead and behaving that way
anyway, only to suffer yet another reverse.

There are a couple of other aspects worth noting about Yasuko’s story.
First, it’s not clear exactly why she finds Mr. Kagami attractive. He
praises her talent (at painting presumably) and encourages her to join
the drama club, but that seems a thin motivation for what appears to
be a years-long crush (*SBF*, 2:114). And when she’s faced with the
actual reality of Mr. Kagami, she seems put off: “You think like an
old guy,” she tells him, “You just aren’t cool,” and then berates him
in her thoughts: “Even now, he says flirty things!” (2:87--89).

Maybe it’s just as Yasuko tells him, referring to herself and her
sisters, “We must have a genetic weakness for guys like you” (*SBF*,
2:88). Whether this is true or not, it does bring up a second key
point about Yasuko.

For Yasuko, and apparently for her sister Kuri (another frustrated
Mr. Kagami admirer), relationships with other women function as
substitutes for the relationships with men they cannot have and are
ultimately secondary to their relationships (or potential
relationships) with men. See, for example, the “Little Women” story of
Kuri and her classmate Komako, who laments, “I don’t think we like
each other the same way” (*SBF*, 2:176--77).

At an all-girls school, girls are more available for relationships
than boys, and for Yasuko (and perhaps Kuri as well) they offer a
less-stressful alternative---as Yasuko says to herself, “Girls are
easy to handle” (*SBF*, 2:113). This again echoes the traditional
Class S idea that relationships with women are “just a phase,”
appropriate for the hothouse atmosphere of an all-girls environment,
but not something that can or should continue beyond graduation.

So what of Yasuko now? With Fumi’s rejection of her, Yasuko’s primary
role in Fumi’s story appears to be over (although Fumi still wonders
for a bit whether she’s genuinely over Yasuko). Yasuko has another
scene with Kyoko (who also appears to be getting over Yasuko, though
more quietly) and ultimately flies off to England, “as beautiful and
brilliant as ever” (*SBF*, 2:146--48, 2:187--88).

Her final achievement at Matsuoka is to take the school’s basketball
team to the national championship game. However, she was not able to
lead them to a final victory---somewhat of a metaphor perhaps for
Yasuko’s life thus far (*SBF*, 2:187).

Where will she go from here? Her worst fate, perhaps, would be to end
up like her sisters Shinako and Kuri: wealthy and carefree, but having
no visible purpose in life beyond gossip and flirtation with their old
schoolmates, apparently unable or unwilling to commit to a deep
relationship. Whatever one might think of her choice, Kazusa at least
has made one and is committed to it. We’ll have to see whether the
same is true of Yasuko.
