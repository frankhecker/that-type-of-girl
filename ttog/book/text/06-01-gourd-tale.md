---
title: "Gourd Tale"
---

## Gourd Tale

If a woman in an office is a willow,<br />
A poetess a violet,<br />
And a teacher an orchid,<br />
Then a factory woman is a vegetable gourd.
{:.epigraph}

Sung by workers in the textile mills of Meiji Japan.[^06-01-01]
{:.epigraph-source}

[^06-01-01]: E. Patricia Tsurumi, “Yet to Be Heard: The Voices of Meiji Factory Women,” *Bulletin of Concerned Asian Scholars* 26, no. 4, 27, [https://&#x200B;doi&#x200B;.org&#x200B;/10&#x200B;.1080&#x200B;/14672715&#x200B;.1994&#x200B;.10416166](https://doi.org/10.1080/14672715.1994.10416166).

Fans celebrated 2019 as the one-hundredth anniversary of the yuri
genre.[^06-01-02] In this chapter, I take a final look back at the
early twentieth-century Japanese society that gave rise to the genre
and tell the story of the unsung girls and women whose work helped
bring that society into being.

[^06-01-02]: Erica Friedman, “Yuri, 1919--2019, from Then to Now,” Anime Herald, February 6, 2019, [https://&#x200B;www&#x200B;.animeherald&#x200B;.com&#x200B;/2019&#x200B;/02&#x200B;/06&#x200B;/yuri&#x200B;-1919&#x200B;-2019&#x200B;-from&#x200B;-then&#x200B;-to&#x200B;-now](https://www.animeherald.com/2019/02/06/yuri-1919-2019-from-then-to-now).

*Sweet Blue Flowers* contains many elements that reflect and
indirectly comment on contemporary Japan, but one aspect of Japanese
society is almost totally absent from its pages: class. It is not
alone in this respect. Class S literature, early (proto-)yuri works
like *Dear Brother*, the light novel series *Maria Watches Over Us*,
and the various yuri works influenced by *Marimite* all feature
relatively affluent students in all-girls schools. Often a younger
middle-class girl enters into an S-like relationship with an older
girl born to great wealth, as Yumi does with Sachiko in *Maria Watches
Over Us*.

In *Maria Watches Over Us*, in particular, the upper-class half of the
pair is depicted as a victim, oppressed by a patriarchal system just
as much as any other girl. Their relationship with a representative of
the middle class is presented as the cure to heal their ills. This
serves to flatter the (presumed middle-class) reader regarding the
value and importance of the middle class while artfully distracting
that same reader from the power that the truly wealthy exercise over
them.[^06-01-03]

[^06-01-03]: Similar relationships are also present in non-yuri manga. For example, the romantic comedy *Kaguya-sama: Love Is War* features a boy from a formerly upper-class family, now reduced to middle-class status, and his relationship with the titular Kaguya, whose family is reputed to be among the wealthiest in Japan. Aka Akasaka, *Kaguya-sama: Love Is War*, trans. Emi Louie-Nishikawa, 21 vols. (San Francisco: VIZ Media, 2018--).

This trope is present in *Sweet Blue Flowers* as well, in the
relationship between Fumi and Yasuko. Yasuko’s family is shown to be
on another level entirely when it comes to their wealth---as Fumi
herself marvels when she visits the Sugimoto estate: “Her family’s
rich...” (*SBF*, 1:303). If *Sweet Blue Flowers* used the trope as did
*Maria Watches Over Us* then Fumi’s love would likely be the key to
healing Yasuko’s emotional immaturity.

*Sweet Blue Flowers* subverts this trope by having Fumi break up with
Yasuko and redirect her attentions to Akira, someone of a similar
station in life. In the meantime, Yasuko matures on her own, helped
perhaps by her getting out of Japan.

But although Fumi and Akira both have middle-class backgrounds, they
are arguably nearer the upper end of the middle-class spectrum. Their
families live in single-family detached houses, and both families own
at least one car. More tellingly, both families can send their
children to private schools---in the case of Akira, what appears in
all respects to be a very expensive and exclusive school. Kyoko’s
family is even more likely to be wealthy; she attended Fujigaya from
elementary school on.[^06-01-04]

[^06-01-04]: According to Japanese government statistics, educating a child in private schools from nursery school through high school is about three times as expensive as educating a child in public schools, with an average cost of ¥17.7 million (about $160,000 at current exchange rates). “Private School Costs Triple Public Education Level through High School,” Nippon.com, October 4, 2018, [https://&#x200B;www&#x200B;.nippon&#x200B;.com&#x200B;/en&#x200B;/features&#x200B;/h00299](https://www.nippon.com/en/features/h00299).

Thus *Sweet Blue Flowers*, like other yuri manga and Class S stories,
portrays a world in which the poor and even the lower-middle class are
absent, their stories untold.

Where then can we find them? Let’s turn back the clock to the late
Meiji era in which *shōjo* culture and Class S literature were born,
and consider a hypothetical girl born on the same day as Nobuko
Yoshiya in 1896, but to a poor peasant family eking out a miserable
living in the hinterlands of Japan. What might her tale have been?

One option for her family, and a very common one at the time, would
have been to sell her to a brothel. But in the Meiji era, another
possibility opened up: sending her to work in a mill producing silk or
cotton thread. In 1907, when our young girl (like Nobuko Yoshiya)
would have been eleven years old, there were over two hundred thousand
girls and women working in such mills, with ages ranging from the
twenties down to the early teens or even younger.[^06-01-05] While
Yoshiya was reading the newly-established magazines for girls and
looking forward to attending high school, our girl’s parents would
likely have been contemplating sending her off to work as a “factory
girl” (*kōjo*).

[^06-01-05]: E. Patricia Tsurumi, *Factory Girls: Women in the Thread Mills of Meiji Japan* (Princeton, NJ: Princeton University Press, 1990), 10, Table 1.1, and 87, Table 4.7.

In earlier times, she might have stayed at home to help with farming
or in-home production of goods. But in the Meiji era, families like
hers needed cash to pay new government taxes and were at the same time
in economic distress due to cheap imports displacing domestic
household production. In turn, the Meiji government needed cash to
meet foreign exchange needs and economic growth to forestall popular
rebellions but saw export opportunities handicapped by free trade
agreements forced on Japan by Western countries. These prevented Japan
from raising tariffs on imports, including textiles.[^06-01-06]

[^06-01-06]: Tsurumi, *Factory Girls*, 19--24.

The government found an answer in the large-scale production of silk
and (later) cotton thread, products that could compete on the
international market and drive export growth. After an initial period
of government-run mills that trained the daughters of low-rank samurai
in new professions, the industry became dominated by private mill
owners employing a workforce of poor urban dwellers and poor migrants
from rural areas. The vast majority of these were girls or young
women.[^06-01-07] Our hypothetical young girl might have been one of
them.

[^06-01-07]: Tsurumi, *Factory Girls*, 25--26, 34--38, 41--42.

What might have been our girl’s experience if she went to work in the
mills? She would likely have been recruited by an independent agent
working on commission from a mill owner. The recruiter would spin
stories of good pay for girls who worked hard, hearty and nutritious
meals for the workers, after-work life in comfortable dormitories
where she could learn to read or take other classes, and days off work
when she could join other girls in sightseeing expeditions. If her
parents were swayed by such stories (and who might not be, knowing
nothing to the contrary?), her father would sign a contract committing
her to work for several years, look forward to the extra money she
would bring the family, and commit her to the care of the
recruiter.[^06-01-08]

[^06-01-08]: Tsurumi, *Factory Girls*, 59--60.

Assuming that she arrived at her destination safely (as some did not,
sold into brothels or to rival mills), she would have found the
reality of the mills to be much different than the promises. The
productivity of Japanese factory workers was still significantly lower
than that of foreign mills, and to successfully compete on price with
foreign silk mills, Japanese mills chose not to try to make workers
more productive but rather to reduce costs (and thus improve profits)
by any means possible.

They did this by lowering wages, forcing workers to work longer hours,
running second shifts, and practicing various forms of wage theft: for
example, fining workers for relatively trivial infractions, or holding
back wages for a year or more---all measures outlined in the contract,
the language of which the typical poor farmer (like our girl’s father)
would be incapable of reading.[^06-01-09]

[^06-01-09]: Tsurumi, *Factory Girls*, 63--67.

Thus from the time our young girl entered the mill, she would have
found herself paying back debts (including the expenses of
transporting her to the factory), incurring fines, and having her
already-meager wages reduced in various ways. She would have found
herself competing with other workers, with the most productive workers
receiving the highest wages, and production quotas increasing but
wages remaining the same. If she were of relatively plain appearance
(like Nobuko Yoshiya), she would likely have found her wages to be
lower than others of fairer face more favored by the mill
owner.[^06-01-10]

[^06-01-10]: Tsurumi, *Factory Girls*, 75--85.

After work hours, she would have been locked in a dormitory to prevent
her escaping, surrounded by high fences topped with barbed wire or
sharpened bamboo stakes. She would have been fed a meager diet of rice
mixed with barley, with the costs of the meals deducted from her
wages. At night she would have slept in a room where each girl had a
single *tatami* mat’s worth of space and might have shared her
sleeping garments with a girl on another shift.[^06-01-11]

[^06-01-11]: Tsurumi, *Factory Girls*, 67--70.

As for the promised education, some mill owners offered rudimentary
classes in Japanese and arithmetic to their workers, but many workers
found themselves too tired to attend such classes after a twelve-hour
shift.[^06-01-12] Our young girl would likely have been left
semi-literate at best. She might have had access to shared copies of
the girls’ magazines in which Nobuko Yoshiya’s stories appeared (20 to
40 percent of factory workers read magazines[^06-01-13]) but might
have found it challenging to understand Yoshiya’s ornate prose.

[^06-01-12]: Tsurumi, *Factory Girls*, 68--69.

[^06-01-13]: Sarah Frederick, *Turning Pages: Reading and Writing Women’s Magazines in Interwar Japan* (Honolulu: University of Hawai‘i Press, 2006), 16.

More typical “educational” fare were lectures intended to persuade
workers to show devotion to their employers, as a loyal retainer might
serve his lord. Like the eighteen-year-old Kikue Yamakawa---later to
become famous as a socialist and feminist activist---our factory girl
might have found herself attending a sermon, in which visiting
Christian missionaries would encourage the girls and women to work
hard like Jesus the carpenter, be obedient and submissive to their
employers, and be grateful for what was given to them. (Yamakawa, who
attended one such sermon as a guest of the missionaries, left the mill
vowing never to work with them again.)[^06-01-14]

[^06-01-14]: Tsurumi, *Factory Girls*, 139--40. Yamakawa: “I felt my whole body shake with shame and rage. What blessings from God came into the lives of these pale young girls with the lifeblood sucked out of them, who had worked all night without sleep next to roaring machines? What blessings deserved thankfulness? Should their slave labor be treated as holy?”

What if she had been a lesbian, like Yoshiya? Watched over like a hawk
by supervisors both on the factory floor and in the dormitory, crammed
into a room at night with twenty or more other girls, allowed to leave
the factory grounds only at long intervals, how could she have
sustained a relationship with another factory girl? It’s more likely
that she would have been sexually harassed or even raped by a male
coworker, a male supervisor, or the mill owner---who had keys to the
dormitory rooms.[^06-01-15]

[^06-01-15]: Tsurumi, *Factory Girls*, 135--36.

If she were lucky, she would complete her contract and return to her
village to live out her life---a life perhaps even harder than that in
the mill if her family were still in poverty. If she were unlucky, she
might have become sick, be pressed to continue to work while ill, and
then (if she took a turn for the worse) be sent home to die. The mills
were breeding grounds for disease, including epidemics of cholera and
dysentery and chronic cases of tuberculosis and beriberi, to which the
girls and young women were made vulnerable by long work hours and
inadequate nutrition. Teenaged factory girls were particularly
affected: their death rate was over twice that of girls in the general
population.[^06-01-16]

[^06-01-16]: Tsurumi, *Factory Girls*, 168--71.

What connects our hypothetical factory girl and her real-life
counterparts to Nobuko Yoshiya, to the world of Class S literature of
which Yoshiya herself was the preeminent practitioner, and to the
world of *shōjo* culture of which that literature was a key element?

The life of a factory girl, the life of a schoolgirl, and indeed the
life of Nobuko Yoshiya herself were made possible because Meiji Japan
lacked one of the prime characteristics of other patriarchal
societies. Although fathers in Japan typically exercised strict
control over whom their daughters could marry, they did not seek to
isolate them entirely from the outside world.

In this respect, Japan differed from patrilineal societies in the
Middle East and---more germane in this context---on the Indian
subcontinent. In Japan, there was a long tradition of peasant boys
*and* girls “going out to work” (*dekasegi*), that is, leaving the
family home to seek employment elsewhere. Poor families were willing
to send their daughters off to work in the mills, accepting the risk
that they would be raped, seduced, or otherwise bring “dishonor” to
the family in return for the economic benefits that they could
provide. In other words, “South Asia had a stronger preference for
female seclusion, and East Asia a stronger preference for female
exploitation.”[^06-01-17]

[^06-01-17]: Alice Evans, “How Did East Asia Overtake South Asia?,” *The Great Gender Divergence* (blog), March 13, 2021, [https://&#x200B;www&#x200B;.draliceevans&#x200B;.com&#x200B;/post&#x200B;/how&#x200B;-did&#x200B;-east&#x200B;-asia&#x200B;-overtake&#x200B;-south&#x200B;-asia](https://www.draliceevans.com/post/how-did-east-asia-overtake-south-asia).

There were several notable women active in the nascent Japanese labor
movement, including Kikue Yamakawa. However, the fact that the vast
majority of mill workers were girls and women likely made the Japanese
factory workforce easier to control than the male factory workers on
the Indian subcontinent, where teenaged girls and adult women were
isolated and kept out of the labor market. This improved the ability
of Japanese factory owners to repress worker unrest, which helped
drive the rapid and profitable growth of the Japanese manufacturing
sector.[^06-01-18]

[^06-01-18]: Pseudoerasmus \[pseud.\], “Labour Repression and the Indo-Japanese Divergence,” *Pseudoerasmus* (blog), October 2, 2017, [https://&#x200B;pseudoerasmus&#x200B;.com&#x200B;/2017&#x200B;/10&#x200B;/02&#x200B;/ijd](https://pseudoerasmus.com/2017/10/02/ijd).

Just as poor families were willing to risk sending their daughters to
the mills, so more affluent families were willing to risk sending
their daughters to schools, even to relatively distant schools where
the girls lived beyond the immediate reach of parental control. If the
Japanese patriarchal system had been more strict, then the very
premise of Yoshiya’s *Yaneura no nishojo* would have been absurd: the
virgins of the title would have had their virginity assured by their
fathers never allowing them to leave the family home.

In the end, the girls and women in the mills helped make the lives of
both the readers and writers of Class S fiction possible. Japan’s
rapid industrialization and its rise as an exporting nation led to
strong economic growth, urbanization, and the emergence of a thriving
middle class. Middle-class families became affluent enough to educate
their girls through middle and high school, and allow the girls
themselves to afford subscriptions to the girls’ magazines in which
the Class S genre began.

A growing Japanese economy also drove the creation of an urban service
sector in which women could find employment as office workers. The
*shōjo* who read girls’ magazines during her teen years gave rise to
the stereotypical “modern girl” (*moga*) who set fashion trends and
read women’s magazines in her twenties. Behind the *shōjo* and the
*moga* stood the *kōjo*, so that when Nobuko Yoshiya’s writings in
those magazines made her one of the richest women in Japan, she stood
at the top of an economic pyramid built in large part by the
anonymous, ill-compensated, and often back-breaking labor of hundreds
of thousands of Japanese factory girls.

If the lives of Japanese factory girls found scant representation in
the Class S stories of the *shōjo* magazines, where if anywhere can we
find their voices, their dreams, their hopes and fears?

Mill owners themselves commissioned reading materials for the workers,
short textbooks glossed with *furigana* for those girls who were at
least semi-literate. They celebrated the factory girl as the backbone
of Japan, who through her hard work would make Japan a great nation
and fulfill her filial duty to her parents, her employer (who
supposedly treasured her more than his own child), and the
emperor.[^06-01-19]

[^06-01-19]: Tsurumi, *Factory Girls*, 93--96. A later example of such propaganda is Akira Kurosawa’s 1944 film *The Most Beautiful*. In the film, we get glimpses of the lives of the girls who work in an optics factory: the work they do, the reverses they suffer, the friendships they enter into, and the support they provide each other. But in the end, the film focuses on how the girls gladly work their fingers to the bone and ruin their health in service to the imperial ambitions of the Japanese state. *The Most Beautiful*, directed by Akira Kurosawa, in *Eclipse Series 23: The First Films of Akira Kurosawa (Sanshiro Sugata / The Most Beautiful / Sanshiro Sugata, Part Two / The Men Who Tread on the Tiger’s Tail)* (1944; New York: Criterion Collection, 2010), 1 hr., 25 min., DVD.

Mill owners also commissioned songs for the workers to sing while at
their work: “Thread is the treasure of the empire! / More than a
hundred million yen worth of exports, / What can be better than silk
thread!”[^06-01-20] But the factory girls preferred their own songs.
If our young girl were as gifted with words as Nobuko Yoshiya, she
might have been one of the many anonymous creators of the workers’
songs that have come down to us.

[^06-01-20]: Tsurumi, *Factory Girls*, 93.

Sometimes they sang of the hardships of life in the mill: “Factory
work is prison work, / All it lacks are iron chains.” Sometimes they
sang of how they were viewed by others, as in the song with which this
chapter begins, or defended themselves against those who looked down
on them: “Don’t sneer at us / Calling us ‘Factory girls, factory
girls’! / Factory girls are / Treasure chests for the company.” And
sometimes they looked to the future, to a time when they would be free
of the mill: “Looking out at the growing darkness / I recall the
evening bell at Takayama Temple. / When my term expires I’ll cross the
Nomugi Pass / And they’ll say, ‘Our daughter is home!’”[^06-01-21]

[^06-01-21]: Tsurumi, *Factory Girls*, 98, 97, 101.

I am far in time and space from the lives of the factory girls who
sang these songs. But in writing about the Class S stories of
yesteryear and the yuri manga of today and tomorrow, I would be remiss
if I did not take the opportunity to honor the memory of the girls and
women whose work helped make that literature possible, but who rarely
if ever grace its pages.
