---
title: "Emotional Incontinence"
---

## Emotional Incontinence

In my first reading of *Sweet Blue Flowers*, I couldn’t make sense of
Fumi’s strange dream in chapter 50, in which young Akira urinates down
her legs and young Fumi laps it up (*SBF*, 4:289--92).
{:first}

But then I thought, what is incontinence? It’s an involuntary release
of bodily fluids due to a lack of control over one’s own body. In a
social context, this lack of control translates into public shame and
embarrassment---embarrassment because one has broken social norms
about proper social behavior (i.e., confining urination to appropriate
spaces and times), and shame because others judge one for that
transgression.

Tears are another example of an involuntary release of bodily fluids,
in this case, due to a lack of control over one’s own emotions. Crying
is a much milder social transgression than public
incontinence. However, in a society focused on social harmony and
maintaining a public “face” that is different and more restrained than
one’s private “face,” openly crying in public could presumably be
considered a transgression as well. If so, it too could occasion shame
and embarrassment.

This brings us to Fumi and Akira. After Akira meets her childhood
friend Fumi on the train without knowing who she is, and their two
mothers arrange a reunion, Akira’s mother asks her, “Do you remember
Fumi?” (*SBF*, 1:28). In the subsequent flashback, young Fumi can’t
make it to the bathroom in time, and her classmates call Akira in from
another elementary school class to help out (1:29--30). (Note the
implication that this is not the first time that Fumi has done this.)
Akira takes control of the situation and gets Fumi to the school nurse
to get cleaned up and changed (1:31).

In a nutshell, young Fumi is a person who cannot control herself and,
in particular, cannot control her own body. Since elementary school,
Fumi has presumably overcome her childhood incontinence. Still, her
continued and frequent crying is another indicator of her lack of
self-control, this time of her emotions.

Akira, secure in her control of her own body and emotions, can then
step in and help Fumi regain both her physical and emotional
equilibrium. Just as she takes control when young Fumi urinates on
herself, she urges young Fumi to stop crying (*SBF*, 3:318). As a
teenager, she continues to be the level-headed person in their
relationship, drying Fumi’s tears just as she once helped her dry her
underpants.

But Akira’s self-control is accompanied by her seeming inability to
feel certain emotions herself. In particular, she does not (cannot?)
feel a strong romantic or sexual attraction towards anyone, including
Fumi. She also does not (cannot?) cry openly. It’s not that Akira is
unemotional in general: throughout the manga, we see her be
alternately happy, angry, embarrassed, nervous, puzzled, and
distraught. But through most of the manga, we never see her cry in the
presence of someone else.[^05-06-01]

[^05-06-01]: Akira does cry once while dreaming of young Fumi and herself (*SBF*, 3:319). But those are private tears and not shed while awake.

Being in love and crying are connected in Akira’s mind. Thinking to
herself, she wonders, “Will I ever like someone ... and end up in
tears like Fumi? ... Fumi is *always* crying ...” (*SBF*, 2:66). Given
her frequent admonitions to Fumi to not be a “crybaby,” it’s no
surprise that Akira might have at least an unconscious bias against
romance as well.

This is, of course, very frustrating to Fumi. She has a powerful
romantic and sexual attraction to Akira, an attraction Akira does not
(and cannot?) feel toward her. From this perspective, I see Fumi’s
dream of Akira’s incontinence as a manifestation of her desire that
Akira not let self-control hold her back from a relationship. Fumi
unconsciously wants Akira to lose control of herself in love, just as
Fumi succumbs to her own emotions (*SBF*, 4:290).

Unlike young Akira, the dreaming Fumi does not see Akira’s
incontinence as an opportunity for her to step in and restore Akira’s
self-control and emotional equilibrium, as Akira had once done for
her. Instead, she responds to Akira’s loss of control in a manner that
seems to welcome it and reads as intensely sexual (*SBF*, 4:291). No
wonder Fumi dreams of Akira shouting “Stop that, Fumi!” and then
covers her face in embarrassment upon waking (4:292).

Beyond Fumi’s dream, the manga has an ongoing theme of Akira and
urination, depicting Akira needing to rush to the bathroom (*SBF*,
1:265, 1:281, 1:374, 2:35, 2:139--40). This occurs so often that it’s
hard to avoid the conclusion that Shimura consciously intended these
scenes to be read metaphorically, perhaps as representing the pressure
of emotions within Akira that she has resisted letting herself
express.

This is capped off by a dream in which Akira imagines herself visiting
Yasuko and Kawasaki in England and urgently needing to use their
bathroom (*SBF*, 4:224--25). This (almost) loss of bladder control has
a parallel loss of emotional control on Akira’s actual trip to
England, as we see Akira’s eyes fill with tears as she talks to Yasuko
about Fumi and their relationship (4:254).[^05-06-02]

[^05-06-02]: Akira’s body betrays her again on the flight from England back to Japan, as she becomes violently ill (*SBF*, 4:262).

The final scene of *Sweet Blue Flowers* touches on the topic of tears
once more. As Fumi and Akira lie in Fumi’s bedroom sometime after
Akira’s confession, one of them muses to herself about wanting to stay
awake and talk all night and concludes, “And I felt like crying”
(*SBF*, 4:365). Shimura does not make explicit whose thoughts these
are, an ambiguity that may be deliberate: as the story ends, perhaps
Akira, as well as Fumi, is willing and able to lose herself in tears,
finally giving in to her overflowing emotions.
