---
title: "Rezubian"
---

## *Rezubian*

It’s a truism that the protagonists in typical yuri works are
romantically and (in more mature works) sexually attracted to other
women but don’t necessarily think of themselves as lesbians: “lesbian
content without lesbian identity,” to quote Erica Friedman’s famous
definition of yuri.[^04-10-01] Fumi in *Sweet Blue Flowers* is no
exception to this rule; the closest she comes to breaking it is
telling her Matsuoka friends that she is indeed “that type of girl.”
And yet, in volume 3, we find a girl who proudly and defiantly
declares herself to be a lesbian (*SBF*, 3:257). What’s going on here,
and how does it fit into the larger framework of *Sweet Blue Flowers*?
{:.first}

[^04-10-01]: Friedman, “Is Yuri Queer?”

It’s noteworthy that this incident occurs in one of the “Little Women”
side stories that Takako Shimura sprinkles throughout the various
volumes of *Sweet Blue Flowers*. Shimura uses these to provide
additional perspectives on the events of the main narrative. In some
cases, they relate past events and provide background information on
supporting characters, as in the stories featuring Hinako, Orie, and
the Sugimoto sisters and their mother (*SBF*, 1:379--81, 2:2--3,
2:159--74, 2:176--77, 2:347--54, 3:165--72).

In other cases, the “Little Women” stories depict events that appear
to be roughly contemporaneous with the main storyline but are not
directly connected to its events or characters. Instead, these
function as commentary on the manga’s themes (*SBF*, 3:353--56,
4:173--76).

The scene between self-proclaimed lesbian Maeda and her friend
Nakajima is an example of the latter. Judging from their school
uniforms, they appear to be students at Fujigaya Women’s Academy. So
the first and simplest function of the story is to let us know that
there are more students at Fujigaya who are attracted to other
girls.

Indeed this is almost a mathematical certainty. Fujigaya probably has
several hundred students in total, and the three high school grades
likely have at least a couple of hundred, assuming two or three
classes of students for each year and about twenty to thirty students
per class.

In a 2019 Japanese government survey, 0.7 percent of those surveyed
identified themselves as “lesbian, gay, or homosexual” (as compared to
3.3 percent of respondents identifying as one or more of lesbian, gay,
bisexual, transgender, or asexual).[^04-10-02] In another more recent
survey (conducted by the Dentsu advertising agency), 1.33 percent of
those surveyed identified themselves as lesbians (compared to 8.9
percent considering themselves members of a “sexual
minority”).[^04-10-03]

[^04-10-02]: Daiki Hiramori and Saori Kamano, “Asking about Sexual Orientation and Gender Identity in Social Surveys in Japan: Findings from the Osaka City Residents’ Survey and Related Preparatory Studies,” *Journal of Population Problems* 76, no. 4 (December 2020), 443--66, [https://&#x200B;www&#x200B;.ipss&#x200B;.go&#x200B;.jp&#x200B;/syoushika&#x200B;/bunken&#x200B;/data&#x200B;/pdf&#x200B;/20760402&#x200B;.pdf](https://www.ipss.go.jp/syoushika/bunken/data/pdf/20760402.pdf). This paper also has some interesting discussions regarding the difficulty of conducting surveys about sexual orientation and gender identity in a Japanese and Asian context.

[^04-10-03]: Dentsu, “First time poll categorizes straight respondents; analyzes their knowledge, awareness of LGBTQ+ matters---Most ‘knowledgeable but unconcerned’; do not think LGBTQ+ issues relate to them---,” April 8, 2021, [https://&#x200B;www&#x200B;.dentsu&#x200B;.co&#x200B;.jp&#x200B;/en&#x200B;/news&#x200B;/release&#x200B;/2021&#x200B;/0408&#x200B;-010371&#x200B;.html](https://www.dentsu.co.jp/en/news/release/2021/0408-010371.html).

Given these figures, we can conclude that in all probability, at least
a few Fujigaya high school students would be considered lesbians by
any reasonable definition---perhaps more than one could count on the
fingers of one hand. Thus Hinako and Orie were likely not alone in
their class, nor Maeda in hers.

What other functions does Maeda’s story serve? Perhaps the most
obvious is to contrast the fictional world of S relationships found in
many modern yuri works to the real world as young Japanese lesbians
might experience it. In the world of yuri fiction (in which *Sweet
Blue Flowers* exists as both homage and critique), students in girls’
schools swoon over imagined pairings (as with Yasuko and Kawasaki in
*Wuthering Heights*). Meanwhile, their parents react with indifference
or even enthusiasm---recall Akira’s mother’s reaction after Akira’s
first day at Fujigaya: “Soon you’ll bring home a girlfriend!” (*SBF*,
1:27).

But in Maeda’s world, she gets called a “lesbo” (the English version’s
translation of *rezu*, a pejorative shortening of *rezubian*) and
“ugly” (translating *busu*, which refers to an ugly woman
specifically). Fortunately, she manages not to let it bother her, but
this is a classic example of homophobic bullying.

One could quibble at Shimura juxtaposing Maeda’s treatment with the
yuri-inflected goings-on elsewhere at Fujigaya. It seems tonally
jarring and inconsistent that both could exist in the same school at
the same time. But as I understand it, this is not so different from
the situation in Japan before and during the time that Shimura was
writing *Sweet Blue Flowers*, and to a large extent, this is still true
today.

The distinction here is between lesbians in manga, anime, and other
forms of entertainment and lesbians in real life. Lesbians are
perfectly acceptable in the context of entertainment, a world in which
the unusual and nonconforming are a source of titillation and
intrigue. Such entertainment can encompass anything from pornography
featuring lesbians to “pure yuri” tales of shy and innocent
schoolgirls. (Indeed, *Sweet Blue Flowers* itself is an example of
this---recall its publication in a magazine titled *Manga Erotics F*.)

But in Japan, the presence and acceptance of lesbians (or LGBTQ people
in general) in entertainment did not carry over to acknowledging and
accepting their presence in society. Western ideas of homosexuality as
abnormal and depraved influenced Japan in the early twentieth century,
but the root problem seems to be different. Lesbians (and LGBTQ
individuals more generally) who choose to live as such do not conform
to the template of the Japanese family that was institutionalized and
propagandized beginning in the Meiji era, and still exerts great
influence today.

In that template, the life history of a woman is to attend high school
and perhaps university and possibly work full-time at a non-career
track job for a few years. She will then leave employment to get
married, have children, and devote her life to them. Once her children
are grown, she will split her time between part-time work and caring
for parents. It is a template for life entirely separate from that
seen as the ideal for a man: He will attend school through university,
get a corporate job after graduation, and at some point acquire a wife
who will keep house and mind children. Meanwhile, he will devote his
life to working and socializing within a predominantly male
environment.

This rigid conception of women’s lives impacts lesbians in at least
two ways. First, it limits their long-term employment prospects and
therefore their ability to support themselves, much less a partner as
well. “In small, medium or large companies there is the assumption
that everyone participates in the same kind of kinship relations.”
Thus lesbians are seen simply as unmarried women who have not yet
found a man.[^04-10-04] They may be able to follow the employment path
of heterosexual single women for a few years but not forever, as their
status as lesbians renders them incompatible with Japanese corporate
expectations for career employees.

[^04-10-04]: Sharon Chalmers, *Emerging Lesbian Voices from Japan* (London: RoutledgeCurzon, 2002), 81.

Second, it means that there is no place for lesbians in the Japanese
family as traditionally conceived. As one lesbian noted, “In Japan
there is a father and a mother and children, and no one can see family
in any other way. Anything else isn’t really family, but only a
distortion, ...”[^04-10-05]

[^04-10-05]: Chalmers, *Emerging Lesbian Voices from Japan*, 81, quoting interviewee Chiho.

There are potential tweaks to this scenario: for example, the man may
also have a mistress and have illegitimate children by her, or his
wife’s family may have officially adopted him as a *mukoyōshi* to
ensure a male heir. However, the basic template remains unchallenged
and is provided official support via the *koseki* system of household
registries (as discussed in a previous chapter).

In this scheme, a lesbian’s existence is inherently incompatible with
the idea of “family.” As a woman, she cannot take on the role of
household head traditionally reserved to men. As a woman who loves
women, she cannot take on the role of wife to the (male) household
head and bearer of her children.

So even though in theory, two lesbians could marry and have either or
both of them give birth to children, many would not consider this a
“family.” Moreover, as discussed above, such a not-family (from the
Japanese perspective) would also be financially nonviable since the
idea of a lesbian as a long-term breadwinner in support of her wife
and children runs up against lesbians’ incompatibility with the
conventional corporate employment narrative.

The approaches societies can take concerning those who don’t conform
to societal norms can range from singling out and condemning them to
simply ignoring them. Many people in Japan seem to have taken the
latter path, rendering Japanese lesbians invisible through what
appears to be a willful refusal to recognize their existence as
lesbians.

For example, Saori Kamano relates her encounters with Japanese
graduate students in 1987 and at the turn of the century. In both
cases, they confidently proclaimed, “There are no lesbians in
Japan.”[^04-10-06] An example in *Sweet Blue Flowers* itself is the
experience of Hinako after she tells her mother about her relationship
with Orie: her mother still works with Hinako’s aunt to try to arrange
a meeting with a man (*SBF*, 4:209). Orie has a similar experience,
with her parents refusing to discuss the issue (4:211).

[^04-10-06]: Saori Kamano, “Entering the Lesbian World in Japan: Debut Stories,” *Journal of Lesbian Studies* 9, no. 1/2 (2005), 12--13, [https://&#x200B;doi&#x200B;.org&#x200B;/10&#x200B;.1300&#x200B;/J155v09n01&#x200B;_02](https://doi.org/10.1300/J155v09n01_02).

Among other things, this imposed invisibility has in the past
inhibited many Japanese lesbians from actually thinking of themselves
as lesbians: it’s hard to conceive of oneself as a member of a
distinct group if you don’t know of anyone else like you. In today’s
world, that knowledge might be just an Internet search away. (In fact,
that might be where Maeda and her persecutors learned about lesbians.)

However, *Sweet Blue Flowers* was created in a world where the
Internet was not as ubiquitous as it is now. And, in any case, it’s
more dramatically effective for Fumi to explore her identity in
conversations with an actual adult lesbian, namely Hinako. The result
is that although she doesn’t yet apply the term “*rezubian*” to
herself, she can come out to her friends and acknowledge that she’s
“that type of girl.”
