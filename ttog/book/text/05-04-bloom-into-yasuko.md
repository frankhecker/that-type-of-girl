---
title: "Bloom into Yasuko"
---

## Bloom into Yasuko

When last I discussed Yasuko Sugimoto, she had graduated from Matsuoka
Girls’ High School and left Japan to study abroad. We might think that
her part in the story had come to an end, but she makes a reappearance
in volumes 3 and 4. Now a university student, she appears to be almost
a new person, with her long hair and stylish outfit---“amazingly
elegant” (*SBF*, 4:240).[^05-04-01]
{:first}

[^05-04-01]: When Akira dreams of a future meeting with Yasuko in England, in the dream Yasuko appears as she did at Matsuoka, emphasizing the contrast between Yasuko as she was and as she is now (*SBF*, 4:224).

One could see this as the result of Yasuko suppressing her “masculine
side” and trying in her newfound femininity to conform to prevailing
heterosexual norms. However, I think this is a misreading of the
situation. As Yasuko herself explained (in interior monologue), her
past “princely” demeanor was an act, something she seemingly put on in
an attempt to conform to people’s expectations of her and emulate her
sister Kazusa’s “tough personality” (*SBF*, 2:110--12).

With Mr. Kagami’s marriage to Kazusa, Yasuko’s emotional breakdown
upon meeting Mr. Kagami after *Wuthering Heights*, and her breakup
with Fumi, Yasuko’s attempt to play the prince was exposed as a
fraud. One might then ask: if Yasuko is not a “girl prince,” what
is she?

The answer is simple: she is a person who is much more comfortable in
her own skin and more able to express her true personality now that
she no longer labors under the strain of playing a role not suited to
her. She looks comfortable in her new outfits, much more so than in
the men’s shirt, tie, and pants she wore to Kazusa’s wedding, looking
for all the world like a high-school *otokoyaku* (*SBF*,
2:72--73). She’s able to laugh at herself and her past foibles,
jokingly telling Shiho not to fall in love with a teacher (4:335). And
she appears happy in her life in England, shopping with Kawasaki for
presents for Kazusa and her baby (4:22--23).

Yasuko’s maturity shows itself most clearly in her interactions with
Akira when Akira visits Yasuko and Kawasaki while on the Fujigaya
class trip to England. After she broke up with Fumi, Yasuko confided
in Akira regarding her crush on Mr. Kagami and her behavior towards
Fumi (*SBF*, 1:347--49). Yasuko now returns the favor, listening to
Akira express her concerns about her relationship with Fumi and
offering her kind words and reassurances (4:252--54, 4:266--67).

If Yasuko was at times a “bad *senpai*” in her dealings with Fumi, one
could equally well call her a “good *senpai*” in her relationship with
Akira. But characterizing their relationship according to a
*senpai*/*kohai* framing seems unnecessary to me. Both in their past
conversations and in their current ones, Yasuko and Akira seem to be
interacting as relative equals, speaking honestly to each other
without regard for social conventions.

There’s one more question worth asking about the “new Yasuko.” To echo
Shinako’s rude questions when Fumi visited Yasuko (*SBF*, 1:309): Is
Yasuko a lesbian? Is she bisexual?

Shimura (very deliberately, I think) leaves this question
unanswered. Certainly, Yasuko’s former schoolmates expect that Yasuko
and Kawasaki, the Heathcliff and Catherine of *Wuthering Heights* and
now roommates in England, are also a couple. See, for example, the
newspaper article speculating on their relationship and Kyoko’s
comments to Yasuko (*SBF*, 2:143, 3:188).

Readers of *Sweet Blue Flowers* could also be forgiven for thinking
this. But Shimura undercuts this expectation by showing that Kawasaki
has acquired an English boyfriend---now occupied with ministering to
her while she’s sick with appendicitis (*SBF*, 4:336, 4:338).

So, where does that leave Yasuko? She is in an envious position:
intelligent, attractive, and wealthy, she now adds to those attributes
a newfound maturity. Yasuko’s parents do not seem to have tried to
force her sisters into particular life choices. Given that she’s the
youngest daughter, it’s probably even less likely that they would do
so to Yasuko. The box in which Yasuko was formerly trapped was one
that she had constructed herself. Now that she has escaped its
confines, she is free to simply be herself.
