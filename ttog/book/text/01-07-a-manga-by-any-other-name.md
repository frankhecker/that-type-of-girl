---
title: "A Manga by Any Other Name"
---

## A Manga by Any Other Name

Why is *Sweet Blue Flowers* named “Sweet Blue Flowers”? We can try to
answer this by starting at the end of the title and working backward.
{:first}

As mentioned previously, *Sweet Blue Flowers* was originally published
in Japanese as *Aoi hana*. “Hana” means “flower”---or “flowers,”
Japanese not having plural forms as English and other languages
do. Shimura’s name-checking of Nobuko Yoshiya and her titling her
first chapter “Flower Story” indicates that “hana” in the title
references *Hana monogatari* or *Flower Tales*, the short story
collection previously discussed.

Next, “aoi.” This means “blue,” so we have “blue flower” or “blue
flowers.” Is there a significance to the color blue in this context?
Unfortunately, I can’t find a list in English of the titles of the
stories in *Hana monogatari*, so I don’t know if this is a reference
to a specific story of Yoshiya’s.

Is the reference to something else? The most well-known reference to a
blue flower in Western literature is from the unfinished novel
*Heinrich von Ofterdingen* by the eighteenth-century German Romantic
poet and philosopher Novalis (Georg Philipp Friedrich Freiherr von
Hardenberg), translated into English as *Henry of Ofterdingen*
and---more germane to this question---translated into Japanese as *Aoi
hana*, the same name as Shimura’s manga. It begins with young Henry
lying in bed, beguiled by an image from a tale told by a stranger: “I
long to behold the blue flower. It is constantly in my mind, and I can
think and compose of nothing else.”[^01-07-01]

[^01-07-01]: Novalis, *Henry of Ofterdingen: A Romance*, trans. John Owen (Cambridge, MA: Cambridge Press, 1842; Project Gutenberg, 2010), chap. 1, [https://&#x200B;gutenberg&#x200B;.org&#x200B;/ebooks&#x200B;/31873](https://gutenberg.org/ebooks/31873).

The blue flower (*blaue Blume*) of Novalis has connotations both
sacred and profane. On the one hand, as Wikipedia puts it, it “stands
for desire, love, and the metaphysical striving for the infinite and
unreachable. It symbolizes hope and the beauty of things.” It later
became a symbol for the entire German Romantic movement.

But the blue flower also has a worldly incarnation in the person of
the young girl Matilda, the fictional counterpart of Sophie von
Kühn. Novalis met Sophie when she was twelve and he twenty-two, and
secretly became engaged to her just before her thirteenth
birthday. The marriage never took place: Sophie died of illness just
over two years later, shortly after her fifteenth birthday.

In the manga, Fumi compares her budding romance with Akira to “a small
flower. A very small flower. ... And you might not know what to do
with a flower like that” (*SBF*, 1:187). Shimura appears to have an
affinity for German novelists and poets (see the appendix on the
titles of the manga chapters), so it’s quite likely that she was
thinking of the “blaue Blume” of the German Romantics, and of
Novalis’s novel in particular, when she titled her manga *Aoi
hana*. However, to my mind the age and status differences found in the
relationship between Henry and Matilda (and between Novalis and
Sophie) are rejected in *Sweet Blue Flowers*.

What about the word “Sweet”? When *Aoi hana* was translated into
French and Spanish, its title was straightforwardly translated as
*Fleurs bleues*[^01-07-02] and *Flores azules*[^01-07-03]
respectively. So why is the English title “Sweet Blue Flowers,” and not
just “Blue Flowers”?

[^01-07-02]: Takako Shimura, *Fleurs bleues*, trans. Satoko Inaba and Margot Maillac, 8 vols. (Paris: Kazé, 2009--15).

[^01-07-03]: Takako Shimura, *Flores azules*, trans. Ayako Koike, 8 vols. (Colombres, Spain: Milky Way Ediciones, 2015--16).

I don’t know for sure, but I *can* say that “Sweet Blue Flowers” was
used as an alternate English title on the Japanese edition of *Aoi
hana* when the manga was published as eight collected volumes. (I
believe this alternate title was also used when the manga began
serialization in *Manga Erotics F*, but I have not found any
confirmation of this.) This title was then carried over when *Aoi
hana* was first published in a (partial) English translation and used
again for the VIZ Media edition.

Finally, what kind of blue flowers might the title refer to? Blue
flowers are somewhat uncommon in nature, one of the reasons for their
symbolic resonance. (For example, there are no naturally occurring blue
roses, and attempts to create them via genetic engineering have been
only partially successful.)

Shimura may have intended to leave the identity of the blue flowers to
our imaginations: in the color pages of the manga there are only two
instances I could find of blue flowers, a seven-petal blue blossom in
the character profile at the beginning of volume 1, and a four-petal
blossom on the back cover of volume 1 of the Japanese edition.

Therefore we’re free to amuse ourselves by identifying a type of
flower to go with the title. A lily would be an obvious choice since
“yuri” is the Japanese word for “lily,” and lilies are associated with
the Class S genre. However, there are no true lilies (of genus
*Lilium*) that are blue. This is perhaps a tell on Shimura’s part:
that *Sweet Blue Flowers* pays homage to but ultimately breaks from
the traditional narratives of Class S and yuri.

Another possible choice of flower is wisteria, which has purple
blossoms that often appear bluish in color. The kanji for “wisteria”
appears in both “Fujigaya” and “Fujisaki,” the city next to Kamakura
where Fujigaya appears to be located. “Wisteria” is also the name of
Akira’s class in her first year at Fujigaya (*SBF*, 1:26). But *Sweet
Blue Flowers* is Fumi’s story just as much as it is Akira’s, and Fumi
attends a different school.

In the end, the identity of the blue flower remains mysterious, at
least to readers in English. Though I have chosen an illustration of
blue flowers to adorn the cover of this book, I’m uncertain which
species it’s intended to depict. I prefer to think of those flowers,
and *Sweet Blue Flowers*, as being uniquely themselves.
