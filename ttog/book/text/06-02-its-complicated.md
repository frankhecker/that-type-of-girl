---
title: "It’s... Complicated"
---

## It’s... Complicated

In its previews of seasonal anime series, the Anime Feminist website
assigns shows to various categories: “Feminist Potential,” “Yellow
Flags,” and so on.[^06-02-01] One of these is “It’s... Complicated.”
An anime in this category “seem\[s\] to be addressing progressive
ideas or themes,” but also “may be biting off more than it can chew,”
or even “\[be\] in danger of fumbling its chosen themes.”
{:first}

[^06-02-01]: Anime Feminist, “2022 Winter Premiere Digest,” January 14, 2022, [https://&#x200B;www&#x200B;.animefeminist&#x200B;.com&#x200B;/2022-winter-premiere-digest/](https://www.animefeminist.com/2022-winter-premiere-digest/).

It’s undoubtedly true that, as I’ve noted in previous chapters, *Sweet
Blue Flowers* features a wider variety of characters and plot elements
than one might expect in a simple “schoolgirl yuri” story. It’s also
true that some of these characters and plot elements aren’t
well-integrated into the story and sometimes arguably detract from it
(for example, the subplot involving Akira and her brother).

But in summing up *Sweet Blue Flowers*, there are also two other ways
in which some might put it into the “It’s... Complicated” category,
having to do with the author and the fate of her characters.

In recent years, many people have promoted works in which “the
protagonist and the author share a marginalized identity,” conceived
as a way to “center the voices that matter most.”[^06-02-02] Although
this “own voices” movement and similar initiatives arose in the United
States in the context of young adult (YA) literature, others have
applied its principles in other countries and contexts. Thus, for
example, Erica Friedman has promoted yuri manga and related works
created by Japanese artists who are publicly-visible
lesbians.[^06-02-03]

[^06-02-02]: Corinne Duyvis, “#OwnVoices,” accessed July 1, 2022, [https://&#x200B;www&#x200B;.corinneduyvis&#x200B;.net/ownvoices/](https://www.corinneduyvis.net/ownvoices/).

[^06-02-03]: Erica Friedman, “‘Own Voices’: Are There Queer Creators Creating Yuri?,” YouTube video, 15:23, December 13, 2020, [https://&#x200B;www&#x200B;.youtube&#x200B;.com/watch?v=eqZeCMWDt08](https://www.youtube.com/watch?v=eqZeCMWDt08).

These and similar efforts focus on positively highlighting
marginalized creators. However, human nature being what it is, they
have also become controversial, especially as it concerns works that
feature marginalized characters but are created by authors who are not
themselves perceived as marginalized.

This issue of “who can write about whom” interacts with broader
political questions around who can speak for, represent, and advocate
for particular marginalized groups. In Japan, in particular, activists
for sexual minority groups have emphasized the importance of the
“*tōjisha*” (“the person \[directly\] concerned”), a term that
originated in the context of legal and administrative proceedings but
came to be applied to groups of people who were discriminated against
in various ways.[^06-02-04]

[^06-02-04]: Mark McLelland, “The role of the ‘tōjisha’ in current debates about sexual minority rights in Japan,” 2009, 4--7, [https://&#x200B;ro&#x200B;.uow&#x200B;.edu&#x200B;.au&#x200B;/artspapers&#x200B;/206](https://ro.uow.edu.au/artspapers/206).

In contrast, *hi-tōjisha* are those who are not directly concerned,
i.e., who are not the subject of discrimination. In this context,
*tōjisha* are perceived to be the best, if not the only, people to
speak about problems affecting their groups, with the opinions of
*hi-tōjisha* seen as less relevant or even invalid.

The distinction between *tōjisha* and *hi-tōjisha* has caused
controversy in other areas beyond the political, including in the
world of manga. One example is the “boy’s love” or “BL” genre, a genre
whose creators and fanbase are perceived as being composed almost
exclusively of heterosexual women. “Some gay *tōjisha* have criticized
the genre’s producers and fans for trading in ‘irresponsible’
representations of gay men and of being blind to the genre’s potential
negative effects upon ‘real gays.’”[^06-02-05]

[^06-02-05]: McLelland, “The role of the ‘tojisha’,” 12--13.

Takako Shimura herself was apparently subjected to similar criticism
regarding *Wandering Son*, a manga featuring transgender
children. Shimura was not only seen as embarking on the manga not
knowing much about transgender people (“she was really basically
winging this for more than half the series”). Some questioned her even
writing about transgender characters in the first place: “amongst
trans people in Japan, there was this feeling: ‘Oh, great. We’re
finally being represented. But ... we’re being represented by someone
who is not one of us.’”[^06-02-06]

[^06-02-06]: Vrai Kaiser, Jacob Chapman, Cayla Coats, and Rachel Thorn, “Chatty AF 21: *Wandering Son* Retrospective (with Transcript),” Anime Feminist, September 3, 2017, [https://&#x200B;www&#x200B;.animefeminist&#x200B;.com&#x200B;/podcast-chatty-af-21-wandering-son-retrospective/](https://www.animefeminist.com/podcast-chatty-af-21-wandering-son-retrospective/).

It’s worth noting that, for the most part, similar criticisms don’t
seem to have been levied against artists working in the yuri genre,
perhaps because the genre has always had creators of different genders
and a more diverse fanbase. The emphasis seems to be more on promoting
positive examples of LGBTQ people writing yuri works (as Erica
Friedman has done) than on “gatekeeping” who should not be writing
them.

Putting aside any controversy about Shimura’s authorship of *Sweet
Blue Flowers*, what about issues of “representation” within the manga
itself? *Sweet Blue Flowers* differs from many yuri works in having a
main character (Fumi) who is clearly and unequivocally a lesbian, in
the sense that she is attracted only to women and is aware that she’s
always been that way: “I fall for girls so easily. It’s the only way
I’ve ever liked a girl” (*SBF*, 3:244). The same could be said of
Hinako and Orie, not to mention Maeda, a character who explicitly
refers to herself as a lesbian (3:357).

This distinguishes them from characters in other yuri works whose
attraction to each other appears to be contingent and situational:
they begin their relationships through a contrived plot device, they
question their attraction to each other (“but we’re girls!”), and the
attraction they do feel seems to be limited to one person, as opposed
to being a generalized sexual orientation.

As it happens, there are also characters in *Sweet Blue Flowers* whose
motivations and orientations can be questioned in similar ways,
including Kyoko, Yasuko, and even Akira. And unlike characters in
typical yuri works, they do not end the manga wholeheartedly committed
to a relationship with another woman: Kyoko marries Ko, and Yasuko’s
orientation is (I think) deliberately left ambiguous. And although
Akira does confess and enter into a renewed relationship with Fumi,
it’s implied that many of Akira’s issues around romance and
(especially) sex remain unresolved.

Many manga and anime have been accused of “yuri-baiting,” hinting at
an attraction between girls and leading the audience to expect that
they’ll become a couple, only to undercut that expectation in one way
or another. Although *Sweet Blue Flowers* is unquestionably a yuri
work, the fate of characters like Kyoko and Yasuko might lead some to
conclude that Takako Shimura is doing something similar: if Shimura is
not misleading the reader, she is at least limiting the amount of
lesbian representation featured in the book and thereby disappointing
readers who would like to see more of it.

Is this a fair criticism? In a review of another of Shimura’s works,
blogger Jaime offers a counter-argument: “what others view as flaws, I
actually see as important qualities in her work. ... that her stories
are messy, her characters are messy, that these series don’t fit into
neat narratives about what the LGBTQ+ community wants as
representation.”[^06-02-07]

[^06-02-07]: Jaime, review of *Even Though We’re Adults*, vol. 4, by Takako Shimura, *Yuri Stargirl* (blog), April 23, 2022, [https://&#x200B;www&#x200B;.yuristargirl&#x200B;.com/2022/04/even-though-were-adults-volume-4-manga.html](https://www.yuristargirl.com/2022/04/even-though-were-adults-volume-4-manga.html).

From this point of view *Sweet Blue Flowers* is just as much about
Akira as it is about Fumi, if not more so. Jaime compares Fumi to “the
‘straight men’ in a comedy movie, they are who the funny people need
to make the jokes land.” Fumi is “the straight-forward LGBTQ+
representation that the love interests and side characters play off
against to show just how messy real people in the real world
are.”[^06-02-08]

[^06-02-08]: Jaime, review of *Even Though We’re Adults*, vol. 4.

In the introduction to this book, I wrote that “\[Kyoko, Akira, and
Fumi\] together can be thought of as representing three different
‘eras’ of yuri”: Kyoko the past, Akira the present, and Fumi the
future. But they can also be seen as representing three different
paths real-life women might take if they find themselves attracted to
other women.

Some, like Kyoko, might leave behind such desires and enter into
heterosexual relationships and eventual marriage to men. Others, like
Fumi, might commit to living as women who love women, secure in their
identity as lesbians, and willing to acknowledge that identity to
friends, family, and (for some) the general public. Others, like
Akira, may have relationships with women but may not be willing or
able to self-identify as lesbians, perhaps because of fear of what
others might think (like Akira, at times) or because (also like Akira,
at times) they remain in a state of questioning who they are and what
they desire.

If *Sweet Blue Flowers* had continued beyond its last chapter, it
would have been interesting to see how Akira and Fumi’s relationship
evolved during their adult life: whether it would have held together,
fallen apart, or cycled between periods of closeness and commitment
and periods of frustration and miscommunication. But Takako Shimura
did not give us that manga, and I think rightly so: although they end
their journey as adults, *Sweet Blue Flowers* is at its heart a tale
of teenagers navigating their way through the “forest of thorns” of
schoolgirl relationships. As noted in the next chapter, Shimura’s
attempt at a genuinely adult yuri manga would have to wait a few
years, as the yuri genre evolved after *Sweet Blue Flowers*.
