---
title: "Shinobu and Ko"
---

## Shinobu and Ko

Despite being nominally an example of “schoolgirl yuri,” *Sweet Blue
Flowers* deviates from many works in that genre in featuring several
male characters. The first to be encountered---and in less than
pleasant circumstances---was Akira Okudaira’s brother, who got kicked
out of his sister’s bed on the second page of volume 1 (*SBF*,
1:8). He spent the remainder of that volume being absurdly
overprotective of his sister, to Akira’s great annoyance, and creepy
in general (1:102--3, 1:109).
{:.first}

In volume 2, Takako Shimura finally gives Akira’s brother a name:
“Shinobu” (*SBF*, 2:182). It’s a name that (if Wikipedia is any guide)
tends to be roughly equally used for both men and women. (For example,
see Shinobu Oshino of Nisio Isin’s *Monogatari* series, an ancient
vampire in the form of an eight-year-old girl.)

Shimura may have meant this as a subtle comment that Shinobu does not
measure up to traditional male standards: at this point in the story,
he’s still living at home, doesn’t appear to have a job or to attend
school (although a subsequent volume does refer to him being a
university student), has no girlfriend (or any hint of having had one
in the past), and seems to have nothing to do in life other than
pestering his sister.

Be that as it may, he now starts shedding his siscon ways and, at
least to some degree, repenting of them. Much of this is not through
his own initiative, but instead comes about as a result of the actions
of others.

This applies, in particular, to Shinobu’s burgeoning relationship with
Fumi’s classmate Miwa Motegi (“Mogi”). As he drives the girls to a
sleepover at Ko Sawanoi’s aunt’s estate (which, of course, he has free
time to do), Mogi finds herself attracted to him, not by anything
Shinobu actively does or says, but just because she thinks “he’s cool”
(*SBF*, 2:8--9, 2:62--63).

Mogi eventually takes the initiative of confessing to Shinobu (*SBF*,
2:133--35). We don’t ever see his reaction or much of any other signs
of his attraction to her; it’s as if he simply passively entered into
the relationship. Whether that’s true or not, it’s all very convenient
from Akira’s point of view because it gets her brother out of her
hair---though she continues to think of him as “super creepy” (2:44).

Shinobu does show at least some self-awareness and potential for
emotional growth. As Ko attempts to initiate him into the mysteries of
golf (that classic sport of upper-middle-class men), he admits to Ko
that he’s overprotective and that Akira will inevitably begin dating
at some point (*SBF*, 2:22--24). (In other words, any siscon dreams he
had are doomed to remain just that.)

However, I won’t go out of my way to praise Shinobu’s emotional growth
or, for that matter, Ko’s suitability as an instructor and role model
for him. I’ll comment on this more in a subsequent chapter, but it’s
not as if Ko, an adult (or nearly so) man fixated on entering into a
relationship with a (now) second-year high school student, is an
examplar of mature masculinity himself. To be uncharitable for a
moment, from the evidence at hand, it seems that mostly what Shinobu
learned from Ko is that it’s OK for an older man to pursue a girl much
younger than himself, as long as she’s not his biological relative.
