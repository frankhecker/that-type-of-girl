---
title: "Stuff like That"
---

## Stuff like That

I’ve come to the end of volume 2, and I still haven’t said much about
what’s going on with Fumi and Akira. Allow me to remedy that, in my
last comments before I wrap up this volume.
{:.first}

Like many schoolgirl yuri relationships, Fumi and Akira’s progresses
relatively slowly, with only three significant developments across the
entire volume.

The first is Fumi’s rejection of Yasuko. Her relationship with Yasuko
firmly cemented Fumi’s identity as a lesbian (to the reader, but I
also think to Fumi herself). At the same time, its ending and her
subsequent recovery left her open to the (re)kindling of her feelings
toward Akira (*SBF*, 1:88--92, 1:135--40, 1:333--35).

That led directly to the next significant development, Fumi’s telling
Akira that she was her “first love” (*SBF*, 2:138--41). Since this was
not an unambiguous confession, it leaves both Akira and Fumi at loose
ends: Akira cannot discern precisely what Fumi’s present-day feelings
are toward herself. She also wonders about both her feelings (or
perceived lack of them) toward Fumi and her more general feelings
about girl-girl relationships.

Meanwhile, Fumi is tortured about what Akira’s feelings are, even to
the point of being led astray, imagining that something is going on
between Akira and Ko Sawanoi. She struggles to regain the
self-confidence she evinced when rejecting Yasuko.

We see this in Fumi’s ultimately unsuccessful attempt to participate
in the Fujugaya drama club’s production of *Rokumeikan* (*SBF*,
2:279--86, 2:308--10). It’s unclear why Fumi actively persists with
this, given her evident discomfort and feelings of inadequacy. She
tells Haruka Ono that she “wanted to show her up” (“her” meaning
Yasuko), but what exactly did Fumi mean by this (2:316)?

Perhaps Fumi wanted to mimic Yasuko’s smooth self-confidence, the
attitude that initially swept Fumi off her feet. Being in the play as
Kiyohara (a supporting role but a major one) would put her in a
similar position to Yasuko in *Wuthering Heights*. With Akira also in
the play, she’d have plenty of opportunities to impress her, just as
Yasuko impressed the girls of Fujigaya the year before.

But, as Fumi ultimately concludes, she’s in no way equipped to step
into Yasuko’s role, much less to “show her up.” “I’m simply not like
Sugimoto. This is just ... the way I am” (*SBF*, 2:308). Fumi walking
away from the play relieves her of that burden. In combination with
her dissatisfaction with her answer to Haruka, it also leads to the
third and final significant development: Fumi’s confession to Akira,
which in essence concludes the volume (2:335). (The final scene at the
Kamakura rail station is inconclusive.)

So where do things stand now, at the end of volume 2? We’ve already
had a kiss (between Yasuko and Fumi) and a confession. In a typical
schoolgirl yuri work, a confession and a kiss would be the climax, and
we’d be ready to call it a success and go on to the next thing. Yet
here we are with another two volumes still to come.

From Fumi’s point of view, things are relatively straightforward: she
knows who she is (a woman who loves women), knows what she wants (both
an emotional and physical relationship with Akira), and has come right
out and asked Akira for just that. The major suspense in subsequent
volumes will be whether Fumi gets what she wants.

As for Akira, she still reads as asexual and (mostly) aromantic, so
whether she’s even able to have that sort of relationship with Fumi is
still in doubt, let alone whether she’s interested in doing so.
However, there are some points we can contemplate.

First, Akira isn’t repulsed by the idea of two girls having a
relationship. Her fear seems to be more that she’ll be embarrassed if
she quizzes Fumi and finds out Fumi’s not interested in her (*SBF*,
2:221). She mainly appears to be somewhat confused and unsure what to
think (2:219--20).

Second, it’s also clear that Akira has no interest in boys or
men. Whatever interest Ko Sawanoi might have had in her, it’s apparent
that she feels nothing towards him---which makes Fumi’s insecurities
about Akira and Ko a bit unrealistic, even if jealousy does make her
temples hurt (*SBF*, 2:156).

Finally, it’s been intimated that girls can arouse some sort of
response in Akira. The first instance was in volume 1 with Yasuko (“I
can see how she’s such a lady-killer.”)  (*SBF*, 1:234). The second
was arguably in volume 2 in her reaction to seeing Fumi and Ryoko
standing next to each other (“... seeing Fumi and Ueda together felt
strange.”) and to Ryoko’s “key-chain” comment (2:276,
2:212--13). Given that all three girls are tall, and both Fumi and
Ryoko have long hair, I don’t think it’s too much to conclude that
Akira may have a “type.” If so, Fumi fits it to a T.

Where will Akira and Fumi go from here? That is the primary question
to be addressed in the final two volumes.
