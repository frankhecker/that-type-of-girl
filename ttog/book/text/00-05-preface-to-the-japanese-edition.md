---
title: "Preface to the Japanese Edition"
style: frontmatter
---

## Preface to the Japanese Edition

After I initially self-published this book, I thought I was done with
it and could turn to my next project. But, as sometimes happens in
life, events didn’t work out that way. Thus I found myself not only
revising and expanding this book but also introducing its Japanese
translation.
{:.first}

In acknowledging the people who helped make this possible, I’ll begin
by remembering Mark McLelland, whose passing in 2020 I learned of only
after publishing the initial version of this book. Professor
McLelland’s writing significantly influenced my thinking; I’ve
referenced yet another of his papers in one of the new chapters of
this book. His colleague James Welker pointed me to remembrances of
his career and helped publicize this book by tweeting about it.

My thanks also go to Erica Friedman, who helped promote the book by
linking to it in her weekly column on her blog *Okazu*. Her book *By
Your Side: The First 100 Years of Yuri Anime and Manga* is a
comprehensive and authoritative take on the history of
yuri. Unfortunately, it came out too late to reference it in mine.

Blogger Jaime of *Yuri Stargirl* inspired me to expand this book. Her
insightful comments on Takako Shimura’s works, including *Sweet Blue
Flowers*, directly led to my writing one of the new chapters, while
another of her comments motivated me to create a second new chapter.

Shimura herself tweeted about the book after I sent her a paperback
copy. Besides leading to additional downloads, her tweets led directly
to the creation of the Japanese edition, as blogger Konsuke of
con-cats.hatenablog.com took advantage of the book’s permissive
licensing to translate the book’s chapters into Japanese and publish
them on his blog.

Konsuke also thoroughly proofread the English edition (which now
benefits from his corrections), added more candidates for inclusion in
the errata appendix, collected material for yet another appendix, and
helped me prepare the Japanese translation for publication in book
form. To him go my sincere thanks, and I hope the thanks of readers in
Japan, for all the work he’s done to make the Japanese edition
possible.
