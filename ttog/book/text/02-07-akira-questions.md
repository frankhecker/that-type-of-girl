---
title: "Akira Questions"
---

## Akira Questions

The acronym LGBTQIA and its more pronounceable near-anagram QUILTBAG
were invented to be more inclusive than the phrase “gay and lesbian”
or the more commonly used acronym LGBT. It’s easy to make fun of
piling letter upon letter in this way, but there’s a serious point
behind these neologisms: the rich variety of people’s identities,
orientations, and emotions can’t be adequately described by a
restricted set of labels. Akira Okudaira is a prime example of that.
{:.first}

Like Fumi Manjome, Akira’s outer impression doesn’t always match her
inner expression. At first glance, she seems to fit the typical
“*genki* girl” stereotype: lively and energetic, self-confident and
enthusiastic.  Her level-headedness and can-do confidence are
indispensable to Fumi, helping her dry her tears and get on with life.

But at the same time, Akira isn’t nearly as confident and assured when
it comes to herself and her feelings, including her feelings
concerning Fumi. When Fumi tells Akira that she’s dating Yasuko
Sugimoto, it throws Akira for a loop: “It’s okay to like a girl,
right? Or am I too dumb to know better?” (*SBF*, 1:144).

At this point in the story, it’s not so much that Akira is questioning
her orientation (one possible meaning of the “Q” in
LGBTQIA)---although she does experience first-hand the “lady-killer”
aura of Yasuko (*SBF*, 1:233--34)---it’s more that she’s questioning
the conventional narrative of heterosexuality. Akira was familiar with
the stories told about girl-girl friendships at a place like Fujigaya
---“that really happens?!” she marvels when Kyoko Ikumi gets a letter
from a junior high girl (1:78)---but now she’s encountering the
emotional reality of it in the person of someone she cares about.

Things are not made easier due to Akira herself being inexperienced in
terms of relationships. She doesn’t seem to have had crushes on anyone
or been on a date of any sort. She welcomes the opportunity to go to
the “singles party” arranged by Kyoko and Ko Sawanoi, but whether due
to her brother’s tagging along or for other reasons, nothing comes of
it (*SBF*, 1:97--98, 1:102--3). The comments from Akira’s brother and
Kyoko about Ko’s interest in her elicit a response of “I’m not ready
for that yet” (1:129--30).

Will Akira ever be ready? It’s too early in the story to say
definitively one way or another. Thus far the evidence hints that
Akira may be asexual or aromantic (two possible meanings of the “A” in
LGBTQIA). Unfortunately, the siscon subplot with her brother clouds
the picture here with its implication of something darker causing
this, instead of this just being the way Akira is. And what about
Fumi, who seems to be beginning to see Akira (or re-see, given the
“first love” memory) as more than just a friend? We’ll have to look to
future volumes for possible answers to these questions.
