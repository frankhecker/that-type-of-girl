---
title: "Men, What Are They Good For?"
---

## Men, What Are They Good For?

Throughout the first three volumes of *Sweet Blue Flowers*, we
encounter adult men and male university students, but not until we
reach volume 4 do we meet a high-school boy. One of Fumi’s fellow
cram-school students, Atsushi Tanaka, confesses to her
(4:270--77).[^05-05-01] His sudden appearance and swift rejection by
Fumi form a parallel with her relationship with Akira (in a chapter
titled “Unrequited Love”) and highlight the limited role men play in
the manga and how it characterizes men in general.
{:first}

[^05-05-01]: The Ishide cram school Fumi and Atsushi attend is presumably named after Den Ishide, Shimura’s friend and sometime assistant (*SBF*, 3:175, 4:270).

If we look back to *Maria Watches Over Us*, the most likely immediate
inspiration for *Sweet Blue Flowers*, we find male characters who,
though peripheral to the main action at Lillian Girls’ Academy,
nevertheless have some role in the story and exercise a fair degree of
agency. There was even a ten-volume spinoff series of light novels
featuring Yuki Fukuzawa, brother of the main character Yumi Fukuzawa,
and his classmates at an all-boys school.

It’s hard to imagine any male character in *Sweet Blue Flower*
meriting such treatment. Instead, the men of the manga are---with some
key exceptions---generally portrayed as ineffectual and passive, acted
upon rather than acting on their own. Atsushi Tanaka, Fumi’s would-be
boyfriend, is a partial exception: he does muster up the courage to
confess to her, something Fumi is not unmindful of. However, he hasn’t
even bothered to learn her actual name (“It’s Makime! Or maybe
not...”), much less tried to converse with her and strike up a
friendship before confessing.

Ko is another exception, but only partially. He doggedly continues his
pursuit of Kyoko, but how much of that is genuine initiative rather
than just following a track laid down for both of them in childhood?
Indeed, as a university student, he would have had plenty of
opportunities to meet other women. His continued attachment to Kyoko
seems at times as implausible and even off-putting as Mamoru’s to
Usagi in *Sailor Moon*.

Our next two examples, Akira’s brother Shinobu and Mr. Kagami, can’t
even muster that level of initiative, at least as portrayed in their
stories. Like his fellow university student Ko, Shinobu seems to have
no interest in or relationships with women his age. He acquires a
girlfriend (and potential future wife) in Fumi’s friend Mogi only
through a plot contrivance seemingly introduced only to close out the
subplot involving his siscon tendencies.

Likewise, Mr. Kagami is pursued by three different Sugimoto sisters
(with Kazusa winning out), even though his appeal to women remains
mysterious: “We must have a genetic weakness for guys like you,”
speculates Yasuko (*SBF*, 2:88). His work persona is no better: it’s a
long-running joke that he can’t be bothered to attend meetings of the
Fujigaya drama club of which he’s ostensibly the faculty advisor. The
situation is so bad that Hinako has to step in to help the club decide
on a production for Akira and Kyoko’s third year (4:165--66). As one
of the drama club presidents tells him, “It doesn’t matter if you’re
there or not” (1:111).

Mr. Kagami does have one accomplishment to his name, though, namely
siring a child (*SBF*, 4:18--20). In this, he joins other fathers
portrayed in the manga. Akira’s father seems to exist in a state of
perpetual bewilderment, while Fumi’s father appears only
briefly. Hinako’s father can’t be bothered to stop reading baseball
news long enough to talk seriously about his daughter’s relationship
with Orie (4:210).

Presumably, all these men work to support their families, although
this topic never comes up in the manga. (Even Mr. Kagami is never
shown in the classroom, let alone teaching.) The overall effect is
that most men in *Sweet Blue Flowers* are portrayed as aliens in a
world they don’t belong to.

As an outsider, I can’t speak from personal experience. Still, from my
reading and general impressions, it seems as if within the middle- to
upper-middle-class milieu in which *Sweet Blue Flowers* is set, the
worlds of Japanese men and women are largely separate and intersect
only in a few times and places.

The world of men is the world of work, the stereotypical salaryman and
his corporate employer, long workdays followed by long nights
socializing with coworkers, and business trips and remote assignments
away from family. The world of women is the world of home, managing a
household, and caring for children and aging parents.

Will these two worlds ever grow closer together? Beyond objective
economic measures such as gender wage gaps and labor force
participation, we can look to changes in culture, particularly those
occurring in the postbubble timeframe (the 1990s to the present). Here
two suggestive phenomena present themselves.

The first is a change in the language used by male protagonists in
that most stereotypical masculine of media, anime adapted from manga
published in the magazine *Shōnen Jump* and targeted at the *shōnen*
demographic of teenaged boys. During the height of Japan’s postwar
economic boom, *shōnen* heroes almost exclusively used the
first-person pronoun *ore*, associated with “the ‘hot-blooded hero,’
an aggressive, no-nonsense character.” But since the middle of the
1990s, many *shōnen* protagonists have begun using *boku* instead, a
more neutral pronoun. The hypothesis is that “expectations for
protagonists in *shōnen* works changed as the power of masculinity
structures that were dominant during the 1980s began to
weaken.”[^05-05-02]

[^05-05-02]: Hannah Dahlberg-Dodd, “Talking like a *Shōnen* Hero: Masculinity in Post-Bubble Era Japan through the Lens of *Boku* and *Ore*,” *Buckeye East Asian Linguistics* 3 (October 2018), 31--42, [https://&#x200B;kb&#x200B;.osu&#x200B;.edu&#x200B;/bitstream&#x200B;/handle&#x200B;/1811&#x200B;/86767&#x200B;/BEAL&#x200B;_v3&#x200B;_2018&#x200B;_Dahlberg&#x200B;-Dodd&#x200B;_31&#x200B;.pdf](https://kb.osu.edu/bitstream/handle/1811/86767/BEAL_v3_2018_Dahlberg-Dodd_31.pdf).

Another example, which came into prominence while Shimura was writing
*Sweet Blue Flowers*, is Japanese media obsession with so-called
“herbivore men”: “young men who are heterosexual but are not assertive
... in trying to pursue women,” and who engage in stereotypical
feminine behaviors like using makeup and having an interest in
fashion.[^05-05-03]

[^05-05-03]: Chris Deacon, “All the World’s a Stage: Herbivore Boys and the Performance of Masculinity in Contemporary Japan,” in *Manga Girl Seeks Herbivore Boy: Studying Japanese Gender at Cambridge*, ed. Brigitte Steger and Angelika Koch (Berlin: LIT Verlag, 2013), [https://&#x200B;www&#x200B;.academia&#x200B;.edu&#x200B;/34610378&#x200B;/All&#x200B;_the&#x200B;_Worlds&#x200B;_a&#x200B;_Stage&#x200B;_Herbivore&#x200B;_Boys&#x200B;_and&#x200B;_the&#x200B;_Performance&#x200B;_of&#x200B;_Masculinity&#x200B;_in&#x200B;_Contemporary&#x200B;_Japan&#x200B;_in&#x200B;_Brigitte&#x200B;_Steger&#x200B;_and&#x200B;_Angelika&#x200B;_Koch&#x200B;_eds&#x200B;_Manga&#x200B;_Girl&#x200B;_Seeks&#x200B;_Herbivore&#x200B;_Boy&#x200B;_Studying&#x200B;_Japanese&#x200B;_Gender&#x200B;_at&#x200B;_Cambridge&#x200B;_LIT&#x200B;_Verlag&#x200B;_2013](https://www.academia.edu/34610378/All_the_Worlds_a_Stage_Herbivore_Boys_and_the_Performance_of_Masculinity_in_Contemporary_Japan_in_Brigitte_Steger_and_Angelika_Koch_eds_Manga_Girl_Seeks_Herbivore_Boy_Studying_Japanese_Gender_at_Cambridge_LIT_Verlag_2013)

The “herbivore men” were accused of violating traditional Japanese
ideals of masculinity and contributing to the nation’s decline in
fertility while at the same time being hailed as blurring gender
boundaries and providing a potential new model for Japanese
men.[^05-05-04]

[^05-05-04]: Deacon, “All the World’s a Stage,” 135, 159--66.

The reality is perhaps more prosaic. Displaying male vanity through an
interest in makeup and fashion no more implies a revolution in male
opinions regarding gender, much less a dismantling of patriarchal
structures, than did the adoption of long hair and earrings by
American men in the 1960s and 70s. Or, to express it in more academic
language, “the mere reconfiguration of conventional hegemonic
masculinity to ‘softer,’ seemingly more egalitarian forms does not
necessarily result in equalizing the relationship between masculinity
and femininity.”[^05-05-05]

[^05-05-05]: Justin Charlebois, “Herbivore Masculinity as an Oppositional Form of Masculinity,” *Culture, Society &amp; Masculinities* 5, no. 1 (Spring 2013), 100.

As for disinterest in sex and marriage, the economic explanation is
more parsimonious: in Japanese government surveys, “those who reported
no interest in heterosexual romantic relationships had lower income
and educational levels and were more likely have no regular
employment.” Japan’s relatively stagnant economy and the decline in
traditional corporate employment meant that more men did not feel
economically secure enough to enter into a marriage, especially given
societal expectations that men will be the primary breadwinners for
their families. And because Japanese norms discourage sexual
activities outside marriage, such men had less interest in finding
sexual partners.[^05-05-06]

[^05-05-06]: Cyrus Ghaznavi, Haruka Sakamoto, Shuhei Nomura, Anna Kubota, Daisuke Yoneoka, Kenji Shibuya, and Peter Ueda, “The Herbivore’s Dilemma: Trends in and Factors Associated with Heterosexual Relationship Status and Interest in Romantic Relationships among Young Adults in Japan---Analysis of National Surveys, 1987–2015,” PLoS ONE 15(11): e0241571, [https://&#x200B;journals&#x200B;.plos&#x200B;.org&#x200B;/plosone&#x200B;/article&#x200B;?id&#x200B;=10&#x200B;.1371&#x200B;/journal&#x200B;.pone&#x200B;.0241571](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0241571), 13.

It’s worth noting that this feeling of financial insecurity appears
*not* to apply to the two leading male characters of *Sweet Blue
Flowers*, Ko and Shinobu. Although Ko does express some concern to
Kyoko’s mother about finding a job (*SBF*, 4:70), he apparently feels
financially secure enough to marry Kyoko. Shinobu is in the same
situation: both Akira and Yassan expect that he’ll marry Mogi
relatively soon, and Mogi appears to agree (4:95, 4:353). So for Ko
and Shinobu, at least, the salaryman ideal is still alive and active
as the “hegemonic masculinity.”

Finally, two men are *not* portrayed as passive and ineffectual: the
father of the Sugimoto sisters, and Count Kageyama, the antagonist of
*Rokumeikan*. Instead, they are men who exercise power over others:
Sugimoto within his company and over his family, Kageyama within the
Japanese state and over his wife, her lover, and her son. They appear
only fleetingly---the first seen but not heard, the second only spoken
to---presumably because their presence is incompatible with the
feminine world that is the manga’s focus. However, we as readers
cannot ignore their existence and that of the patriarchal society
within which *Sweet Blue Flowers* is situated.
