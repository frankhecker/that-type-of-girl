---
title: "Rokumeikan"
---

## *Rokumeikan*

Now that a new school year has begun at Fujugaya, it’s time for the
school’s drama club to choose a new play to put on. This year they
decide to put on *Rokumeikan*, a 1956 play by the Japanese author
Yukio Mishima.[^03-07-01] I’ll defer talking about the play’s plot and
characters and how they integrate with the themes of *Sweet Blue
Flowers*; that will have to wait until the next volume when the drama
club will perform the play. For now, I’ll concentrate on its author
and setting.
{:.first}

[^03-07-01]: Yukio Mishima, “The Rokumeikan: A Tragedy in Four Acts,” in *My Friend Hitler, and Other Plays of Yukio Mishima*, trans. Hiroaki Sato (New York: Columbia University Press, 2002). The lines from the play quoted in *Sweet Blue Flowers* are not from Sato’s translation. They were presumably translated by John Werry, translator of the manga.

Any discussion of *Rokumeikan* inevitably starts with its author.
Non-Japanese readers are far more likely to know the name “Yukio
Mishima” than they are to know the play itself. Mishima is most famous
for the way he died, committing *seppuku* in 1970 at the age of 45
after an unsuccessful attempt to rally the soldiers at a Japan
Self-Defense Forces base to revolt in the name of the emperor.

Non-Japanese readers are also likely to know that Mishima was
allegedly a closeted gay man (though his widow strenuously denied
this). This constitutes subtext for *Sweet Blue Flowers*, but I don’t
think it’s the most relevant aspect of the choice of
*Rokumeikan*. (Among other things, I think one of the themes of *Sweet
Blue Flowers* is the rejection of subtext.)

Instead, I think we can best understand the significance of
*Rokumeikan* to *Sweet Blue Flowers* by looking not at its author but
at the play itself.

Although Mishima is best known outside Japan as a novelist, he was
also a major playwright with several dozen plays to his credit. He
occupied a position in 1950s and 60s Japan comparable to that of his
contemporaries Tennessee Williams and Arthur Miller in America. Far
from being a Takurazuka-style fantasy, *Rokumeikan* is a serious
literary work. Fujigaya putting on a production would be like an
American high school putting on a production of *A Streetcar Named
Desire* or *Death of a Salesman*.

Like those plays, *Rokumeikan* caught the imagination of
audiences. After its initial production in 1956, the play went on tour
to thirty-five cities. It was produced again in 1962 and 1963, and
then in 2006 was revived to celebrate the 50th anniversary of the
premiere.[^03-07-02] A television adaptation aired in January 2008
while Takako Shimura was writing *Sweet Blue Flowers*. This TV movie
would likely have been the form in which Akira encountered it, hence
her comment to Ryoko Ueda: “Oh, right! It was originally a play”
(*SBF*, 2:230).

[^03-07-02]: Mami Harano, “Anatomy of Mishima’s Most Successful Play *Rokumeikan*” (master’s thesis, Portland State University, 2010), [https://&#x200B;pdxscholar&#x200B;.library&#x200B;.pdx&#x200B;.edu&#x200B;/cgi&#x200B;/viewcontent&#x200B;.cgi&#x200B;?article&#x200B;=1386&#x200B;&context&#x200B;=open&#x200B;_access&#x200B;_etds](https://pdxscholar.library.pdx.edu/cgi/viewcontent.cgi?article=1386&context=open_access_etds), 55.

Unlike a typical Takurazuka production, *Rokumeikan* is not set in a
foreign land but in Japan itself, during one of the most critical eras
of its history, and touches on key issues still relevant in Japan
today. Before Rokumeikan was a play, it was a building (“Deer-cry
Hall”) constructed in the 1880s as a guest house cum meeting center
for foreign diplomats visiting Japan. The Rokumeikan was designed by a
British architect working in the French style and featured balls and
banquets attended by Japanese nobles and bureaucrats dressed in
Western suits and gowns.

The Rokumeikan could thus be seen as either a sign of Japan’s
emergence as a recognized equal to Britain, France, and other Western
powers or an attempt by Japan to mimic Western ways to the detriment
of native Japanese traditions.

This harks back to the chapter contrasting Fujigaya and Matsuoka. How
should modernity and tradition be balanced? Should one be bound to
tradition? Celebrate it but move on? Actively repudiate it? What is
lost, and what is gained, with each approach? Over the years, Fujigaya
itself has gone from an emblem of modernity in the Meiji
era---established by representatives of a foreign religion and based
on a Western model---to a bastion of tradition in twenty-first-century
Japan.

These tensions display themselves in the setting of *Rokumeikan*, the
characters’ actions within the play, and the lives of the girls who
portray those characters. I’ll discuss this topic in more depth when
we get to volume 3.
