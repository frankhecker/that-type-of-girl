---
title: Title page
style: title-page
---

{% include metadata %}

{{ title }}
{:.title-page-title}

{{ subtitle }}
{:.title-page-subtitle}

{{ creator }}
{:.title-page-author}

Self-published
{:.title-page-publisher}
