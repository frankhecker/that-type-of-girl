---
title: "Girl Friends and Girlfriends"
---

## Girl Friends and Girlfriends

Now that I’ve come to the end of my commentary on volume 1 of *Sweet
Blue Flowers*, it’s a good time to consider some of the side
characters in this volume. First up are Fumi’s three classmates and
friends at Matsuoka: Yoko Honatsugi (“Pon”), Miwa Motegi (“Mogi”), and
Misako Yasuda (“Yassan”). They aren’t very fleshed out as characters,
so much so that it can be difficult keeping their nicknames straight,
much less their full names. Their introduction at the beginning of
volume 1 identifies them merely as “lively girls.” They aren’t
featured at all in the character list at the beginning of part 2 of
volume 1 (volume 2 of the Japanese edition) (*SBF*, 1:3, 1:198--99).
{:.first}

The main functions of Pon, Mogi, and Yassan seem to be giving Fumi
someone else other than Yasuko to interact with at Matsuoka, drawing
Fumi out of her introversion, and getting her involved with the
*Wuthering Heights* production and other activities at Fujigaya. Time
will tell whether they emerge as developed characters in their own
right or simply exist to drive the plot in various directions.

Last but not least are Orie and Hinako (no family names given), who
appear in the side story “Little Women” at the end of volume 1, along
with Yasuko’s sister Shinako, here portrayed in high school. Orie has
a crush on Shinako, apparently her senior, and confides in Hinako,
only to find that Hinako has a crush on her. “Soon after, I fell in
love with Hinako,” Orie recalls (*SBF*, 1:379--81), as she and Hinako
enact Erica Friedman’s “Story A” on a single page: “There is a girl,
she likes another girl. The other girl likes her. They like each
other. The end.”[^02-12-01]

[^02-12-01]: Erica Friedman, “Overthinking Things 04/03/2011: 40 Years of the Same Damn Story, Pt. 1,” *The Hooded Utilitarian* (blog), April 3, 2011, [https://&#x200B;www&#x200B;.hoodedutilitarian&#x200B;.com&#x200B;/2011&#x200B;/04&#x200B;/overthinking&#x200B;-things&#x200B;-04032011](https://www.hoodedutilitarian.com/2011/04/overthinking-things-04032011).

Is there more to Orie’s and Hinako’s story than this? A story beyond
“Story A”? That will have to wait until future volumes. In the
meantime, it’s worth noting two things: First, other than Fumi and
Yasuko, Orie and Hinako are the only two girls referenced as being in
a relationship. Second, and perhaps even more important, unlike Fumi
and Yasuko, Orie and Hinako’s appears to be a relationship of equals,
between girls in the same class and of the same age. This means that
they may break out of the straitjacket of age-based hierarchy in which
Class S and yuri works often bind women who love women.
