---
title: "The Play’s the Thing"
---

## The Play’s the Thing

Takako Shimura’s use of Yukio Mishima’s 1956 play *Rokumeikan* in
volume 3 of *Sweet Blue Flowers* is probably the best example of her
fondness for using theatrical plays as elements in her manga. The
interplay between the events and characters of the play and the events
and characters of the manga contributes to that volume being, to my
mind, the best of the series.
{:.first}

The bits of the play presented in *Sweet Blue Flowers* are somewhat
fragmentary. Shimura assumes a readership familiar with at least the
basic outline of the play and its main characters. For those who have
not read the play, here‘s a summary of the plot:

*Act 1.* On the emperor’s birthday, November 3, 1886, a group of
aristocratic women gather at a teahouse on the estate of Count
Hisatoshi Kageyama, a high-ranking government minister, and watch the
military review being held in the emperor’s honor. (This first scene
is also the first depicted in the manga (*SBF*, 2:233--35).)
Kageyama’s wife Asako (played by Kyoko Ikumi in the performance shown
in *Sweet Blue Flowers*) joins them. Asako, an ex-geisha elevated to
the aristocracy by her marriage, is uncomfortable with her new status
and never appears in public.[^04-03-01]

[^04-03-01]: Mishima, *Rokumeikan*, 5--8.

One of the women appeals to Asako on behalf of her daughter Akiko
(played by Akira Okudaira in *Sweet Blue Flowers*) and Akiko’s lover
Hisao. Hisao supports the opposition party, and Akiko fears that he
will disrupt that night’s ball at the Rokumeikan and attempt to
assassinate Count Kageyama. Perturbed at hearing Hisao’s name, Asako
agrees to help, and after the ladies depart, she meets with
him.[^04-03-02]

[^04-03-02]: Mishima, *Rokumeikan*, 9--12.

Asako reveals to Hisao that she is his mother by her former lover
Einosuke Kiyohara, leader of the opposition party, who took Hisao in
after his birth. Hisao expresses his resentment of his father’s
neglect of him and his treatment compared to Kiyohara’s legitimate
children and reveals that he plans to kill not Kageyama but
Kiyohara.[^04-03-03]

[^04-03-03]: Mishima, *Rokumeikan*, 14--16.

*Act 2.* After Hisao leaves, Asako reaches out to Kiyohara (played by
Ryoko Ueda in *Sweet Blue Flowers*) and meets him at the teahouse. She
tells him she knows about the plan to disrupt the ball and urges him
to abandon it. Kiyohara resists until she tells him that she plans to
leave her private sphere and attend the ball herself.[^04-03-04]

[^04-03-04]: Mishima, *Rokumeikan*, 16--23.

Kiyohara leaves as Count Kageyama and his retainer Tobita enter the
scene. Overheard by Asako, their conversation reveals that Kageyama
knows about the plot to disrupt the ball and, with Tobita as his
intermediary, is the mastermind behind Hisao’s plan to kill Kiyohara.
The bloodthirsty Tobita protests that Kageyama did not give him the
task of assassination.[^04-03-05]

[^04-03-05]: Mishima, *Rokumeikan*, 23--27.

Asako reveals herself and tells Kageyama that there will be no
disturbance at the ball, and after Tobita leaves, tells Kageyama of
her plan to attend. She then urges Kageyama to persuade Hisao to
abandon his plan to kill his father, explaining her interest as simply
that of helping a friend’s daughter. Kageyama agrees on the condition
that the ball not be disrupted. After Asako leaves, he grabs her maid
Kusano and forces himself upon her.[^04-03-06]

[^04-03-06]: Mishima, *Rokumeikan*, 27--31.

*Act 3.* Upstairs at the Rokumeikan before the ball, Akiko and Hisao
talk of Asako’s role in bringing Hisao to the ball. They kiss, after
which Asako enters and busies herself with directing the workers
decorating the rooms. Meanwhile, having seduced Kusano with the
promise of his favor, Kageyama extracts from her the information that
Asako is Hisao’s mother and Kiyohara’s former lover.[^04-03-07]

[^04-03-07]: Mishima, *Rokumeikan*, 32--37.

After conversing briefly with Asako, Kageyama seeks out Tobita and
tells him that plans have changed: since Kiyohara called off the
original plot to disrupt the ball, Tobita should now arrange a
disruption himself. Kageyama then tells Kusano to summon Kiyohara to
the Rokumeikan that evening in Asako’s name.[^04-03-08]

[^04-03-08]: Mishima, *Rokumeikan*, 37--41.

Akiko and Hisao talk of their plans to elope together and leave for a
foreign tour. Kageyama interrupts them and upbraids Hisao for his
giving in to romance and abandoning his plans. He tells Hisao that
(contrary to what Asako told Hisao) a break-in will occur, and the
(unnamed) target of Hisao’s assassination plot will be present on the
grounds outside the Rokumeikan. Kageyama hands Hisao a pistol, and he
accepts it.[^04-03-09]

[^04-03-09]: Mishima, *Rokumeikan*, 41--45.

Kageyama rejoins Asako and her friends, and they drink a toast to the
emperor’s health---marred by the ill omen of Asako accidentally
dropping her glass.[^04-03-10]

[^04-03-10]: Mishima, *Rokumeikan*, 45--46.

*Act 4.* As Asako, Kageyama, and their fellow aristocrats talk among
themselves, the invited dignitaries begin to arrive at the ball. These
include Prime Minister Hirobumi Itō, Minister of the Army Iwao Ōyama
and his wife, the former Sutematsu Yamakawa (see the previous
chapter), and various foreign guests.[^04-03-11]

[^04-03-11]: Mishima, *Rokumeikan*, 46--47. Mami Harano incorrectly refers to Count Kageyama as the prime minister. Harano, “Anatomy of Mishima’s Most Successful Play,” 1, 11, 17, 36--37, 39, 42, 46--47.

After the guests enter the ballroom, a report comes up from downstairs
of men brandishing swords and destroying decorations. Asako goes to
the head of the stairs and faces them down, after which Kageyama
quietly directs Tobita to have the men withdraw. Meanwhile, thinking
that his father has betrayed both him and Asako in ordering the plot
to proceed, Hisao flies into a rage and leaves the
building.[^04-03-12]

[^04-03-12]: Mishima, *Rokumeikan*, 48--49.

Soon after, shots are heard, and a distraught Kiyohara enters,
explaining that Hisao is dead: Kiyohara was fired upon by an assailant
hiding in the dark and fired back in self-defense, subsequently
discovering that he had killed his son. Perceiving that Hisao had
deliberately misdirected his shot, Kiyohara concludes that Hisao
wanted to be killed by his father as an act of revenge upon
him.[^04-03-13]

[^04-03-13]: Mishima, *Rokumeikan*, 49--50.

Kiyohara declares himself done with politics and sardonically
congratulates Kageyama on achieving his goal of eliminating a
political enemy. He also tells Asako that the men who broke into the
Rokumeikan were not his own, declares that he kept his promise
(implying that in calling him to the scene, Asako had not kept hers),
swears that he will never see her again, and exits.[^04-03-14]

[^04-03-14]: Mishima, *Rokumeikan*, 50--51.

Tobita exits as well (“with a conspiratorial air,” per the stage
directions), as do Akiko and her mother after Asako attempts to
comfort them, leaving Asako and Kageyama to face each other. Kageyama
taunts Asako for believing in “fairy tales” of trust and cooperation
between people, in ignorance of the real world of politics, while
Asako accuses him of knowing and wanting nothing but power.[^04-03-15]

[^04-03-15]: Mishima, *Rokumeikan*, 51--53.

Asako declares her intention to leave Kageyama for Kiyohara, the
arrival of the Imperial Princess is announced, the orchestra plays
while Asako and Kageyama dance, and Asako thinks she hears a pistol
shot in the distance. The music stops, Kageyama tells Asako the sound
was only fireworks, and then the music and dance continue as the
curtain falls.[^04-03-16]

[^04-03-16]: Mishima, *Rokumeikan*, 53--54.
