---
title: "Loving Hierarchy"
---

## Loving Hierarchy

*Maria Watches Over Us* (*Maria-sama ga miteru*, or *Marimite* to its
fans) looms like a colossus over the twenty-first-century yuri genre,
including *Sweet Blue Flowers* in particular. Understanding what
*Sweet Blue Flowers* might be saying thus requires our first
understanding what kind of work *Maria Watches Over Us* is.
{:.first}

Beginning with a short story by Oyuki Konno in 1997, *Maria Watches
Over Us* eventually grew into a multimedia franchise. It includes a
series of over three dozen light novels by Konno (published from 1998
to 2012, overlapping serialization of *Sweet Blue Flowers*), a
nine-volume manga adaptation, a four-season anime adaptation (the only
version released in English translation),[^01-06-01] a live-action
film, audio CDs, and other products.[^01-06-02]

[^01-06-01]: *Maria Watches Over Us*, directed by Yukihiro Matsushita and Toshiyuki Kato (2004--2009; Houston: Sentai Filmworks, 2020), Blu-ray Disc, 1080p HD.

[^01-06-02]: For an overview of the franchise and an assessment of its significance, see Erica Friedman, “*Maria-sama ga miteru*: 20 Years of Watching Mary Watching Us,” *Okazu* (blog), January 28, 2018, [https://&#x200B;okazu&#x200B;.yuricon&#x200B;.com&#x200B;/2018&#x200B;/01&#x200B;/28&#x200B;/maria&#x200B;-sama&#x200B;-ga&#x200B;-miteru&#x200B;-20&#x200B;-years&#x200B;-of&#x200B;-watching&#x200B;-mary&#x200B;-watching&#x200B;-us](https://okazu.yuricon.com/2018/01/28/maria-sama-ga-miteru-20-years-of-watching-mary-watching-us).

At first glance, the existence of *Maria Watches Over Us* poses a
problem for the thesis I advanced in an earlier chapter: that postwar
changes in Japan, including in particular the introduction of
coeducation and the decline in arranged marriages, removed the social
context within which the Class S literature of the early twentieth
century had flourished. *Maria Watches Over Us* also features
“passionate friendships” between students at an all-girls Catholic
school and thus seems to conform to the traditional Class S template
pioneered by Nobuko Yoshiya and others. Why then is it so popular in
twenty-first-century Japan?

To resolve this seeming paradox, we need to look more closely at
*Maria Watches Over Us* and the differences between it and traditional
Class S works. First, even though not all girls in early
twentieth-century Japan attended high school, much less Catholic high
schools, those who did attended all-girls schools. Thus the settings
of typical Class S works were not too far removed from their own
experiences.

However, in the early twenty-first-century timeframe of *Maria Watches
Over Us* the upper-class all-girls Catholic school environment
portrayed in the series would likely be foreign to almost all of its
readers and viewers. Though depicting events in contemporary Japan,
the exotic setting of *Maria Watches Over Us* makes it more akin to a
work of fantasy.

Second, the life prospects of the characters in *Maria Watches Over
Us* are not nearly as constrained as those of the girls in traditional
Class S stories. In particular, with one exception (Sachiko
Ogasawara), no girls are depicted as being compelled into an arranged
marriage---and it appears that even Sachiko may escape that
fate. Almost all of the characters are implied to have a fair amount
of freedom in making their life choices after high school, even to
take somewhat unusual paths (like pursuing a widowed man with a young
child, as Eriko Torii does).

As I interpret it, the core theme of traditional Class S works is that
a girl’s time at an all-girls school is a brief period during which
she can exercise a measure of free choice in entering into a deep and
fulfilling relationship with another girl, before she is called to
fulfill her assigned duty as a “good wife and wise mother,” serving a
husband chosen for her by others and a Japanese state that envisions
no other role for her. At best, that fleeting period of happiness must
end in permanent separation from the one she loves. At worst, it will
end in death for her or her partner.

That theme is echoed in the “Forest of Briars” episode of *Maria
Watches Over Us*, which features a novel initially thought to be
written by Sei Satō about her relationship with her younger classmate
Shiori Kubo.[^01-06-03] But, in fact, that novel was written by a
woman who attended Lillian Girls’ Academy many years ago---a plot
twist that serves to distance that Class S story from the very
different tale told by *Maria Watches Over Us*.

[^01-06-03]: *Maria Watches Over Us*, season 1, episode 10, “The Forest of Briars.”

So, if *Maria Watches Over Us* is not simply a traditional Class S
story updated to modern Tokyo, what is it? I contend that *Maria
Watches Over Us* is best thought of as a utopian fantasy featuring a
benevolent hierarchically-ordered society sustained by kindness,
empathy, and love---a fantasy rendered at least superficially
plausible by the fact that the society’s members are all women. The
central concerns of *Maria Watches Over Us* are how to find a suitable
place for oneself in such a hierarchically-ordered society and, having
done so, how best to perpetuate that society by bringing others into
its embrace.

That hierarchical order is sustained through the *sœur* (“sister”)
system, in which older girls enter into relationships of close
friendship and affection with younger girls. Those younger girls then
take on *sœurs* of their own, advancing up the ranks of the age-based
hierarchy as their seniors graduate.

There are many attractive features of this hierarchically-ordered
society. First, it is relatively inclusive, with girls able to find a
place in it irrespective of their family wealth or social
status. Thus, for example, the main protagonist, everygirl Yumi
Fukuzawa, finds herself chosen as the “*petite sœur*” of Sachiko, the
aristocratic scion of one of the wealthiest families of
Japan.[^01-06-04]

[^01-06-04]: However, this spirit of inclusion extends only so far. None of the girls in *Maria Watches Over Us* appear to be genuinely lower-class---not surprising given the presumed expense of attending an exclusive private school. In particular, Yumi’s father owns a small design firm and is thus solidly situated within the *petite bourgeoisie*.

The *sœur* system also accommodates relationships between girls with
very different personalities (for example, Yumi and Sachiko) and
relationships of varying characters and degrees of intensity, like the
superficially distant but actually deep relationship between Sei Satō
and her *petite sœur* Shimako Tōdō.

The system is also flexible enough to survive occasional “problems of
succession,” like Sei delaying her selection of a *sœur* and picking
someone two grades below herself (instead of one grade, as is the
usual practice) or Yoshino Shimazu picking as a *petite sœur* Nana
Arima, a girl who hasn’t yet graduated from middle school into high
school. Even choices of a successor made almost at random, like
Sachiko’s selection of Yumi as her *sœur* or Yoshino’s selection of
Nana, work out fine in the end, as if guided by the hand of the Virgin
Mary herself.

Finally, and perhaps most important, the *sœur* system also offers the
opportunity for positive personal growth. As a new *petite sœur* Yumi
must learn what Sachiko needs from her, and what she must learn to
properly be a *grande sœur* herself to Tōko Matsudaira, to grow “from
an ‘average’ young woman to a commander among her peers, guiding with
gentle pressure.”[^01-06-05] And through Yumi’s relationships with
Sachiko and Tōko, they become better people as well.

*Maria Watches Over Us* thus embodies in contemporary fiction Nobuko
Yoshiya’s contention that relationships between younger girls and
older ones (or, for that matter, between girls and their teachers) are
critical to their developing “a beautiful, moral, social, and
non-self-centered character.”[^01-06-06]

[^01-06-05]: Friedman, “*Maria-sama ga miteru*: 20 Years.”

[^01-06-06]: Suzuki, “The Translation of Edward Carpenter’s *Intermediate Sex*,” 208.

But no matter how benign it may be, the social order of *Maria Watches
Over Us* is still one organized as a relatively rigid hierarchy based
on age and seniority, one that in some respects is reminiscent of
traditional Confucian teachings or the ideologies of imperial
Japan. Like the hierarchies promoted by those, the society of Lillian
Girls’ Academy is a hierarchy of hierarchies.

First, there are the groups of girls linked together by *sœur*
relationships (a girl, her *petite sœur*, that girl’s *petite sœur*,
and so on). These are analogous to the extended family unit of the
traditional Japanese household (*ie*), with each generation deferring
to those above it.

Then there are three groups of *sœurs* that are set apart from other
girls as a sort of hereditary aristocracy, the so-called “Rose
families.” The heads of these families, that is, the oldest girls
within each of these three sets of *sœurs*, even have the equivalent
of titles of nobility: Rosa Chinensis, Rosa Foetida, and Rosa
Gigantea.

The student council of the school is supposedly chosen in democratic
elections. In practice, the Roses control the council, just as the
governments of Meiji-era Japan were dominated by the former samurai
who became part of the newly-(re)constituted Japanese nobility. In
*Maria Watches Over Us* there are two challengers who seek election to
the council, but both are easily defeated and the system of hereditary
succession preserved. (See also below.)

As student council members, the Roses are shown to be enlightened
rulers. Unlike the student councils in many manga and anime, they do
not lord it over the students, seek to shut down clubs, or attempt to
restrict students’ behavior through petty regulations.

Instead, they seek to narrow the perceived distance between themselves
and other students. For example, Sachiko’s *grande sœur* Yōko Mizuno
shows concern that ordinary students are afraid to approach the Roses
and expresses her desire to open up the Rose Mansion (the building
that serves as their gathering place) to anyone who wishes to
visit.

However, the scope of this benevolent order is limited in both space
and time. First, these hierarchies are themselves embedded in the
larger hierarchies of Japanese society. Erica Friedman claims that
“fans [of *Maria Watches Over Us*] are treated to a world in which
women are assumed to be able to lead and to command without
question.”[^01-06-07] But that world ends at the gates of Lillian
Girls’ Academy, the boundary beyond which the patriarchy reigns. The
nuns who run the school are subject to the authority of the local
(male) bishop, who is himself a mid-rank figure in the (male) church
hierarchy presided over by the Pope.

[^01-06-07]: Friedman, “*Maria-sama ga miteru*: 20 Years.”

Likewise, Sachiko, the epitome of aristocratic grace and ability,
rules supreme within the school in concert with her fellow
Roses. However, though she is the sole child of the family, she is
apparently deemed unfit to lead the Ogasawara corporate empire as an
adult. Her proposed marriage with Suguru Kashiwagi would lead to his
taking over the companies. If that engagement ends, some other man
would presumably be found to take his place, whether through marriage
to Sachiko or adoption into the Ogasawara family.

For the most part, the hierarchical order of *Maria Watches Over Us*
goes unquestioned by those who participate in it---and almost all of
the girls do participate in it. There are at least four exceptions to
this, two political and two personal, each illustrating various
aspects of the social order and how it is preserved against potential
threats.

The first political challenge is brought by Shizuka Kanina, also known
as Rosa Canina---analogous to a usurper granting themselves a royal
title.[^01-06-08] She runs for the student council essentially to get
the attention of Sei. Having achieved that goal, she leaves school to
study voice in Italy, exiling herself from Japan and (by implication)
its social hierarchies.

[^01-06-08]: *Maria Watches Over Us*, season 1, episode 6, “Rosa Canina.”

The second challenger is Sachiko’s cousin Tōko. Insecure in her
position in her family due to her having been adopted into it, Tōko’s
story resembles the stories of bastard sons of the aristocracy, born
of nobility but estranged from it by the circumstances of their birth,
of the elite but at the same time not of it.

In some stories, this results in the bastard son leading a revolution
against the class of which they are ostensibly a member. This finds
echoes in Tōko’s student council run, in which she apparently seeks to
overthrow the tradition of the Rose families running the
council.[^01-06-09] However, Yumi’s kindness wins Tōko over, Yumi
makes Tōko her *petite sœur*, and she becomes part of the Rosa
Chinensis family, one day to assume the position of Rose that she had
dreamed of as a child. Thus, like many past rebellious sons of
nobility, she ends her story co-opted into the hierarchical order
against which she had once inveighed.

[^01-06-09]: *Maria Watches Over Us*, season 4, episode 9, “The Masked Actress.”

As for personal challenges to the social order, the more significant
is the relationship of Sei and Shiori, the intensity and passion of
which reads as being lesbian in nature.[^01-06-10] However, they are
not criticized for engaging in lesbian activity specifically.

[^01-06-10]: *Maria Watches Over Us*, season 1, episode 11, “The White Petals.”

Rather Sei and Shiori are criticized, explicitly by their elders and
implicitly by the story framing, for letting their absorption in each
other lead to their neglecting their roles in the two hierarchical
orders to which they have respectively committed themselves: Sei to
her *sœurs* and the *sœur* system, and Shiori to the Church that she
intends to join as a nun. (Note that despite her affection for Shiori,
Sei explicitly rejects making Shiori her *sœur*.) To put it another
way, Sei and Shiori’s true sin is the sin of individualism, and their
subplot is resolved by their repenting of it.

The other case of rejecting the hierarchical order is not a rejection
at all. The “ace of the photography club,” Tsutako Takeshima, who
loves to photograph others but hates to be photographed herself,
exists outside of the *sœur* system, observing it closely but not
participating in it herself. She is a fairly transparent stand-in for
the author, and as such does not constitute a threat to the social
order.

Given the above, why might *Maria Watches Over Us* be so popular, both
with the teenaged girls for whom it was originally written and the
older all-genders audience that makes up a significant part of
*Marimite* fandom?

For teenaged girls, besides the emotional drama (tending to melodrama)
for which *shōjo* manga, anime, and light novels are known and loved,
*Maria Watches Over Us* offers a vision of a charmed school existence,
in which bullying is seemingly nonexistent and almost all troubles are
ultimately dissolved in a sea of mutual kindness and
support.[^01-06-11] I can see many a girl picturing herself in Yumi’s
place, plucked from obscurity to become the valued companion and
helpmeet to the elegant and cultured Sachiko, who seemingly has
everything but desperately needs what only Yumi can provide.

[^01-06-11]: The most serious instance of attempted bullying is perpetrated on Yumi by three wealthy girls whose families are friends with Sachiko’s. This, however, occurs outside the walls of Lillian Girls’ Academy, and is thoroughly defeated by a wise elder charmed by Yumi’s sincerity and artlessness. *Maria Watches Over Us*, season 3, episode 1, “Vacation of the Lambs.”

This emotional identification can extend to other characters: *Maria
Watches Over Us* features such a wide variety of personalities and
relationships that almost any young reader or viewer can find some
aspect of the story that speaks to them personally.

As for the older audience, it may be no accident that the popularity
of *Maria Watches Over Us* grew as Japan was entering its second “lost
decade” of economic stagnation. *Maria Watches Over Us* offers an
appealing vision of an alternative Japan, of a Japan as some like to
think of it. The world of Lillian Girls’ Academy is a hierarchical
social order that works for all those who are part of it. Those on the
lower rungs of the ladder are valued and cared for, not ignored or
exploited---comfort food for “freeters” and others for whom the
traditional Japanese promise of lifetime corporate employment (never
extended to all of society, especially women) will remain forever
unfulfilled.

The popularity of the *Maria Watches Over Us* franchise led to others
creating works inspired by it. Independently-published *dōjinshi*, fan
fiction, and other types of unofficial derivative works sought to
extend the story, explore different pairings of the characters, or
show favorite “ships” in more compromising positions. Among others,
*Strawberry Panic* reused the Catholic girls’ school setting and other
*Marimite* tropes to tell a story of schoolgirl affections more
directed at the male gaze.[^01-06-12]

[^01-06-12]: Sakurako Kimino, *Strawberry Panic*, trans. Michelle Kobayashi and Anastasia Moreno, 3 vols. (Los Angeles: Seven Seas Entertainment, 2008).

And, of course, there’s *Sweet Blue Flowers*, which seems to have been
something of a side project for Takako Shimura while she was creating
*Wandering Son*. She began serialization of *Sweet Blue Flowers* in
2004, about a year and a half after serialization of *Wandering Son*
began. She ended it in 2013, a month before *Wandering Son* ended,
having published one chapter every other month instead of the monthly
schedule for *Wandering Son*.

Thus Shimura had enough spare time to do a second manga (albeit on a
less demanding schedule), an available outlet in *Manga Erotics F*
(*Wandering Son* was being serialized in *Comic Beam*), an all-genders
adult audience familiar with tales of love between schoolgirls, a
story structure to echo and adapt, and presumably something she wanted
to say.

What was that something? As noted previously, I think it was about
presenting a model of loving relationships between women that does not
conform to a hierarchical framework like that in *Maria Watches Over
Us*, relationships in which two girls come to each other not as
*grande sœur* and *petite sœur*, not as *senpai* and *kōhai*, not as
superior and inferior, but rather as equal individuals.
