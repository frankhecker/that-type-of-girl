---
title: Copyright
style: copyright-page

# The Liquid tags here fetch metadata
# from this book's YML file in _data
---

{% include metadata %}

# Copyright
{:.non-printing}

{{ title }}: {{ subtitle }}\\
{{ publisher }}, Ellicott City, MD 21042\\
©&nbsp;2022 {{ creator }}\\
{{ rights }}\\
Published {{ date }}, last revised {{ modified }}

{% include identifiers scheme="ISBN" %}

Cover illustration by Ola Tarakanova.
