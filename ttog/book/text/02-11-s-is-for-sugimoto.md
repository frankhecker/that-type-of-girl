---
title: "S Is for Sugimoto"
---

## S Is for Sugimoto

From Yasuko Sugimoto, I now turn to her family: her sisters Kuri,
Kazusa, and Shinako, and her mother Chie (who is not named in volume
1). The Sugimoto family occupies a particular place in the story of
*Sweet Blue Flowers*, related to the overall theme of the work as I
see it.
{:.first}

First, as Fumi marvels when she first visits Yasuko’s home, “Her
family’s rich!” (*SBF*, 1:303). To be clear, Fumi’s and Akira’s
families are far from poor. They live in detached houses (instead of
apartments), own cars, and can afford to send their daughters to
exclusive (and presumably expensive) private schools. However, the
Sugimotos are in another class entirely. It’s no accident that Takako
Shimura modeled the Sugimoto family home on a guest house on the Tokyo
estate of Marquis Toshinari Maeda, head of a leading samurai clan and
(later) general in the Japanese army (1:378).

That wealth presumably comes from the position of Yasuko’s father.
We’ve already encountered the fathers of both Fumi and Akira. But the
Sugimoto family patriarch is unseen, unnamed, and unmentioned, at
least in the story thus far. The only male presence in the Sugimoto
household is Mr. Ogino, the family servant Fumi initially mistakes for
Yasuko’s father (*SBF*, 1:299).

In the father’s absence, the Sugimoto women occupy their time with
gossip and Mahjong. Except for Kazusa, an artist who formerly taught
at Fujigaya for a brief time, they appear to have done little else
since graduating from school. Of course, they all went to Fujigaya,
and appear to have been part of that school’s Class S milieu. Shinako
appears in the side story “Little Women” as the unrequited love
interest of other girls (“everyone admires her,” Orie sighs) (*SBF*,
1:379), and (as Kuri remarks) “Mother wants to brag about how popular
she was as a girl,” rather like a former high school sports star who
continually retells stories of his exploits (1:305).

To my mind, the Sugimoto sisters and their mother represent another of
Shimura’s critiques of Class S and yuri tropes. The Sugimoto women
have achieved a sort of freedom from the patriarchal culture of Japan,
to speak frankly about intimate relationships between women (much to
Fumi’s embarrassment and consternation, after she’s outed by Yasuko),
and to indulge in such relationships themselves.

However, it is a freedom that is open to only to a wealthy few (who do
not have to worry about discrimination against LGBTQ people at work or
elsewhere), is limited in time and place, and is ultimately at the
sufferance of the men who wield financial and other control over these
women’s lives. When those men return from their company headquarters,
or golf courses, or mistresses, like Sir Thomas Bertram returning from
Antigua in *Mansfield Park*,[^02-11-01] they will reassert their
authority and that of the male-dominated culture of Japan.

[^02-11-01]: Jane Austen, *Mansfield Park* (London: 1814; Project Gutenberg, 1994), chap. 19, [https://&#x200B;gutenberg&#x200B;.org&#x200B;/ebooks&#x200B;/141](https://gutenberg.org/ebooks/141).

In other words, this is not the sort of freedom that Fumi needs or
wants, even if she could achieve it in the first place. The Sugimoto
women may be catalysts whose actions inadvertently help Fumi realize
her identity and desires, but they can never be her models.
