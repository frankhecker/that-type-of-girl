---
title: "Appendix 3: Errata"
---

## Appendix 3: Errata

I noticed only a few errors or other issues with the VIZ Media edition
of *Sweet Blue Flowers*. Leaving aside matters of hyphenation (where
sometimes the available space forces an incorrect hyphenation), here’s
what I believe to be the complete list of errata and related issues.
{:.first}

### Volume 1 {#appendix-3-errata-volume-1}

*SBF*, 1:84, panel 2: “But you’d make a hot heroine!” The original
Japanese text (*Aoi hana*, 1:84) is identical to the original Japanese
text for page 2:242, panel 1 (*Aoi hana*, 4:62), which shows Kyoko
remembering what Yasuko said. However, this identical text is
translated with very different meanings in the English edition: on
page 2:242 Yasuko says that Kyoko could be an “otherworldly princess.”
The translation on page 2:242 appears to be closer to the original
meaning of the Japanese text.

*SBF*, 1:206, panel 1: “But you’re a principle character!” “Principle”
should be “principal.”

*SBF*, 1:288, panel 1: “Lots of families have attended Matsuoka since the
Meiji period.” Given the context, it’s clear that the reference to
Matsuoka should instead be to Fujigaya.

*SBF*, 1:337, panel 3: “Interview with Kyoko Sugimoto.” “Kyoko” should be
“Yasuko.”

### Volume 2 {#appendix-3-errata-volume-2}

*SBF*, 2:116, panel 3: “She never thinks about whether it’s right for
her.” The text in the English edition implies that Yasuko is thinking
this about Kyoko. The original Japanese text (*Aoi hana*, 3:116) does
not support this reading, but instead appears to reflect Yasuko
criticizing herself for acting towards Mr. Kagami as Kyoko did towards
her. One alternative translation is, “I had no idea at the time that
I was acting the same way.”

*SBF*, 2:159, panel 1: “You’ll forget all about her after graduation.”
The original Japanese text (*Aoi hana*, 3:159) is identical to the
original Japanese text for page 2:171, panel 3 (*Aoi hana*,
3:171). However, this identical text is translated with very different
meanings in the English edition: on page 2:171 it is translated as
“You’ll forget *me* after I graduate” (emphasis added). It appears
that the second translation is the correct one: on page 2:159 Orie is
telling Hinako that Hinako will forget about her (Orie). On page 2:171
Hinako is recalling what Orie said to her (Hinako) after Kawakubo
tells Hinako that Hinako will forget about her (Kawakubo).

*SBF*, 2:265: The corresponding page in the Japanese edition (*Aoi
hana*, 4:85) has all text in a lighter typeface, the convention used
in the Japanese edition to indicate a flashback to an earlier
conversation.  However, the English edition has text in the typeface
used for conversations in the present day, as on pages 2:264 and
2:266.

### Volume 3 {#appendix-3-errata-volume-3}

*SBF*, 3:175, panel 1: “And Kazuo Ogatsuka came to help ...” “Kazuo
Ogatsuka” should be “Kazuo Ogatsu.”

*SBF*, 3:332, panel 6: “Tengui Cloths.” “Tengui” should be “Tenugui.”

### Volume 4 {#appendix-3-errata-volume-4}

*SBF*, 4:94, panel 3: “Manjome.” “Manjome” should be “Motegi.”

*SBF*, 4:121, panel 4: “Asuka Arai.” This appears to be a misreading
of the Japanese text, which gives the character’s name as “Tomoka
Arai” (*Aoi hana*, 7:119). The kanji corresponding to “Asu&#x2060;-”
and “Tomo&#x2060;-” are very similar in appearance, which likely
accounts for the misreading.

*SBF*, 4:234--36: These pages repeat the dialogue and action from a
scene in volume 2 when Hinako assumes her role as Akira’s homeroom
teacher (2:204--5). Presumably, the pages in volume 4 were intended to
be a flashback to the events of volume 2. However, the style used for
dialogue text in volume 4 is not that normally used in the manga for
flashbacks (italicized text in a lighter typeface), making it appear
to the unwary reader that the events of 4:234--36 are occurring in the
present.

This appears to be an error carried over from the Japanese edition of
*Aoi hana*. That edition also uses a lighter typeface for dialogue in
flashbacks (see, e.g., *Aoi hana*, 8:51) but does not do so on pages
8:54--56, the pages corresponding to 4:234--36 in the English edition.

*SBF*, 4:262, panel 1: “I’ll be in the bathrrom.” “Bathrrom” should be
“bathroom.”

*SBF*, 4:271, panel 3: “Atsushi Tanaka.” This appears to be a
misreading of the Japanese text, which gives the character’s name as
“Atsushi Taguchi” (*Aoi hana*, 8:91). The kanji corresponding to
“-&#x2060;naka” and “-&#x2060;guchi” are similar in appearance, and
the paper on which the name is written is wrinkled. These factors
likely account for the misreading.

*SBF*, 4:336, panel 5: “Your friend is in the hospital, right? For
appendicitis? How unfortunate ...” 4:337, panel 1: “How mean! She left
her friend and came back alone!” “No, I didn’t! We came back
together!” This sequence doesn’t make sense when interpreted according
to the most straightforward reading.

The reference to “your friend” (*SBF*, 4:336) is clearly to Kawasaki,
according to information conveyed by Ueda (4:338). The straightforward
reading is that the reference to “her friend” (4:337) is also to
Kawasaki and that Yasuko is being accused (by Shinako?) of leaving
Kawasaki alone in England while sick.

Yasuko’s subsequent statement apparently refutes that accusation,
implying that Kawasaki had also returned to Japan. But this is refuted
in turn by Ueda’s comment that Kawasaki is still in England being
cared for by others (*SBF*, 4:338).

The original Japanese text of the accusation against Yasuko contains
the word “kōhai” (“junior”), which may refer to Ueda (*Aoi hana*,
8:157). So perhaps the charge is that Yasuko left Ueda alone in Japan
(i.e., to care for Kawasaki), an accusation that (as Yasuko asserts)
is, in fact, not true. This may be a case where the English
translation doesn’t quite capture the meaning of the Japanese.
