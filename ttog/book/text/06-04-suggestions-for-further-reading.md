---
title: "Suggestions for Further Reading"
---

## Suggestions for Further Reading

Below I’ve highlighted a selection of works that I consider
particularly significant and well worth seeking out for anyone
interested in the topics discussed in this book. I’ve also included a
final section of works in Japanese that I’d like to see released in
official English translations.
{:.first}

See the full bibliography for more information on these works, along
with a complete list of all the other works I consulted in writing
this book and have referenced in its pages.

### Class S Culture and Literature

- Alice Mabel Bacon, *Japanese Girls and Women*. Bacon provides a
  contemporary perspective on the lives of Japanese girls and women in
  the Meiji era. She grew up with Sutematsu Yamakawa, a Japanese girl
  studying in the US, and was also close friends with Yamakawa’s
  fellow US students, Ume Tsuda and Shige Nagai. (See the chapter
  “Setting the Stage.”) Bacon visited and taught in Japan in
  1889--1890 and 1900--2. She wrote this book afterward with
  assistance from Tsuda (who wanted her participation to remain
  anonymous).
- Edward Carpenter, *The Intermediate Sex: A Study of Some
  Transitional Types of Men and Women*. This collection of essays is
  an early example of what we’d refer to today as LGBTQ
  advocacy. Carpenter’s ideas influenced Nobuko Yoshiya’s thinking on
  S relationships, as discussed in the chapter “Homage to Yoshiya.”
  See, in particular, the 1899 essay “Affections in Education,”
  reprinted in this volume and translated into Japanese in 1920 by the
  feminist socialist Kikue Yamakawa.
- Hiromi Tsuchiya Dollase, *Age of Shōjo: The Emergence, Evolution,
  and Power of Japanese Girls’ Magazine Fiction*. *Age of Shōjo* is a
  book-length treatment of Class S culture and literature, including
  how it was influenced by Western literature in translation.
- Sarah Frederick, “Not That Innocent: Nobuko Yoshiya’s Good Girls.”
  Frederick provides an overview of Yoshiya’s life and work, with a
  particular focus on her novel *Yaneura no nishojo* (*Two Virgins in
  the Attic*), for which Frederick provides a detailed plot
  description and translates several passages. (Frederick is also
  working on a book project focusing on Yoshiya’s work in the context
  of twentieth-century Japanese literature and history.)
- Gregory M. Pflugfelder, “‘S’ is for Sister: School Girl Intimacy and
  ‘Same-Sex Love’ in Early Twentieth-Century Japan.” Pflugfelder
  provides a good overview of Class S culture from the late nineteenth
  century to World War II, emphasizing how it was perceived by
  sexologists, journalists, and feminists. The paper also references
  interviews with several women who had first-hand experience of S
  relationships while schoolgirls. Unfortunately, Pflugfelder does not
  quote the women at length---ironic since the last line of the paper
  laments that Japanese schoolgirls were “more often spoken to than
  listened to.”
- Deborah Shamoon, *Passionate Friendship: The Aesthetics of Girls’
  Culture in Japan*. This is another book-length treatment of Class S
  culture and literature, including a discussion of its postwar fate.
- Nobuko Yoshiya, *Yellow Rose*. The only work by Nobuko Yoshiya
  available in an official English translation (by Sarah Frederick),
  this story from *Hana monogatari* gives a taste of Yoshiya’s
  sentimental themes and ornate prose.
{:.bibliography}

### Yuri Literature and Criticism

- Nicki Bauman, *The Holy Mother of Yuri*. This regularly-updated blog
  covers yuri-related news stories and reviews yuri works, including
  *dōjinshi* and visual novels released in English.  Bauman has also
  published on the demographics of the yuri readership (see the
  bibliography).
- Erica Friedman (ed.), *Okazu*. Written by Friedman with occasional
  guest posts, this is the most well-known and authoritative blog
  covering the yuri genre and related topics. It features regular
  reviews of yuri manga and light novels (in both Japanese and English
  editions) and news reports on yuri-related events.
- Erica Friedman, *By Your Side: The First 100 Years of Yuri Manga and
  Anime*. Friedman’s recently-published book is the most comprehensive
  history of the yuri genre yet, with essays discussing topics ranging
  from Nobuko Yoshiya and Class S literature to current yuri works and
  classic yuri tropes.
- Verena Maser, “Beautiful and Innocent: Female Same-Sex Intimacy in
  the Japanese Yuri Genre.” To my knowledge, Maser’s 2015 PhD
  dissertation is the most extensive academic treatment of the yuri
  genre in English. It discusses the disputed boundaries of the genre,
  its history, how yuri manga and related works are produced and
  distributed, and the results of a survey of yuri fans.
- Yuricon, “Essays.” Part of the Yuricon website that hosts the
  *Okazu* blog, this web page contains a comprehensive collection of
  links to yuri-related resources in English. It is a recommended
  starting point for anyone interested in exploring the history of the
  yuri genre.
{:.bibliography}

### Lesbianism and Feminism in Japan

- Sharon Chalmers, *Emerging Lesbian Voices from Japan*. This 2002
  book is a pioneering treatment in English of the lives of Japanese
  lesbians, marred in places by excessive academic jargon.  It
  includes extensive quotes from women interviewed by Chalmers.
- Hiroko Kakefuda, *On Being a “Lesbian”* (*“Rezubian” de aru to iu
  koto*). Far from being a dry academic work of theory, Kakefuda’s
  1992 book is eminently readable, a combination of personal memoir
  and exploration of various topics around being a lesbian in Japan.
- Mark McClelland, Katsuhiko Suganuma, and James Welker, eds., *Queer
  Voices from Japan: First-Person Narratives from Japan’s Sexual
  Minorities*. *Queer Voices from Japan* is a collection of interviews
  and first-person articles discussing the lives of LGBTQ individuals
  in postwar Japan. Several chapters provide glimpses into the lives
  of Japanese lesbians from the 1950s to the 1990s. The book also
  includes insider accounts of the early years of the Japanese lesbian
  feminist movement, for anyone wanting more detail than is found in
  James Welker’s paper (referenced below).
- James Welker, “From Women’s Liberation to Lesbian Feminism in Japan:
  *Rezubian Feminizumu* within and beyond the *Ūman Ribu* Movement in
  the 1970s and 1980s.” This is a good overview of the early history
  of the lesbian feminist movement in Japan, including a discussion of
  how it was influenced by the contemporary lesbian feminist movement
  in the US.
{:.bibliography}

### Other Works of Interest

- David Chapman and Karl Jakob Krogness, eds., *Japan’s Household
  Registration System and Citizenship:* Koseki, *Identification, and
  Documentation*. Reading detailed discussions of the history and
  application of Japanese laws and regulations relating to personal
  identity and household registration can be a tough slog. However,
  this volume is invaluable as an inside look into how the Japanese
  state regulates the lives of individuals and their families,
  especially those in marginalized groups.
- Marius B. Jansen, *The Making of Modern Japan*. It’s difficult to
  make sense of twentieth and twenty-first-century Japanese social and
  literary movements without some knowledge of Japanese history. You
  can skip the portion of the book covering the Tokugawa shogunate if
  you’re impatient, but the discussion of the Meiji, Taishō, and Shōwa
  eras provides helpful background for the Class S and yuri genres and
  Japanese popular culture in general.
- kyuuketsukirui, “Don’t Want to Know What I’ll Be without You.”
  Unfortunately, there’s not a lot of *Sweet Blue Flowers* fan
  fiction; other yuri works like *Bloom Into You* and the *Kase-San*
  series have an order of magnitude more fan-written stories. Of the
  fan fiction that does exist, this is my favorite: almost four
  hundred words that provide an intimate picture of Akira and the
  nature of her love for Fumi.
- Yoshio Sugimoto, *An Introduction to Japanese Society*. A
  comprehensive overview of various aspects of Japanese society,
  including social classes, the educational system, gender and the
  family, politics, ethnicities, religion, and culture. It’s written
  from the viewpoint that Japan is not “uniquely monocultural” but
  rather is “fraught with cultural diversity and class competition”
  (from the preface). The book was intended to be a university
  textbook; this latest edition adds a concise history of Japan, links
  to online sources and videos, and suggestions for student research
  projects.
{:.bibliography}

### Works in Japanese

- *Eureka*, November 2017. This special issue of a literary magazine
  focuses on the work of Takako Shimura. It includes a nineteen-page
  interview with Shimura and what appears to be a complete
  bibliography of her works.
- Takako Shimura, *Awashima hyakkei*. Set at a girls’ school
  associated with a Takarazuka-like musical theater troupe, this manga
  (initially published online) looks to be Shimura going all-out in
  her fascination with the stage. Begun in 2011, it was on hiatus for
  a few years but recently resumed publication.
- Nobuko Yoshiya, *Hana monogatari*. This is Yoshiya’s famous
  collection of Class S stories published from 1916 to 1924.
- ---------. *Yaneura no nishojo*. Erica Friedman and others have
  hailed this 1919 novel as the first yuri (or proto-yuri) work.
{:.bibliography}
