---
title: "Yasuko Acts Out"
---

## Yasuko Acts Out

If you read (only) the first volume of *Sweet Blue Flowers*, or if
you’ve only watched the anime adaptation, you’d be forgiven for
thinking that Yasuko Sugimoto was the most important character other
than Fumi Manjome. Although Akira Okudaira is on the cover, through
most of the first volume Akira primarily serves as a companion for
Fumi, lending her a sympathetic ear and a shoulder to cry on. Fumi’s
coming out drives the plot in this volume, and Yasuko is the key
driver of that development.
{:.first}

With that in mind, it’s worth thinking about who Yasuko actually is
and what she represents. What Yasuko represents is relatively easy to
discern: she’s (at least superficially) the *Sweet Blue Flowers*
version of the “girl prince” archetype, whose evolution over the years
has been explored by Erica Friedman.[^02-10-01]

[^02-10-01]: Erica Friedman, “Overthinking Things 05/03/2011: 40 Years of the Same Damn Story, Part 2,” *The Hooded Utilitarian* (blog), May 2, 2011, [https://&#x200B;www&#x200B;.hoodedutilitarian&#x200B;.com&#x200B;/2011&#x200B;/05&#x200B;/21840](https://www.hoodedutilitarian.com/2011/05/21840)

The girl prince’s attributes include a somewhat masculine presentation
and appearance (Yasuko’s short hair and above-average height),
excellence at sports or other pursuits not thought of as traditionally
feminine (Yasuko is a star on Matsuoka’s basketball team), a somewhat
cool and distant attitude (most pronounced in Yasuko’s relationship
with Kyoko Ikumi), and attractiveness to other girls and women (as
noted in my previous discussion of the *Wuthering Heights*
production).

As Friedman notes, a classic example of the girl prince is Haruka
Tenoh, Sailor Uranus in *Sailor Moon*, dashing race-car driver and
lover of Michiru Kaioh (Sailor Neptune). The archetype is common
enough to be parodied, for example, in the character of Yū Kashima in
*Monthly Girls’ Nozaki-kun*,[^02-10-02] or to be critically
interrogated, for example, with Utena Tenjoh in *Revolutionary Girl
Utena*.

[^02-10-02]: Izumi Tsubaki, *Monthly Girls’ Nozaki-kun*, trans. Leighann Harvey, 12 vols. (New York: Yen Press, 2015--).

I believe that the character of Yasuko is an example of the latter.
More specifically, *Sweet Blue Flowers* presents several examples of
common yuri tropes and then inverts or critiques them (see, for
example, my previous comments on Fumi and Chizu). As far as the
students at Matsuoka and Fujigaya are concerned, Yasuko totally has
her act together, just like any other proper girl prince.

But it becomes clear from the events of the first volume that Yasuko
is as messed-up as any teenaged girl, veering between an emotional
breakdown when Mr. Kagami appears at the *Wuthering Heights* cast
party and rebelliousness and feigned bravado during Fumi’s visit to
the Sugimoto home (*SBF*, 1:249--51, 1:306--8). Even her sexual
orientation gets questioned (somewhat cruelly) by her sisters: “So
you’re a lesbian? ... So *at the moment*, this is who you like?
... Then I guess you’re bisexual, huh?” (1:307--8).

At the same time, Yasuko can step back and diagnose her situation and
accept some responsibility for herself and her actions toward
others. See, for example, her comments toward the elementary school
student starring in *The Little Prince*, in which Yasuko may be
comparing herself to Heathcliff: “a troubled man,” prone to “cause
other people trouble,” but not exactly a “bad guy” (*SBF*, 1:225--27).

She also acknowledges to Fumi that “My feelings are a mess ... and I
took it out on you,” and tells her they should stop seeing each
other---although even here it’s not clear at all that Yasuko is
entirely forthcoming about the background to all this (*SBF*,
1:320). It’s Akira, not Fumi, that figures out the connection with
Mr. Kagami (1:347).

But, in the end, Yasuko is a failure as a girl prince. Where in
another yuri work she might end up as the senior partner in a
foregrounded relationship with a junior girl, here she seems to
represent a dead end for Fumi. And not just for Fumi either: the end
of the volume sees Fumi and Kyoko commiserating with each other over
their being “dumped” by Yasuko (*SBF*, 1:375--76). Whatever part
Yasuko is destined to play in the remainder of *Sweet Blue Flowers*,
it will not be that of the prince.
