---
title: "Appendix 4: Reviews"
---

## Appendix 4: Reviews

Now that I’ve finished my comments on *Sweet Blue Flowers*, let’s take
a look at what other people thought of it. This appendix contains a
not-quite-comprehensive list of reviews of volumes 1 through 4 of the
VIZ Media edition, omitting video reviews and reader reviews posted to
online bookstores and book review websites.
{:.first}

### Volume 1 {#appendix-4-reviews-volume-1}

Rose Bridges at Anime News Network. ANN is the most prominent anime
news and review site; they also do a fair number of manga
reviews. Bridges gave *Sweet Blue Flowers* an overall B+ grade, with a
B for story and an A for art. “Overall, this release is an excellent
way to dive into a yuri manga that’s a cut above the rest. *Sweet Blue
Flowers* still has plenty of its genre’s trappings, but also enough
bite for those seeking something more realistic.”[^07-04-01]

[^07-04-01]: Rose Bridges, review of *Sweet Blue Flowers*, vol. 1, by Takako Shimura, Anime News Network, October 20, 2017, [https://&#x200B;www&#x200B;.animenewsnetwork&#x200B;.com&#x200B;/review&#x200B;/sweet&#x200B;-blue&#x200B;-flowers&#x200B;-2&#x200B;-in&#x200B;-1&#x200B;-edition&#x200B;/gn&#x200B;-1&#x200B;/&#x200B;.122727](https://www.animenewsnetwork.com/review/sweet-blue-flowers-2-in-1-edition/gn-1/.122727).

Ash Brown at *Experiments in Manga*. A generally favorable review that
highlights Shimura’s artwork and its relation to theatrical
performance, as well as the realism of character actions and
interactions. “*Sweet Blue Flowers* is a wonderful series. The manga
is emotionally resonate, with a realistic portrayal of the experiences
of young women who love other young women.”[^07-04-02]

[^07-04-02]: Ash Brown, review of *Sweet Blue Flowers*, vol. 1, by Takako Shimura, *Experiments in Manga* (blog), October 27, 2017, [http://&#x200B;experimentsinmanga&#x200B;.mangabookshelf&#x200B;.com&#x200B;/2017&#x200B;/10&#x200B;/sweet&#x200B;-blue&#x200B;-flowers&#x200B;-omnibus&#x200B;-1](http://experimentsinmanga.mangabookshelf.com/2017/10/sweet-blue-flowers-omnibus-1).

Alex Cline at Adventures in Poor Taste. A generally favorable review
on a website focusing on popular culture. Cline liked the characters
and how they were handled, and thought the artwork stood out. One
criticism he voiced was regarding a lack of clarity in some scenes
regarding who was talking and where the scenes fit in the overall
timeline. “Overall, *Sweet Blue Flowers* Vol. 1 is a solid start for
the series. The characters are likable and well introduced, and the
artwork throughout is beautiful. With that said, none of the volume’s
more emotional moments are very memorably so. This is a volume that
shows promise and generates enough interest to warrant giving the next
installment a look, but it doesn’t quite reach greatness as is. I
would recommend it, but not enthusiastically so.”[^07-04-03]

[^07-04-03]: Alex Cline, review of *Sweet Blue Flowers*, vol. 1.

Amelia Cook at *Otaku USA*. *Otaku USA* is a print and online magazine
covering anime and manga; Cook is also the founder of the Anime
Feminist website. Her review is favorable. She particularly calls out
the depiction of the four main characters (Akira, Fumi, Yasuko, and
Kyoko) as realistic and nuanced. She rated *Sweet Blue Flowers* as
“recommended.” “*Sweet Blue Flowers* [paints] a picture of everyday
life with complicated young women going through important formative
experiences. You’ll end the 400-page volume rooting for them all to
have a happy ending.”[^07-04-04]

[^07-04-04]: Amelia Cook, review of *Sweet Blue Flowers*, vol. 1, by Takako Shimura, *Otaku USA*, December 9, 2017, [https://&#x200B;www&#x200B;.otakuusamagazine&#x200B;.com&#x200B;/sweet&#x200B;-blue&#x200B;-flowers&#x200B;-review](https://www.otakuusamagazine.com/sweet-blue-flowers-review).

Leroy Douresseaux at The Comic Book Bin. A favorable review (score 8
out of 10) on a general comics site. “Fans of yuri and shojo romance
will want to smell the *Sweet Blue Flowers*.”[^07-04-05]

[^07-04-05]: Leroy Douresseaux, review of *Sweet Blue Flowers*, vol. 1, by Takako Shimura, The Comic Book Bin, October 3, 2017, [http://&#x200B;www&#x200B;.comicbookbin&#x200B;.com&#x200B;/sweetblueflowers001&#x200B;.html](http://www.comicbookbin.com/sweetblueflowers001.html).

EyeSpyeAlex \[Alexandra Nutting\] at The Geekly Grind. A favorable
review on a website focused on anime, manga, and video games. “At the
end of the day, I really enjoy *Sweet Blue Flowers*. The characters
feel real and have a depth and complexity to their lives. While the
visuals could be a little more striking, it fits the down to earth
tone of the manga.”[^07-04-06]

[^07-04-06]: EyeSpyeAlex \[Alexandra Nutting\], review of *Sweet Blue Flowers*, vol. 1, by Takako Shimura, The Geekly Grind, October 7, 2017, [http://&#x200B;www&#x200B;.thegeeklygrind&#x200B;.com&#x200B;/sweet&#x200B;-blue&#x200B;-flowers&#x200B;-part&#x200B;-one](http://www.thegeeklygrind.com/sweet-blue-flowers-part-one).

Erica Friedman at *Okazu*. Friedman is one of the most well-known
promoters and reviewers of yuri manga and anime, and hers is the
single most authoritative site in English for yuri-related news. She
also did previous reviews of the Japanese edition of *Aoi hana*. In
this review she rated volume 1 as 8 out of 10 overall, with art and
characters at 8 and story and “yuri” at 7. “Although the opening and
the ending are---in my opinion---very weak, the rest of the story is
excellent. It’s got surprising depth and breadth. Characters that
surround Fumi and Akira are as well-developed as they and as
interesting.”[^07-04-07]

[^07-04-07]: Erica Friedman, review of *Sweet Blue Flowers*, vol. 1.

Sean Gaffney at *A Case Suitable for Treatment*. A favorable review
from a manga-focused site. Gaffney acknowledges that the long delay in
bringing out a complete official translation of *Sweet Blue Flowers*
makes it seem less distinctive compared to more recent works like
*Bloom Into You* or *Kiss and White Lily for My Dearest Girl*. “*Sweet
Blue Flowers* is absolutely worth reading and checking out, both if
you like yuri and if you like Takako Shimura. It’s also only four
volumes, so shouldn’t devastate your bookshelf too much.”[^07-04-08]

[^07-04-08]: Sean Gaffney, review of *Sweet Blue Flowers*, vol. 1, by Takako Shimura, *A Case Suitable for Treatment* (blog), September 30, 2017, [http://&#x200B;suitablefortreatment&#x200B;.mangabookshelf&#x200B;.com&#x200B;/2017&#x200B;/09&#x200B;/30&#x200B;/sweet&#x200B;-blue&#x200B;-flowers&#x200B;-omnibus&#x200B;-1](http://suitablefortreatment.mangabookshelf.com/2017/09/30/sweet-blue-flowers-omnibus-1).

Helen at TheOASG. A generally favorable review at a group anime and
manga blog, albeit with some concerns expressed about the use of yuri
tropes, possible queer-baiting, and the reaction by Yasuko’s family to
her and Fumi’s relationship being unrealistic. Helen rated *Sweet Blue
Flowers* at 3 out of 5. “*Sweet Blue Flowers* ... treats its
characters as people, not characters created for the reader’s gaze but
real teenaged girls dealing with the always overly-complicated world
of high school. But it still remains to be seen just how many times
these girls have their hearts broken and mended by the time they
graduate.”[^07-04-09]

[^07-04-09]: Helen, review of *Sweet Blue Flowers*, vol. 1, by Takako Shimura, TheOASG, December 7, 2017, [https://&#x200B;www&#x200B;.theoasg&#x200B;.com&#x200B;/reviews&#x200B;/manga&#x200B;/sweet&#x200B;-blue&#x200B;-flowers&#x200B;-volume&#x200B;-1&#x200B;-review](https://www.theoasg.com/reviews/manga/sweet-blue-flowers-volume-1-review).

Chuck Hodgin at *School Library Journal*. A very favorable review that
was highlighted on the web pages for *Sweet Blue Flowers* on two
online bookstores. “These collected first volumes of Shimura’s manga
tell an honest, poignant story about the joys, pains, and loves of gay
and bisexual young women. ... A no-brainer for yuri (manga focusing on
lesbian romance) fans, but strong enough to recommend to romance
readers and general manga enthusiasts.”[^07-04-10]

[^07-04-10]: Chuck Hodgin, review of *Sweet Blue Flowers*, vol. 1, by Takako Shimura, in Kent Turner, “25 LGBTQAI+ Titles for Pride Month---and Onward,” *School Library Journal*, June 12, 2018, [https://&#x200B;www&#x200B;.slj&#x200B;.com&#x200B;/&#x200B;?detailStory&#x200B;=25&#x200B;-lgbtqai&#x200B;-titles&#x200B;-celebrate&#x200B;-pride](https://www.slj.com/?detailStory=25-lgbtqai-titles-celebrate-pride).

Terry Hong at *BookDragon*. A favorable review on a blog hosted by the
Smithsonian Asian Pacific American Center. “For impatient readers, an
11-part anime adaptation debuted in 2009. For manga purists who
appreciate the gorgeous art on the page, Shimura has never
disappointed.”[^07-04-11]

[^07-04-11]: Terry Hong, review of *Sweet Blue Flowers*, vol. 1, by Takako Shimura, *BookDragon* (blog), December 22, 2017, [https://&#x200B;smithsonianapa&#x200B;.org&#x200B;/bookdragon&#x200B;/sweet&#x200B;-blue&#x200B;-flowers&#x200B;-vol&#x200B;-1&#x200B;-takako&#x200B;-shimura&#x200B;-translated&#x200B;-adapted&#x200B;-john&#x200B;-werry](https://smithsonianapa.org/bookdragon/sweet-blue-flowers-vol-1-takako-shimura-translated-adapted-john-werry).

livresdechevet at *More Bedside Books*. A generally favorable review
that focuses in particular on translation issues and changes from
previous digital releases of volume 1. “All in all *Sweet Blue
Flowers* is an enduring series about maturing and girls in love with
other girls finally receiving print treatment in English. ... Whether
someone is familiar with the genre and history or not it’s a story
with characters that can reach out to teenagers as well as older
readers.”[^07-04-12]

[^07-04-12]: livresdechevet \[pseud.\], review of *Sweet Blue Flowers*, vol. 1, by Takako Shimura, *More Bedside Books* (blog), accessed February 12, 2022, [https://&#x200B;morebedsidebooks&#x200B;.tumblr&#x200B;.com&#x200B;/post&#x200B;/166815771350&#x200B;/sweet&#x200B;-blue&#x200B;-flowers&#x200B;-1&#x200B;-english&#x200B;-viz](https://morebedsidebooks.tumblr.com/post/166815771350/sweet-blue-flowers-1-english-viz).

Susan at Lesbrary. A favorable review on a website focused on lesbian
literature. She notes that the characters both look and behave like
typical teenagers, and praises the scenes of Fumi and Yasuko coming
out as both realistic and unusual in manga. “I really recommend *Sweet
Blue Flowers*. It’s cute and emotional, and is a marginally more
realistic depiction of teenage romantic drama than I expect from
manga!”[^07-04-13]

[^07-04-13]: Susan, review of *Sweet Blue Flowers*, vol. 1, by Takako Shimura, Lesbrary, January 9, 2019, [https://&#x200B;lesbrary&#x200B;.com&#x200B;/susan-reviews-sweet-blue-flowers-by-takako-shimura/](https://lesbrary.com/susan-reviews-sweet-blue-flowers-by-takako-shimura/).

### Volume 2 {#appendix-4-reviews-volume-2}

Melina Dargis at The Fandom Post. A lukewarm to favorable review.
She gave it a grade B for content, B for art, A for packaging, and A
for text/translation. “While reading the second volume of this manga,
I kept feeling lost in the story. There seemed to be a lot of jumping
around and some reading in between the lines that perhaps I was just
not good at it. The main story does come through, but with such a
large cast of characters, it’s was hard to follow what was in
everyone’s heart. That distraction is why I gave the content a lower
grade. ... Yet, beyond everything that could have been better, this
story is a story that is desperately needed for not only manga, but
mainstream reading in general.”[^07-04-14]

[^07-04-14]: Melina Dargis, review of *Sweet Blue Flowers*, vol. 2, by Takako Shimura, The Fandom Post, April 11, 2018, [https://&#x200B;www&#x200B;.fandompost&#x200B;.com&#x200B;/2018&#x200B;/04&#x200B;/11&#x200B;/sweet&#x200B;-blue&#x200B;-flowers&#x200B;-vol&#x200B;-02&#x200B;-manga&#x200B;-review](https://www.fandompost.com/2018/04/11/sweet-blue-flowers-vol-02-manga-review).

Leroy Douresseaux at The Comic Book Bin. A favorable review: grade A,
score 8 out of 10.[^07-04-15]

[^07-04-15]: Leroy Douresseaux, review of *Sweet Blue Flowers*, vol. 2, by Takako Shimura, The Comic Book Bin, January 8, 2018, [http://&#x200B;www&#x200B;.comicbookbin&#x200B;.com&#x200B;/sweetblueflowers002&#x200B;.html](http://www.comicbookbin.com/sweetblueflowers002.html).

EyeSpyeAlex \[Alexandra Nutting\] at The Geekly Grind. A very
favorable review, with volume 2 rated 9 out of 10 overall, 8.6 for
story, 9.5 for art, and 9 for character. “Despite having an unclear
storyline at times, I really enjoy *Sweet Blue Flowers*. Each volume
is longer than your typical manga volume, which is nice as it means
the story doesn’t need to rely on cliff hangers. The soft art style
and simple dialogue gives this manga a calming effect. *Sweet Blue
Flowers* is by no means a page turning thriller, and I appreciate
that. It’s nice to have a manga to read that I can enjoy for its’
[sic] relaxing story.”[^07-04-16]

[^07-04-16]: EyeSpyeAlex \[Alexandra Nutting\], review of *Sweet Blue Flowers*, vol. 2, by Takako Shimura, The Geekly Grind, January 5, 2018, [http://&#x200B;www&#x200B;.thegeeklygrind&#x200B;.com&#x200B;/sweet&#x200B;-blue&#x200B;-flowers&#x200B;-part&#x200B;-two](http://www.thegeeklygrind.com/sweet-blue-flowers-part-two).

Erica Friedman at *Okazu*. Friedman rated volume 2 as 8 out of 10
overall (same as volume 1), with art and characters at 8, story at 7,
and “lesbian” at 4. (Friedman usually assigns works a “yuri” score,
as she did for volume 1. I presume she switched it to “lesbian” here
because she thought *Sweet Blue Flowers* is not a typical yuri work.)
“This is an excellent English release and I think we can expect it to
maintain this high quality. ... If you haven’t already picked up this
‘new classic’ of Yuri, I definitely recommend it, for having a depth
of early 20th century literary history and still being grounded in the
present.”[^07-04-17]

[^07-04-17]: Erica Friedman, review of *Sweet Blue Flowers*, vol. 2, by Takako Shimura, *Okazu* (blog), January 8, 2018, [https://&#x200B;okazu&#x200B;.yuricon&#x200B;.com&#x200B;/2018&#x200B;/01&#x200B;/08&#x200B;/yuri&#x200B;-manga&#x200B;-sweet&#x200B;-blue&#x200B;-flowers&#x200B;-volume&#x200B;-2&#x200B;-english](https://okazu.yuricon.com/2018/01/08/yuri-manga-sweet-blue-flowers-volume-2-english).

Sean Gaffney at *A Case Suitable for Treatment*. A generally favorable
review, though not as favorable as for volume 1. “*Sweet Blue Flowers*
is a good series. That said, it’s exhausting as well, and I suspect
that it’s best enjoyed either in one gulp---waiting till the other two
omnibuses are out---or in smaller quantities, such as reading only
half and then coming back. There is such a thing as too much
Fumi. (And too \[sic\] be fair, too much Akira, though that’s slightly
less pressure-heated.)”[^07-04-18]

[^07-04-18]: Sean Gaffney, review of *Sweet Blue Flowers*, vol. 2, by Takako Shimura, *A Case Suitable for Treatment* (blog), December 22, 2017, [http://&#x200B;suitablefortreatment&#x200B;.mangabookshelf&#x200B;.com&#x200B;/2017&#x200B;/12&#x200B;/22&#x200B;/sweet&#x200B;-blue&#x200B;-flowers&#x200B;-omnibus&#x200B;-2](http://suitablefortreatment.mangabookshelf.com/2017/12/22/sweet-blue-flowers-omnibus-2).

Susan at Lesbrary. A favorable review that praises the variety of
relationships shown (“healthy and unhealthy relationships,
reciprocated feelings and not”). “It’s still a good series and I
really need to know where it’s going next, because I just want all of
these girls to be happy!”[^07-04-19]

[^07-04-19]: Susan, review of *Sweet Blue Flowers*, vol. 2, by Takako Shimura, Lesbrary, February 13, 2019, [https://&#x200B;lesbrary&#x200B;.com&#x200B;/susan-reviews-sweet-blue-flowers-volume-2-by-takako-shimura/](https://lesbrary.com/susan-reviews-sweet-blue-flowers-volume-2-by-takako-shimura/).

### Volume 3 {#appendix-4-reviews-volume-3}

Melina Dargis at The Fandom Post. Dargis gave volume 3 a grade B for
content, B- for art, A- for packaging, and A for text/translation.
“From a reviewer’s point of view, this story is difficult to summarize
its content. In one chapter, it easily has up to five several short
scenes that are only a few pages in length. This is very different
from most manga where a chapter usually focuses on a specific
character or situation. This is nice for reading, however. What is
also nice about this style of storytelling is that it feels like it’s
more like watching a story versus reading one. It also keeps the story
interesting, moving and more in depth, but for some it might be
distracting and confusing.”[^07-04-20]

[^07-04-20]: Melina Dargis, review of *Sweet Blue Flowers*, vol. 3, by Takako Shimura, The Fandom Post, April 11, 2018, [https://&#x200B;www&#x200B;.fandompost&#x200B;.com&#x200B;/2018&#x200B;/12&#x200B;/08&#x200B;/sweet&#x200B;-blue&#x200B;-flowers&#x200B;-vol&#x200B;-03&#x200B;-manga&#x200B;-review](https://www.fandompost.com/2018/12/08/sweet-blue-flowers-vol-03-manga-review).

EyeSpyeAlex \[Alexandra Nutting\] at The Geekly Grind. A very
favorable review: rated 9.1 out of 10 overall, 8.8 for story, 9.2 for
art, and 9.3 for characters. “Funny, endearing, and a little
sexy. Those are the words I would use to describe the latest volume of
*Sweet Blue Flowers*.”[^07-04-21]

[^07-04-21]: EyeSpyeAlex \[Alexandra Nutting\], review of *Sweet Blue Flowers*, vol. 3, by Takako Shimura, The Geekly Grind, March 25, 2018, [http://&#x200B;www&#x200B;.thegeeklygrind&#x200B;.com&#x200B;/sweet&#x200B;-blue&#x200B;-flowers&#x200B;-volume&#x200B;-4&#x200B;-review](http://www.thegeeklygrind.com/sweet-blue-flowers-volume-4-review) \[sic\].

Erica Friedman at *Okazu*. Friedman rated volume 3 as 8 out of 10
overall (the same score as volumes 1 and 2), with art and characters
at 8, story at 7, “lesbian” at 6, and “service” at 1. “This volume is,
in my opinion the strongest of what Viz will release as four
volumes. We can see the progress the young women make as people,
before the story turns back into itself to fulfill the requirements of
a romance series.”[^07-04-22]

[^07-04-22]: Erica Friedman, review of *Sweet Blue Flowers*, vol. 3, by Takako Shimura, *Okazu* (blog), April 11, 2018, [https://&#x200B;okazu&#x200B;.yuricon&#x200B;.com&#x200B;/2018&#x200B;/04&#x200B;/11&#x200B;/yuri&#x200B;-manga&#x200B;-sweet&#x200B;-blue&#x200B;-flowers&#x200B;-volume&#x200B;-3&#x200B;-english](https://okazu.yuricon.com/2018/04/11/yuri-manga-sweet-blue-flowers-volume-3-english).

Sean Gaffney at *A Case Suitable for Treatment*. A generally favorable
review, with qualifications. “*Sweet Blue Flowers* ... also
has the same issues that it’s had before. ... That said, this is still
a very good volume, and since I believe it ends with the fourth book,
there’s no reason for you not to get it so that you can wallow in
pangs of young love once more.”[^07-04-23]

[^07-04-23]: Sean Gaffney, review of *Sweet Blue Flowers*, vol. 3, by Takako Shimura, *A Case Suitable for Treatment* (blog), March 20, 2018, [http://&#x200B;suitablefortreatment&#x200B;.mangabookshelf&#x200B;.com&#x200B;/2018&#x200B;/03&#x200B;/20&#x200B;/sweet&#x200B;-blue&#x200B;-flowers&#x200B;-omnibus&#x200B;-3](http://suitablefortreatment.mangabookshelf.com/2018/03/20/sweet-blue-flowers-omnibus-3).

### Volume 4 {#appendix-4-reviews-volume-4}

EyeSpyeAlex \[Alexandra Nutting\] at The Geekly Grind. A very
favorable review: rated 9.5 out of 10 overall, 9.0 for story, 9.5 for
art, and 9.0 for characters. “As far as finales go, *Sweet Blue
Flowers* nailed theirs. This latest volume had the perfect balance of
drama and resolution. The pacing was great and held my attention the
entire time.”[^07-04-24]

[^07-04-24]: EyeSpyeAlex \[Alexandra Nutting\], review of *Sweet Blue Flowers*, vol. 4, by Takako Shimura, The Geekly Grind, July 2, 2018, [http://&#x200B;www&#x200B;.thegeeklygrind&#x200B;.com&#x200B;/sweet&#x200B;-blue&#x200B;-flowers&#x200B;-volume&#x200B;-4&#x200B;-review&#x200B;-2](http://www.thegeeklygrind.com/sweet-blue-flowers-volume-4-review-2).

Erica Friedman at *Okazu*. Friedman rated volume 4 as 9 out of 10
overall (a slightly higher score than previous volumes), with art,
characters, and story at 9, “service” at 3, and “LGBTQ” at 10. “Here’s
the the \[sic\] thing that’s amazing about *Sweet Blue Flowers*---it
started serialization in 2005. It’s 13 years old. More than a decade
ago it was a beacon of Yuri. In 2018, it’s an important stepping stone
to where we are now, and now that we have a definitive edition for
this in English, it’s time to move forward into a genre that has
matured.”[^07-04-25]

[^07-04-25]: Erica Friedman, review of *Sweet Blue Flowers*, vol. 4.

Sean Gaffney at *A Case Suitable for Treatment*. A generally favorable
review, with some qualifications. “The final volume of *Sweet Blue
Flowers* shows off all the strengths and weaknesses of this particular
series. ... I do think this series ended at just the right length---it
would have been exhausting to carry on for 3--4 more volumes. In the
end, *Sweet Blue Flowers* had its bittersweet moments, but the end
showed that sweetness can win out.”[^07-04-26]

[^07-04-26]: Sean Gaffney, review of *Sweet Blue Flowers*, vol. 4, by Takako Shimura, *A Case Suitable for Treatment* (blog), July 3, 2018, [http://&#x200B;suitablefortreatment&#x200B;.mangabookshelf&#x200B;.com&#x200B;/2018&#x200B;/07&#x200B;/03&#x200B;/sweet&#x200B;-blue&#x200B;-flowers&#x200B;-omnibus&#x200B;-4](http://suitablefortreatment.mangabookshelf.com/2018/07/03/sweet-blue-flowers-omnibus-4).

Jaime at *Yuri Stargirl*. A favorable review with qualifications
regarding the translation and localization. She rates volume 4 as 8
out of 10 (“Highly Recommended”) and the series overall as 9 out of 10
(“Essential”). “Not only is [*Sweet Blue Flowers*] one of my all-time
favorite anime but it is also one of my all-time favorite mangas and
so to finally have the story complete made my week. ... The story,
naturally, is wonderful ... .”[^07-04-27]

[^07-04-27]: Jaime, review of *Sweet Blue Flowers*, vol. 4, by Takako Shimura, *Yuri Stargirl* (blog), June 22, 2018, [https://&#x200B;www&#x200B;.yuristargirl&#x200B;.com&#x200B;/2018&#x200B;/06&#x200B;/sweet&#x200B;-blue&#x200B;-flowers&#x200B;-aoi&#x200B;-hana&#x200B;-vol&#x200B;-4&#x200B;.html](https://www.yuristargirl.com/2018/06/sweet-blue-flowers-aoi-hana-vol-4.html).
