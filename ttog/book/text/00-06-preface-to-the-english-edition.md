---
title: "Preface to the English Edition"
style: frontmatter
---

## Preface to the English Edition

Between 2004 and 2013 the Japanese comic artist Takako Shimura
published her manga *Aoi hana* (“Blue Flower\[s\]”) in the (now
defunct) magazine *Manga Erotics F*. *Aoi hana*, a work in the “yuri”
genre featuring romantic relationships between girls or women,
depicted the high school years of two Japanese teenaged girls, Fumi
and Akira, their renewal of a friendship forged in their childhood,
Fumi’s coming out as a lesbian, her desire to be more than a friend to
Akira, and Akira’s uncertain and halting response to that desire.
*Aoi hana* was collected in a Japanese edition of eight
volumes[^00-06-01] and eventually released in English in a four-volume
omnibus edition as *Sweet Blue Flowers*.[^00-06-02]
{:.first}

[^00-06-01]: Takako Shimura, *Aoi hana*, 8 vols. (Tokyo: Ohta Books, 2006--13).

[^00-06-02]: Takako Shimura, *Sweet Blue Flowers*, trans. John Werry, 4 vols. (San Francisco: VIZ Media, 2017--18). Unless otherwise noted, all citations to *Sweet Blue Flowers* are to this edition, hereafter cited in the text as *SBF*.

As a relatively recent manga fan, I enjoyed reading *Sweet Blue
Flowers* and found myself intrigued by it. It seemed to me that
Shimura set out to create a work that was not just another simple tale
of schoolgirls in love, but that---albeit often messily and
imperfectly---was trying to say something about the yuri genre itself
and about the Japanese society in which that genre arose and was
popularized. This book is my own messy and imperfect attempt to
explore what that something might be---or, at least, what I imagine
it might be.

In writing these notes, I encountered three significant
obstacles. First, I do not know Japanese and must rely on the English
version. Any nuances that are “lost in translation” are lost on me.

Second, I am not young, not a woman, not queer, and not Japanese---in
other words, I’m about as far away from directly identifying with the
characters of this manga and their life experiences as it’s possible
to be. I write as an outsider, with all the limitations that an
outsider has in trying to interpret dialogue, events, and
cultural and social contexts.

Finally, as mentioned above, I am a relatively recent reader of manga,
and I have no education or experience as a critic of literature in
general or manga in particular.

Therefore this book is best thought of as a collection of tentative
and personal answers to idiosyncratic questions that came to me based
on my limited perspective. I welcome comments and corrections that
might improve my understanding and possible future editions of this
book. (See the colophon for details.)

### The plan of the book

In case anyone reading this book is at the same time reading through
the four volumes of *Sweet Blue Flowers*, I’ve divided the book as
follows:

The initial chapters contain minimal spoilers and are intended as
background reading for the series. The chapters in the following four
sections discuss the events of each volume and (with minor
exceptions) contain spoilers for the series only through the end of
that volume. Finally, the concluding chapters contain my final
thoughts on the yuri genre and the place of *Sweet Blue Flowers*
within it.

I have also included other material that may be of interest, including
an index of characters, errata for the VIZ Media edition, a summary of
previously-published reviews, and suggestions for further reading.

I discuss the official English release of *Sweet Blue Flowers* as
published by VIZ Media in print and e-book form; all page references
are to that edition. I do not discuss the anime adaptation except to
compare it with the manga. When I do so, I assume that readers have
seen all eleven episodes of the anime. There were also two previous
authorized English e-book releases of volume 1 of the Japanese
edition, one of which I discuss briefly concerning translation
choices. Occasionally I go back to the Japanese edition to puzzle out
the exact terms that Shimura used.

In discussing the characters, I follow the conventions used in the VIZ
English release: Western order for given name and family name (Akira
Okudaira, not Okudaira Akira) and simplified romanization (Manjome,
not Manjoume or Manjōme). I follow Wikipedia’s conventions for
Japanese names and terms outside the context of *Sweet Blue Flowers*.

I’ve included notes and a bibliography for those wishing to further
explore works that I cite. However, in general I have not included
citations for works available only in Japanese or for information that
can be easily found in Wikipedia or similar online sources.

Finally, some parts of *Sweet Blue Flowers* touch on issues meriting a
content note. I have included such notes when I discuss those issues.

### Resources and inspirations

At this point most authors would acknowledge the contributions of
those who helped them in the writing of their books. However, this
book is a solo effort that I created as a private spare-time
project. I can thus say more truly than most that any faults in it are
mine and mine alone.

But although I cannot include a conventional list of acknowledgments,
I would be remiss in not mentioning those without whom this book would
not exist or would be a more amateurish affair than it already is.

First and foremost, I owe thanks to Takako Shimura, without whom there
would be no *Aoi hana* or *Sweet Blue Flowers* for the world to read
and for me to write about. Thanks also go to the team at VIZ Media who
brought *Aoi hana* to us in English in complete and definitive form as
*Sweet Blue Flowers*: translator John Werry, editor Pancha Diaz,
Monalisa De Asis, who did touch-up art and lettering, and Yukiko
Whitley, who did the design.

To try to remedy my complete lack of knowledge about gender and
sexuality in the context of Japanese history, culture, and society,
and how these are reflected in manga, anime, and other works, I took
advantage of the extensive academic literature produced by scholars in
these fields. Those whose books, papers, and other works I found
particularly useful include (in alphabetical order) Sharon Chalmers,
Hiromi Tsuchiya Dollase, Sarah Frederick, Mark McLelland, Verena
Maser, Gregory Pflugfelder, Jennifer Robertson, Deborah Shamoon,
Michiko Suzuki, and James Welker.

In writing at length about *Sweet Blue Flowers* I am following in the
footsteps of the many Western fans writing online about manga and
anime, especially those who review and critically analyze works in
depth. I particularly single out those whose work I read early in my
time as a manga and anime fan, and continue to read with pleasure
today: the writers and editors of the Anime Feminist website
(“Japanese pop culture through a feminist lens”),[^00-06-03] and Erica
Friedman, creator and editor of the *Okazu* blog and Yuricon website
covering and promoting all things yuri.[^00-06-04]

[^00-06-03]: “About Us,” Anime Feminist, [https://&#x200B;www&#x200B;.animefeminist&#x200B;.com&#x200B;/about](https://www.animefeminist.com/about).

[^00-06-04]: Erica Friedman, ed., *Okazu* (blog), [https://&#x200B;okazu&#x200B;.yuricon&#x200B;.com](https://okazu.yuricon.com).

Finally, in writing down my idiosyncratic views regarding *Sweet Blue
Flowers* I took inspiration from Adam Mars-Jones and his book *Noriko
Smiling*.[^00-06-05] Mars-Jones set himself against previous Western
interpreters of the films of Yasujirō Ozu and proposed a different
take on Ozu’s film *Late Spring*, analyzing its story of a woman in
postwar Japan who did not want to get married and speculating freely
as to why she might have felt that way. Whether his conclusions are
objectively “correct” or not, I can’t help but admire his ambition and
approach.

[^00-06-05]: Adam Mars-Jones, *Noriko Smiling* (London: Notting Hill Editions, 2011).

Takako Shimura is not as great an artist as Ozu, and I am not as good
a writer as Mars-Jones. Nonetheless, I’ve tried to do something
similar in this book, speculating at length about what to my mind
Shimura might be saying in her own story of two twenty-first-century
Japanese schoolgirls.
