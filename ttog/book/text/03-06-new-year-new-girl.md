---
title: "New Year, New Girl"
---

## New Year, New Girl

We’re now into the second half of volume 2 of *Sweet Blue Flowers*
(volume 4 in the original Japanese edition). Fumi’s and Akira’s first
years at their respective high schools have ended, and they are now
second-year students. Yasuko Sugimoto has left the scene (as discussed
in a previous chapter), making room for a new secondary character,
Haruka Ono (*SBF*, 2:182).
{:.first}

Haruka continues the *Sweet Blue Flowers* tradition of having
interesting secondary characters beyond the central duo of Fumi and
Akira. (This isn’t universal, though: Fumi’s Matsuoka friends Mogi,
Yassan, and Pon continue to have relatively little to do, and their
characterization remains thin.)

Haruka performs several critical functions within the story. First,
she’s entertaining in her own right, with her outsized personality and
vocal brashness. She also provides a pair of fresh eyes to view the
goings-on at Fujigaya, especially because she transferred in and did
not attend Fujigaya’s elementary or middle schools.

In this respect and others (including her height), she resembles
Akira. It’s almost as if she’s a younger version of Akira, with her
wide-eyed admiration of all things Fujigaya. She’s even visually
framed the same way as she enters the tunnel leading to the school and
repeats Akira’s comments to herself about being a “Fujigaya lady”
almost word for word (*SBF*, 2:194--95). “She’s just like you were!”
Kyoko marvels (2:195).

One might ask, why do we need an Akira clone when we already have
Akira? The most straightforward answer is that second-year Akira is
much changed from first-year Akira. She’s been thrown for a loop by
Fumi’s interest in her and seems somewhat uncertain and almost lost,
not knowing what to do about the situation and not knowing whom to
turn to for advice and a sympathetic ear. “You seem so distant,” Fumi
tells her (*SBF*, 2:249).

Haruka reminds us of the most appealing traits of Akira: her energetic
and impulsive nature, her sense of justice, and her willingness to
speak and act in support of what she thinks is right. Even their
motivations in attending Fujigaya are similarly ditzy: Akira seems to
have done it almost on a whim, while Haruka was moved by her
admiration of Yasuko---not realizing that Yasuko didn’t attend
Fujigaya (*SBF*, 2:288--90). It’s no surprise that Fumi becomes
friends with Haruka, despite their age difference and their attending
different schools. Many of the things that attract her to Akira
attract her to Haruka as well.

Haruka is also in a way going through a similar self-assessment as
Akira. Just as Fumi’s confession to her prompts Akira to question both
her feelings toward Fumi and her feelings about lesbian relationships
generally, Haruka’s discovery of her sister’s love letter causes her
to think about the issue as well, involving as it does someone close
to her (*SBF*, 2:322).

Haruka also helps drive the plot in various ways, both large and
small. She discovers Ryoko Ueda reading in the library and urges she
be cast in the school play, encourages and counsels Fumi in her
(ultimately unsuccessful) attempt to play a role as well, and then
apologizes to Fumi for her pushiness after Fumi calls it quits (*SBF*,
2:259--60, 2:262, 2:291, 2:294, 2:314--16).

Finally, Haruka’s conversation with Fumi about her sister (“I think my
sister is in love with a girl!”), Fumi’s response to Haruka about that
revelation, and Fumi’s regret over what she feels is the inadequacy of
that response, set up the climactic scene of volume 2, Fumi’s
confession to Akira of her true feelings (*SBF*, 2:328--31,
2:333--38).

Altogether Haruka is a most amusing and appealing character. By the
end of volume 2, she’s become a force to be reckoned with at Fujigaya
(“You sure have a lot of older friends,” remarks Akira) and reinforces
the theme of the subversion of hierarchy that I discussed in previous
chapters (*SBF*, 2:318). Where her character will go in volume 3
remains to be seen, but her presence is certainly one of the
highlights of volume 2.
