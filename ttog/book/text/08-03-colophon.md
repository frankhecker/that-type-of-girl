---
title: "Colophon"
---

## Colophon

This book was created using the Electric Book workflow[^08-03-01] operating
on a set of plain text source files written in the kramdown variant of
the Markdown formatting language.[^08-03-02]
{:.first}

[^08-03-01]: “The Electric Book Template,” Electric Book Works, accessed March 21, 2020, [https://&#x200B;electricbookworks&#x200B;.github&#x200B;.io&#x200B;/electric&#x200B;-book&#x200B;/index&#x200B;.html](https://electricbookworks.github.io/electric-book/index.html).

[^08-03-02]: “kramdown,” Thomas Leitner, updated January 2019, [https://&#x200B;kramdown&#x200B;.gettalong&#x200B;.org](https://kramdown.gettalong.org).

The text for the PDF and print versions is set in a combination of
CrimsonPro[^08-03-03] for body text and Source Sans Pro[^08-03-04] for
display text. The text for the cover is set in Sorts Mill
Goudy[^08-03-05] and Libre Franklin.[^08-03-06]

[^08-03-03]: “CrimsonPro,” Jacques Le Bailly, Sebastian Kosch, et al., accessed May 19, 2020, [https://&#x200B;github&#x200B;.com&#x200B;/Fonthausen&#x200B;/CrimsonPro](https://github.com/Fonthausen/CrimsonPro).

[^08-03-04]: “Source Sans Pro,” Adobe, accessed April 25, 2020, [https://&#x200B;github&#x200B;.com&#x200B;/adobe&#x200B;-fonts&#x200B;/source&#x200B;-sans&#x200B;-pro](https://github.com/adobe-fonts/source-sans-pro).

[^08-03-05]: Barry Schwarz, “Sorts Mill Goudy,” Google Fonts, accessed December 7, 2021, [https://&#x200B;fonts&#x200B;.google&#x200B;.com&#x200B;/specimen&#x200B;/Sorts+Mill+Goudy](https://fonts.google.com/specimen/Sorts+Mill+Goudy).

[^08-03-06]: Impallari Type, “Libre Franklin,” Google Fonts, accessed December 7, 2021, [https://&#x200B;fonts&#x200B;.google&#x200B;.com&#x200B;/specimen&#x200B;/Libre+Franklin](https://fonts.google.com/specimen/Libre+Franklin).

The cover image was created from an original illustration by Ola
Tarakanova,[^08-03-07] using the Pixelmator Pro image processing
software.[^08-03-08]

[^08-03-07]: Ola Tarakanova, “Watercolor Blue Flowers Stock Illustration,” iStock by Getty Images, accessed December 24, 2021, [https://&#x200B;www&#x200B;.istockphoto&#x200B;.com&#x200B;/vector&#x200B;/watercolor&#x200B;-blue&#x200B;-flowers&#x200B;-gm1148509305&#x200B;-310182255](https://www.istockphoto.com/vector/watercolor-blue-flowers-gm1148509305-310182255).

[^08-03-08]: “Pixelmator Pro,” Pixelmator Team, accessed December 7, 2021, [https://&#x200B;www&#x200B;.pixelmator&#x200B;.com&#x200B;/pro](https://www.pixelmator.com/pro).

The Markdown files for the book’s text are available in the
public repository
[https://&#x200B;gitlab&#x200B;.com&#x200B;/frankhecker&#x200B;/that&#x200B;-type&#x200B;-of&#x200B;-girl](https://gitlab.com/frankhecker/that-type-of-girl),
along with instructions and other files needed to create the book’s
PDF and EPUB output files.  Please submit suggested corrections as
issues against the repository or via email to the author.
