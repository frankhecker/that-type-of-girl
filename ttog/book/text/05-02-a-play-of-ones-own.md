---
title: "A Play of One’s Own"
---

## A Play of One’s Own

One of the most effective aspects of *Sweet Blue Flowers* is how
Takako Shimura uses the annual Fujigaya theater festival to move the
plot forward and indirectly comment on the manga’s themes. In volume
4, the Fujigaya drama club’s annual production is a student adaptation
of *The Three Musketeers* set at Fujigaya Women’s Academy itself. It
is joined by an initial effort by the heretofore-dormant drama club at
Matsuoka Girls’ High School, a student play based loosely on Fumi’s
experiences as a young lesbian (*SBF*, 4:167--68, 4:284--85).
{:first}

In terms of themes, we see an overall progression from year to year
towards more Japanese settings, more contemporary timeframes, and more
stories centering female agency and authorship.

The first year’s play was an adaptation of *Wuthering Heights*, a
nineteenth-century novel set in late eighteenth-century England. As
Akira’s father remarked, even though it was not a musical, it had the
general air of a Takarazuka Revue production, with Yasuko playing the
*otokoyaku* role as Heathcliff (*SBF*, 1:236--46). The accompanying
plot highlighted the limitations of the “girl prince” role, as the
relationship between Fumi and Yasuko foundered on Yasuko’s emotional
immaturity and Fumi’s jealousy.

The other plays that year were also adaptations of Western works: the
nineteenth-century American novel *Little Women* and the
twentieth-century French fantasy *The Little Prince* (*SBF*, 1:235).

Akira’s second year at Fujigaya saw a slate of plays all rooted in
Japanese literature and history: the tenth-century tale of Princess
Kaguya, an adaptation of Yasunari Kawabata’s short story “The Dancing
Girl of Izu” (originally published in 1926), and Yukio Mishima’s 1956
play *Rokumeikan*, set in the late nineteenth century (*SBF*, 3:70,
3:75, 3:80--87, 3:92--99, 3:102--3, 3:105--6).

All three of these feature women subject to the attention of or
domination by men: Kaguya is beset by suitors, including the emperor
himself, escaping them only by returning to the Moon. The young Izu
dancer becomes the object of the fantasies of a male university
student. Finally, and most tragically, the ex-geisha Asako loses both
her ex-lover and her son by him due to the political schemes of her
powerful and jealous husband.

*Rokumeikan*, in particular, featured the debut of Kyoko as Asako,
with Asako’s fate---bereft of her true love and trapped in a loveless
marriage---implied to possibly be Kyoko’s own in future, given the
state of her relationship with Ko (*SBF*, 3:88--91). *Rokumeikan* also
saw Akira take her turn on the stage as the ingénue Akiko (3:84--85,
3:92--95), and Fumi try and fail to audition for a part (2:279--86,
2:308--10, 2:313--16). Fumi’s effort nonetheless directly led to her
gaining the friendship of Haruka and indirectly to her confiding in
Hinako, the partner of Haruka’s sister Orie (2:328--31, 2:333-34,
3:48---50, 3:218, 3:224--27, 3:243--44).

Volume 4 sees Akira take over leadership of the Fujigaya drama club,
with Kyoko as her second-in-command. They struggle with deciding what
plays to put on for the high school, middle school, and
elementary school productions. Initially, this seems like a replay of
their first-year experience, as all the candidate plays are again
based on Western works (*SBF*, 4:47--48).

They eventually decide to do *The Three Musketeers*, but Kyoko for one
longs for them to do something original (*SBF*, 4:76--78,
4:88--89). As usual, their putative advisor, Mr. Kagami, is no help at
all, not even showing up for meetings to discuss their plans. But
Hinako attends in his place and makes the key suggestions: to break
with the Takarazuka formula by having the main characters be women,
rather than male characters played by women, and update the setting
from seventeenth-century France to Fujigaya itself (4:165--67).

The resulting performance elicits the disapproval of the nuns who run
Fujigaya and offends some audience members, including Akira’s aunt
Keiko (*SBF*, 4:167, 4:169). Why might this be? After all, *The Three
Musketeers* is a universally popular work, beloved by young and old
alike.

I speculate that the audience’s discomfort arose because the play,
though ostensibly based on a historical novel set in a different time
and place, as adapted and modified per Hinako’s advice could be seen
as an indirect commentary on Fujigaya Women’s Academy itself. By
implication, the play promoted a vision of female agency and liberation
from the constraints of gender that was at odds with the traditions of
Fujigaya, as the action of the original novel---from
spur-of-the-moment fighting to seducing women---was enacted by
Fujigaya students playing a fantasy version of themselves.[^05-02-01]

[^05-02-01]: Although it’s not anti-clerical per se, *The Three Musketeers* also pokes gentle fun at the Church (for example, in the chapter “The Thesis of Aramis” addressing Aramis’s vacillation between being a musketeer and becoming a priest), another possible reason for the Fujigaya nuns’ disapproval if any of that humor made its way into the play. Alexandre Dumas, *The Three Musketeers*, trans. Richard Pevear (New York: Penguin Books, 2007), chapter 26, Kindle.

The girls of the Fujigaya drama club, inspired by a lesbian
teacher,[^05-02-02] thus took a fictional template of “masculine”
adventure and made that story their own. It’s likely not a coincidence
that the other Fujigaya play mentioned in this volume, an adaptation
of *The Diary of Anne Frank*, also features a girl writing her own
story---and in the direst of circumstances.

[^05-02-02]: They were also rescued from a potential disaster by another lesbian, as Orie took off work at Hinako’s request to retrieve costumes that Haruka had accidentally left at home (*SBF*, 4:152--56, 4:161).

That theme of girls writing their own stories continues with the
performance of the Matsuoka drama club of a play, *Heavenly
Creatures*, written by Fumi’s friend Pon and loosely based on Fumi’s
life.[^05-02-03] The roots of that performance lay in the junior high
experiences of Pon, Mogi, and Yassan, in which they eagerly joined the
drama club as first-year students and were warmly welcomed by the
third-year students.

However, after the third-years graduated and the second-year students
assumed their place at the top of the age-based hierarchy, they lorded
it over the three friends and other drama club members. The resulting
discord led to the eventual dissolution of the club (*SBF*,
4:191--98).

[^05-02-03]: “Heavenly Creatures” is also the title of chapter 46 of the manga, which tells the story of the play’s genesis (*SBF*, 4:186). However, in the Japanese edition, both the play and chapter title are *Otome no inori*, which can be literally translated as *A Maiden’s Prayer*. Shimura, *Aoi hana*, 8:6, 8:25, 8:104.

Yassan, in particular, was so discouraged by this experience that she
accepted as fate Matsuoka’s lack of a cultural festival and the fact
that Matsuoka’s drama club consisted only of herself, Mogi, and Pon
(*SBF*, 4:199, 1:23). After Akira’s attempt to lobby the Matsuoka
faculty failed, Fumi did join the drama club, but it remained
relatively moribund (3:271--73).

However, as the girls of the drama club entered their third year at
Matsuoka, two separate events revived the club’s fortunes. First, an
enthusiastic first-year student, inspired by the acting manga *Glass
Mask*, joined the drama club and persuaded her friend to do likewise
(*SBF*, 4:15--18). Together they also lobbied the school’s faculty and
students to support a cultural festival at Matsuoka (4:187--90).

Second, rather than putting on an existing play, the girls of the
drama club created a play of their own, inspired by one of their
own. The previous Christmas, a conversation about boyfriends and a
question from Pon had led to Fumi coming out to her three friends
(*SBF*, 3:304--11). That night Pon began writing a play about a girl
like Fumi and (after asking Fumi’s permission) presented the completed
script as a proposed entry into the theater contest the club planned
to participate in (4:102--5).

Though Pon wrote the play as a (presumed) heterosexual girl exercising
her imagination on what she may have gleaned from observing and
talking to Fumi, Fumi herself found the play to be relatively true to
life (“I kept reading myself into it!”) (*SBF*, 4:106). In fact, lines
from the play (“That small, shy crybaby has died. What’s inside me now
is a strong will to be near the woman I love”) are later echoed by
Fumi herself as interior monologue (4:111--12, 4:185, 4:208,
4:279--80).

The play is an award-winning success, though it is performed only for
the theater contest judges and other contest participants. This
relatively small group of people echoes Fumi’s own coming out, thus
far limited only to her friends and Hinako. Nevertheless, the play
finds its audience in the form of a girl who, mistaking Fumi for the
playwright, tells her how moving she found the performance and script
and how much it resonated with her feelings (*SBF*, 4:284--88).

It’s worth noting here that the appearance of the girl in question
does not conform to any of the traditional yuri types (types which
*Sweet Blue Flowers* itself contains): the tall “*nadeshiko* beauty”
with long black hair (represented in the manga by Fumi), the short
lighter-haired “*genki* girl” (Akira), or the handsome “girl prince”
(Yasuko).

She’s just an ordinary girl, a bit plain, a bit plump. As such, she
represents an intrusion of reality into the stereotypical yuri
fantasy---almost a breaking of the fourth wall in which someone who
could be a typical young Japanese lesbian takes her brief turn upon
the stage. Perhaps she reads yuri manga like *Sweet Blue Flowers*, or
even hopes to be like Takako Shimura and other yuri creators and one
day write and draw them.

Although we as readers don’t learn what Fumi might have told the girl
about her own relationship to the play, it’s clear that despite the
girl’s ordinariness, Fumi sees her as a kindred spirit and even a
potential romantic partner had things worked out differently:
“Wouldn’t it have been nice if we had fallen in love with each other?”
(*SBF*, 4:288).

But back in the reality of the story, Fumi is frustrated at the
breakdown of her relationship with Akira, who is racked with doubts
about her ability to return Fumi’s love and satisfy her desires. These
doubts apparently intensified as Akira read Pon’s play and gained new
insight into Fumi’s feelings, and she felt compelled to voice them to
Fumi. Fumi assured her that she’d be able to move on if they broke up,
and Akira concluded that break up they must indeed (*SBF*, 4:201--7,
4:269).

And there things stand with Fumi and Akira as we enter the concluding
chapters of *Sweet Blue Flowers*.
