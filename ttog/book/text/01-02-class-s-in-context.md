---
title: "Class S in Context"
---

## Class S in Context

As previously noted, *Sweet Blue Flowers* is generally considered to
fall within the genre of “yuri,” that is, manga, anime, and related
works with lesbian themes and content. Before yuri as we know it today
were Class S works, an early twentieth-century literary genre
featuring intense emotional relationships between adolescent
girls---“passionate friendships,” to use Deborah Shamoon’s
phrase.[^01-02-01]

[^01-02-01]: Deborah Shamoon, *Passionate Friendship: The Aesthetics of Girl’s Culture in Japan* (Honolulu HI: University of Hawai‘i Press, 2012).

*Sweet Blue Flowers* harks back to these earlier works, paying homage
to, interrogating, and sometimes parodying their tropes. If we wish to
understand the manga better, it helps to take a closer look at S
relationships and literature and their genesis in the late Meiji and
early Taishō eras (roughly 1900--20).

Erica Friedman has referred to yuri works as “lesbian content without
lesbian identity.”[^01-02-02] However, in the case of Class S works
there are conflicting views on whether the content itself is even
lesbian in nature, especially in the context of the time and how S
relationships were treated by the girls who participated in them and
by society at large.[^01-02-03]

[^01-02-02]: Erica Friedman, “Is Yuri Queer?,” Anime Feminist, June 7, 2019, [https://&#x200B;www&#x200B;.animefeminist&#x200B;.com&#x200B;/feature&#x200B;-is&#x200B;-yuri&#x200B;-queer](https://www.animefeminist.com/feature-is-yuri-queer).

[^01-02-03]: See, for example, Deborah Shamoon’s claim that “terms like ‘lesbian,’ ‘homosexual,’ or *dōseiai* \[same-sex love\] are fraught with political, social, and clinical meanings that do not reflect how the girls themselves talked about their relationships.” Shamoon, *Passionate Friendship*, 35.

This is a controversy I leave to be debated by those more
knowledgeable than I.[^01-02-04] Instead, I turn my attention to a
different question, namely the social circumstances by which there
came to be a Class S genre in the first place. Other people have
investigated this topic in depth; besides Deborah Shamoon, see, for
example, the work of Hiromi Tsuchiya Dollase,[^01-02-05] Gregory
Pflugfelder,[^01-02-06] and Michiko Suzuki.[^01-02-07]

[^01-02-04]: See, for example, Sarah Frederick’s and Erica Friedman’s separate responses to Shamoon. Sarah Frederick, review of *Passionate Friendship: The Aesthetics of Girls’ Culture in Japan*, by Deborah Shamoon, *Mechademia*, October 7, 2013, [https://&#x200B;www&#x200B;.mechademia&#x200B;.net&#x200B;/2013&#x200B;/10&#x200B;/07&#x200B;/book&#x200B;-review&#x200B;-passionate&#x200B;-friendship](https://www.mechademia.net/2013/10/07/book-review-passionate-friendship). Erica Friedman, review of *Passionate Friendship: The Aesthetics of Girls’ Culture in Japan*, by Deborah Shamoon, *Okazu* (blog), February 6, 2014, [https://&#x200B;okazu&#x200B;.yuricon&#x200B;.com&#x200B;/2014&#x200B;/02&#x200B;/06&#x200B;/passionate&#x200B;-friendship&#x200B;-the&#x200B;-aesthetics&#x200B;-of&#x200B;-girls&#x200B;-culture&#x200B;-in&#x200B;-japan](https://okazu.yuricon.com/2014/02/06/passionate-friendship-the-aesthetics-of-girls-culture-in-japan).

[^01-02-05]: Hiromi Tsuchiya Dollase, *Age of Shōjo: The Emergence, Evolution, and Power of Japanese Girls’ Magazine Fiction* (Albany: SUNY Press, 2019).

[^01-02-06]: Gregory M. Pflugfelder, “‘S’ Is for Sister: School Girl Intimacy and ‘Same-Sex Love’ in Early Twentieth-Century Japan,” in *Gendering Modern Japanese History*, ed. Barbara Monoly and Kathleen Uno, 133--90 (Cambridge, MA: Harvard University Asia Center, 2005), [https://&#x200B;doi&#x200B;.org&#x200B;/10&#x200B;.1163&#x200B;/9781684174171&#x200B;_006](https://doi.org/10.1163/9781684174171_006).

[^01-02-07]: Michiko Suzuki, *Becoming Modern Women: Love and Female Identity in Prewar Japanese Literature and Culture* (Palo Alto: Stanford University Press, 2009).

In this chapter, I focus on four aspects of Class S relevant to *Sweet
Blue Flowers*, beginning with the origin of the all-girls schools that
became a staple of Class S and (later) yuri works. These schools
appear in *Sweet Blue Flowers* in the form of Matsuoka Girls’ High
School (Fumi’s school) and (especially) Fujigaya Women’s Academy
(which Akira attends).

After the restoration of imperial rule in 1868 that marked the
beginning of the Meiji era, the leaders of the Japanese government
began a frantic effort to modernize Japan. A critical element of that
modernization was mass education, seen as the key to the success of
the Western powers in creating industrialized societies rich and
powerful enough to dominate the world, including Japan.

Thus as early as 1872 the government attempted to set out a national
plan to create a new Japanese education system. Its goal was that
“education ... shall be so diffused that there may not be a village
with an ignorant family, nor a family with an ignorant member.” Such
an education was not to exclude women: “Learning ... is to be equally
the inheritance of nobles and gentry, farmers and artisans, males and
females.” By the end of the 1870s almost a quarter of girls eligible
for elementary school were attending school, compared to over half of
eligible boys.[^01-02-08]

[^01-02-08]: Benjamin Duke, *The History of Japanese Education: Constructing the National School System, 1872--1890* (New Brunswick NJ: Rutgers University Press, 2009), 71--76, 73, 281.

This period also saw the establishment in Japan of “mission schools”
(founded and run by Christian missionaries) that offered education
through high school---especially important for girls since
state-sponsored schools did not provide them any education past
elementary school. In 1872, Catholic nuns established the first school
for girls (and perhaps one of the inspirations for Fujigaya Women’s
Academy) in Yokohama, less than twenty kilometers northwest of
Kamakura, in which *Sweet Blue Flowers* is set.[^01-02-09]

[^01-02-09]: Joseph L. Van Hecken, *The Catholic Church in Japan Since 1859*, trans. John Van Hoydonck (Tokyo: Herder Agency, 1960), 156--57, [https://&#x200B;archive&#x200B;.org&#x200B;/details&#x200B;/catholic&#x200B;church&#x200B;in&#x200B;0000&#x200B;heck](https://archive.org/details/catholicchurchin0000heck). This school still exists, as the Saint Maur International School, although it is now coeducational.

The mission schools proved very popular with Japan’s emerging
upper-middle class and could command fees for tuition and board as
high as sixty dollars a year.[^01-02-10] For comparison, a
contemporary American visitor to Japan found that “three or four
dollars will cover the cost of food for a month for one person, and
women servants expect only a few dollars in wages for that
time.”[^01-02-11] Sending a girl to a mission school was thus about as
expensive as feeding her or paying the salary of a family servant---no
wonder it was seen as a luxury affordable mainly by affluent
households.

[^01-02-10]: Margaret E. Burton, *The Education of Women in Japan* (New York: Fleming H. Revell, 1914), 56--58, [https://&#x200B;archive&#x200B;.org&#x200B;/details&#x200B;/education&#x200B;women&#x200B;ja&#x200B;00&#x200B;burt&#x200B;uoft](https://archive.org/details/educationwomenja00burtuoft).

[^01-02-11]: Alice Mabel Bacon, *Japanese Girls and Women*, rev. ed. (Boston: Houghton Mifflin, 1919), 311, [https://&#x200B;archive&#x200B;.org&#x200B;/details&#x200B;/japanese&#x200B;girls&#x200B;wom&#x200B;00&#x200B;baco&#x200B;_2](https://archive.org/details/japanesegirlswom00baco_2).

In part because of the cost, the total number of girls educated in
mission schools was relatively low. As of 1909, the total number of
Japanese girls in Catholic mission schools was not quite six thousand,
in twenty-six schools.[^01-02-12] The number of girls in other
Christian mission schools was even smaller: as of 1914, about four
thousand high school girls in total, spread across fifty
schools.[^01-02-13]

[^01-02-12]: Van Hecken, *Catholic Church in Japan*, 181.

[^01-02-13]: Burton, *Education of Women*, 254--55.

Although the mission schools originally had a monopoly on girls’
education beyond elementary school, this was no longer true by the end
of the Meiji era. In 1899 the Japanese government passed a new order
extending girls’ education beyond the previously mandated six years of
elementary school. This led to a rapid increase in the number of girls
in high school, from less than ten thousand at the turn of the century
to almost fifty-six thousand in 1910, including girls in mission
schools and other private schools.[^01-02-14] By 1920 there were over
one hundred twenty-five thousand women in higher girls’
schools.[^01-02-15]

[^01-02-14]: Jason G. Karlin, *Gender and Nation in Meiji Japan: Modernity, Loss, and the Doing of History* (Honolulu: University of Hawai‘i Press, 2014), chap. 4 n49, EPUB, [https://&#x200B;www&#x200B;.academia&#x200B;.edu&#x200B;/42197271&#x200B;/Gender&#x200B;_and&#x200B;_Nation&#x200B;_in&#x200B;_Meiji&#x200B;_Japan&#x200B;_Modernity&#x200B;_Loss&#x200B;_and&#x200B;_the&#x200B;_Doing&#x200B;_of&#x200B;_History](https://www.academia.edu/42197271/Gender_and_Nation_in_Meiji_Japan_Modernity_Loss_and_the_Doing_of_History).

[^01-02-15]: Suzuki, *Becoming Modern Women*, 170n31.

The number of girls graduating from high school remained a relatively
small fraction of the overall population, around five percent of all
girls near the turn of the century, rising only to twenty-five percent
by the end of World War II. Nevertheless, they formed a large enough
group to evolve a distinct culture of their own, a culture that
reflected their middle- and upper-class background and predominantly
urban environment. As an elite group within what was already a
relatively elite population, the mission school girls, in particular,
helped popularize Western notions of individuality and romantic love,
Christian ideals of spiritual love, and the use of Christian symbols
such as the white lily.[^01-02-16]

[^01-02-16]: Shamoon, *Passionate Friendship*, 30--33.

This brings me to the second topic I want to discuss, one that is also
relevant to *Sweet Blue Flowers*, namely marriage customs in general
and arranged marriages in particular. In the Taishō era, four out of
five marriages were arranged; in two out of five marriages, the couple
had never even met before their wedding. Only three percent of
marriages were considered to be “love marriages.”[^01-02-17]

[^01-02-17]: Suzuki, *Becoming Modern Women*, 67--68.

This contrast between Western and Christian ideals absorbed in school
and traditional Japanese marriage arrangements was bound to be a
source of conflict, as vividly described by Alice Mabel Bacon, an
American observer writing in 1891:

> Another difficulty, in fitting the new school system into the
> customs of the people, lies in the early age at which marriages are
> contracted. Before the girl has finished her school course, her
> parents begin to wonder whether there is not danger of her being
> left on their hands altogether, if they do not hand her over to the
> first eligible young man who presents himself. Sometimes the girl
> makes a brave fight, and remains in school until her course is
> finished; more often she succumbs and is married off, bids a weeping
> farewell to her teachers and schoolmates, and leaves the school, to
> become a wife at sixteen, a mother at eighteen, and an old woman at
> thirty.[^01-02-18]

[^01-02-18]: Bacon, *Japanese Girls and Women*, 55.

To me this offers a key to the prevalence of S relationships and the
literature that popularized them: as Yukari Fujimoto claims, it’s
possible that such relationships were attractive to girls in large
part because they were relationships in which they could exercise
almost entirely free choice in selecting their partners---perhaps the
only opportunity in their lives to do so.[^01-02-19] (Where girls were
free to exercise choice in their marriage partners to some extent,
this presumably amounted only to being allowed to reject some
candidates out of those presented to them by their family.)

[^01-02-19]: Yukari Fujimoto, “Where Is My Place in the World? Early Shōjo Manga Portrayals of Lesbianism,” trans. Lucy Frazier, *Mechademia* 9 (2014), 26, [https://&#x200B;doi&#x200B;.org&#x200B;/10&#x200B;.5749&#x200B;/mech&#x200B;.9&#x200B;.2014&#x200B;.0025](https://doi.org/10.5749/mech.9.2014.0025). However, I take issue with Fujimoto’s description of S relationships as a “fantasy of love between girls,” a characterization that could be interpreted as trivializing the reality of these relationships and what they might have meant to the girls themselves.

This may explain the importance in both S relationships and literature
of courtship rituals by which one girl approaches another girl as a
potential partner and the other girl decides whether to accept her
affections, for example, as portrayed in the 1937 novel *Otome no
minato* (*The Girls’ Harbor*).[^01-02-20] These rituals most notably
include letters sent by one girl to another to propose an S
relationship and an exchange of gifts to signify their entering into
the relationship.

[^01-02-20]: Shamoon, *Passionate Friendship*, 38--45. At the time, *Otome no minato* was credited to the Japanese modernist writer (and future Nobel Prize for Literature winner) Yasunari Kawabata, but in fact it was written by Kawabata’s female disciple Tsuneko Nakazato. Deborah Shamoon, “Class S: Appropriation of ‘Lesbian’ Subculture in Modern Japanese Literature and New Wave Cinema,” *Cultural Studies* 35, no. 1, 32--34, [https://&#x200B;doi&#x200B;.org&#x200B;/10&#x200B;.1080&#x200B;/09502386&#x200B;.2020&#x200B;.1844259](https://doi.org/10.1080/09502386.2020.1844259).

Unlike gifts like dowry and bride price in traditional marriage
customs, which are predominantly exchanges between families, here
gifts are exchanged between the girls themselves acting as free
individuals. And while the practice of sending letters to prospective
partners has deep roots in Japanese culture, here such letters are
sent by girls to girls, taking the role that in traditional works like
*The Tale of Genji* was performed by men.

The agency girls showed in pursuing their own choice of partners in an
S relationship is mirrored in the agency they showed in writing their
own stories about such relationships, a third aspect of Class S
culture and literature relevant to *Sweet Blue Flowers*.

As noted above, in the early twentieth century, girls attending high
school were predominantly from the middle and upper class. Their
relative affluence combined with a high degree of literacy made them
an attractive market for literary works targeted at the emerging
demographic of adolescent girls (*shōjo*). These included foreign
works translated and localized for a Japanese audience, most notably
Louisa May Alcott’s *Little Women*.[^01-02-21]

[^01-02-21]: Dollase, *Age of Shōjo*, chap. 1.

However the more significant development was the establishment and
growth of home-grown Japanese magazines featuring stories and articles
of interest to girls. These magazines were edited by men and featured
articles written by men, especially those of a didactic nature that
sought to promote the ideal of girls’ education as preparation for
marriage and motherhood. However, they also become forums to which
girls themselves contributed, in the form of both readers’ comments
and submitted stories, and training grounds for women
writers.[^01-02-22]

[^01-02-22]: Dollase, *Age of Shōjo*, 19--30; Shamoon, *Passionate Friendship*, 48--57.

Finally, let me return to the question with which I started, namely
the extent to which S relationships could be considered lesbian in
nature. Whatever position one might take on this matter in general,
there can be no question that some of the girls in these relationships
were, in fact, lesbians by any reasonable definition, whether they
explicitly identified themselves as such or not (another point we’ll
see echoed in *Sweet Blue Flowers*). As I discuss in the next chapter,
one of those lesbians became the most famous author of Class S
stories.
