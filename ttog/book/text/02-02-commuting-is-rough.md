---
title: "“Commuting is Rough”"
---

## “Commuting is Rough”

*Content note: This chapter discusses sexual harassment.*

No sooner does Akira take leave of her brother than she has another
unpleasant encounter. While she stands on the platform waiting to
board a train to Fujigaya, a salaryman stands too close to her, an
invasion of her personal space not justified by the area being overly
crowded. She then notices another girl (Fumi, though Akira does not
know this) and stands next to her for protection: “She’s so tall! I’ll
stick close to *her*” (*SBF*, 1:13--14).
{:.first}

However, Fumi’s height does not protect her from harassment. Once on
the train, someone (perhaps the same man?) gropes Akira and then
Fumi. Fumi is too embarrassed to say or do anything, but Akira comes
to the rescue, whacking the perpetrator with her schoolbag. Akira
sympathizes with Fumi (“Commuting is rough, *huh*?”) and then accepts
her thanks, after which Fumi takes her leave and walks away crying
(*SBF*, 1:15--17).

Shimura’s audience would, of course, be familiar with the problem of
men groping women and girls on trains (*chikan*), given its prevalence
in Japan. In various surveys, over a quarter to up to seventy percent
of Japanese young and adult women have reported experiencing
groping. The problem is severe enough that several Japanese railway
operators added women-only cars to their trains.[^02-02-01]

[^02-02-01]: Mitsutoshi Horii and Adam Burgess, “Constructing Sexual Risk: ‘Chikan,’ Collapsing Male Authority and the Emergence of Women-Only Train Carriages in Japan,” *Health, Risk &amp; Society* 14, no. 1 (2012), 42, [https://&#x200B;doi&#x200B;.org&#x200B;/10&#x200B;.1080&#x200B;/13698575&#x200B;.2011&#x200B;.641523](https://doi.org/10.1080/13698575.2011.641523).

Akira and Fumi’s plight would also have been familiar to the
schoolgirls of late Meiji-era Japan, whose lives gave rise to *shōjo*
culture and Class S literature. As Japan’s economic growth led to an
increase in Tokyo’s population and the creation of new suburbs to
house new arrivals, railway operators built a series of rail lines to
serve white-collar workers and others commuting to the city
center.[^02-02-02]

[^02-02-02]: Alisa Freedman, “Commuting Gazes: Schoolgirls, Salarymen, and Electric Trains in Tokyo,” *Journal of Transport History* 23, no. 1 (March 2002), 23, [https://&#x200B;doi&#x200B;.org&#x200B;/10&#x200B;.7227&#x200B;/TJTH&#x200B;.23&#x200B;.1&#x200B;.4](https://doi.org/10.7227/TJTH.23.1.4).

Schoolgirls also used these railway lines to travel from their homes
to the rapidly expanding system of public and private high schools for
girls. “From the last decade of the nineteenth century, the number of
female students (*jogakusei*) increased, and the image of the teenage
schoolgirl dressed in *hakama*, wearing hair ribbons, and traversing
Tokyo or its suburbs on a bicycle or by train frequently appeared in
popular literature and the mass media.”[^02-02-03]

[^02-02-03]: Freedman, “Commuting Gazes,” 23.

Commuter trains and streetcars brought together people of all classes
and genders into a shared space: “Passengers holding different tickets
rode together, and the train car became a travelling universe,
grouping unrelated people for a brief moment. For the first time, men
and women of various social classes were forced literally to look upon
each other in new ways.” Not all of this attention was benign. In
particular, “female students became both idealised as model modern
women and eroticised as sexual objects. ... their fashions and figures
could be observed and even evaluated by other passengers on commuter
trains.”[^02-02-04]

[^02-02-04]: Freedman, “Commuting Gazes,” 26, 30.

The obsession of male commuters with commuting schoolgirls found
literary form in Tayama Katai’s 1907 short story “The Girl Watcher.”
The story chronicles the life of a man (an editor at a Tokyo publisher
and former writer of girls’ literature) who spends his daily train
journeys not looking at the scenery but instead finding ways to
surreptitiously observe the young women who share his railway
carriage. In his late thirties, married with two children, he neglects
his wife, whom he thinks has “passed her prime,” though she is only in
her mid-twenties. Instead he indulges in “this bad habit of getting
obsessed with young women.”[^02-02-05]

[^02-02-05]: Tayama Katai, “The Girl Watcher,” in *The Quilt and Other Stories by Tayama Katai*, trans. Kenneth G. Henshall (Tokyo: University of Tokyo Press, 1981), 171, 173. Freedman translates the story’s title as “The Girl Fetish,” on the basis that “the original Japanese \[*Shôjobyô*\] includes the word *byô*, which connotes an illness or psychological disorder.” Freedman, “Commuting Gazes,” 34n4.

The story’s protagonist confines his activities to looking at the
girls. For that, his friends condemn him as a coward: “Now if it was
us, well, we wouldn’t be satisfied with just *thinking* about
them---the force of instinct would soon raise its head, wouldn’t you
say?” They speculate that “he couldn’t fool his instincts, so finally
he had to resort to self-abuse for his pleasures,” and conclude that
“you can’t live unless you follow your instincts!”[^02-02-06]

[^02-02-06]: Katai, “The Girl Watcher,” 174--75.

Other men of the period thought the same, so much so that writers of
the period warned parents not to let their daughters ride the trains
during workers’ commuting hours. To preserve their innocence, a
railway operator introduced a “Flower Train” (*hana densha*),
designated “For Use by Women Only.”[^02-02-07]

[^02-02-07]: Freedman, “Commuting Gazes,” 30--31.

And so it went for the next hundred years, as new generations of
schoolgirls had to deal with the unwanted attentions of new
generations of salarymen and other male workers. Train groping goes
unmentioned in *Sweet Blue Flowers* past the first chapter. However,
it is no doubt an ongoing possibility during the daily commutes of
Fumi, Akira, and their classmates, a low-level but ever-present hazard
in their lives---as Akira is well aware: “I guess they target girls
from Fujigaya” (*SBF*, 1:15).

Like rape, child sexual abuse, and related crimes, “*chikan*
victimisation is likely to damage victims’ sense of self-control over
their own environment and make them believe that they live in a
dangerous world. This constructs women’s fear of crime victimisation
greater than men’s ... [and] discourages women from using public
transport.”[^02-02-08]

[^02-02-08]: Horii and Burgess, “Constructing Sexual Risk,” 44--45.

This discouragement has real consequences for the women involved. In
another country with an even more severe problem with street
harassment, women enrolling in university seek safety at the expense
of their education. To have a significantly safer commuting route,
they choose lower-quality colleges, ranked several places lower than
the colleges that their entrance examination scores would otherwise
qualify them for. They also pay much more in travel expenses to take
safer commuting routes---up to twice what they pay in
tuition.[^02-02-09]

[^02-02-09]: Gorija Borker, “Safety First: Perceived Risk of Street Harassment and Educational Choices of Women,” (job market paper, Department of Economics, Brown University, 2018), [https://&#x200B;data2x&#x200B;.org&#x200B;/wp&#x200B;-content&#x200B;/uploads&#x200B;/2019&#x200B;/11&#x200B;/Perceived&#x200B;Risk&#x200B;Street&#x200B;Harassment&#x200B;and&#x200B;Ed&#x200B;Choices&#x200B;of&#x200B;Women&#x200B;_Borker&#x200B;.pdf](https://data2x.org/wp-content/uploads/2019/11/PerceivedRiskStreetHarassmentandEdChoicesofWomen_Borker.pdf).

This puts women at an economic disadvantage: “Choosing a worse ranked
college is likely to have long-term consequences since college quality
affects a student’s academic training, network of peers, access to
labor opportunities, and lifetime earnings.”[^02-02-10] Although Japan
is overall safer for women, Fumi and Akira will likely also find that
train groping and related street harassment impose an invisible tax
that they must pay as women---a tax made more onerous because Fumi at
least will likely never marry, and hence will not have the economic
support of a husband.

[^02-02-10]: Borker, “Safety First,” 3.

Whether they intend to or not, train gropers also enforce a central
patriarchal norm: every woman must ally with and commit herself to a
“good” man to secure the protection of her person from “bad” men. For
example, in the romantic comedy manga *My Love Story!!* (*Ore
monogatari!!*), the brawny high-school student Takeo Gōda rescues the
petite Rinko Yamato from a train groper, after which she falls for him
and becomes his girlfriend.[^02-02-11]

[^02-02-11]: Kazune Kawahara, *My Love Story!!*, vol. 1, trans. Ysabet Reinhardt MacFarlane and JN Productions (San Francisco: Viz Media, 2014), 1:17--21.

Unlike his *bishōnen* friend Makoto Sunakawa, Takeo is not
conventionally attractive. But his physical strength ensures that he
can protect both himself and Rinko from people and things that might
harm them. In contrast, Rinko cannot even defend herself and must
always depend on Takeo.

When contrasting good and bad men above, I wrote “good” and “bad” in
quotes because which men are good and which are bad can often be
ambiguous. For example, *My Love Story!!* and many other manga and
anime depict train gropers as clearly criminal, unattractive,
perverse, and by implication set apart from the “good guys.”

But in *Sweet Blue Flowers* the man invading Akira’s personal space on
the platform appears perfectly innocuous at first glance, just another
typical salaryman on his way to work. Indeed he may act as a devoted
husband and father at home, just as a man who jumps in to protect
women on the train may beat his wife and children if they anger
him.

By implication, *Sweet Blue Flowers* questions this division of men
into “good” and “bad” and rejects the norm that a woman must ally
herself with one man for protection from others. It is not a man who
comes to the aid of the two girls, but rather Akira who acts to
protect herself and Fumi---and recall that at this point in the story,
Akira is coming to the aid of a girl otherwise unknown to her. To
repeat what I wrote in the previous chapter, whether Shimura
consciously intended this or not, the incidents on the station
platform and the train, in combination with the actions of Akira’s
brother (countered by Akira’s mother), serve “as an example of how
women have reason to distrust men and turn to other women ... in
response for their protection.”
