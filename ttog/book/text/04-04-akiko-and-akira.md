---
title: "Akiko and Akira"
---

## Akiko and Akira

I now come to the actual performance of *Rokumeikan*, in which Akira
Okudaira plays Akiko Daitokuji, the lover of Hisao Kiyohara.
{:.first}

Like almost all the characters in *Rokumeikan*, Akiko is a member of
the Japanese aristocracy. As a marchioness, her mother is the wife of
a marquess, the second-highest rank in the hierarchy of Japanese noble
families (*kazoku*) established in the early Meiji era.

Akiko’s social status may thus seem far above that of Akira, but it’s
worth noting that at least some of Akira’s ancestors may have been
equally high-born. The family name Okudaira is shared by Nobumasa
Okudaira, a feudal lord who fought with Ieyasu Tokugawa and Nobunaga
Oda in the wars that established the Tokugawa shogunate. Tokugawa gave
his eldest daughter in marriage to Okudaira, so Akira herself may be
descended from one of the most influential figures in Japanese
history.

Akiko is also the youngest character in *Rokumeikan*. Her age is not
given in the play, but I guess that she is around sixteen or seventeen
years old, or in other words, about the same age as Akira. Since the
play takes place in 1886, Akiko would thus be a “Meiji girl” in the
same sense that Akira is a “Heisei girl,” born in the new era and
knowing nothing of life before it---in contrast to all the other
characters in the play.

It’s therefore no coincidence that Akiko is the character most in tune
with the new spirit of Meiji Japan, and the one who looks most to the
West rather than to traditional Japan. As her mother says, “she loves
radical things.” Hisao is one of those “radical things,” not “a man of
the lower class, but ... on the side of that class.” Akiko first meets
him not at an arranged meeting but rather by chance at a performance
of “Charine’s circus horses,” where Hisao picks up the imported
European handbag her mother had accidentally dropped.[^04-04-01]

[^04-04-01]: Mishima, *Rokumeikan*, 9--10.

That evening at the Rokumeikan, Akiko plans to leave Japan in the
morning with Hisao on a trip to Europe arranged by her mother, to
return only when (or if?) her father gives her permission to marry.
It’s not clear if Akiko’s mother intends to accompany the couple. If
not, this would presumably be a decided breach of social norms on
Akiko’s part: not only to refuse an arranged marriage and marry for
love, but to travel alone with a man not her husband or father.
Similarly, Akira is considering her own breach of contemporary
Japanese social norms in contemplating entering into a lesbian
relationship with Fumi.

In her disregard for social norms, Akiko is not condemned by her
mother and her friends, but instead receives their support. They too
seem to welcome “this new wonderful age,” as Akiko’s mother’s friends
call it, “this age where women are able to bask in the sun for the
first time in hundreds of years.” It is to help Akiko that her mother
requests Asako meet with Hisao, the event that kicks off the play’s
action. As her mother says, in a line quoted in *Sweet Blue Flowers*
(*SBF*, 3:92), “I want my daughter to experience a full life in the
new era, and have the life that I never had.”[^04-04-02] So too will
Akira’s friends support her in her own “new era,” though she initially
fears they will not.

[^04-04-02]: Mishima, *Rokumeikan*, 8--9.

Continuing the parallels between Akira and the character she plays,
even their names are similar, though with a twist: Akiko’s name has
the *-ko* ending often used for girls’ given names in Japan, while
Akira’s name is (if Wikipedia is any guide) more traditionally used
for boys and men (e.g., the film director Akira Kurosawa). It’s
possible that Shimura chose this name specifically to echo that of
Akiko and to emphasize how Akira might go beyond Akiko in violating
the strictures Japanese society has historically placed on
women.[^04-04-03]

[^04-04-03]: With respect to Akira’s name, it’s worth noting that “Akiko” is also the name of the younger and more immature woman in Nobuko Yoshiya’s *Yaneura no nishojo*, a character thought to be based on Yoshiya herself.

In the end, Akiko’s hopes come to naught. Hisao is dead, and the best
Asako can do is to advise Akiko to live on: “Hisao did not die for
you. So it would be useless for you to follow him in
death.”[^04-04-04] Consistent with the themes of *Sweet Blue Flowers*,
there is also an implied message here for Akira and her fellow
students: though there may be men in your life, don’t make your
existence dependent on theirs.

[^04-04-04]: Mishima, *Rokumeikan*, 51.
