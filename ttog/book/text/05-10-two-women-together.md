---
title: "Two Women Together"
---

## Two Women Together

two women together is a work<br />
nothing in civilization has made simple
{:.epigraph}

Adrienne Rich[^05-10-01]
{:.epigraph-source}

[^05-10-01]: Adrienne Rich, “Twenty-One Love Poems,” in *The Dream of a Common Language: Poems 1974--1977* (New York: W. W. Norton, 1978), 35, [https://&#x200B;archive&#x200B;.org&#x200B;/details&#x200B;/dream&#x200B;of&#x200B;common&#x200B;lan&#x200B;0000&#x200B;rich](https://archive.org/details/dreamofcommonlan0000rich).

After the excitement of Kyoko’s wedding and the shock of Akira’s
confession to Fumi, we now come to the final pages of *Sweet Blue
Flowers*. Akira’s “rude awakening” from sleep at the beginning of
chapter 1 of the manga is replaced by morning greetings between Akira
and Fumi, after a night in which they reenact as adults the
conversation-filled sleepovers they enjoyed as children and teenagers.
{:first}

If *Sweet Blue Flowers* were a Class S story of spiritual love between
two girls, it would have ended in a tearful parting once they
graduated high school. If it were like many yuri stories, the
confession would have been followed with a kiss, and the “Story A”
formula would have been fulfilled: two girls like each other, the
end. If it were like other modern schoolgirl yuri stories, it might
have ended with lovemaking, as do, for example, Milk Morinaga’s *Girl
Friends*[^05-10-02] and *Hana &amp; Hina After School*.[^05-10-03]

[^05-10-02]: Milk Morinaga, *Girl Friends*, vol. 5, trans. Anastasia Moreno (Los Angeles: Seven Seas Entertainment, 2017), 129--36.

[^05-10-03]: Milk Morinaga, *Hana &amp; Hina After School*, vol. 3, trans. Jennifer McKeon (Los Angeles: Seven Seas Entertainment, 2017), 149--52.

But in *Sweet Blue Flowers* Akira and Fumi do not part.[^05-10-04]
Instead, Fumi’s interior monologue strongly implies that their lives
will be intertwined for the next ten or twenty years (*SBF*,
4:360--61). Though the manga echoes the “Story A” structure and
concludes with Akira’s confession, previous chapters already showed
their first kiss. There is but the hint of one here. Finally, they’ve
already spent a night together experiencing physical intimacy
(4:141--46), but here they wake up in separate beds. If they’ve made
love to each other, it was through their words, not their bodies.

[^05-10-04]: Among other things, we can see this as Shimura’s final homage to Nobuko Yoshiya, here to the ending of *Yaneura no nishojo*.

Why does *Sweet Blue Flowers* end this way? If, as I’ve hypothesized,
a primary theme of the manga is the valorization of equality in
relationships, the final scene shows how that might play out in
practice. Akira has acknowledged that she is romantically attracted to
Fumi and has traveled a fair distance in bridging the gap between
their separate conceptions of what their relationship is and should
be.

As for Fumi, the manga leaves ambiguous when or even whether she will
have the physical relationship with Akira that she so clearly
craves. Just as Akira came around to Fumi’s view of their relationship
as a romance and not simply friendship, Fumi may have to accept that
Akira’s nature means that the romantic aspect of their relationship
may always far outweigh the sexual.

But what matters is that they can (re)start their relationship on
equal grounds, negotiating the contours of that relationship as
partners, and as partners facing the outside world
together---including coming out to the rest of their friends, to their
families, and perhaps also to their future co-workers and other
associates.

To echo Adrienne Rich, nothing in contemporary Japan will make their
life together simple. They live in a country where social attitudes
toward LGBTQ people are slowly evolving, but where such evolution is
impeded by a conservative government thus far unwilling to reform the
patriarchal character of Japanese laws relating to marriage and the
family.

Though such social and political concerns form the background to the
lives of the women of *Sweet Blue Flowers*, at its heart is the story
of two people struggling to discover whom they love and how they can
mutually express that love in their lives with each other. Adrienne
Rich also wrote, “two people together is a work / heroic in its
ordinariness.”[^05-10-05] The last page of *Sweet Blue Flowers* takes
a final look back at Fumi and Akira’s friendship as children, but the
chapter as a whole looks forward to the “work” that will be Fumi and
Akira’s relationship in the years to come.

[^05-10-05]: Rich, “Twenty-One Love Poems,” 35.
