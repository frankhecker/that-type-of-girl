---
title: "Setting the Stage"
---

## Setting the Stage

Before I comment on the play itself, I think it’s helpful to discuss
the historical context around *Rokumeikan*. The play is set in 1886,
in the middle of the Meiji era (1868--1912), one of the most
tumultuous and consequential eras in the history of Japan. The
historical setting of *Rokumeikan* would be as familiar to modern
Japanese schoolgirls like those at Fujigaya and Matsuoka as the Civil
War period is to us in the US.
{:.first}

This chapter is in the service of my commentary on *Sweet Blue
Flowers* and the girls who populate its pages. I therefore thought it
appropriate to discuss the history of Meiji-era Japan and the
Rokumeikan from the point of view of Sutematsu Yamakawa, Shige Nagai,
and Ume Tsuda,[^04-02-01] three ordinary girls whose extraordinary
lives were documented by Janice Nimura[^04-02-02] and by Akiko Kuno,
one of their great-grandchildren.[^04-02-03] Yamakawa, the oldest of
them, was born in 1860, only a few years after Commodore Matthew Perry
and his “black ships” showed up in Tokyo harbor in 1853, demanding
that Japan open its ports to the US.

[^04-02-01]: As adults, Ume Tsuda and Shige Nagai changed their given names to Umeko and Shigeko, respectively, reflecting the increasing use of the suffix *-ko* for women’s names in the late Meiji era, a trend that saw almost universal use of *-ko* by the end of the Taishō era. See, for example, Yuri Komori, “Trends in Japanese First Names in the Twentieth Century: A Comparative Study,” *International Christian University Publications 3-A*, *Asian Cultural Studies* 28 (2002), 75--76, [https://&#x200B;icu&#x200B;.repo&#x200B;.nii&#x200B;.ac&#x200B;.jp&#x200B;/&#x200B;?action&#x200B;=repository&#x200B;_action&#x200B;_common&#x200B;_download&#x200B;&item&#x200B;_id&#x200B;=1637&#x200B;&item&#x200B;_no&#x200B;=1&#x200B;&attribute&#x200B;_id&#x200B;=18&#x200B;&file&#x200B;_no&#x200B;=1](https://icu.repo.nii.ac.jp/?action=repository_action_common_download&item_id=1637&item_no=1&attribute_id=18&file_no=1).

[^04-02-02]: Janice P. Nimura, *Daughters of the Samurai: A Journey from East to West and Back* (New York: W. W. Norton, 2015), Kindle.

[^04-02-03]: Akiko Kuni, *Unexpected Destinations: The Poignant Story of Japan’s First Vassar Graduate*, trans. Kirsten McIvor (Tokyo: Kodansha International, 1993).

With no navy and no national military, the Tokugawa shogunate
struggled to resist pressures from the US and other countries. In 1858
it signed a series of “unequal treaties” that favored Western powers
and impinged upon Japanese sovereignty. Resentment of Western
influence and long-standing grievances with the Tokugawas then led to
a prolonged period of civil strife, ending in 1867--68 with a civil
war in which forces fighting in the name of the emperor decisively
defeated pro-government forces. Sutematsu Yamakawa, daughter of a
mid-rank samurai on the losing side, was slightly wounded by shrapnel
in one of the final battles, and her sister-in-law was
killed.[^04-02-04]

[^04-02-04]: Nimura, *Daughters of the Samurai*, chap. 2.

Fourteen-year-old Prince Mutsuhito became emperor in 1867, with the
new “Meiji” (“enlightened rule”) era proclaimed in 1868 with the fall
of Edo (now Tokyo) and the formation of a new government populated by
many energetic and relatively young mid-rank samurai. They embarked
upon a crash course of importing Western knowledge, technology, and
experts to make Japan a modern power as fast as possible.

One of those men, Kiyotaka Kuroda, had been impressed with American
women while visiting the US. He conceived the fantastical scheme of
sending a group of Japanese girls to the US for a ten-year stay to
learn American ways and come back to educate a new generation of
Japanese girls. After an initial recruitment effort failed, the
government succeeded in finding five low- to mid-rank samurai families
who had been on the losing side, were living in relative poverty, and
were therefore willing to let their girls leave home so as not to have
to support them.[^04-02-05]

[^04-02-05]: Nimura, *Daughters of the Samurai*, chap. 3.

The five girls left Japan in 1871 as part of the famous Iwakura
mission along with a group of high-ranking government officials,
scholars, and male students charged to visit foreign nations and bring
back information of use to Japan. The oldest two girls soon returned
to Japan due to ill health and homesickness. However, Sutematsu
Yamakawa (eleven years old), Shige Nogai (ten), and Ume Tsuda (six)
found places with American families. They soon learned English, made
close American friends, and became socialized in a manner typical of
upper-middle-class American girls of the period.[^04-02-06]

[^04-02-06]: Nimura, *Daughters of the Samurai*, chap. 4, 6--7.

While the girls were away, Japan saw a blooming of intellectual
discourse, the formation of grass-roots political movements, and the
creation of nascent political parties, as elements within society and
government contended over what political and cultural ideas and
institutions were most appropriate for Japan.

The three girls returned in the early 1880s, Sutematsu Yamakawa having
graduated from Vassar College (the first Japanese woman to receive an
American college degree) and Shige Nogai having earned a certificate
in music from Vassar. All three girls experienced severe culture
shock, with Ume Tsuda having completely forgotten how to speak
Japanese. They also found that foreign ideas were not as popular as
when they left for America, as a conservative backlash was
building.[^04-02-07]

[^04-02-07]: Nimura, *Daughters of the Samurai*, chap. 9--10.

Shige Nogai soon entered into a love match with a fellow Japanese
student who had attended the US Naval Academy. She went to work as a
music teacher, continuing her career while bearing and raising six
children. At one point, she was the highest-paid female employee in
Japan. Some scholars contend that she’s depicted in a woodblock print
showing a dance at the Rokumeikan---the pianist on the right to whom
the other pianist seems to be looking for cues.[^04-02-08] Her husband
eventually became a baron and an admiral in the Imperial Japanese
Navy, and she a baroness.[^04-02-09]

[^04-02-08]: Toyohara Chikanobu, *Kiken butō no ryakuzu*, 1888, [https://&#x200B;en&#x200B;.wikipedia&#x200B;.org&#x200B;/wiki&#x200B;/Rokumeikan&#x200B;#&#x200B;/media&#x200B;/File:Chikamatsu&#x200B;_Kiken&#x200B;_buto&#x200B;_no&#x200B;_ryakuke&#x200B;.jpg](https://en.wikipedia.org/wiki/Rokumeikan#/media/File:Chikamatsu_Kiken_buto_no_ryakuke.jpg).

[^04-02-09]: Nimura, *Daughters of the Samurai*, chap. 10.

Sutematsu Yamakawa struggled to find work suitable to her upbringing
and education and ended up accepting an offer of marriage from Iwao
Ōyama, minister of war and a former general in the Imperial Japanese
Army. Ōyama, twenty years her senior, was looking for a wife familiar
with Western ways to assist him in his political and diplomatic
activities. In time, Sutematsu became a pillar of the Japanese
aristocracy, advising the empress herself on Western culture and
fashion.[^04-02-10]

[^04-02-10]: Kuni, *Unexpected Destinations*, 118--22, 133--50. Nimura, *Daughters of the Samurai*, chap. 10.

As Countess (later Princess) Ōyama, she became known as the “Lady of
the Rokumeikan” for her role in hosting events there after its
construction in 1883. (In fact, she appears as a minor character in
the play *Rokumeikan*.) Ōyama also introduced American-style
philanthropy to Japan, including a charity bazaar held at the
Rokumeikan.[^04-02-11] This event was also memorialized in a woodblock
print, depicting Countess Ōyama and her daughter Hisako in the center
of the image.[^04-02-12]

[^04-02-11]: Kuni, *Unexpected Destinations*, 162--64.

[^04-02-12]: Toyohara Chikanobu, *Rokumei-kan ni okeru kifujin jizenkai no zu*, 1884, [https://&#x200B;en&#x200B;.wikipedia&#x200B;.org&#x200B;/wiki&#x200B;/File:Rokumei&#x200B;-kan&#x200B;_ni&#x200B;_okeru&#x200B;_kifujin&#x200B;_jizenkai&#x200B;_no&#x200B;_zu&#x200B;.jpg](https://en.wikipedia.org/wiki/File:Rokumei-kan_ni_okeru_kifujin_jizenkai_no_zu.jpg).

One of the primary beneficiaries of Sutematsu’s philanthropy was Ume
Tsuda, who had the worst time adjusting to life in Japan. She first
obtained employment as a private tutor to the children of Hirobumi
Itō, soon to become Japan’s first prime minister. She then taught at
the Peeresses’ School, which Itō set up (with assistance from Countess
Ōyama) to educate the daughters of the Imperial family and Japanese
nobility.[^04-02-13] (Prestigious girls’ schools like Fujigaya Women’s
Academy would later offer an equivalent experience for the daughters
of Japan’s upper and upper-middle classes.)

[^04-02-13]: Nimura, *Daughters of the Samurai*, chap. 11.

Ume Tsuda became frustrated by the conservatism of the Peeresses’
School and the expectations of her family and others that she
marry. Due to her youth, she had not been able to attend college while
in America, and hence she applied for and was granted permission and
funding to go back to the US to complete her education. Tsuda enrolled
at the recently-opened Bryn Mawr College for women and graduated with
a bachelor’s degree. She then returned to Japan, having also found
time to (anonymously) assist an American friend, Alice Mabel Bacon, in
writing a book critical of Japanese laws and educational policies
relating to girls and women.[^04-02-14]

[^04-02-14]: Nimura, *Daughters of the Samurai*, chap. 13.

After some time and assistance from Countess Ōyama, Ume Tsuda realized
her dream of opening her own school, the Women’s Institute for English
Studies (*Joshi Eigaku Juku*). Its mission was to train teachers for
Japan’s newly-mandated middle schools for girls. Tsuda was soon joined
by Anna Cope Hartshorne, her close friend from Bryn Mawr, who became
her partner in both work and life. (Like Nobuko Yoshiya and her
partner Chiyo Monma, Tsuda and Hartshorne bought a cottage together in
Kamakura.)[^04-02-15]

[^04-02-15]: Nimura, *Daughters of the Samurai*, chap. 14--15.

In 1905 the Women’s Institute for English Studies enrolled nearly a
hundred and fifty students. It was so highly regarded that its
graduates received a government exemption from taking the teaching
certification exam.[^04-02-16] Those women, in turn, taught the
schoolgirls of the late Meiji and Taishō eras who created *shōjo*
culture, and introduced them to Western literature and ideas. In
Nobuko Yoshiya’s 1923 Class S story “Yellow Rose,” Misao Katsuragi,
the protagonist, is an English teacher newly-graduated from Tsuda’s
institute.[^04-02-17]

[^04-02-16]: Nimura, *Daughters of the Samurai*, chap. 14.

[^04-02-17]: Sarah Frederick, translator’s introduction to *Yellow Rose*, by Nobuko Yoshiya. The story identifies Katsuragi’s alma mater as “a certain English Academy ... in Gobanchō, near the British Embassy in Tokyo.” The Women’s Institute for English Studies moved to that location in 1903. Nimura, *Daughters of the Samurai*, chap. 15.

Ume Tsuda spent her last years in ill health, living in Kamakura with
Anna Hartshorne. After she died in 1929, the Women’s Institute for
English Studies was renamed in her honor, eventually becoming Tsuda
College and then (more recently) Tsuda University. Hartshorne herself
left Japan in 1940, on the brink of war, never to return. She died in
1957, a year after the first production of *Rokumeikan* and almost a
century after the beginning of the Meiji era.[^04-02-18]

[^04-02-18]: Nimura, *Daughters of the Samurai*, chap. 15.

The Rokumeikan itself was long gone by then. Its use had declined
with the rise of conservative sentiment and anti-Western feeling, and
it was sold in 1890 to become a private club for the aristocracy. The
building fell into disuse and was eventually demolished in 1941, as
Japan went to war with the Western powers whose diplomats it had once
invited to dance at the Rokumeikan.

There’s an intriguing parallel between the three twenty-first-century
girls who are the main characters in *Sweet Blue Flowers* and the
three nineteenth-century girls of *Daughters of the Samurai*. Kyoko
resembles Sutematsu Yamakawa, thwarted in her original desire and
falling back on marriage with an older man of higher social
status. We can only hope that Kyoko will find happiness in such a
marriage, as Yamakawa did in hers.

Fumi resembles Ume Tsuda. She will remain unmarried, as Tsuda did
(unless marriage equality comes to Japan). Our hope for Fumi is that,
like Tsuda, she will also find a woman, whether Akira or another, who
will be her lifelong companion.

As for Akira, her fate is not yet clear---though I doubt she’ll have
six children, like Shige Nogai. We can only hope that Akira also finds
someone to love, just as Shige did.
