---
title: "Hot Springs Episode"
---

## Hot Springs Episode

As they did after the performance of *Wuthering Heights* in volume 1,
the students of Fujigaya take advantage of the school break after the
performance of *Rokumeikan* to get away for a brief vacation. This
time the accommodations are courtesy of Haruka Ono, whose grandparents
happen to own a hot springs inn (*onsen*) and are happy to welcome
her and her friends, not just from Fujigaya but from Matsuoka as well
(*SBF*, 3:141--43, 3:196--98).
{:first}

The *onsen* visit is a staple of anime and manga set in high
schools. It’s usually an excuse for comedic hijinks as teenaged boys
employ various stratagems to try to catch a glimpse of teenaged girls
in the bath. The girls’ naked bodies are typically wreathed in
strategically-placed plumes of steam---steam that sometimes magically
disappears when it comes time for anime DVD or Blu-ray Disk releases.

In *Sweet Blue Flowers*, Shimura too provides the readers of *Manga
Erotics&nbsp;F* (and us) with some “fan service,” but I think the
significance of this section lies in more than mere titillation.
Shimura draws Fumi’s naked body, but she depicts it from Akira’s point
of view (*SBF*, 3:204--6).

Akira has listened to Fumi talk about her having sex with Chizu and
her desire to have a physical relationship with Akira. However, Akira
has not been confronted with the reality of what that implies until
now. Underneath Akira’s embarrassment and confusion, we sense that she
is finding herself physically attracted to a woman’s body.
(“S... S-Sorry! But you’re so pretty---I *had* to stare.”) But she
doesn’t know yet what to do with that feeling.

Although Fumi also gets a chance to see Akira naked, the more
significant event for her is after the bath, when her staying too long
in the hot springs causes her to almost faint. Her collapsing on the
bench attracts the attention of Hinako Yamashina and Orie Ono, who are
chaperoning the girls on their visit (*SBF*, 3:214--17).

Since Fumi doesn’t go to Fujigaya, she doesn’t know Hinako (or she
Fumi). However, due to Fumi’s friendship with Haruka, she knows that
Haruka’s sister Orie has a girlfriend, and Fumi figures out that that
girlfriend is Hinako (*SBF*, 2:330). So Fumi takes the opportunity to
confide in someone who’s a relative stranger but who might understand
what Fumi’s going through and be a sympathetic listener (3:218,
3:225--27).

Erica Friedman has pointed out the importance of yuri protagonists
“[having] an example of an adult woman in a stable [lesbian]
relationship ... a person to get advice from and ... a role
model.”[^04-09-01] The key phrase here is “in a stable relationship.”
In other words, Hinako already has a partner and has no romantic or
sexual interest whatsoever in Fumi. This marks a clear contrast to
other yuri and Class S works in which older women seek out younger
women and seduce them.

[^04-09-01]: Erica Friedman, Review of *Yagate Kimi ni Naru*, vol. 3, by Nio Nakatani, *Okazu* (blog), January 26, 2017, [https://&#x200B;okazu&#x200B;.yuricon&#x200B;.com&#x200B;/2017&#x200B;/01&#x200B;/26&#x200B;/yuri&#x200B;-manga&#x200B;-yagate&#x200B;-kimi&#x200B;-ni&#x200B;-naru&#x200B;-volume&#x200B;-3&#x200B;-&#x200B;%e3&#x200B;%82&#x200B;%84&#x200B;%e3&#x200B;%81&#x200B;%8c&#x200B;%e3&#x200B;%81&#x200B;%a6&#x200B;%e5&#x200B;%90&#x200B;%9b&#x200B;%e3&#x200B;%81&#x200B;%ab&#x200B;%e3&#x200B;%81&#x200B;%aa&#x200B;%e3&#x200B;%82&#x200B;%8b](https://okazu.yuricon.com/2017/01/26/yuri-manga-yagate-kimi-ni-naru-volume-3-%e3%82%84%e3%81%8c%e3%81%a6%e5%90%9b%e3%81%ab%e3%81%aa%e3%82%8b).

For example, compare Hinako’s behavior toward Fumi with Misao
Katsuragi’s toward Reiko in Nobuko Yoshiya’s “Yellow Rose.” The ornate
language and concluding heartbreak obscure the fact that, in essence,
“Yellow Rose” is a tale of a teacher exploiting her age and superior
position to initiate a romantic relationship with a younger student
placed in her charge.[^04-09-02]

[^04-09-02]: Yoshiya, *Yellow Rose*, chap. 2. Katsuragi is twenty-two years old and in her first teaching assignment when she meets seventeen-year-old Reiko. Although Katsuragi appears to be an adult already when the story begins, Yoshiya downplays this by repeatedly referring to her as a girl (e.g., “this girl Misao”).

Not so with Hinako, who behaves toward Fumi as one would expect a
teacher to act toward a student, repeating the scrupulousness she
previously showed in her dealings with another student (*SBF*,
2:167). (This is true of other characters in *Sweet Blue Flowers*;
see, for example, Mr. Kagami and Yasuko.) Although the manga is silent
regarding exactly what advice Hinako provides to Fumi, she does ask
Fumi two key questions: “What kind of relationship do you want with
[Akira]?” and “Do you want to be a couple?” The unstated implication
is, “Do you want you and Akira to be a couple like Orie and me?”
(3:243).

We’ve previously seen that Hinako and Orie were in the same year in
high school and began their relationship after Orie’s crush on the
older Shinako ended (*SBF*, 2:160, 2:163--64, 2:168). Though Fumi has
no way of knowing this, their situation thus resembled Akira and
Fumi’s after Fumi broke up with Yasuko.

However, Fumi does see that Hinako and Orie are two women whose
relationship endured beyond graduation, and this has an impact on
her. As the chapter concludes, Fumi recalls Hinako and Orie and thinks
to herself that she would like a similar relationship with Akira. Not
realizing that Akira is not asleep, she speaks aloud of her feelings
and is surprised by Akira telling her in turn of hers. Although
Akira’s feelings toward Fumi are not the same as Fumi’s toward her,
there’s something there. That something is significant enough that
Akira wants them to date and explore where their relationship might go
next (*SBF*, 3:246--56).
