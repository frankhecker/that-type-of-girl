---
title: "Four Weddings (No Funeral)"
---

## Four Weddings (No Funeral)

For a manga set in high school, *Sweet Blue Flowers* sure has a lot of
weddings (and talk of more). Chizu’s marriage and Fumi’s anger at it
help (re)start Fumi’s friendship with Akira. Kazusa’s wedding to Mr.
Kagami (re)surfaces emotions that Yasuko had suppressed, which
indirectly leads to her break-up with Fumi. As the story heads to its
conclusion, Kyoko weds Ko and Hinako and Orie envision what their
wedding might be like, should it ever be possible.

The earliest wedding depicted in *Sweet Blue Flowers* is that of
Kayoko to Akihiko Ikumi (*SBF*, 4:62). Unlike the other wedding
pictures, which show the couples in Western attire, theirs features
them in traditional Japanese outfits. Or perhaps I should write
“traditional” in quotes since what seems to be a time-honored style is
in many ways a product of the modern era, “a ‘cultural product’
... invented in order to further the business interests of the
purveyors of this product ... \[and\] a purveyor of a sense of
cultural identity.”[^05-09-01]

[^05-09-01]: Ofra Goldstein-Gidoni, *Packaged Japaneseness: Weddings, Business, and Brides* (Honolulu: University of Hawai‘i Press, 1997), 3--4.

That “cultural identity” is a product of the Meiji-era project to
forge Japan into a unified modern nation. That nation-building project
touched all areas of Japanese life, including weddings. The state
formalized Shinto as a set of rituals binding the Japanese with each
other and with the emperor. Those rituals then became the heart of the
wedding ceremony. Formerly held at home, weddings became a public
display in 1900, when the future Taishō emperor married his bride in a
Shinto ceremony held at a shrine. It was a reaction against the
perceived dangers of Westernization, “a public statement about
Japanese identity in contrast to others through the wedding
ceremony.”[^05-09-02]

[^05-09-02]: Teresa A. Hiener, “Shinto Wedding, Samurai Bride: Inventing Tradition and Fashioning Identity in the Rituals of Bridal Dress in Japan,” PhD diss., University of Pittsburgh, 1997, 3, 12--13, 144--55.

In contrast to this state ceremony, weddings for the general populace
continued to be held at home in the Taishō and early Shōwa periods,
including weddings of those descended from the samurai class. The
bride’s outfit was typically “a white under kimono (*shiromuku*) under
a formal black adult’s kimono,” with “hair oiled and pulled up into a
Japanese hair style (*nihonga*) which was not much more elaborate than
the way most of them wore their hair on any given day.”[^05-09-03]

[^05-09-03]: Hiener, “Shinto Wedding, Samurai Bride,” 56--57.

However, bridal outfits became more elaborate over time, especially
after World War II. Bombing had destroyed the large homes previously
used for weddings, so new “wedding palaces” catered to postwar
brides. (The palaces included their own Shinto shrines in which to
hold ceremonies.) As the economy grew in the postwar period, this
wedding industry (for such it was) made available to the broad
Japanese middle class bridal clothing and accessories previously
confined to the samurai class. Those items combined with a style of
makeup otherwise associated with geisha and Kabuki theater to create
the image of the “Japanese bride” we know today.[^05-09-04]

[^05-09-04]: Goldstein-Gidoni, *Packaged Japaneseness*, 34--39. Hiener, “Shinto Wedding, Samurai Bride,” 139--41.

That image is often paired with images of modernity---a bullet train,
a computer chip, or the Tokyo skyline---to portray Japan as a nation
racing into the future while remaining respectful of and rooted in the
past. It is part and parcel of the process of “samuraization,” “the
configuring of Japanese identity according to a perception of samurai
lifestyle from the Tokugawa Period.”[^05-09-05]

[^05-09-05]: Hiener, “Shinto Wedding, Samurai Bride,” 17.

This process includes imagining modern Japanese men, especially
salarymen, as torch-bearers of the samurai spirit. Akihiko Ikumi was
presumably one such man. His marriage to Kayoko occurred at the high
point of Japanese economic power and self-confidence, the late 1980s
and the end of the Shōwa era. (In retrospect, it may also have marked
the high point of the Japanese patriarchal system.)

However, Japan soon slid into its “lost decades” of economic
stagnation and diminished prospects. All the other weddings depicted
in *Sweet Blue Flowers* feature Western wedding gowns and other
Western attire, a turning away from the (partly invented and imagined)
Japanese marriage traditions.

This carries over to other aspects of *Sweet Blue Flowers*. Unlike
other manga set in high schools, there are no visits to Shinto shrines
or Buddhist temples. And when the students go on class trips in volume
4, they go not to Kyoto (a traditional destination for Japanese
high-school classes), but England and Nagasaki, famous as the site
where European goods and ideas first entered Japan.

Kazusa and Mr. Kagami’s wedding, the next one pictured in the
chronology of *Sweet Blue Flowers*, is held in the Catholic chapel of
Fujigaya Women’s Academy, complete with an organist, stained glass
windows, and a cross on the building’s exterior (*SBF*, 2:93,
2:96). Its setting represents the other significant development of the
Meiji era, the influx of Western ideas and, in particular, Western
religion. That religion was influential not so much as a religion per
se, but as a source of images (including the white lily) and
traditions (such as the celebration of Christmas) divorced from their
religious roots and incorporated into Japanese popular culture.

In Kyoko and Ko’s wedding, the religious symbolism is gone: although
their wedding was presumably also held at Fujigaya (note the
transition into the scene showing the woods surrounding the school),
the exterior of the building in which they are married, and the
interior rooms at the reception, betray no hint of Christianity
(*SBF*, 4:335--37, 4:339--40, 4:348).

Unlike the wedding of Kazusa and Mr. Kagami, the symbols of the
patriarchy are absent as well, as there is no sign of Kyoko’s father
(or Ko’s). (We hear mention of Kyoko’s and Ko’s mothers only in the
run-up to the event.) Instead, Kyoko and Ko walk down the aisle by
themselves, surrounded by their friends (*SBF*, 4:332--33, 4:349).

The fourth and final wedding shown in *Sweet Blue Flowers* is imagined
only, as Hinako and Orie respond to Haruka’s query, “Hina, do you and
Orie want a wedding too?” They picture themselves in wedding gowns
even more Western and “fashion-forward” than Kyoko’s and Kazusa’s
(*SBF*, 4:350--51).

However, they have ambiguous feelings about marriage itself
(“Actually, an official ceremony isn’t that important ...”) and are
passive regarding anything they might do to make such a wedding
possible (“But if no one would mind...”) (*SBF*, 4:350). Like the
characters in many other yuri manga who imagine weddings and wedding
dresses for themselves, they do not (yet?) identify themselves as
members of a community that has shared interests and can work to
achieve shared political goals.

We find an unlikely exception to this omission in the manga *Love Me
for Who I Am*, superficially a story notable mainly for fluffy art and
*moe* characters. In volume 3, a lesbian joins her nonbinary friend
and their co-worker, a trans girl, as they leave the community they’ve
created with each other at “Café Question” and join the larger LGBTQ
community at “Rainbow Festa.” As they wander among the people, events,
and booths, her companions stop to sign a petition for marriage
equality.[^05-09-06]

[^05-09-06]: Kata Konayama, *Love Me for Who I Am*, vol. 3, trans. Amber Tamosaitis (Los Angeles: Seven Seas Entertainment, 2021), 97--101.

It is at once the simplest and potentially most consequential of
political acts. Their quickly-written signatures mark a personal
crossing of the Rubicon, a transition from inner thoughts and private
conversations to public support of a cause near to their hearts and
those of their friends. As their futures unfold, they may progress
from signing petitions to soliciting signatures for them, from
watching parades to marching in them, perhaps even to organizing,
leading, or inspiring them.

Who among the characters of *Sweet Blue Flowers* might one day make
that journey? One candidate is Haruka: bold, outgoing, able to make
friends easily, her concern for her sister might motivate her to
become an activist---though she still seems to be coming to terms with
Orie’s relationship with Hinako.

Another is Akira. Her presidency of the drama club shows she has a
talent for organizing. Her behavior throughout the series shows
fearlessness and a strong sense of justice, at least where other
people are concerned. And now that she’s acknowledged her love for
Fumi, who knows what actions that love might spur her to?

But I must put such speculation on hold, as I turn to the final scenes
of *Sweet Blue Flowers*.
