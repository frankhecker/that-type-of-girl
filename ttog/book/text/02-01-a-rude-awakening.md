---
title: "A Rude Awakening"
---

## A Rude Awakening

*Content note: This chapter discusses child sexual abuse.*

Often when we encounter the cultural products of other countries we
are brought up short by certain features of them. Manga and anime are
no exception. Here we are expecting a sweet tale of cute schoolgirls,
and no sooner do we begin reading *Sweet Blue Flowers* than we find
Akira Okudaira’s brother being caught in her bed (*SBF*, 1:8). This is
no case of prepubescent siblings cuddling together: since she’s
starting high school, we know Akira is fifteen years old, and since he
has a driver’s license, we know her brother is at least eighteen if
not older.
{:.first}

What’s going on here? One possibility is that Shimura is making fun of
a common manga and anime trope: the older brother with a “sister
complex.” Akira’s brother pretty much fits the “siscon” template to a
T: no girlfriend to be seen, still living at home, no apparent job or
anything else to keep him occupied, and last but not least, no evident
realization that he’s doing something wrong and unwanted.

Shimura gets a laugh out of Akira’s brother hypocritically warning her
to “watch out for gropers!” (*SBF*, 1:11) and a subsequent chapter
makes fun of his overprotectiveness in following her as she goes on a
planned group date (1:102). The joke seems somewhat off, though: if
the point of the story is to show the evolving relationship between
Akira and Fumi, why undercut that at the very beginning by putting her
brother front and center?

Instead of being a joke, one could speculate that Shimura’s intent is
much darker, in particular, that she is implying that Akira is the
victim of ongoing sexual abuse from her brother. Under this
interpretation, Akira’s disinterest in relationships might not be
simply a reflection of the way she is, but rather the result of
childhood trauma that leaves her emotions numbed. But this seems
somewhat at odds with the relatively light-hearted portrayals of both
Akira and her brother elsewhere in the manga---though I’ll defer to
the judgment of abuse survivors regarding this question.

My interpretation is somewhat in the middle: beyond being a nod to a
familiar trope that could be exploited to humorous ends, Shimura may
have also intended this as an example of how women have reason to
distrust men and turn to other women (in this case, Akira’s mother)
in response for their protection. The subsequent scene on the train
reinforces this, as discussed in the next chapter.
