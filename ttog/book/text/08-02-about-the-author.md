---
title: "About the Author"
---

## About the Author

The author’s interest in things Japanese was sparked by a business
trip he took to Japan many years ago---a trip that coincidentally saw
him spend two weeks near the future settings of *Sweet Blue
Flowers*. His interest in manga and anime is much more recent.
{:.first}

He has not written, nor does he plan to write, any other books about
manga, anime, or Japanese popular culture in general. You can find his
articles on other topics at [frankhecker.com][fh] or follow him on
Twitter at [@hecker][tw]. You can contact him via email at
frank@frankhecker.com.

[fh]: https://frankhecker.com/
[tw]: https://twitter.com/hecker
