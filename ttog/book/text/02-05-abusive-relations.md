---
title: "Abusive Relations"
---

## Abusive Relations

*Content note: This chapter discusses child sexual abuse.*

Having learned about Akira’s brother’s bad behavior, now it’s time for
Fumi’s cousin Chizu to give readers a double dose. First, the facts as
presented thus far in the series: Fumi is now fifteen years old. (See
my previous discussion on this point.) Chizu is getting married, so
presumably she’s finished high school and is at least eighteen,
perhaps even older if she went to university. Chizu wanted to (and
apparently did) have sex with Fumi at a time when (judging from
appearances) Fumi was younger than fifteen (*SBF*, 1:65).
{:.first}

Fumi seems to have had genuine feelings for Chizu and consented to her
advances. But regardless of Fumi’s feelings, to my mind, this is
clearly a case of abuse: Chizu used Fumi for her own purposes and then
abandoned her when she got married. As with Akira’s brother, we can
ask, what is this episode doing in this “schoolgirl yuri” story?

Unfortunately, the subplot with Chizu might imply to some that Fumi is
a lesbian only because she was “recruited” as a child. This notion
defames lesbians as predators on children and lessens the emotional
impact of Fumi’s journey to coming out. It casts her as a victim
“groomed” to a role and unable to move beyond a past trauma, not as a
person with agency expressing her inherent identity.

As an outsider to the culture, I can only guess at Takako Shimura’s
thinking here, but I doubt that was her intent. Here are my guesses as
to what’s going on, for what it’s worth:

Recall again that *Sweet Blue Flowers* was originally serialized in
*Manga Erotics F*. Shimura (or her editor) may have felt that
including this sort of subplot made the series more appealing to the
magazine’s target audience of adults.

Also, other Shimura works published in *Manga Erotics F* and elsewhere
include questionable content of this general type. For example,
readers of *Wandering Son* will recall the scene when an adult
character grabs a trans youth’s crotch to investigate their
genitals.[^02-05-01]

[^02-05-01]: Shimura, *Wandering Son*, 2:100--101.

Are there any ways in which Chizu’s subplot contributes to the story?
I presume that one purpose was to use the relationship and its end as
a trigger for Fumi’s rebound relationship with Yasuko Sugimoto. Having
Fumi’s prior relationship be with a schoolmate closer to her age would
complicate the narrative of Akira’s being Fumi’s first love and
(perhaps) her true love. Her time with Chizu also means Fumi is more
sexually experienced than Akira---a factor in future volumes as their
relationship evolves.

Finally, whether Shimura intended this or not (and I suspect she may
have), Chizu’s relationship with Fumi serves as a dark mirror of Class
S and yuri narratives that feature older girls or women entering into
relationships with younger girls. (For example, Nobuko Yoshiya’s story
“Yellow Rose” features a twenty-two-year-old teacher and a
seventeen-year-old student.)[^02-05-02]

[^02-05-02]: Yoshiya, *Yellow Rose*, chap. 2.

These traditional tropes reflect an age-based social hierarchy in
which one partner is senior to and (in social terms, at least)
dominates the other. As the senior members of the pairings graduate or
marry, they leave the world of intimate female-female relationships to
live in a male-dominated and -centric world. The junior members, in
turn, become the seniors to a new crop of girls in a never-ending
cycle.  (See, for example, my previous discussion of the workings of
the *sœur* system in *Maria Watches Over Us*.)

In contrast, based on the evidence thus far *Sweet Blue Flowers* seems
to valorize relationships between equals and implicitly criticize
unequal relationships based on age or other hierarchies. Clearly, the
relationship between Fumi and Akira is becoming more important to the
story than Fumi’s relationship with her senior Yasuko, with Kyoko’s
unrequited crush on Yasuko and Yasuko’s on Mr. Kagami providing
additional negative takes on unequal relationships. From this point of
view, Chizu’s relationship with Fumi offers the reader another example
of the potential harms inherent in and inseparable from some classic
yuri tropes.
