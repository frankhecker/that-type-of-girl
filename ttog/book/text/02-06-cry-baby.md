---
title: "Cry Baby"
---

## Cry Baby

Fumi Manjome is a crybaby. That’s pretty much the extent of the
characterization she gets in the early chapters of *Sweet Blue
Flowers*: she cries on the train when a man sexually harasses her
(*SBF*, 1:16--17). In a flashback, she cries when she wets her pants
in elementary school (1:30) and when she moves away from Akira
(1:32). She cries (on multiple occasions) after she learns that her
cousin Chizu is getting married and has abandoned her (1:43, 1:50,
1:62--63, 1:66). She cries before Yasuko kisses her for the first time
(1:115), when she tells Akira about dating Yasuko (1:139--40), when
Yasuko breaks up with her at the Sugimoto house (1:314), and while
commiserating with Kyoko Ikumi afterward (1:376).
{:.first}

If this were all there were to Fumi’s character, *Sweet Blue Flowers*
would be a pretty depressing and uninteresting manga. But although
Fumi is very prone to crying, she’s more than the sum of those
instances. It’s not that she suffers from a debilitating emotional
condition: it’s more that she feels things very deeply and doesn’t
hold back expressing her emotions.

And as we’re beginning to see, there’s a steel core to Fumi’s
personality: she doesn’t wallow in her tears but (typically with help
from Akira) goes on to deal with whatever’s upsetting her. She doesn’t
cling to Chizu after her abandonment, and she moves to get out of the
Sugimoto house as soon as possible once she sees how things are with
Yasuko.

This is why Fumi is my favorite character in *Sweet Blue Flowers*: she
doesn’t hold things in and present a false front to the world. She
doesn’t get passive-aggressive and lash out at people. Instead, she
cries, spends a bit of time deciding what to do, and then gets on with
her life.
