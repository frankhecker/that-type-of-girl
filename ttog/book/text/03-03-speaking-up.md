---
title: "Speaking Up"
---

## Speaking Up

As I wrote in a previous chapter, despite her tendency to break out in
tears, “there’s a steel core to Fumi’s personality.” That steel core
is shown most clearly in the aftermath of Kazusa’s wedding when Yasuko
tries to escape her feelings about Mr. Kagami by tagging along with
Akira and her brother to meet Fumi in Enoshima (*SBF*, 2:100--101).
{:.first}

Despite her past involvement with Yasuko and her meeting Kazusa at the
Sugimoto home, Fumi did not attend Kazusa’s wedding for various
reasons, including not being a Fujigaya student and breaking up with
Yasuko. Akira seems not to have even told Fumi about the wedding (“Oh,
but I guess you couldn’t,” observes Kyoko), and Fumi appears to have
forgotten all about it until reminded of it by Akira’s mother when
trying to call Akira at home (*SBF*, 2:92, 2:76--77).

Fumi decides to go to Enoshima on the spur of the moment. Her initial
motivation seems to be to have some time to herself (*SBF*,
2:90--91). Although she initially protests, Fumi appears happy to have
Akira come to meet her. However, when Yasuko invites herself as well,
things come to a head, especially after Yasuko inserts herself into a
dispute between Akira and her brother over his wanting to stay with
the group (2:100, 2:106).

The overall sequence of events seems to be driven by the
*senpai-kōhai* dynamic characteristic of Japanese school life (and
indeed Japanese life in general). Students in lower grades defer to
those in upper grades, and as students advance in grade they will be
in turn deferred to by those coming up behind them.

Many manga and anime set in high schools (including *Sweet Blue
Flowers*) are structured around this progression. New first-year
students arrive, former first-years advance to their second year and
have the new first-years to be *senpai* to, former second-years enter
their third and final year and reach the top of the school hierarchy,
and third-year students leave to face a world in which they will have
new *senpai* to defer to. It’s common for establishing shots of school
classrooms to show the external signs marking the class year of the
students within so that the viewer can keep track of the students’
place in the hierarchy.

At this point in the story, Yasuko is a third-year student, and Akira
and Fumi are only first-years, so the status gap between them is as
large as it is possible to be within a high school. Yasuko exploits
her *senpai* status, first in inviting herself to go with Akira and
her brother to Enoshima, then in taking Akira’s brother’s side in his
argument with Akira over his staying (“Why is she taking the lead?!”
Akira fumes), and finally in proposing they “all hang out together” as
a group (*SBF*, 2:100, 2:106--7).

Akira is clearly unhappy with the turn of events but remains silent.
Instead, it is Fumi who actually verbalizes her opposition, with a
single word: “No.” She goes on to tell Yasuko, “I don’t want to walk
with you,” and commands her to “stop bothering Akira” (*SBF*, 2:108).

Akira is shocked, and I think rightly so. Fumi’s words seem to be a
significant breach of the standard *senpai-kōhai* protocol. Fumi goes
on to compound it later in the trip by rejecting Yasuko’s attempt to
make up (“I wanted to see you, Fumi.” “Well, *I* didn’t want to see
*you*.”)  and then uttering a final and crowning insult: “Grow up”
(*SBF*, 2:121--23).

Fumi later has second thoughts about the exchange (“I’m pretty selfish
... aren’t I?” she asks Akira) (*SBF*, 2:128). But her prior words to
Yasuko can’t be unsaid, and I believe that in Shimura’s framing Fumi
was absolutely right to say them. First, Yasuko was, in fact, abusing
her position as *senpai* in her attempt to get back together with
Fumi, inserting herself into a private outing she had no inherent
right to participate in.

Second, and I think more important, these scenes are consistent with
Shimura’s framing of other relationships in the manga thus far,
including Fumi’s with Chizu and Kyoko’s with Yasuko. To quote myself
again: “*Sweet Blue Flowers* seems to valorize relationships between
equals and implicitly criticize unequal relationships based on age or
other hierarchies.”

The difference here is between criticizing bad actions within a
hierarchical system seen as otherwise promoting harmony within society
versus criticizing the very idea of hierarchy itself. I believe that
Shimura is doing the latter: she is implicitly criticizing the
dominance hierarchies characteristic of traditional Class S and yuri
works (hierarchies which are themselves embedded in larger
hierarchies), and proposing a new model of yuri based on individualism
and equality.

Fumi is the primary embodiment of this model within the world of
*Sweet Blue Flowers*. But, as we shall see, she is not the only one,
or even the first one.
