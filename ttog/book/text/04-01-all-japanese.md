---
title: "All Japanese"
---

## All Japanese

Before I get to the main attraction (i.e., *Rokumeikan*), let me take
a moment to consider the other plays presented at the Fujigaya drama
festival. The first point worth noting (as Kazusa does while reading
the flyer) is that all the plays are Japanese in origin and on
Japanese themes (*SBF*, 3:10).
{:.first}

This contrasts with the previous year, when (as I previously wrote)
the plays seemed “designed to avoid as much as possible anything that
hints of transgression ... through productions that are isolated in
time and place from modern Japan, and thus avoid direct commentary on
contemporary Japanese society.” None of this year’s plays take place
in modern Japan, but a play like *Rokumeikan* is certainly more
relevant to contemporary Japanese society than *Wuthering Heights*.

What about the other two? *The Bamboo Cutter* is based on a
thousand-year-old Japanese folktale (*Taketori monogatari*), also
known as *The Tale of Princess Kaguya* (*Kaguya-hime no monogatari*),
about an old childless couple who discover a baby in a stalk of bamboo
and raise her as their own. As she grows to become a young woman, she
is beset by suitors, including the emperor, but refuses them all and
is eventually carried away to the moon to rejoin her people.

*The Bamboo Cutter* seems simply a charming tale fit for elementary
school students. But as Caroline Cao points out, it can be
interpreted as implicitly criticizing a father driven by “patriarchal
obsessions” to seek social status for himself through his daughter’s
marriage. “Had Kaguya’s father not been oblivious to her evident pain
and so presumptuous of her welfare, perhaps Kaguya would have lived
happily in an earthly life ... away from the greed of men seeking to
make a wife out of her.”[^04-01-01]

[^04-01-01]: Caroline Cao, “The Patriarchal Pains of Womanhood in the Films of Studio Ghibli’s Isao Takahata,” Anime Feminist, January 25, 2019, [https://&#x200B;www&#x200B;.animefeminist&#x200B;.com&#x200B;/feature&#x200B;-the&#x200B;-patriarchal&#x200B;-pains&#x200B;-of&#x200B;-womanhood&#x200B;-in&#x200B;-the&#x200B;-films&#x200B;-of&#x200B;-studio&#x200B;-ghiblis&#x200B;-isao&#x200B;-takahata](https://www.animefeminist.com/feature-the-patriarchal-pains-of-womanhood-in-the-films-of-studio-ghiblis-isao-takahata).

*The Izu Dancer* (*Izu no odoriko*) is a more modern tale, based on a
1926 short story by the famous Japanese author Yasunari
Kawabata. Better known in the West as “The Dancing Girl of Izu,” the
story was an early highlight in a career that eventually saw Kawabata
win the Nobel Prize for Literature in 1968.

“The Dancing Girl of Izu” has been translated into English by Edward
Seidensticker[^04-01-02] and later by J. Martin Holman.[^04-01-03]
(The Holman translation is supposedly more faithful to the Japanese,
but I think the Seidensticker translation reads better as English.)
The Wikipedia article for the story calls it “a lyrical and elegiac
memory of early love,” but to my Western eye it’s also more than a bit
creepy.

[^04-01-02]: Yasunari Kawabata, “The Izu Dancer,” trans. Edward Seidensticker, in *The Izu Dancer, and Other Stories*, Yasunari Kawabata and Yasushi Inoue, trans. Edward Seidensticker and Leon Picon (Tokyo: Tuttle, 2011). Kindle.

[^04-01-03]: Yasunari Kawabata, “The Dancing Girl of Izu,” in *The Dancing Girl of Izu, and Other Stories*, 3--33, trans. J. Martin Holman (Washington, DC: Counterpoint, 1998).

Why might I think that? Let’s look at a summary of the plot (from an
anonymous contributor to Answers.com): “The nineteen year old
narrator, an introspective student on a holiday from an upper class
school in Tokyo, ... meets and becomes infatuated with a young dancer
in a traveling family of entertainers. At first he feels a vague
erotic attraction to her. But when he sees her in the nude in a public
bath, he realizes that she is still a \[thirteen-year-old\] child,
still pure and innocent. This changes his feelings for her to a loving
brother-like protector. He is accepted by and becomes close to the
family. ... At the end the narrator and the little dancer part with
the promise that they will meet again. Yet we understand, as the
narrator seems to realize, that this will never happen; this sweet
tender moment in life has passed, and the love they feel is
impossible.”[^04-01-04]

[^04-01-04]: “What Is a Plot Summary of The Izu dancer’?,” Answers.com, accessed November 28, 2019, [https://&#x200B;www&#x200B;.answers&#x200B;.com&#x200B;/Q&#x200B;/What&#x200B;_is&#x200B;_a&#x200B;_plot&#x200B;_summary&#x200B;_of&#x200B;_The&#x200B;_Izu&#x200B;_dancer](https://www.answers.com/Q/What_is_a_plot_summary_of_The_Izu_dancer).

(The Seidensticker version gives the student’s and the girl’s ages as
nineteen and thirteen respectively, the Holman version as twenty and
fourteen. I presume this is because Holman is literally translating
ages written according to the older Japanese system, in which a child
is considered to be one year old at birth and their age increases by
one year at every New Year.)

On the one hand, one could agree with critic Mark Morris that this is
a story “about cleansing, purification ... \[a\] narrative vision that
... generates impulses of release, near jouissance, by means of an
effacement of adult female sexuality and its replacement by an
impossible white void of virginity ....,” and see it as a worthy
literary accomplishment.[^04-01-05] On the other hand, if one is
familiar with the many anime and manga featuring prepubescent “waifus”
worshiped for their purity and innocence then the story can be a bit
harder to take.

[^04-01-05]: Mark Morris, “Orphans,” review of *The Dancing Girl of Izu, and Other Stories*, by Yasunari Kawabata, trans. J. Martin Holman, *New York Times*, October 12, 1997, [https://&#x200B;archive&#x200B;.nytimes&#x200B;.com&#x200B;/www&#x200B;.nytimes&#x200B;.com&#x200B;/books&#x200B;/97&#x200B;/10&#x200B;/12&#x200B;/reviews&#x200B;/971012&#x200B;.12morrist&#x200B;.html](https://archive.nytimes.com/www.nytimes.com/books/97/10/12/reviews/971012.12morrist.html).

So, how has *The Dancing Girl of Izu* been able to inspire at least
six films, three television dramas, and (in *Sweet Blue Flowers*) an
adaptation for the stage deemed suitable for a production by middle
schoolers in a stodgy girl’s academy? Some adaptations side-step the
implications of the plot by aging the dancing girl up. For example, in
the 1933 silent film version, the girl’s age goes unmentioned, and the
twenty-four-year-old star Kinuyo Tanaka is hard to mistake for a
minor.[^04-01-06]

[^04-01-06]: *The Dancing Girl of Izu*, directed by Heinosuke Gosho (Shochiku, 1933), 1 hr., 32 min., [https://&#x200B;www&#x200B;.youtube&#x200B;.com&#x200B;/watch&#x200B;?v&#x200B;=yd36RJ0nzdM](https://www.youtube.com/watch?v=yd36RJ0nzdM).

Other adaptations age the student down, as the Fujigaya production
presumably does. And some may not care, just as many people watching
the anime adaptation of *Sailor Moon* don’t seem to care that
thirteen-year-old Usagi Tsukino has a boyfriend who’s a university
student.

The more interesting question is, does Takako Shimura care? I have no
way of knowing. But I will repeat what I have written multiple times
now, that *Sweet Blue Flowers* seems to implicitly endorse
relationships between equals (Akira and Fumi, Orie and Hinako)
relative to relationships between those unequal in age or other
aspects (Chizu and Fumi, Yasuko and Fumi, and Ko and Kyoko). So from
that point of view, I hope you’ll forgive me if I interpret Shimura’s
reference to *The Izu Dancer* as a subtle hint that contemporary
Japanese society still has some issues when it comes to young girls
and older guys.
