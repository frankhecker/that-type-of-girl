---
title: "Journey to the West"
---

## Journey to the West

I’ve previously noted that the VIZ Media edition of *Sweet Blue
Flowers* is relatively Westernized compared to many other translated
manga, particularly in its complete omission of Japanese
honorifics. As a result, some may accuse the VIZ editorial team of
removing the Japanese aspects of the manga to appeal to Western
audiences---analogous to past practices that turned Usagi Tsukino of
*Sailor Moon* into “Serena” and her boyfriend Mamoru into “Darien
Shields.”
{:first}

But *Sweet Blue Flowers* was already relatively deficient in
“Japaneseness” in its original form as *Aoi hana*, especially compared
to other manga and anime set in Japanese high schools. For example, it
lacks (or has in only minimal form) scenes set at cultural festivals,
typically the sites of romantic encounters. Fujigaya Women’s Academy
does have a cultural festival independent of the theater festival, but
it is shown on only one page (*SBF*, 1:201). Matsuoka Girls’ High
School has no cultural festival at all---a source of continual
complaints by Yassan, Pon, and Mogi.

*Sweet Blue Flowers* also lacks any scenes set at fireworks shows,
again a common manga and anime setting where couples can show
affection and boys can compliment girls on how good they look wearing
yukata.

Even more interesting, *Sweet Blue Flowers* has almost no scenes that
depict or even hint at traditional Japanese religious practices
associated with Buddhism or Shintō. The characters do not visit
temples at the new year or for a festival---again, all staples of
conventional high school manga and anime. Also, none of the scenes set
in the characters’ houses depict the traditional alcoves containing
pictures of the deceased (for example, Fumi’s dead grandmother) and
associated offerings.[^05-03-01] With one minor exception, the only
religious symbols, buildings, or people depicted in *Sweet Blue
Flowers* are those associated with Christianity, a Western import to
Japan.[^05-03-02]

[^05-03-01]: Although I doubt that Shimura explicitly intended this, the absence of markers of “ancestor worship” is consistent with the rejection of age-based hierarchies that I see as a central theme in *Sweet Blue Flowers*.

[^05-03-02]: The minor exception is a shrine gate (*torii*) shown as part of a Kamakura streetscape (*SBF*, 4:200). However, the manga does not depict the shrine itself.

Beyond religion, the characters of *Sweet Blue Flowers* are shown in
several contexts as oriented toward the West and (by implication)
rejecting at least certain aspects of Japanese culture. Most notably,
both Yasuko and Kawasaki leave Japan to study abroad in England. While
there, they live an idyllic existence in a cozy English house with a
friendly English landlady, and Kawasaki even acquires an English
boyfriend (*SBF*, 4:246, 4:338). They are followed in turn by Akira
and her fellow Fujigaya students, who travel to England on their
school trip, and later by Ueda, who also elects to study in England
(4:239--43, 4:337--39).

Unlike the students at the exclusive and expensive Fujigaya Women’s
Academy, the Matsuoka students can’t afford to go abroad for school
trips. However, even though they stay close to home, they reject a
stereotypical “Japanese” school trip. In particular, they do not even
consider a trip to Kyoto, the traditional capital and symbolic heart
of Japan that is the go-to destination for school trips in most manga
and anime.

Instead, their main options appear to be a visit to the park-like
island setting of Yakushima (off the southern tip of Kyushu) or the
Huis Ten Bosch theme park near Nagasaki (*SBF*, 4:101). Huis Ten Bosch
features a collection of imitation Dutch buildings commemorating the
time when Nagasaki was a Dutch trading post where Western goods and
(perhaps more important) Western ideas first entered Japan.

Ultimately the students choose Huis Ten Bosch and Nagasaki over
Yakushima---not to mention Kyoto (*SBF*, 4:108). However, when the
Matsuoka school trip is depicted in a later chapter of *Sweet Blue
Flowers*, the focus is not on the imitation-Western setting of Huis
Ten Bosch, but on the real-life Glover Garden in Nagasaki---home to a
Scottish merchant who was a key figure in Japan’s early
industrialization (4:118, 4:131--35). Even Glover Garden does not
satisfy their appetite for things Western, though: Fumi and her
friends still prefer the real West to the West-in-Japan, dreaming of
trips to Europe and exclaiming, “I wanna go to London too!”
(4:219--20, 4:244, 4:293--94).

What might this focus on the West and Western-influenced settings mean
in the context of *Sweet Blue Flower*? Erica Friedman discusses a
1990s yuri trope, “Going to America:” “‘America’ was the place where
lesbians fantasized they could go to be free. Many a classic Yuri
manga would end with the two women ‘Going to America’ as a cipher for
‘and they lived happily ever after.’”[^05-03-03]

[^05-03-03]: Friedman, *By Your Side*, 70.

England serves a similar function in *Sweet Blue Flowers*. It is where
the characters can go (in body or in spirit) to do things they cannot
do in Japan. Yasuko escapes the prison of her role as the “girl
prince” and becomes a better and happier person (*SBF*, 4:22--23,
4:130--31, 4:242). Akira can confide in Yasuko and have a candid
conversation about her relationship with Fumi (4:252--54,
4:266--67).[^05-03-04] And with the help of an intervention by Hinako,
the editor-in-chief of the Fujigaya student newspaper overcomes her
apparent anger at Hinako for rejecting her confession and repents of
her earlier gossiping about Hinako’s sexual orientation (4:247--49,
4:310--12).

[^05-03-04]: It’s also worth noting that although Akira doesn’t like flying in general, the manga shows her getting airsick only on the return trip. It’s as if her body is rebelling against leaving the West and going back to Japan (*SBF*, 4:81, 4:262--64).

In the middle of the scenes of Glover Garden, Shimura inserts a panel
showing a sign advertising a tea house named “Freedom,” the English
edition’s translation of *Jiyūtei* (“freedom pavilion”) (*SBF*,
4:134). Under that name, the building housed one of the first
Western-style restaurants in Japan, founded by a Japanese chef in the
early Meiji era.[^05-03-05]

[^05-03-05]: “Former Jiyūtei Restaurant,” Nagasaki Minamiyamate Glover Partners, accessed June 20, 2022, [https://&#x200B;www&#x200B;.glover-garden&#x200B;.jp&#x200B;/former-jiyutei-restaurant](https://www.glover-garden.jp/former-jiyutei-restaurant).

A few years later, the word “*jiyū*” (“freedom”) was used first in the
name of the Freedom and People’s Rights Movement and then in the name
of the Jiyūtō (“Freedom Party” or “Liberal Party”). Both were founded
by Taisuke Itagaki, the model for the character of Kiyohara in the
play *Rokumeikan* (played by Ueda).

The focus on the West in *Sweet Blue Flowers* (and, by implication, on
freedom as experienced in the West) harks back to the Meiji era when
Japan attempted to use Western ideas and technology to make itself
into a modern nation. At the same time, Western-style education for
girls and Western ideals of individualism and pure romantic love gave
rise to Class S culture and literature.

In its characters’ obsession with the West and Western things, *Sweet
Blue Flowers* also looks forward: to a day when the hopes raised for
the status of women in the Meiji era (that “new, wonderful age,” as
one of the characters in *Rokumeikan* calls it[^05-03-06]) will be
fulfilled, and Akira, Fumi, and their friends will “experience a full
life in the new era” (*SBF*, 3:92).

[^05-03-06]: Mishima, *Rokumeikan*, 8.
