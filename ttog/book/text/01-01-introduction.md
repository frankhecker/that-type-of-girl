---
title: "Introduction"
---

## Introduction

I begin my consideration of *Sweet Blue Flowers* by looking at its
author, her manga in general, and what I consider to be the main
themes of *Sweet Blue Flowers* in particular.
{:first}

Very little has been published about Takako Shimura in English. Her
English Wikipedia entry has a scant seven sentences apart from the
listing of her works. The Japanese Wikipedia entry is a bit longer but
does not appear to have much more information about Shimura herself.

According to Wikipedia, Shimura was born on October 23, 1973, in
Kanagawa prefecture. I could find no indication of where exactly in
the prefecture Shimura was born. However, Kanagawa prefecture includes
both the port of Yokohama and the popular tourist destination
Kamakura, in which *Sweet Blue Flowers* is set.

Shimura’s first published work under that name was in February 1997,
when she was twenty-three years old. Since then, she has published
about two dozen works, depending on how you count them; some of these
were one-shots, while others were serialized comics later published in
book form.

Relatively few of Shimura’s works are available in licensed English
translations. Of her works listed on Wikipedia, only four have had
official releases in English at the time of writing: *Sweet Blue
Flowers* (*Aoi hana*), *Happy-Go-Lucky Days*[^01-01-01] (*Dōnika naru
hibi*), *Wandering Son*[^01-01-02] (*Hōrō musuko*), and her more
recent effort, *Even Though We’re Adults* (*Otona ni natte
mo*).[^01-01-03]

[^01-01-01]: Takako Shimura, *Happy-Go-Lucky Days*, trans. RReese, 2 vols. (Gardena, CA: Digital Manga Guild, 2013). Kindle.

[^01-01-02]: Takako Shimura, *Wandering Son*, trans. Rachel Thorn, 8 vols. (Seattle: Fantagraphics Books, 2011--).

[^01-01-03]: Takako Shimura, *Even Though We’re Adults*, trans. Jocelyne Allen, 3 vols. (Los Angeles: Seven Seas Entertainment, 2021--).

Of the three earlier works previously published, only *Sweet Blue
Flowers* is still readily available in complete form. Even that
occurred after multiple false starts: volume 1 of *Aoi hana* was
translated into English and published as *Sweet Blue Flowers* by
JManga and then by Digital Manga,[^01-01-04] but neither publisher
released any further volumes. The manga was also adapted into an anime
series of eleven episodes, subsequently released in a subtitled
English version.[^01-01-05] (Plans for a second season were abandoned,
apparently due to poor sales of the anime’s DVDs in Japan.)

[^01-01-04]: Takako Shimura, *Sweet Blue Flowers*, trans. Jeffrey Steven LeCroy, vol. 1 (Gardena, CA: Digital Manga, 2014), Kindle.

[^01-01-05]: *Sweet Blue Flowers*, directed by Kenichi Kasai (2009; Grimes, IA: Lucky Penny Entertainment, 2013), DVD.

*Happy-Go-Lucky Days* was published complete in two volumes (digital
only), but is no longer available for sale in the US. However, an
anime film featuring selected stories from the manga was released in
2020, including a version with English subtitles.[^01-01-06]

[^01-01-06]: *Happy-Go-Lucky Days*, directed by Takuya Satō (2020; Houston: Sentai Filmworks, 2021), 55 min., Blu-ray Disc, 1080p HD.

Publication of *Wandering Son*, Shimura’s most well-known work, was
halted after the release of eight hardcover volumes (of a total of
fifteen). It is now out of print and is not available in digital
format. It, too, received an incomplete anime adaptation, subsequently
released with English subtitles for online streaming in the US and
other countries.[^01-01-07]

[^01-01-07]: *Wandering Son*, directed by Ei Aoki (Aniplex, 2011), [https://&#x200B;www&#x200B;.crunchyroll&#x200B;.com&#x200B;/hourou&#x200B;-musuko&#x200B;-wandering&#x200B;-son](https://www.crunchyroll.com/hourou-musuko-wandering-son).

At least two long interviews of Shimura have been published but both
remain untranslated into English. The afterwords in the various
volumes of *Sweet Blue Flowers* and *Wandering Son* contain Shimura’s
comments on various aspects of her life. However, these are mostly
trivial and don’t shed much light on how she approaches her work in
terms of favorite themes, opinions on social and cultural issues, and
so on.[^01-01-08]

[^01-01-08]: However, as a fan of the film director Yasujirō Ozu I was amused to learn that Shimura was once so entranced by an old television drama featuring Chishū Ryū, who portrayed older men in many of Ozu’s most famous films, that she bought a book of photographs of Ryū, titled *Grandpa*. Shimura, *Wandering Son*, 5:222.

To know more about Shimura as an artist, we need to therefore look at
the works themselves, starting with where they were published.

Many of Shimura’s manga, including *Wandering Son*, were serialized in
*Comic Beam* magazine. Both *Happy-Go-Lucky Days* and (as previously
noted) *Sweet Blue Flowers* were serialized in *Manga Erotics F*
magazine. Both of these magazines are (or were, in the case of *Manga
Erotics F*) part of what’s been referred to as a “fifth column” of
Japanese manga magazines: existing outside of the four main
demographic-based categories of magazines (for boys, girls, men, and
women) and featuring a wide variety of stories in various genres.[^01-01-09]

[^01-01-09]: Erica Friedman, “Overthinking Things 03/02/2011,” *The Hooded Utilitarian* (blog), March 2, 2011, [https://&#x200B;www&#x200B;.hoodedutilitarian&#x200B;.com&#x200B;/2011&#x200B;/03&#x200B;/overthinking&#x200B;-things&#x200B;-03022011](https://www.hoodedutilitarian.com/2011/03/overthinking-things-03022011).

Thus the first thing we can discern about Shimura is that although her
manga, like *Sweet Blue Flowers*, may feature children from elementary
to high school age, for the most part they were written for an
all-genders adult audience, with all that implies in terms of both
content (including sexual content) and the knowledge that she assumes
on the part of her readers.

As far as I can tell, most if not all of Shimura’s manga are set in
contemporary Japan: no historical fiction, and no fantasies set in
other worlds---although supernatural elements are sometimes present,
as in some of the stories in *Happy-Go-Lucky Days*. Shimura is also
known for her focus on issues of sex and gender, and in particular for
her stories about LGBTQ characters, including the transgender youth of
*Wandering Son* and the lesbians of *Sweet Blue Flowers*. These are
also set in present-day Japan and offer commentary (albeit often
indirect) on contemporary Japanese society.

Keeping the above in mind, here are my tentative thoughts as to what
*Sweet Blue Flowers* is about:

First, *Sweet Blue Flowers* pays homage to past works in Japanese
dealing with romance between girls or women, including in particular
the early twentieth-century “Class S” genre set in all-girls
schools. Shimura assumes that readers are familiar with the tropes of
this genre and makes mention of and alludes to the most well-known
Class S author, Nobuko Yoshiya, and her most famous work, the *Hana
monogatari* (*Flower Tales*) series of short stories (*SBF*, 1:6,
1:190).[^01-01-10]

[^01-01-10]: Nobuko Yoshiya, *Hana monogatari*, 2 vols. (Tokyo: Kawade Shobō Shinsha, 2009). With the exception of one story (“Yellow Rose”), the *Hana monogatari* series is not available in an official English translation.

I believe that *Sweet Blue Flowers* is at the same time a
consciously-intended critique of the Class S genre and (by
implication) subsequent works in the genre that came to be known as
“yuri.” This critique is directed not only at particular yuri tropes,
such as the “girl prince,” but also at the assumptions embedded in
many if not most yuri works, including in particular the idea of
relationships between women structured according to a hierarchy of age
and status.

In contrast, I see *Sweet Blue Flowers* as highlighting relationships
between women who are equal to each other and meet each other as
individuals, relationships that are (by implication) opposed to and
(to the extent possible) exist outside of the
hierarchically-structured patriarchal society of Japan. Although the
manga does not fully engage with what such an opposition and existence
would entail in practice, it is far more grounded in reality than yuri
works that posit a “yuritopia” in which men do not exist.

The themes of *Sweet Blue Flowers* are embodied in its two main
characters, Fumi Manjome and Akira Okudaira, and in their friend,
Kyoko Ikumi, whose presence in the story is so large as to almost make
her a third main character. The three girls together can be thought of
as representing three different “eras” of yuri: Kyoko the Class S past
of ephemeral relationships between schoolgirls ultimately destined for
arranged marriages, Akira the “pure yuri” present of sexual innocence
and shy and tentative romance, and Fumi the LGBTQ future of women who
come to self-consciously identify as lesbians.

I believe that Takako Shimura would have expected her adult audience
to bring to their reading of *Sweet Blue Flowers* at least a general
familiarity with the history of the Class S and yuri genres and with
key works in those genres. The following chapters discuss various
aspects of that history that I think are useful for a deeper
understanding of *Sweet Blue Flowers* and its place in the yuri genre.
