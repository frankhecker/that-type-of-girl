---
title: "Old School, New School"
---

## Old School, New School

As I mentioned earlier, *Sweet Blue Flowers* pays homage to and (to
some extent) subverts traditional Class S and yuri tropes. One of the
first ways that shows up is in the contrast drawn between Fujigaya
Women’s Academy and Matsuoka Girls’ High School, the schools attended
by Akira Okudaira and Fumi Manjome, respectively.
{:.first}

Fujigaya is the type of all-girls school made famous in works like
*Maria Watches Over Us*: separated from the normal world by a tunnel
at the end of a long uphill path (*SBF*, 1:20), a Catholic-flavored
hothouse in which (according to the trope) the students engage in
never-ending rounds of crushes and potential crushes. As Akira’s
mother tells her, “Soon you’ll bring home a girlfriend!” (1:27).

(The call-out to *Maria Watches Over Us* is even more explicit in the
Digital Manga translation, in which Akira’s brother asks her, “So does
everyone say ‘good day to you’?,” a common English translation of the
formal expression “*gokigenyō*” made famous by *Maria*.)

However, despite the association with S relationships, Fujigaya is
better thought of as an institution whose primary purpose is promoting
and propagating traditional values around marriage and class. Michi
Kawai, who ran a Christian school in Tokyo in the 1930s and 1940s,
wrote: “There is an increasing tendency for well-to-do Protestant
families in Japan to send their children, especially girls, to
Catholic institutions. ... Is it the policy of Catholics to gather the
children of the wealthy and those of high station in life?”[^02-03-01]

[^02-03-01]: Michi Kawai, *My Lantern*, 3rd ed. (Tokyo: privately-pub., 1949), 224.

Kawai’s conclusion: “The families who are socially ambitious wish to
send their children to exclusive schools so that after graduation
their daughters may be married into families of social standing. This
is a natural desire for any parent, and Catholic educationists were
clever enough to diagnose the need and the church rich enough to start
one or two very exclusive schools in large cities with lovely big
campuses and many consecrated Sisters of different orders, working
together with efficient Japanese staffs.”[^02-03-02]

[^02-03-02]: Kawai, *My Lantern*, 225.

Kawai also noted the recent founding of the Catholic-sponsored Nogi
Girls’ High School near Kamakura: “Very soon the school will be
overflowing with pupils because it again meets the demand of the
people who have the money and the leisure to live all the year round
in that exclusive neighborhood.”[^02-03-03]

[^02-03-03]: Kawai, *My Lantern*, 225. Nogi Girls’ High School was named after Count (or General) Maresuke Nogi, a Japanese war hero of the Meiji era, subsequent head of the Peers’ School that educated the sons of Japan’s noble families, and a mentor to the future Emperor Hirohito.

Nogi Girls’ High School was subsequently renamed Shonan Shirayuri
Gakuen High School, and under that name exists to this day, its junior
and senior high school campus located only a short distance inland
from Enoshima.[^02-03-04] It also has an affiliated kindergarten and
elementary school. Although the history of Shonan Shirayuri Gakuen
High School dates back only to just before World War II, its setting,
mission, and target demographic make it the closest real-life
equivalent to Fujigaya Women’s Academy.[^02-03-05]

[^02-03-04]: Shonan Shirayuri Gakuen, “History of the School,” accessed December 5, 2021, using Google Translate, [https://&#x200B;www&#x200B;.shonan&#x200B;-shirayuri&#x200B;.ac&#x200B;.jp](https://www.shonan-shirayuri.ac.jp). “Shonan” refers to the coastal area centered on Enoshima, while “Shirayuri Gakuen” translates literally as “White Lily Academy.”

[^02-03-05]: It’s also worth noting that “Fujigaya” shares its initial kanji with Fujisawa, the city west of Kamakura where Shonan Shirayuri Gakuen’s various schools are located.

The most notable difference is that as a more recent institution
Shonan Shirayuri Gakuen has more modern architecture. As Shimura
mentions in the afterwords to parts 1 and 2 of volume 1, she used the
Kamakura Museum of Literature as the model for the physical appearance
of Fujigaya (*SBF*, 1:193, 1:378). The museum grounds even feature a
tunnel like the one Akira walks through on her first day of school
(1:20).[^02-03-06]

[^02-03-06]: “Category:Literature Museum of Kamakura,” Wikimedia Commons, Wikimedia Foundation, last modified June 23, 2018, [https://&#x200B;commons&#x200B;.wikimedia&#x200B;.org&#x200B;/wiki&#x200B;/Category:Literature&#x200B;_Museum&#x200B;,&#x200B;_Kamakura](https://commons.wikimedia.org/wiki/Category:Literature_Museum,_Kamakura).

In contrast to Fujigaya, Matsuoka appears to be a typical
nondenominational single-sex high school. Akira’s mother sees it as
particularly academically rigorous (*SBF*, 1:36), but it’s otherwise
indistinguishable from other modern educational institutions. Based on
evidence in other volumes (2:309, 3:311--12), Matsuoka appears to be
located near the Kamakurakōkō-Mae station on the Enoden railway, near
the site of the real-life Kamakura Senior High School, a coeducational
public high school.

Unlike Fujigaya, which names its classrooms after flowers (Akira is in
the Wisteria class),[^02-03-07] Matsuoka uses a more prosaic naming
scheme (Fumi starting in class 1-A). To further enhance the contrast,
Matsuoka students wear dark jumpers with blouse, tie, and (optional)
jacket---a handsome but severe uniform that’s a far cry from the
(often fetishized) sailor-style uniforms worn at Fujigaya and
countless other schools in anime and manga.[^02-03-08]

[^02-03-07]: As previously noted, the kanji for “wisteria” is the same as that in “Fujigaya” and “Fujisawa.”

[^02-03-08]: And in real life, too: the uniforms worn by the girls at Shonan Shirayuri Gakuen High School resemble the Fujigaya uniforms. They feature a fleur-de-lis, the lily-derived design that is the school’s emblem.

When schools like Fujigaya were first established during the Meiji
era, I’m sure that the first generation of students saw them as
excitingly modern. However, in the twenty-first-century timeframe of
*Sweet Blue Flowers* the manga’s characters perceive Fujigaya as a
bastion of traditional values. When Fumi and her friends from Matsuoka
visit Fujugaya, they admire it for its refined atmosphere and elegant
trappings: tea parties! a chapel! bay windows in the library!  (*SBF*,
1:85--86). If *Sweet Blue Flowers* were a traditional yuri work, I’m
sure that the story would never stray beyond its ivied walls.

But although *Sweet Blue Flowers* pays homage to traditional yuri, the
trajectory of this first volume points beyond it. I don’t think that
it’s a coincidence that its most modern character, Fumi Manjome, is a
student at Matsuoka and not at Fujigaya, or that her first kiss with
Yasuko Sugimoto takes place among the austere steel shelves of
Matsuoka’s library, not the stylish wooden shelves of Fujigaya’s
(*SBF*, 1:114--16).
