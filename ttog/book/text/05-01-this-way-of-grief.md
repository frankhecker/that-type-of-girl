---
title: "This Way of Grief"
---

## This Way of Grief

In a previous chapter, I discussed the fate of Asako in the play
_Rokumeikan_ as a cautionary tale for Kyoko. But there is another
cautionary tale for her much closer to hand, namely the life of her
mother, Kayoko.
{:first}

If we take the story of *Sweet Blue Flowers* as taking place in the
early years of the twenty-first century, Kyoko would have been born
soon after the beginning of the Heisei era in 1989. Kayoko would then
have married sometime in the late 1980s, in the waning years of the
Shōwa era and Japan’s economic boom.

Kayoko’s marriage was to some degree arranged. For many years she had
been friends with her future husband, Akihiko. However, rather than
confessing to her directly, Akihiko requested a marriage interview
“behind her back,” as it were, asking her parents (or other
relatives---Kayoko’s aunt is the only one mentioned). No one bothered
to tell Kayoko about this (*SBF*, 4:60--61).

Kayoko’s marriage, and her subsequent relationship with her husband,
are presented as adhering to patriarchal norms. Unlike the other
weddings pictured in the manga, Kayoko is shown not in a Western-style
wedding dress but wearing a white wedding kimono (_shiromuku_). Her
headdress (_tsunokakushi_) symbolizes the effacement of her
selfishness and jealousy (covering its “horns”) in favor of obedience
and devotion to her husband (*SBF*, 4:62).

Before his marriage, Akihiko is portrayed as somewhat tentative and
shy, but that is the last time we see his face: he appears obscured in
Shimura’s depiction of their wedding (*SBF*, 4:62).[^05-01-01] Like
the father of the Sugimoto sisters, Akihiko has disappeared into his
role as familial patriarch, and traditional attitudes downplayed
before his marriage now come to the fore: for example, he prizes the
fact that Kayoko was a virgin before her marriage (4:62--63).

[^05-01-01]: This is reminiscent of the conclusion of Yasujirō Ozu’s 1949 film *Late Spring*, in which Noriko defers to her father’s wishes and marries a man recommended to her by her aunt. She is shown dressed in kimono and *tsunokakushi*, ready for her wedding, but the film never shows her husband’s face. *Late Spring*, 1:38:12.

These attitudes again become salient in the aftermath of Kyoko’s
losing her way in a forest as a young child. For Kyoko, this is a
major event in her relationship with Ko, continually referenced
throughout the manga. Ko’s rescue of Kyoko is a metaphor for their
relationship: Ko sees in Kyoko someone he can love and protect, and
Kyoko is alternately grateful for his coming to her aid and upset that
she needed it.

But for Kayoko, that same event marks the beginning of the unraveling
of her relationship with Akihiko, as he accuses her of failing to
protect his child (*SBF*, 4:65). Kayoko takes the blame on herself,
and between Kyoko’s crying and her husband’s scolding, concludes that
she is “truly an awful mother” (4:66).

Kayoko speculates that her shortcomings are why Akihiko grows
distant. But as she herself recognizes, it’s more likely that he had
simply grown tired of her and (as her sisters told her) preferred the
company of younger women (*SBF*, 4:68--69).

Whatever the reason, it’s strongly implied that Akihiko essentially
abandoned his wife and daughter to their own devices. He presumably
continues his monetary support of Kayoko and Kyoko, including paying
for Kyoko’s education. But we do not encounter him elsewhere in the
manga.

Akihiko is not alone in distancing himself from his family. Ko’s
(unnamed) father has also been absent for years (*SBF*, 2:50), and as
noted previously, the father of the Sugimoto sisters shows up for
Kazusa’s wedding (2:94) but is otherwise unseen, unheard, and
unmentioned.

His wife Chie Sugimoto appears to accept this with equanimity, as does
Ko’s mother. What distinguishes Kayoko and makes her the object of
criticism by others is not being a wife mostly abandoned by her
husband---this is portrayed as a normal state of affairs, at least for
the class to which these families belong---but rather her reaction to
it.

Although this is not made explicit in the text, Kayoko appears to have
become severely depressed in the wake of her husband’s leaving her. We
do not see her face in the first three volumes but only hear her
conversations with Kyoko and Ko. It is not until volume 4 that we
first see her in the present day (*SBF*, 4:67--71). She appears
haggard and disheveled---a far cry from the young woman depicted
before her marriage, and when Kyoko got lost in the forest
(4:64)---although at this point in the story, she would likely still
have not yet turned forty.

Kayoko’s troubles, in turn, affected her relationship with her
daughter, as Kyoko’s emotions became a toxic stew of shame, guilt,
pity, and anger: “I couldn’t stop her from breaking. It was Dad’s
fault, but I couldn’t stop him either. I hate both of them. ... My
whole family is worthless” (*SBF*, 3:101, 3:103).

The situation was complicated by Kayoko turning to religion in her
despair. In volume 2, Kyoko thinks to herself, “Mom has her own
special god. One just for her. ... Mom has her own special god
... because she needed one” (*SBF*, 2:301, 2:324). In volume 3, we
learn that Kayoko participates in a religious community of some
sort. Kyoko again: “I learned that she and a lot of people I didn’t
know were praying to God. And her God is a little different than the
one I know” (3:100).[^05-01-02]

[^05-01-02]: The difference in capitalization (“god” vs. “God”) between the three quotes appears not to be significant. The Japanese text uses *kami-sama* in all three cases.

What this difference might be exactly is never explained, but it was
different enough from Japan’s traditional mix of Shinto and Buddhism,
or the Catholicism of Fujigaya Women’s Academy, to upset Kyoko: “But
that embarrassed me. It was my father’s fault ... but I was ashamed of
my mother. I thought she was pitiful” (*SBF*, 2:324).

Kayoko’s mental state becomes a topic of conversation for others as
well. When Kyoko and her friends visit Ko’s family’s summer home,
Akira overhears Ko’s mother expressing her opinion about Kayoko:
“She’s sick. And I don’t like that sort of thing. ... Any sort of
complication.” In Ko’s mother’s mind, Kayoko’s troubles threaten to
taint the previously-arranged union of Ko and Kyoko and the future of
the Sawanoi line: “It looks bad for someone like that to join the
family. I feel sorry for Kyoko, but ... But what if she has a child?”
(*SBF*, 2:50--51).

Again, though this is not made explicit, the reasonable conclusion
here is that Ko’s mother (and others) think that Kayoko is mentally
ill and shun her because of it. If so, this is consistent with what
appears to be relatively more severe stigmatization of mental illness
in Japan versus other countries. “In the Japanese general population,
few people think that people can recover from mental
disorders. ... [The] majority of the general public in Japan keep a
greater social distance from individuals with mental illness,
especially in close personal relationships.”[^05-01-03]

[^05-01-03]: Shuntaro Ando, Sosei Yamaguchi, Yuta Aoki, and Graham Thornicroft, “Review of Mental-Health-Related Stigma in Japan,” *Psychiatry and Clinical Neurosciences* 67, no. 7 (November 2013), 471, [https://&#x200B;onlinelibrary&#x200B;.wiley&#x200B;.com&#x200B;/doi&#x200B;/10&#x200B;.1111&#x200B;/pcn&#x200B;.12086](https://onlinelibrary.wiley.com/doi/10.1111/pcn.12086).

In a different story, Kayoko might have sought professional help. In
*Sweet Blue Flowers*, Kayoko’s recovery, such as it is, begins when
(after a long absence) Ko visits her, and then he and Kyoko reconcile
(*SBF*, 4:69--72). After that, Kayoko throws herself into planning for
their subsequent wedding, whatever objections Ko’s family might have
had presumably having been overcome. Even Ko’s mother, who was
previously so negative towards Kayoko and (by extension) Kyoko, takes
note of Kayoko’s work and looks forward to seeing Kyoko as a bride
(4:333).

And in the context of her social milieu and the expectations placed on
her, Kayoko has every right to be satisfied and happy: whatever the
state of her own marriage, she has achieved her long-time goal of
seeing her daughter make a good match and can rest from her
labors. However, whether Ko will be another Akihiko, and Kyoko another
Kayoko, is a question left hanging in the joy of their wedding---a
wedding where both Akihiko and Kayoko are presumably present but are
nowhere to be seen.
