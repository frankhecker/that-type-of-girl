---
title: "The Halting Problem"
---

## The Halting Problem

Why does *Sweet Blue Flowers* end where it does? To answer that, let’s
look at a different question, or rather multiple questions: How long
should a manga run? How long can it run? Will it ever end?
{:first}

In western comics, this last question is unanswerable: corporate
ownership of copyrights and trademarks means that a popular comic can
exist in perpetuity, like a corporation itself. It’s impossible to
predict which comics will ever halt publication and which will not.

On the other hand, there’s an expectation that manga will not survive
the artist’s death---that one day we will see an end to long-running
works. But that day may not be soon.[^05-07-01]

[^05-07-01]: There are at least twenty-five manga series with a hundred or more volumes in print, most of them written and illustrated by a single person. “List of Manga Series by Volume Count,” Wikipedia, last modified January 31, 2022, [https://&#x200B;en&#x200B;.wikipedia&#x200B;.org&#x200B;/wiki&#x200B;/List&#x200B;_of&#x200B;_manga&#x200B;_series&#x200B;_by&#x200B;_volume&#x200B;_count](https://en.wikipedia.org/wiki/List_of_manga_series_by_volume_count).

Even with other manga that are not so long-lived, commercial success
can lead to extended runs as the story gets stretched out to satisfy
readers’ desires for more entertainment and publishers’ desires for
more profit. Other series are canceled well before the end of their
natural life, with the story left hanging or rushed to a conclusion.

Where does *Sweet Blue Flowers* fit in this picture? It is relatively
short by Japanese standards at only eight volumes (in the original
Japanese edition). For example, *Wandering Son* went to fifteen
volumes, and it’s not unusual for manga to go to thirty or forty.

I’ve seen occasional speculation that *Sweet Blue Flowers* was brought
to a close prematurely. Indeed, the last chapters seem somewhat
rushed. For example, in chapter 51, Shimura resorts to the shortcut of
having an omniscient narrator herald Akira’s change of heart (*SBF*,
4:325), as opposed to showing this happening through more gradual plot
developments and internal monologues.

On the other hand, a case can be made that Shimura ended the manga
where she wanted to end it and that it had come to a natural stopping
point.

First, why end it at fifty-two chapters? Shimura may have done this in
homage to Nobuko Yoshiya’s *Hana monogatari*, which contained
fifty-two stories. The title of the last chapter is “Sweet Blue
Flowers” (“*Aoi hana*” in the Japanese edition), repeating the title
of the manga itself, just as the title of the first chapter, “Flower
Story” (“*Hana monogatari*”) namechecked the title of Yoshiya’s series.

Assuming that this is intentional, I see this as Shimura concluding
the series by subtly highlighting its status as her response to
Yoshiya, the ideals of S relationships, and the Class S literary
genre.

Second, why end the manga at this point in the plot? Arguably, once
Fumi and Akira graduated from high school, it was time to wrap things
up. The manga as a whole, like many manga about the lives of Japanese
schoolgirls, is structured around the rhythm of the school year,
particularly the summer breaks and the Fujigaya theater festival.

Once that rhythm is absent, the framework underlying the plot is
broken, and the manga moves toward its conclusion. It lasts only long
enough to take the characters into adulthood, bring Kyoko’s story to a
conclusion with her marriage to Ko, and see Fumi and Akira enter into
a new phase of their relationship.

I think Shimura did rush things a bit to bring the story to an end at
a possibly symbolic fifty-two chapters. However, plotwise, I believe
she intended to end *Sweet Blue Flowers* exactly where she did, with
the reader left to imagine the continuation of Fumi and Akira’s life
together.
