---
title: "Homage to Yoshiya"
---

## Homage to Yoshiya

In the introduction, I noted that *Sweet Blue Flowers* builds and
comments on previous works in the yuri genre. That begins even before
the start of the story proper: chapter 1 is titled “Flower Story” in
homage to *Hana monogatari* (*Flower Tales*), a series of Class S
fictions written in the early twentieth century by author Nobuko
Yoshiya (*SBF*, 1:4).
{:.first}

There are at least four keys to understanding Yoshiya’s life and work:
she was a lesbian living in the patriarchal society of early
twentieth-century Japan, a literary prodigy, a child of relative
privilege and affluence, and the sole sister to three
brothers.[^01-03-01]

[^01-03-01]: Jennifer Robertson, “Yoshiya Nobuko: Out and Outspoken in Practice and Prose,” in *Same‐Sex Cultures and Sexualities: An Anthropological Reader*, ed. Jennifer Robertson (Malden, MA: Blackwell, 2005), 196--97, [https://&#x200B;doi&#x200B;.org&#x200B;/10&#x200B;.1002&#x200B;/9780470775981&#x200B;.ch11](https://doi.org/10.1002/9780470775981.ch11).

Her family belonged to the middle class (her father was the police
chief of a provincial city), so Yoshiya escaped being sent by her
family to work in a factory or brothel, the fate of many a girl in
Meiji-era Japan. Instead, she was able to attend school, where her
literary gifts were recognized by her teachers early on. Despite her
showing promise as a writer, she felt neglected by her mother, who
held very traditional views on men’s and women’s roles and showed
favoritism towards her brothers. After graduating from high school,
she was able to leave her home at the age of nineteen and move to
Tokyo to live in relative independence.[^01-03-02]

[^01-03-02]: Hiromi Tsuchiya Dollase, “Yoshiya Nobuko’s ‘Yaneura no nishojo’: In Search of Literary Possibilities in ‘Shōjo’ Narratives,” English supplement, *U.S.-Japan Women’s Journal*, no. 20/21 (2001), 153, [https://&#x200B;www&#x200B;.jstor&#x200B;.org&#x200B;/stable&#x200B;/42772176](https://www.jstor.org/stable/42772176).

As a lesbian Yoshiya was better equipped than most to spin compelling
tales of girls in S relationships, although the nature of Japanese
society in the early twentieth century placed certain limits on the
forms those tales could take, and the strategies she could pursue as
an author and as a woman who loved women. On the one hand, the
emerging magazines for girls popularized the idea of same-sex
relationships of affection within all-girls schools, influenced by
Western and Christian notions of romantic love and
individualism. Yoshiya was at the heart of this trend.

At the same time Christian morality and the scientific aura around
writings by Western sexologists promoted a view of same-sex
relationships as “diseased” or “abnormal” if taken beyond certain
bounds---thus, for example, the public scandal over the 1911 double
suicide of two twenty-year-old women who were former
schoolmates.[^01-03-03]

[^01-03-03]: Pflugfelder, “‘S’ is for Sister,” 153--55.

Such incidents were not uncommon for the time, and sensational stories
about them were a perennial feature in the popular press during the
period in which Yoshiya achieved fame and financial success as a
writer. Given that a persistent theme of these stories was the
potential danger of schoolgirl romances, exactly the sort of topics
Yoshiya was writing about, Yoshiya was forced to navigate these issues
in her art and her other writings, and in her own life. In doing this,
she can be seen as following three different strategies, depending on
the time and situation.

First, in *Hana monogatari* Yoshiya depicted schoolgirl romances (or
similar relationships between students and teachers) as inherently
limited in time and place, fated to bloom for a season and then to
wither and die under the harsh blasts of Japanese social norms, as the
girls entered adulthood and the responsibilities of marriage. The
alternative was, if not unthinkable, at least unspeakable.

For example, in “Yellow Rose” the teacher who spoke of the life and
loves of Sappho to her student companion finds herself powerless to
argue against the girl’s parents’ plans for an arranged marriage, and
instead instantly agrees to help persuade her into it: “She had a
whole array of arguments why it was not a good idea for parents to
decide whom their children married. But now, standing before these
particular parents, none of those arguments seemed the least
convincing.”[^01-03-04]

[^01-03-04]: Nobuko Yoshiya, *Yellow Rose*, trans. Sarah Frederick, 2nd ed. (Los Angeles: Expanded Editions, 2016), chap. 4, Kindle.

Yoshiya also took pains in her nonfiction writing to reassure the
Japanese public that schoolgirl relationships were proper and even
beneficial for the students involved, drawing on the work of the
English socialist Edward Carpenter to lend support to her
argument.[^01-03-05]

[^01-03-05]: Michiko Suzuki, “The Translation of Edward Carpenter’s *Intermediate Sex* in Early Twentieth-Century Japan,” in *Sexology and Translation: Cultural and Scientific Encounters Across the Modern World*, ed. Heike Bauer (Philadelphia: Temple University Press, 2015), 205--9.

Carpenter had in turn been influenced by the English poet and literary
critic John Addington Symonds. In the tradition of many Western
writers since the Renaissance, Symonds sought in the classical world
an alternative to Christian morality, privately publishing in 1883 the
innocuously titled *A Problem in Greek Ethics*.[^01-03-06] Symonds
claimed that the “boy love” of classical Greece---“a passionate and
enthusiastic attachment subsisting between man and youth, recognised
by society and protected by opinion”---at its best embodied a noble
ideal of masculine friendship: “The lover taught, the [beloved]
learned; and so from man to man was handed down the tradition of
heroism.”[^01-03-07]

[^01-03-06]: Shane Butler, “A Problem in Greek Ethics, 1867–2019: A History,” John Addington Symonds Project, accessed February 13, 2022, [https://&#x200B;symondsproject&#x200B;.org&#x200B;/greek&#x200B;-ethics&#x200B;-history](https://symondsproject.org/greek-ethics-history).

[^01-03-07]: John Addington Symonds, *A Problem in Greek Ethics, being an Inquiry into the Phenomenon of Sexual Inversion, addressed especially to medical psychologists and jurists* (London: privately-pub., 1901), 8, 13, [https://&#x200B;archive&#x200B;.org&#x200B;/details&#x200B;/cu31924021844950](https://archive.org/details/cu31924021844950).

Carpenter maintained a correspondence with Symonds and was sent a copy
of *A Problem in Greek Ethics* in 1892 or 1893[^01-03-08]. He applied
a similar model to contemporary British society and in particular to
education in his essay “Affection in Education,” written in the 1890s,
first published in 1899, and later included in his book *The
Intermediate Sex*.[^01-03-09] Carpenter decried “the confusion in the
public mind ... which so often persists in setting down any attachment
between two boys, or between a boy and his teacher, to nothing but
sensuality”: “Who so fit ([teachers] sometimes feel) to enlighten a
young boy and guide his growing mind as one of themselves, when the
bond of attachment exists between the two?”[^01-03-10]

[^01-03-08]: Symonds to Carpenter, 29 January 1893, in *Letters of John Addington Symonds*, ed. Herbert M. Schueller and Robert L. Peters, vol. 3, *1885--1893* (Detroit MI: Wayne State University Press, 1969), 810--811, [https://&#x200B;archive&#x200B;.org&#x200B;/details&#x200B;/letters&#x200B;of&#x200B;john&#x200B;add&#x200B;0003&#x200B;symo](https://archive.org/details/lettersofjohnadd0003symo).

[^01-03-09]: Josephine Crawley Quinn and Christopher Brooke, “‘Affection in Education’: Edward Carpenter, John Addington Symonds, and the Politics of Greek Love,” in *Ideas of Education: Philosophy and Politics from Plato to Dewey*, ed. Christopher Brooke and Elizabeth Frazer (London: Routledge, 2013), 255.

[^01-03-10]: Edward Carpenter, “Affection in Education,” in *The Intermediate Sex: A Study of Some Transitional Types of Men and Women* (New York: Mitchell Kennerly, 1912), 97, [https://&#x200B;www&#x200B;.google&#x200B;.com&#x200B;/books&#x200B;/edition&#x200B;/The_Intermediate_Sex&#x200B;/gbcNAAAAYAAJ](https://www.google.com/books/edition/The_Intermediate_Sex/gbcNAAAAYAAJ).

The primary focus of both Symonds and Carpenter was on male
homosexuality, with relationships between women either ignored or
implied to be inferior to those between men. When turning his
attention to schoolgirl relationships, Carpenter wrote that “they
are for the most part friendships of a weak and sentimental turn, and
not very healthy either in themselves or in the habits they lead
to.”[^01-03-11]

[^01-03-11]: Carpenter, “Affection in Education,” 98--99. Symonds was even more severe on the subject of lesbianism: after nodding toward the early example of Sappho, he claimed that “later Greeks, while tolerating, regarded it rather as an eccentricity of nature or a vice, than as an honourable and socially useful emotion. ... Consequently, while the Greeks utilised and ennobled boy-love, they left Lesbian love to follow the same course of degeneracy as it pursues in modern times.” Symonds, *A Problem in Greek Ethics*, 71.

Thus after the translation of *The Intermediate Sex* into Japanese (in
1914 and again in 1919) it was “quite surprising and unexpected that
in Japan this work was used to defend schoolgirl same-sex intimacy.”
In two essays published in 1921 and 1923 respectively, Nobuko Yoshiya
“ignored Carpenter’s negative presentation of female-female intimacy
and instead adopted his positive arguments for male-male attachments”
to justify relationships between an older girl and a younger one, or
between a teacher and one of her students.[^01-03-12]

[^01-03-12]: Suzuki, “The Translation of Edward Carpenter’s *Intermediate Sex*,” 206--8.

However, Yoshiya was not content with simply portraying intimacy
between women according to the template set by *Hana monogatari*, and
essayed a second strategy to address this topic. During the same
period that Yoshiya was publishing the stories of *Hana monogatari* in
girls’ magazines she also published what Erica Friedman has called
“the source material for much of what we consider to be ‘Yuri,’” the
novel *Yaneura no nishojo* (*Two Virgins in the Attic*).[^01-03-13]

[^01-03-13]: Erica Friedman, review of *Yaneura no nishojo*, by Nobuko Yoshiya, *Okazu* (blog), May 10, 2010, [https://&#x200B;okazu&#x200B;.yuricon&#x200B;.com&#x200B;/2010&#x200B;/05&#x200B;/09&#x200B;/yuri&#x200B;-novel&#x200B;-yaneura&#x200B;-no&#x200B;-nishojo](https://okazu.yuricon.com/2010/05/09/yuri-novel-yaneura-no-nishojo). Nobuko Yoshiya, *Yaneura no nishojo* (Tokyo: Kokusho Kankōkai, 2003). Like *Hana monogatari*, *Yaneura no nishojo* has never had an official English translation.

Although it also features a relationship between young women, *Yaneura
no nishojo* has some significant differences from the stories of *Hana
monogatari*. First, it is a novel (Yoshiya’s first), not a short
story. Yoshiya apparently wrote it as a personal work rather than a
commercial endeavor, an outlet for topics and themes that she wanted
to write about but could not in girls’ magazines. Like many first
novels, it is semi-autobiographical, based on her time attending a
teacher’s school after graduating from high school.

The most important setting in the novel (the “attic” of the title) is
not a girls’ school but a women’s dormitory run by the “Young Women’s
Association,” a thinly-veiled reference to the Young Women’s Christian
Association of Japan.[^01-03-14] Although it was originally founded in
1905 as one element in the many overseas Christian missionary
initiatives, the YWCA of Japan had less emphasis on evangelism and
more on outreach to middle-class women, especially those in
postsecondary education. One of the principal activities of the Tokyo
YWCA was to establish hostels to house the young women who were coming
to Tokyo to attend school, with the first two hostels opening in 1908
and admitting both Christian and non-Christian residents.[^01-03-15]

[^01-03-14]: Suzuki, *Becoming Modern Women*, 43.

[^01-03-15]: Margaret Prang, *A Heart at Leisure from Itself: Caroline Macdonald of Japan* (Vancouver: UBC Press, 1995), 41--42, 61.

Yoshiya stayed in one of these hostels (or another one built
subsequently) while studying to become a kindergarten teacher and it
became the model for the dormitory in *Yaneura no nishojo*. She
further distanced the setting from a Christian one by dropping the “C”
from YWCA, and having the two main characters (Akiko and Akitsu)
reject Christianity. Michiko Suzuki suggests that *Yaneura no nishojo*
sets same-sex love in direct opposition to this religion: “Akiko must
overcome Christian teachings and its practice in the dormitory in
order to acknowledge her true self.”[^01-03-16] If so, this is
reminiscent of Symonds’s and Carpenter’s attempts to do an end run
around the Christian proscription of homosexuality and justify its
practice in terms of an alternative morality.[^01-03-17]

[^01-03-16]: Suzuki, *Becoming Modern Women*, 46.

[^01-03-17]: In this regard it’s worth noting that, unlike some of their contemporaries, both Symonds and Carpenter promoted a vision of homosexuality as encompassing not only spiritual but also physical relationships. Quinn and Brooke, “‘Affection in Education’: Edward Carpenter,” 259--60.

The pairing in *Yaneura no nishojo* is also reminiscent of Carpenter’s
writing promoting relationships between younger boys and older boys or
men, and Yoshiya’s own gloss on that work. In real life Yoshiya was
already an adult when she moved into the YWCA dorm and began a
relationship with Yukie Kikuchi. However, Akiko, the character in
*Yaneura no nishojo* modeled on herself, is depicted as being of
indeterminate age and relatively childish compared to Akitsu, the
character modeled on Kikuchi: “an immature character, constantly in
tears, melancholic, and nostalgic for the past. ... [Akiko] is the
classic figure of the younger girl who adores her older lover from
afar.”[^01-03-18]

[^01-03-18]: Suzuki, *Becoming Modern Women*, 44--45.

The comparison with earlier stories only goes so far: unlike the
characters in *Hana monogatari*, Akiko and Akitsu continue their
relationship past the end of the story, with at least the possibility
raised that they will share their life from then on.[^01-03-19]
Yoshiya herself broke off her relationship with Kikuchi, but found her
own life partner only a few years later when she met Chiyo Monma.

[^01-03-19]: Again, this has parallels in Symonds and especially Carpenter, who “sought to present male love as ‘unswerving devotion and life-long union,’” in contrast to the Greek model in which such relationships were, like S relationships, inherently time-limited. Quinn and Brooke, “‘Affection in Education’: Edward Carpenter,” 260.

Yoshiya and Monma’s life together was in some ways characterized by
the same age dynamic found in *Yaneura no nishojo* and *Hana
monogatari*: Yoshiya was three years older than Monma, and in their
letters to each other Monma addressed Yoshiya as “elder sister.”
Yoshiya also famously formalized their relationship by adopting Monma,
in effect making Monma her daughter in the eyes of the law. But this
did not necessarily mean that the two did not conceive of each other
as equal partners in love: their expressed desire to each other was to
enter into a marriage, and Yoshiya apparently adopted the scheme of
adoption only after it became clear in the postwar period that there
was no possibility of Japanese law being changed to allow them to
marry.[^01-03-20]

[^01-03-20]: Robertson, “Yoshiya Nobuko: Out and Outspoken,” 201--3.

After she met Monma, Yoshiya had one final burst of writing on the
subject of same-sex love, in the magazine *Kuroshōbi* (*Black Rose*)
that she independently published from January to August 1925. In
particular, the story “Aru orokashiki mono no hanashi” (“A Tale of a
Certain Foolish Person”) seems to be a dark companion of *Yaneura no
nishojo*, as a relationship between two women ends with one of them
killed by a man in a shocking act of violence, implied to be the
result of her turning away from her partner to pursue a conventional
marriage.[^01-03-21]

[^01-03-21]: Suzuki, *Becoming Modern Women*, 54--59.

The next year Yoshiya and Monma established a household
together. Yoshiya began writing for women’s magazines “to establish a
literary niche (apart from girls’ fiction) and to secure a solid,
broad readership amid the shifting political and social landscape of
the late 1920s and 1930s.” Her new strategy was to deemphasize the
idea of romantic relationships between girls or women in favor of
“[representing] adult same-sex love not as an *alternative* to
heterosexuality but as a kind of sisterhood, an integral part of
female identity that *complements* heterosexuality.”  This allowed her
characters to avoid the fate of the typical Class S protagonist,
instead experiencing “female same-sex love ... as an intense
friendship that endures despite (or because of) experiences of
marriage and motherhood.”[^01-03-22]

[^01-03-22]: Suzuki, *Becoming Modern Women*, 60. Italics in the original.

Yoshiya lived the last years of her life with Monma in the coastal
city of Kamakura---the city in which the main action of *Sweet Blue
Flowers* is set. In an afterword, Takako Shimura describes a trip she
and her editor took to Kamakura to get reference photographs on which
to model various buildings portrayed in the manga. They try to visit
Yoshiya’s house, now a museum, but unfortunately find it
closed (*SBF*, 1:190).

Nobuko Yoshiya died on July 11, 1973, with Chiyo Monma at her
side---coincidentally, only three months before Takako Shimura was
born. The Class S literature that she had helped pioneer over half a
century earlier had by that time been succeeded by a new type of
literature for girls, as discussed in the next chapter.
