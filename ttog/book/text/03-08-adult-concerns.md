---
title: "Adult Concerns"
---

## Adult Concerns

I earlier speculated that Shimura was promoting a “new model of yuri
based on individualism and equality,” and noted that Fumi was neither
the only nor the first example of this in the world of *Sweet Blue
Flowers*. I was thinking specifically of Hinako Yamashina and Orie
Ono.
{:.first}

Orie and Hinako (as they were introduced initially) first showed up at
the very end of volume 1, as junior students to Shinako Sugimoto who
eventually fell in love with each other (*SBF*, 1:379--81). I wrote in
a previous chapter that “Orie’s and Hinako’s appears to be a
relationship of equals” and speculated that they might “break out of
the straitjacket” imposed by traditional Class S and yuri tropes.

That speculation is confirmed in the second half of volume 2, as Orie
and Hinako re-enter the story as full-fledged adults (with family
names to boot). Hinako Yamashina is the homeroom teacher for Akira’s
second-year class, and Orie Ono is the elder sister of first-year
student (and friend to Akira and Fumi) Haruka Ono. Their
(re)appearance is significant for multiple reasons:

First, Orie and Hinako represent a group that in classic schoolgirl
yuri tales is not supposed to exist, namely adult women who have
relationships with other adult women after graduation. Their very
existence puts the lie to the idea that “it’s just a phase” and that
in the end, every girl-turned-woman must conform to the prevailing
heterosexual ideal.

Second, Hinako is a potential role model for students attracted to
other girls, reassuring them that they are not alone or abnormal. This
is highlighted in the “Orie and Hinako” segment in the middle of
volume 2, in which the student Kawakubo (only her family name is
given) talks with Hinako and complains about her parents thinking her
“sick” for liking girls. After hearing her out, Hinako affirms that
she too “liked a girl” (leaving unsaid the fact that she still does)
(*SBF*, 2:165--70).

It’s worth stopping for a moment here to re-emphasize a point I’ve
made previously about *Sweet Blue Flowers*. Although Kawakubo tries
her best to get Hinako to reciprocate her feelings, Hinako shuts her
down cold: “I don’t want a student as my girlfriend.” Her response
echoes Mr. Kagami’s previous rejection of Yasuko’s advances. Their
actions seem to me to reflect more than just caution on the part of
Hinako and Mr. Kagami, or a feeling on their part that student-teacher
relationships are unhealthy.

To my mind, it’s consistent with the repeated framing in *Sweet Blue
Flowers* of equal relationships as superior to relationships marked by
age, status, and power differentials---a clear rejection of the idea
promoted by both Nobuko Yoshiya and *Maria Watches Over Us*, that a
relationship with an older girl (or a teacher, as Yoshiya adds) is
essential to a girl’s proper socialization and emotional development.

Finally, the fact that Hinako and Orie are adults with adult concerns
introduces a note of realism into a genre often marked by fluffy
fantasies. As Akira listens to her fellow students talk, it’s clear
that she’s worried about the consequences of reciprocating Fumi’s
feelings (*SBF*, 2:204--8). But the stakes are much higher for Orie
and Hinako.

Orie’s refusal to marry has caused conflicts with her parents and
heartbreak for her mother (*SBF*, 2:329). Hinako is presumably at risk
of losing her job if the rumors circulating among the students were to
come to the attention of the school administration and were then to be
confirmed. In this context, Hinako’s admission to Kawakubo, even if
couched in the past tense, is especially risky. What if, after being
rejected, Kuwakubo were to turn against Hinako and seek revenge?

The presence of Hinako and Orie does not make *Sweet Blues Flowers* an
adult yuri work. The primary focus is still on the budding
relationship between Fumi and Akira. However, I think it’s fair to say
that although *Sweet Blue Flowers* is still an example of “schoolgirl
yuri,” it’s schoolgirl yuri that is increasingly concerned with issues
that can only be called adult.
