---
title: "Equally Jealous"
---

## Equally Jealous

In the previous chapter, I wondered why Takako Shimura ended *Sweet
Blue Flowers* where she did and concluded that the story had come to a
natural stopping point, but the ending was somewhat rushed. One
unfortunate consequence of that rush is the somewhat muddled
resolution of Akira’s relationship with Fumi.
{:first}

After their graduation from high school, Fumi and Akira had come to a
point where Akira felt she could no longer continue their relationship
(*SBF*, 4:318--20, 4:323). At that point, the story could go in
several directions: Akira and Fumi could remain apart, going their
separate ways through life without further contact; they could reach
an accommodation in which Fumi found someone else to be her partner,
and Akira continued as Fumi’s close friend; or they could get back
together and try to restart their relationship as a couple.

If we perceive Fumi as the main protagonist of *Sweet Blue Flowers*
and the manga itself as a realistic fiction, then either of the first
two scenarios makes the most sense. After her conversation with Hinako
and Pon’s writing a play based on her experiences, Fumi is coming into
her own as a self-identified lesbian. The logical next step would be
to begin a new relationship with another woman equally
self-identified. In particular, Fumi could meet someone else while
attending university---and, in fact, the manga nods in that direction
by introducing her new friend Hanae Mori (*SBF*, 4:324--26).

As it turns out, though, Fumi’s friendship with Hanae is simply a
friendship and not a prelude to a relationship, as Hanae has a
boyfriend (*SBF*, 4:333). But before we as readers find that out, we
watch with Akira as Fumi and Hanae talk and laugh together, and Akira
grows jealous that Fumi has seemingly found someone else (4:326--27).

Akira’s jealousy drives the final section of the plot. But why
jealousy? It seems somewhat of a *deus ex machina*, a rather clichéd
and clumsy way to motivate Akira to overcome her indecisiveness and
hesitation regarding Fumi’s love of her---and to do so in as low a
page count as possible.

However clumsy though it might be, I think one can make a case for why
this development makes sense in the context of the story and themes of
*Sweet Blue Flowers*. Earlier plot points have also partially
foreshadowed Akira’s jealousy and its role in bringing her and Fumi
together.

Start with the assumption that one of the core themes of the manga is
the valorization of equality in relationships, as I’ve argued in
previous chapters. Now consider that the relationship between Fumi and
Akira as it’s developed over the course of the manga is manifestly
unequal. Fumi has strong feelings of romantic love and sexual
attraction towards Akira that Akira herself does not and---from her
perspective---cannot reciprocate: “My brain and my body just aren’t in
sync” (*SBF*, 4:319).

How might Fumi’s and Akira’s relationship---and in particular, the
emotions they bring to it---become more equal? Here it’s worth noting
two points, one about Akira and one about Fumi. Although Akira is not
strongly romantically or (especially) sexually attracted to Fumi, she
is by no means emotionless. As discussed in a previous chapter, Akira
is perfectly able to experience feelings of happiness and friendship
and also experience strong negative emotions like anger and
irritation.

As for Fumi, she’s also capable of experiencing strong negative
emotions, with the most notable being jealousy. Her anger at Chizu’s
marriage seems clearly connected to her jealousy towards Chizu’s
husband, for whom Chizu abandoned her (at least in Fumi’s view). And,
of course, Fumi’s jealousy regarding Yasuko’s feelings toward
Mr. Kagami was a major factor, perhaps *the* major factor, in their
breaking up. Even as she (rightly) dismisses the idea that Akira and
Ko might be dating, she gives herself a headache thinking about it:
“When I get jealous, my temples hurt” (*SBF*, 2:156).

It’s therefore thematically appropriate, if not fully motivated by the
manga, that Akira should be provoked into jealousy herself at the
sight of Fumi’s apparent ease and friendliness with another woman---a
woman who (unlike Fumi’s high school friends) is unknown to Akira and
thus (in Akira’s mind) might be Fumi’s new lover.

While at this point in the story Akira’s emotions may not fully match
Fumi’s in all respects, in her jealousy she feels an emotion that in
its nature and intensity equals any that Fumi has displayed. In a
sense, Fumi’s dream (discussed in a previous chapter) has been
fulfilled: Akira has moved beyond her previous confusion and
hesitation regarding Fumi and let her emotions out. And now that
jealousy has so overwhelmed her, Akira looks into her heart to explore
what this implies about her evolving feelings toward Fumi.
