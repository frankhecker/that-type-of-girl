---
title: "Ko and Kyoko"
---

## Ko and Kyoko

Though they didn’t name her, one reviewer of volume 1 questioned Kyoko
Ikumi’s place in the overall plot: “There is also a character whose
importance to the story fluctuates in such a way that it’s unclear
whether they are a side character or if they are a fourth, but
underdeveloped, lead.”[^03-02-01]
{:.first}

[^03-02-01]: Alex Cline, review of *Sweet Blue Flowers*, vol. 1, by Takako Shimura, Adventures in Poor Taste, October 19, 2017, [https://&#x200B;aiptcomics&#x200B;.com&#x200B;/2017&#x200B;/10&#x200B;/19&#x200B;/sweet&#x200B;-blue&#x200B;-flowers&#x200B;-vol&#x200B;-1&#x200B;-review](https://aiptcomics.com/2017/10/19/sweet-blue-flowers-vol-1-review).

My take is that Kyoko is indeed intended as a lead character and
arguably is more important to the overall story and its themes than
Yasuko Sugimoto. First, it’s clear that Kyoko is Akira’s closest
friend other than Fumi, and given her status as Akira’s same-year
classmate at Fujigaya, that’s likely to remain the case.  Kyoko has
also grown closer to Fumi after volume 1 ended with them dissolving
into tears together over their respective infatuations with Yasuko
(*SBF*, 1:374--76).

Thematically Kyoko and Ko’s relationship somewhat mirrors that of
Akira and Fumi, beginning in childhood and marked by one depending on
the other. However, the situations are subtly different, in a way that
echoes traditional gender roles: Fumi’s love for Akira was first
sparked by Akira’s helping her, but Ko’s love seems to date to his
helping Kyoko.

But there are more differences beyond that. The age difference between
Ko and Kyoko introduces a note of inequality into their relationship,
unlike the inherent equality between Akira and Fumi. (Kyoko is
nominally the person who has the power to say yes or no to the
relationship, with Ko wanting it more than she does. However, this is
just an example of the traditional “man the seeker, woman the sought”
dynamic, and it remains unclear exactly how much freedom of action
Kyoko has.)

Even more significant is that Ko and Kyoko’s relationship is
ultimately rooted in familial and societal expectations: that they
will be married and that their marriage will cement an alliance
between families. Ko’s mother’s concerns about his relationship with
Kyoko emphasize this: “It looks bad for someone like that to join the
family.” She may feel sorry for Kyoko herself, but from her point of
view, Kyoko’s mother’s (ambiguously described) condition and its
possible reoccurrence in Kyoko and her children threaten the future
of the Sawanoi family (*SBF*, 2:49--51).

In contrast, if Akira and Fumi eventually enter into a relationship,
they will be doing so as equal individuals, acting on their own in
defiance of social norms and the possible disapproval of their
families or even their friends.

It’s clear where Takako Shimura’s sympathies lie: Despite the
increased focus on Kyoko, Akira and Fumi are the “stars” of *Sweet
Blue Flowers*, and everything we’ve seen to date, including Fumi’s
rejection of Yasuko, indicates that the manga valorizes equality and
individuality in relationships. From this point of view, Kyoko and Ko,
like Fujigaya, represent the past, with Fumi and Akira representing,
if not the future, at least the promise of it.
