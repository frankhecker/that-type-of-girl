---
title: "Lizzy-chan and Darcy-san"
---

## Lizzy-chan and Darcy-san

It seems to be obligatory for reviewers of translated manga to comment
on the quality of the translation. Since I don’t speak or read
Japanese, my opinions on the English translation of *Aoi hana* are
next to worthless. However, it *is* worth noting that compared to
other manga I’ve read, the VIZ Media edition of *Sweet Blue Flowers*
is very westernized: signage and sound effects are converted to
English only (as opposed to leaving them as is and adding a
translation), and except for some food names the characters don’t use
any Japanese vocabulary or expressions.
{:.first}

That aspect of the translation shows up most strongly in the complete
absence of honorifics. The English subtitles for the anime adaptation
of *Sweet Blue Flowers* use “A-chan” and “Fumi-chan,” “Manjome-san”
and “Sugimoto-senpai,” but here it’s just “Akira,” “Fumi,” “Manjome,”
and so on.

Manga critic and translator Rachel Thorn has expressed her opposition
to indiscriminately retaining honorifics in English translations, for
example, in volume 1 of her translation of Takako Shimura’s *Wandering
Son*: “Retention of Japanese honorifics without good reason seems to
me to be an affectation intended to make self-described *otaku* feel
part of an exclusive club ....”[^01-08-01] I’ve always thought keeping
honorifics in translations was pretty harmless, but I’m beginning to
see her point.

[^01-08-01]: Rachel Thorn, “Snips and Snails, Sugar and Spice: A Guide to Japanese Honorifics as Used in *Wandering Son*,” in Shimura, *Wandering Son*, 1:iii.

Suppose as a fan your concern is that the translation show the
subtleties of interpersonal relationships. In that case, there’s a
perfectly good way to do that in English, one that’s quite familiar to
anyone who’s read a lot of eighteenth and nineteenth-century English
literature. For example, Jane Austen’s *Pride and Prejudice* contains
English equivalents for almost all typical usages of Japanese
honorifics (*-senpai* being the major exception): Mr. Darcy is almost
always “Mr. Darcy,” is plain “Darcy” to his friend Mr. Bingley, and is
never referred to by his given name “Fitzwilliam.” (We do not learn it
until almost halfway through the novel, when Elizabeth Bennet’s aunt,
Mrs. Gardiner, mentions it.)[^01-08-02]

[^01-08-02]: Jane Austen, *Pride and Prejudice* (London: 1813; Project Gutenberg, 2013), vol. 1, chap. 3, vol. 2, chap. 2, [https://&#x200B;gutenberg&#x200B;.org&#x200B;/ebooks&#x200B;/42671](https://gutenberg.org/ebooks/42671).

Similarly, Elizabeth Bennet is at different times and to different
people “Miss Bennet,” “Miss Elizabeth Bennet,” “Miss Elizabeth” (used
by the pompous Mr. Collins, who presumes too much), “Miss Eliza
Bennet” (used condescendingly by Miss Bingley, Elizabeth’s more
well-born rival), and (to her friend Charlotte and her sister Jane)
“dear Eliza” or “dearest Lizzy” (similar to “A-chan” for Akira). It is
a mark of his affection and the deepening of their relationship that
at the conclusion of the novel Darcy addresses her simply as
“Elizabeth.” However, for her he is “Mr. Darcy” to the end.[^01-08-03]

[^01-08-03]: Austen, *Pride and Prejudice*, vol. 1, chap. 10, vol. 1, chap. 6, vol. 1, chap. 18, vol. 1, chap. 8, vol. 1, chap. 22, vol. 1, chap. 6, vol. 3, chap. 16.

Why don’t manga translators use this strategy? Probably because fans
accustomed to ubiquitous informality in modern English usage would
find such language incredibly stilted. They don’t want their manga to
read like a Regency novel, instead preferring the satisfaction of
knowing the difference between *-san* and *-sama*. The John Werry
translation of *Sweet Blue Flowers* does have characters address each
other either by given name or family name to distinguish degrees of
formality, and Mr. Kagami is always “Mr. Kagami.” But otherwise it’s
directed to the non-*otaku* reader.

This translator’s choice also bears on what I believe to be one of the
themes of *Sweet Blue Flowers*: Western fans of anime and manga happen
to live in societies that are relatively unmarked by hierarchies of
age, class, gender, and so on---at least by historical standards. They
do not know what it is like to live day-to-day in a society in which
the very language makes explicit these hierarchies.

For example, Rachel Thorn notes that Japanese people will query
others if needed to ascertain which of them is older, so that they may
adopt the correct mode of speech. “There are no ‘brothers’ or
‘sisters’ in Japan: only ‘older brothers,’ ‘younger brothers,’ ‘older
sisters,’ and ‘younger sisters.’ Even in the case of twins, one is
arbitrarily defined as the ‘elder.’”[^01-08-04]

[^01-08-04]: Thorn, “Snips and Snails, Sugar and Spice,” 1:iii.

I wonder how many Western fans of anime and manga would truly want to
live in a society organized by age and seniority---just as, for
example, I wonder how many present-day readers of Jane Austen would
want to live in a society organized by class, with everyone expected
to abide by a strict code of expression in speaking to their
“inferiors” and deferring to their “betters.”

Moreover, the use of honorifics is not always a harmless exoticism (as
Western fans might view it) or a mark of respect (as people in Japan
or other Asian countries might consider it). Often they serve not just
to mark hierarchies but reinforce them. In Yasujirō Ozu’s film *Late
Autumn*, a young woman confronts three middle-aged men she accuses of
meddling in the affairs of her friend’s mother. They invite her to
sit, but she insists on remaining standing. One of them tries to
regain the upper hand by addressing her as “Yuri-chan,” but she
angrily retorts, “Call me Yuriko.”[^01-08-05]

[^01-08-05]: *Late Autumn*, directed by Yasujirō Ozu, in *Eclipse Series 3: Late Ozu (Early Spring / Tokyo Twilight / Equinox Flower / Late Autumn / The End of Summer)* (1958; New York: Criterion Collection, 2007), 1:41:24, DVD.

How does *Sweet Blue Flowers* view such hierarchies? I’ll have more
to say about this further on, but I think this particular feature of
the English translation provides a hint.
