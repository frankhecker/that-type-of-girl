---
title: "Yuri After Sweet Blue Flowers"
---

## Yuri After *Sweet Blue Flowers*

It’s now been eighteen years since Takako Shimura first began
serialization of *Aoi hana* in *Manga Erotics F* in 2004, ten years
since its first volume was released in English as *Sweet Blue Flowers*
in 2012, and nine years since Shimura completed its serialization
in 2013. In this chapter I look at how the yuri genre has evolved
since that time and pronounce my final judgment on *Sweet Blue
Flowers*.
{:first}

The yuri genre is still relatively insignificant in the grand scheme
of things, with yuri manga series making up a tiny percentage of the
overall manga market. In comparison, the BL genre featuring
relationships between men has several times more titles.[^06-03-01]
However, the years since *Aoi hana* ended serialization have seen a
significant expansion of the yuri genre. More than nine out of ten
yuri series began publication after the year *Aoi hana* began
serialization, and more than half of them began publication after *Aoi
hana* concluded.[^06-03-02]

[^06-03-01]: “Browse manga,” Anime Planet, accessed February 2, 2022, [https://&#x200B;www&#x200B;.anime&#x200B;-planet&#x200B;.com&#x200B;/manga&#x200B;/all](https://www.anime-planet.com/manga/all). A search for manga in general returns 1,336 pages of results after excluding works tagged as *manwha* and *manhua* (Korean and Chinese comics respectively), “OEL” (Western comics drawn in a manga style), webtoons, light novels, and web novels. Adding a search filter for works tagged “GL” returns 45 pages of results, about 3 percent of the total. (Anime Planet uses an older scheme devised by Western fans in which works are categorized as “yuri” only if they feature sexually explicit content.) Filtering for works tagged “BL” returns 269 pages of results, about 20 percent of the total and six times the number of “GL” results.

[^06-03-02]: “Browse manga,” Anime Planet. As of February 2, 2022, there were 1,564 manga series tagged as “GL” with a year of first publication listed. Of these, 1,432, or 92 percent, began publication in 2005 or later, and 875, or 56 percent, began publication in 2014 or later.

More and more of these yuri manga are being officially released in
English translation. Anime-focused streaming services like Crunchyroll
have expanded the audience for anime in the US and other countries
outside Japan. That has driven up Western readership for manga, the
source material for most anime, as the Western market for manga has
rebounded from a sales crash in the late 2000s. Yuri manga, in
particular, have also benefited from the increased visibility and
acceptance of LGBTQ individuals in popular culture and the increased
popularity of e-books, which makes publishing manga with niche appeal
a more attractive business proposition than traditional print
publication.

At the time of writing, almost the entire spectrum of manga publishers
is releasing yuri works in English. Such publishers include
mainstream Japanese-owned companies like VIZ Media (publisher of
*Sweet Blue Flowers*), independent US publishers like Seven Seas
Entertainment for whom LGBTQ-themed works form a significant part of
their output, and niche yuri-only imprints like Lilyka (part of
Digital Manga) that specialize in self-published works (*dōjinshi*)
that in the past would never have had an official English release.

Rather than attempt to provide a comprehensive overview of the current
yuri scene, I instead briefly discuss some representative works that
illustrate various themes in the evolution of yuri.

First up is *Kiss and White Lily for My Dearest Girl*, which carries
on the tradition of classic yuri tropes.[^06-03-03] Like many other
works in this vein, it is set in a “yuritopia,” an all-girls school in
a world where men are absent and every girl finds another girl (or, in
one case, more than one) to pair up with.

[^06-03-03]: Canno, *Kiss and White Lily for My Dearest Girl*, trans. Jocelyne Allen, 10 vols. (New York: Yen Press, 2013--19).

*Yuri is My Job!* (the first yuri manga from mainstream publisher
Kodansha USA) parodies yuri tropes while still following time-honored
conventions.[^06-03-04] Its characters spend their time after school
working in a yuri-themed café featuring the fictional Liebes Girls
Academy, in which, as in *Maria Watches Over Us*, older girls pick
younger girls to be their “schwestern” (the equivalent of *Maria*’s
*sœurs*). In keeping with the works that it’s parodying, *Yuri is My
Job!* teases the possibility of romantic relationships developing
between the girls while they’re “off the clock,” without (as of the
time of writing) committing to that being the case.

[^06-03-04]: Miman, *Yuri is My Job!*, trans. Diana Taylor, 8 vols. (New York: Kodansha, 2016--).

Next are two series that have broken out of the pack, achieved
widespread popularity, and had well-regarded anime adaptations.

*Bloom Into You*, which began publication in Japan (as *Yagate kimi ni
naru*) two years after *Aoi hana* ended its run, is in many ways a
successor work to *Sweet Blue Flowers*.[^06-03-05] It shares with the
earlier work common yuri tropes, including a pairing between a tall
beauty with long black hair and a shorter girl with shorter and
lighter-colored hair, who feels unable at first to experience
love. *Bloom Into You* also features elements more unique to *Sweet
Blue Flowers*, including a play used to shed light on the characters
and their emotions and a lesbian teacher and her partner who mentor
the main characters.

[^06-03-05]: Nio Nakatani, *Bloom Into You*, trans. Jenni McKeon, 8 vols. (Los Angeles: Seven Seas Entertainment, 2017--20).

However, *Bloom Into You* perpetuates the hierarchical
*senpai*-*kōhai* dynamic that (to my mind) *Sweet Blue Flowers*
rejects as the basis for a relationship between women. It also
relegates the one unequivocally lesbian girl (the closest equivalent
to Fumi) to the role of a side character whose love for her classmate,
one of the two main characters, goes unrequited. (Fortunately, that
side character is given her due as the lead character in a spinoff
series of light novels.)[^06-03-06]

[^06-03-06]: Hitoma Iruma, *Bloom Into You: Regarding Saeki Sayaka*, trans. Jan Cash and Vincent Castenada, 3 vols. (Los Angeles: Seven Seas Entertainment, 2019--20).

The two main characters in the *Kase-san* series (five volumes,
beginning with *Kase-san and Morning Glories*[^06-03-07]) differ in
appearance and temperament, like Fumi and Akira, but also like Fumi
and Akira, they meet each other as classmates and equals. The
follow-on series *Kase-san and Yamada* goes beyond *Sweet Blue
Flowers* to explore the main characters’ relationship beyond high
school in more depth.[^06-03-08]

[^06-03-07]: Hiromi Takashima, *Kase-san and Morning Glories*, trans. Jocelyne Allen (Los Angeles: Seven Seas Entertainment, 2017).

[^06-03-08]: Hiromi Takashima, *Kase-san and Yamada*, trans. Jocelyne Allen, 2 vols. (Los Angeles: Seven Seas Entertainment, 2020--).

All of the works discussed above are primarily focused on girls in
high school, as the vast majority of yuri works have been. However, in
recent years there have been a growing number of works featuring adult
couples as the central characters. Many of these are now seeing
official releases in English. One work worth noting is *The Conditions
of Paradise*, a story collection originally published in 2006 by
long-time yuri manga artist Akiko Morishima.[^06-03-09] It and its
follow-up volumes contain stories from *Comic Yuri Hime*, the most
prominent Japanese magazine devoted to the yuri genre.

[^06-03-09]: Akiko Morishima, *The Conditions of Paradise*, trans. Elina Ishikawa-Curran (Los Angeles: Seven Seas Entertainment, 2020).

Though they may focus on adults and not on teenagers, many adult yuri
manga follow *Bloom Into You*, *Kase-san*, and other schoolgirl yuri
manga in perpetuating the idea of yuri as “lesbian content without
lesbian identity.”[^06-03-10] They feature women who have emotional
and physical relationships with other women but do not apply the term
“lesbian” to themselves. (In *Sweet Blue Flowers* Fumi never goes
beyond describing herself circumspectly as “that type of girl,” but
the word “lesbian” *is* used in conversation, albeit only by side
characters (*SBF*, 1:307, 1:309, 3:357, 3:359).)

[^06-03-10]: Erica Friedman, “Overthinking Things 04032011.”

That is not true of the next two works I discuss, both of which escape
the boundaries of the yuri genre entirely. *My Lesbian Experience with
Loneliness* puts the issue of identity front and center in its title.
However, the “lesbian experience” as such forms just a part of the
work, most of which focuses on the author’s struggles with her mental
and physical health.[^06-03-11] In their honest and often emotionally
raw portrayal of the author’s life, the manga and its sequels resemble
the autobiographical and semi-autobiographical comics created by women
cartoonists in the West (some lesbian, some not), including works by
Alison Bechdel, Julie Doucet, and Phoebe Gloeckner.

[^06-03-11]: Kabi Nagata, *My Lesbian Experience with Loneliness*, trans. Jocelyne Allen (Los Angeles: Seven Seas Entertainment, 2017).

In contrast, issues of gender and sexual orientation are front and
center in *Our Dreams at Dusk: Shimanami Tasogare*, whose characters
are variously gay, lesbian, transgender, nonbinary (or “X-gender,” to
use the roughly equivalent Japanese term), asexual, aromantic, or
unsure of their identity. *Shimanami Tasogare* also foregrounds the
importance of finding and creating an LGBTQ community, something rare
to nonexistent in yuri works.[^06-03-12]

[^06-03-12]: Yuhki Kamatani, *Our Dreams at Dusk: Shimanami Tasogare*, trans. Jocelyne Allen, 4 vols. (Los Angeles: Seven Seas Entertainment, 2019).

This lack is especially felt in many adult yuri works. They either
recreate the unrealistic school-based “yuritopia” in work settings
populated almost solely by lesbians, as in the *Saturday* series of
*dōjinshi*,[^06-03-13] or find improbable ways to bring characters
together in a relationship, as in *I Married My Best Friend to Shut My
Parents Up*.[^06-03-14]

[^06-03-13]: Ruri Hazuki, *Saturday: Introduction* (Gardena CA: Lilyka, 2019).

[^06-03-14]: Naoko Kodama, *I Married My Best Friend to Shut My Parents Up*, trans. Amber Tamosaitis (Los Angeles: Seven Seas Entertainment, 2019).

Having found an audience in the West and publishers to serve that
audience, what’s next for the yuri genre? I have no power of prophecy,
so my best guess is a continuation of past and present trends relating
to (sub)genre, audience, format, and creators.

I do not doubt that fluffy tales of schoolgirls in love will continue
to be produced. But they will coexist with a growing number of adult
yuri works (including “office yuri” featuring workplace romances), as
well as yuri-flavored takes on *isekai* stories of fantasy worlds,
science fiction stories, mysteries, thrillers, horror tales, and so
on. They will likely be joined over time by more works based squarely
in the LGBTQ experience, works which may not be yuri as such but will
appeal to many readers in the all-genders audience driving
yuri’s growing popularity.

Here *Sweet Blue Flowers* proved prophetic, not so much in its subject
matter but in its publication in a magazine (*Manga Erotics F*) that
explicitly targeted such an all-genders adult audience. It was an
early indication that (as Nicki Bauman claims), “yuri is for
everyone,” and that the present audience for yuri, both in Japan and
elsewhere, contains people of all age groups, gender identities, and
sexual orientations.[^06-03-15]

[^06-03-15]: Nicki Bauman, “Yuri Is for Everyone: An Analysis of Yuri Demographics and Readership,” Anime Feminist, February 12, 2020, [https://&#x200B;www&#x200B;.animefeminist&#x200B;.com&#x200B;/yuri&#x200B;-is&#x200B;-for&#x200B;-everyone&#x200B;-an&#x200B;-analysis&#x200B;-of&#x200B;-yuri&#x200B;-demographics&#x200B;-and&#x200B;-readership](https://www.animefeminist.com/yuri-is-for-everyone-an-analysis-of-yuri-demographics-and-readership).

In this regard, the example of *Comic Yuri Hime* is instructive. The
magazine’s publisher attempted to split its audience in two by
spinning off a separate magazine *Comic Yuri Hime S* targeted at men,
only to end the experiment and fold it back into the main magazine
three years later. However, this was not simply a reversion to the
previous state. Instead, the language used to address readers of the
“new” *Comic Yuri Hime* signified a more inclusionary approach by the
editors, even when the nature of the content itself did not
change.[^06-03-16]

[^06-03-16]: Hannah E. Dahlberg-Dodd, “Script Variation as Audience Design: Imagining Readership and Community in Japanese Yuri Comics,” *Language in Society* 49, no. 3 (2020), 365--66, 372--74, [https://&#x200B;doi&#x200B;.org&#x200B;/10&#x200B;.1017&#x200B;/S0047404519000794](https://doi.org/10.1017/S0047404519000794).

In terms of format, I doubt we will see many actual yuri anime, as
opposed to anime in which yuri relationships are present only as
subtext. Even with the low pay of animators, anime production is too
expensive for works for which the potential audience is still
relatively small. Instead, the action will be in formats for which
both the costs of creation and the costs of distribution are
relatively low: besides digital versions of manga, we are seeing
yuri---and will see more---in the form of light novels, “super light”
novels (designed to be easily read on smartphones), webcomics, and
visual novels.

Finally, just as manga aesthetics and themes have influenced
non-Japanese comic artists around the world, there is a growing body
of yuri-influenced works---including webcomics, in
particular---created outside Japan and marketed using the “GL”
label. This is especially true in East Asia, with popular works
created in South Korea, Thailand, China, and the
Philippines.[^06-03-17]

[^06-03-17]: For example, an Anime Planet search using the tags “GL” and “webtoons” returns well over two hundred works, almost all created in the last six years and nearly all created and published outside Japan. “Browse manga,” Anime Planet.

What is the place of *Sweet Blue Flowers* in all this? As I wrote in
the introduction, I think it, like its characters, tries to span the
past, present, and future of yuri in a single work. Straddling like
this can be awkward, and sometimes *Sweet Blue Flowers* wobbles more
than a bit.

For example, I think that the Class S-like environment of Fujigaya
coexists uneasily with the more modern sensibility exemplified by Fumi
and some other characters. I also believe the resolution of the
relationship between Fumi and Akira (and the implication that since
childhood they were fated to be together) works better as a symbolic
reconciliation of two modes of yuri than it does as a picture of
real-life individuals.

However, to me the virtues of *Sweet Blue Flowers* greatly outweigh
its flaws. These virtues include the sensitive portrayal of its three
main characters (not forgetting Kyoko here), the deftly-handled
narrative of Fumi coming out as a lesbian, and the rejection of
hierarchy and emphasis on equality in relationships that I see running
as a thread throughout the entire series.

In the end, I’ll agree with Erica Friedman that *Sweet Blue Flowers*
was “an important stepping stone to where we are now” and that “it’s
time to move forward into a genre that has matured.”[^06-03-18] My
journey with *Sweet Blue Flowers* ends here. But Takako Shimura is
taking her next step (at least as far as readers in the West are
concerned) with the release in English of her adult yuri manga *Even
Though We’re Adults*. It marks a welcome return by Shimura to the
genre that she helped bring into the twenty-first century.

[^06-03-18]: Erica Friedman, review of *Sweet Blue Flowers* vol. 4, by Takako Shimura, *Okazu* (blog), July 9, 2018, [https://&#x200B;okazu&#x200B;.yuricon&#x200B;.com&#x200B;/2018&#x200B;/07&#x200B;/09&#x200B;/yuri&#x200B;-manga&#x200B;-sweet&#x200B;-blue&#x200B;-flower&#x200B;-volume&#x200B;-4&#x200B;-english](https://okazu.yuricon.com/2018/07/09/yuri-manga-sweet-blue-flower-volume-4-english).
