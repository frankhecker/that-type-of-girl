---
title: "Appendix 5: Chapter Titles"
---

## Appendix 5: Chapter Titles

Many if not most of the titles of the chapters of *Sweet Blue Flowers*
are taken from, or allude to, various novels, poems, songs, and
films. Some of these are identified in the translation notes to the
VIZ Media edition. This appendix provides sources (or possible
sources) for almost all of the titles; note that some of these
attributions are speculative to one degree or another. The Japanese
titles of the chapters are included in parentheses.
{:.first}

### Volume 1 {#appendix-5-chapter-titles-volume-1}

Chapter 1, “Flower Story” (“*Hana monogatari*”). From Yoshiya Nobuko’s
1924 collection of Class S stories (the title of which is usually
translated as *Flower Tales*).

Chapter 2, “Stand by Me” (“*Sutando bai mī*”). From the 1986 film
directed by Rob Reiner or the 1961 song by Ben E. King used as the
theme song for the film.

Chapter 3, “Spring Breeze” (“*Haru no arashi*”). From the Japanese
title of the 1910 novel *Gertrude* by Hermann Hesse.

Chapter 4, “Waking Up in the Morning” (“*Asa mezamete wa*”). From the
Japanese title of the poem “*Morgens steh’ ich auf und frage*” by
Heinrich Heine.

Chapter 5, “Secret Flower Garden” (“*Himitsu no hanazono*”). From the
Japanese title of the 1911 novel *The Secret Garden* by Frances
Hodgson Burnett.

Chapter 6, “Beautiful Youth” (“*Seishun wa uruwashi*”). From the
Japanese title of the 1916 novella *Schön ist die Jugend* by Hermann
Hesse.

Chapters 7--9, “*Wuthering Heights*, Parts 1--3”
(“*Arashigaoka*”). From the 1847 novel by Emily Brontë, from which the
Fujigaya play is adapted.

Chapter 10, “Young Leaves” (“*Wakaba no koro*”). From the 1996
television drama and the Japanese title of its theme song, the 1969
Bee Gees song “First of May.”

Chapter 11, “A New Day” (“*Atarashiki hi*”). Possibly a reference to
the Japanese title of *La Nouvelle Journée*, the tenth and final
volume of the 1904--12 novel *Jean-Christophe* by Romain Rolland.

Chapter 12, “Don’t Say Goodbye” (“*Sayonara wa iwanaide*”). From the
1986 song by Sonoko Kawai (the B-side of her number-one hit “*Aoi
stasion*”), the Japanese title of the 1971 song “Never Can Say
Goodbye” by the Jackson 5, or (less likely) the Japanese title of the
1986 film *Every Time We Say Goodbye*, directed by Moshé Mizrahi.

Chapter 13, “Love Is Blindness” (“*Koi wa mōmoku*”). Possibly from the
Japanese title of the 1976 song “Love Is Blind” by Janis Ian.

### Volume 2 {#appendix-5-chapter-titles-volume-2}

Chapters 14--15, “A Midsummer Night’s Dream, Parts 1 and 2”
(“*Natsu no yo no yume*”). From the play by William Shakespeare.

Chapters 16--17, “The Happy Prince, Parts 1 and 2” (“*Kōfuku no
ōji*”). From the story in the 1888 collection *The Happy Prince and
Other Tales* by Oscar Wilde.

Chapter 18, “Winter Fireworks” (“*Fuyu no hanabi*”). From the 1946 play
by Osamu Dazai.

Chapter 19, “The Bells of Spring” (“*Haru no kane*”). From the 1985
film directed by Koreyoshi Kurahara.

Chapter 20, “Tsujigahana.” From the 1972 film directed by Noboru
Nakamura.

Chapters 21--24, “Rokumeikan, Parts 1--4.” From the 1956 play by Yukio
Mishima, performed by the Fujigaya drama club.

Chapter 25, “Faster than Love” (“*Ai yori hayaku*”). Possibly from the
1998 novel by Ayako Saitō.

### Volume 3 {#appendix-5-chapter-titles-volume-3}

Chapters 26--29, “Rokumeikan, Parts 5--8.” See above.

Chapter 30, “After the Banquet” (“*Utage no ato*”). From the 1960
novel by Yukio Mishima.

Chapters 31--33, “The Door into Summer, Parts 1--3” (“*Natsu e no
tobira*”). From the 1957 novel by Robert Heinlein.

Chapter 34, “As You Like It” (“*Oki ni mesu mama*”). From the play by
William Shakespeare.

Chapter 35, “A Planet in Love” (“*Koi suru wakusei*”). From the
Japanese title of the 1994 film *Chungking Express*, directed by Wong
Kar-wai.

Chapter 36, “First Love” (“*Hatsukoi*”). Possibly from the 1860
novella *Pervaya lyubov* by Ivan Turgenev.

Chapter 37, “The Lily of the Valley” (“*Tanima no yuri*”). From the
1835 novel *Le Lys dans la vallée* by Honoré de Balzac.

Chapter 38, “A Christmas Carol” (“*Kurisumasu kyaroru*”). From the
1843 novella by Charles Dickens.

### Volume 4 {#appendix-5-chapter-titles-volume-4}

Chapter 39, “A Little Princess” (“*Shōkōjo*”). From the 1905 novel by
Frances Hodgson Burnett.

Chapters 40--41, “Melody, Parts 1 and 2” (“*Chīsana koi no
merodī*”). From the Japanese title of the 1971 film *Melody*, directed
by Waris Hussein.

Chapter 42, “Froth on the Daydream” (“*Hibi no awa*”). From the 1947
novel *L’Écume des jours* by Boris Vian. The novel was later adapted
into the 2001 film *Chloe*, directed by Gō Rijū.

Chapter 43, “The Best Little Girl in the World” (“*Kagami no naka no
shōjo*”). From the 1978 novel by Steven Levenkron and the subsequent
1981 film adaptation, directed by Sam O’Steen. The Japanese chapter
title (lit. “the girl in the mirror”) is the Japanese title of the
film.

Chapter 44, “On a Spring Night” (“*Haru no yoru ni*”). Probably not a
direct reference, but this chapter shares a title with a song sung by
Aoi Teshima in the 2006 film *Tales from Earthsea* (*Gedo senki*),
directed by Gorō Miyazaki.

Chapter 45, “The Three Musketeers” (“*San jūshi*”). From the novel by
Alexandre Dumas, the basis of the play performed by the Fujigaya drama
club.

Chapter 46, “Heavenly Creatures” (“*Otome no inori*”). From the 1994
film directed by Peter Jackson, released in Japan under the title
*Otome no inori* (lit. “maiden’s prayer”), which is also the Japanese
chapter title. Another possible source for the Japanese chapter title
is the song “Maiden’s Prayer” composed by Tekla Bądarzewska-Baranowska
in 1856 and recorded many times since then.

Chapter 47, “Crime and Punishment” (“*Tsumi to batsu*”). From the 1866
novel by Fyodor Dostoevsky, later adapted as a 1953 manga series by
Osamu Tezuka.

Chapter 48, “A Flower of This World” (“*Kono yo no hana*”). From the
1955 novel by Makoto Hōjō, the 1955 films based on the book, directed
by Toshimasa Hozumi, and the theme song of the films, sung by Chiyoko
Shimakura.

Chapter 49, “Unrequited Love” (“*Katakoi*”).  Possibly from the
Japanese title of the 1858 novella *Asya* by Ivan Turgenev.

Chapter 50, “Even Though I’m Waiting for You” (“*Kimi
matedomo*”). Possibly from the 1949 film directed by Noboru Nakamura
and the song of the same title featured in the film, sung by Aiko
Hirano. Another possible source is the 1974 television drama with the
same title, directed by Eizo Yamagiwa.

Chapter 51, “Wandering the World of the Seventh Sense” (“*Dainana
kankai hōkō*”). From the 1931 novella by Midori Osaki, also referenced
in the title of Sachi Hamano’s 1998 film about Osaki’s life and work,
*In Search of a Lost Writer: Wanderings in the Realm of the Seventh
Sense* (*Dainana kankai hōkō: Osaki Midori o sagashite*).

Chapter 52, “Sweet Blue Flowers” (“*Aoi hana*”). From the title of the
manga, the Japanese title of which may be taken from the Japanese
title of the 1802 novel *Heinrich von Ofterdingen* by Novalis.
