---
title: "Takarazuka Time"
---

## Takarazuka Time

It’s time for the cultural festival, that staple of manga and anime
set in high schools. However, the point of this festival isn’t the
festival per se, but the plays within it, put on by the Fujigaya drama
club.
{:.first}

The school play is a frequently appearing element in Takako Shimura’s
work. It serves as a source of, well, drama, but more specifically, is
used to put the characters into situations that allow them to imagine
themselves as they wish to be, or show how others perceive
them. Recall, for example, the performance of *Romeo and Juliet* in
*Wandering Son*, in which Shuichi envisions herself in (but does not
get) the part of Juliet.[^02-09-01]

[^02-09-01]: Shimura, *Wandering Son*, 6:20--24, 6:99--100.

Unlike Matsuoka (where the drama club consists only of Fumi’s three
friends), at Fujigaya the drama club is a long-standing institution
(“they take it very seriously,” Akira tells Fumi) that includes at
least a couple of dozen students. The three divisions of Fujigaya each
put on a production: *The Little Prince*, *Little Women*, and (for the
high school) *Wuthering Heights*. The first two are interesting if
only for their titles (“princes” and “little women” being a theme
here, as we’ll see), but the main focus is on *Wuthering Heights*
(*SBF*, 1:74).

The play, and the casting of Yasuko Sugimoto in the male role of
Heathcliff, evoke the Takarazuka Revue, an all-woman musical theater
group formed in the early twentieth century (the same period that saw
the creation and popularization of the Class S genre). To ensure
readers get the connection, Shimura has the characters twice
explicitly reference the Revue (*SBF*, 1:55, 1:241).[^02-09-02]

[^02-09-02]: Shimura also has an ongoing manga *Awashima hyakkei* (sometimes romanized as *Awajima hyakkei*) set in a girls’ school resembling the school that trains Takarazuka performers. (The title is a play on *Fugaku hyakkei*---better known in the West as *One Hundred Views of Mount Fuji*---Hokusai’s famous series of woodblock prints.) Unfortunately, *Awashima hyakkei* has not had an official release in English. Takako Shimura, *Awashima hyakkei*, 3 vols. (Tokyo: Ōta Shuppan, 2015--).

The image of the Takarazuka Revue in Japanese popular culture is
multi-faceted and shot through with ambiguity, with the Revue “the
focus of heated debates about the construction and performance of
gender.”[^02-09-03] It embodies a tension between the idea of women
performing a male role for entertainment and instruction and the idea
of women behaving in stereotypically masculine ways (including having
women as romantic and sexual partners) as part of their core identity.

[^02-09-03]: Jennifer Robertson, “The Politics of Androgyny in Japan: Sexuality and Subversion in the Theater and Beyond,” *American Ethnologist* 19, no. 3 (August 1992), 422, [https://&#x200B;doi&#x200B;.org&#x200B;/10&#x200B;.1525&#x200B;/ae&#x200B;.1992&#x200B;.19&#x200B;.3&#x200B;.02a00010](https://doi.org/10.1525/ae.1992.19.3.02a00010).

The Takarazuka Revue was founded as a corporate venture (to promote
tourism and sell railroad tickets) and remains such today. It was and
is motivated to conform to both popular ideals and government policy
dictating the proper roles of men and women in society. The theory was
that “by performing as men, females learned to understand and
appreciate males and the masculine psyche \[so that\] when they
eventually retired from the stage and married ... they would be better
able to perform as ‘good wives, wise mothers,’ knowing exactly what
their husbands expected of them.”[^02-09-04]

[^02-09-04]: Robertson, “The Politics of Androgyny in Japan,” 427.

However, at the same time, a large part of the attraction of
Takarazuka productions to their mainly female audience is the frisson
of seeing women act in non-feminine ways both socially and sexually:
“female fans of all ages, classes, and educational levels do not see a
man on stage, but rather *acknowledge* a female body performing in a
capacity that transgresses the boundaries of received
femininity.”[^02-09-05]

[^02-09-05]: Robertson, “The Politics of Androgyny in Japan,” 433. Italics in the original.

This history of tension and conflicting visions of women’s place
replays itself in *Sweet Blue Flowers*. As a traditional educational
institution, Fujigaya’s mission is to prepare women for their “proper”
place in Japanese society. The *Wuthering Heights* production and
other plays are intended as socially-approved entertainment and
instruction suitable for students and their parents. As such, they are
designed to minimize anything that hints of transgression.

That strategy is implemented in large part through productions that
are isolated in time and place from modern Japan, and thus avoid
direct commentary on contemporary Japanese society: *Wuthering
Heights* in late eighteenth and early nineteenth-century England,
*Little Women* in mid-nineteenth-century America, and *The Little
Prince* in a French-influenced science-fictional setting. (See also
the ubiquity of Takarazuka productions in foreign historical settings,
most notably *The Rose of Versailles*, set in late-eighteenth-century
France.)

But despite Fujigaya’s “official” intent regarding these all-girls
plays, unofficially they evoke similar responses to Takarazuka
productions: the students ooh and aah over the poster showing Yasuko
in a glamorous pose as Heathcliff, take and exchange photographs of
“Heathcliff” and “Catherine” in romantic poses, and speculate whether
they’ll kiss (*SBF*, 1:210--11).

Like the S relationships discussed in previous chapters, this aspect
of Fujigaya productions remains universally known and discussed but
never officially acknowledged, let alone accepted or endorsed.  Given
Shimura’s love of plays as devices to drive the plot and highlight
themes, we’ll undoubtedly see more of them in future volumes of *Sweet
Blue Flowers*. It will be interesting to see whether and how these
productions depart from the standard Takarazuka template.
