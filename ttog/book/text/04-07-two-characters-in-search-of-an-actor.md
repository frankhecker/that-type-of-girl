---
title: "Two Characters in Search of an Actor"
---

## Two Characters in Search of an Actor

I previously wrote about the patriarch of the Sugimoto clan, a man
notable for the lack of attention paid to him in *Sweet Blue Flowers*.
In my final commentary on the Fujigaya production of *Rokumeikan*, I
consider two other men notable for their absence in the manga, Hisao
and Count Kageyama.
{:.first}

Hisao at least rates a mention in *Sweet Blue Flowers*. In the
performance of *Rokumeikan*, Akiko (played by Akira) mentions that he
is in danger (though the manga omits the reason why). The girl playing
Hisao is subsequently called to the stage, after which the narration
explains that he is Asako’s estranged son (*SBF*, 3:94--95). But we
are left ignorant of who played Hisao and are not made privy to
anything he says.

With Count Kageyama, the erasure is (almost) complete. Like Hisao, his
actor is never identified, and we never hear his words. Unlike Hisao,
he is not referenced at all in the manga, either by his name or by his
role in the story.

As with Yasuko’s father, we may ask, why might this be? As in that
case, the most straightforward answer is that they are peripheral to
the story Shimura is telling. They are men, and *Sweet Blue Flowers*
is about girls becoming women, specifically women who love women. With
Hisao, we have the additional factor that he is explicitly identified
as Akiko’s lover. It would detract from the story of Akira and Fumi’s
tentatively blossoming love to have another girl play a love interest
opposite Akira.[^04-07-01]

[^04-07-01]: This would be especially true if the Fujigaya performance followed Mishima’s stage directions: after meeting Akiko at the Rokumeikan, “Hisao holds her in his arms and kisses her for a long time.” Mishima, *Rokumeikan*, 33.

But, as with Yasuko’s father, we can explore this absence further,
starting with Hisao. Hisao is a type familiar from Mishima’s other
works, his life, and Japanese history: the hotheaded young man, whose
discontents and violent tendencies alternately affront and are
exploited by the Japanese (male) establishment.

Hisao’s resentment at his father’s treatment of him first leads him to
contemplate assassinating Kiyohara. Persuaded to desist only by the
intervention of his newly-revealed mother, Asako, he lets himself be
goaded again into resentment and action by Kageyama’s words and
scheming---only to rebel against Kageyama as well by deliberately
mis-aiming his shot at Kiyohara, subsequently getting himself killed
by Kiyohara’s return fire.

I think the term “toxic masculinity” is overused, but if it applies to
anything, it applies to Hisao’s actions in this play. Hisao has a
chance to turn from the path he is taking, to leave Japan with Akiko
and make a new life with her. Instead he throws it all away to engage
in a self-destructive act---an act that in his mind means a great
deal, but in the grand scheme of things makes no difference
whatsoever, other than to bring pain to his mother, father, and lover.

This offers another reason why Hisao is downplayed in the manga: to
audiences familiar with the play, he is by his omission highlighted as
a negative role model, especially for young girls like Akira and Fumi,
and especially for a story like *Sweet Blue Flowers*. In older Class S
works, suicide (for that is what Hisao’s actions amount to) might be
the end game for some, frustrated in their inability to escape the
strictures of society. However, it has no place in the world of *Sweet
Blue Flowers*.

Its message would rather be that of Asako to Akiko after she learns of
Hisao’s death and despairs of life: “You can’t say any such
weak-hearted thing. You must by all means try to live.”[^04-07-02] Or
in other words: “Don’t be Hisao.”

[^04-07-02]: Mishima, *Rokumeikan*, 51.

What then of Count Kageyama? As I implied above, he is the Voldemort
of the *Rokumeikan* production portrayed in *Sweet Blue Flowers*,
“he-who-must-not-be-named.” But, unlike Voldemort, Kageyama is
seemingly successful in his role as the “Big Bad.”

When Kageyama’s original plan to employ Hisao goes awry, he discovers
what Asako has done and arranges a new scheme to achieve his aim. He
successfully plays on Hisao’s sense of masculinity to persuade him to
resume his plan of assassinating Kiyohara---a plan that, even if
seemingly unsuccessful, eliminates Kiyohara as a political force (as
Kiyohara himself notes). He then (it is strongly implied) has Kiyohara
killed to finish the job that Hisao could not. In plain terms, he
wins, and everyone else---Asako, Akiko, Hisao, and Kiyohara---loses.

Kageyama is not portrayed in the play as an unrelievedly evil villain.
He is jealous of Asako’s relationship with Kiyohara, and apparently
yearns to have what they have with each other---“I was jealous of that
indescribable trust that exists between you and Kiyohara”---even as he
scoffs at the possibilities of love and trust between two people: “It
is an absurd thing. Human beings can’t make pledges or trust each
other unconditionally as you and Kiyohara have done. ... That sort of
thing should never exist in our human world.”[^04-07-03]

[^04-07-03]: Mishima, *Rokumeikan*, 52.

But whatever his feelings, his actions are contemptible, and Asako
calls him out for it at the climax of act 4: “Please do not talk about
love and human beings any more. Those words are unclean. When they
come out of your mouth, they become repellent. You are clean as ice
only when you totally isolate yourself from human emotions. Please do
not bring in love and humane feelings with your *sticky* hands. This
is unlike you.”[^04-07-04]

[^04-07-04]: Mishima, *Rokumeikan*, 53. Italics in the original.

As it happens, this is the only time Kageyama appears in *Sweet Blue
Flowers* even indirectly, as Kyoko rehearses this speech. (I have used
Hiroaki Sato’s translation here instead of the one in the manga
because I think it better conveys the sense of what Asako is saying.)
Midway through, Kyoko stops, lost in thought, until prompted by
another person---perhaps the anonymous girl playing Kageyama, whom we
glimpse only from behind (*SBF*, 3:104-5).

What was Kyoko thinking? Earlier in the manga, she thought to herself,
“Did dad fall for a woman like Asako?” (*SBF*, 3:99). Is she comparing
her father to Kageyama?

And what of herself? Whatever ardor Ko felt before seems to have
cooled, replaced with frustration at Kyoko’s behavior towards
him---and perhaps also a jealousy born of whatever he might know or
guess of Kyoko’s feelings toward Yasuko. In turn, Kyoko’s renewed
desire to get married reeks of desperation and a desire to escape her
family situation---as Ko points out to her (*SBF*, 3:90--91).

Perhaps Kyoko stopped to think that what happened to her mother and
Asako might one day happen to herself: that she and Ko might enter
into a marriage with at least some lingering feelings of love and
affection, only to have it all end in cruelty and coldness. Kyoko
could not save her mother---“I couldn’t stop her from breaking”
(*SBF*, 3:101). If it ever came to that, could she save herself?
