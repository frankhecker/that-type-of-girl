---
title: "This Year’s Star"
---

## This Year’s Star

Although Akira is one of the most important characters in *Sweet Blue
Flowers*, the character Akiko that she plays in *Rokumeikan* is of
lesser importance---not exactly a bit part, but not a major role by
any means. With Ryoko Ueda we have the reverse: the character she
plays, Einosuke Kiyohara, is arguably the second or third most
important in *Rokumeikan* (after Asako and comparable to Count
Kageyama), but Ueda herself is one of the lesser characters in *Sweet
Blue Flowers*.
{:.first}

First, a bit about Kiyohara. In the play, he is described as “the
leader of the opposition group,” a group described as “the remnants of
the Liberal Party.”[^04-05-01] One of the first political parties in
Japan, the Liberal Party (*Jiyūtō*) was formed in 1881 as an outgrowth
of the Freedom and People’s Rights Movement (*Jiyū Minken Undō*), one
of Japan’s first mass political and social movements. Both the
movement and the party advocated for democratically-elected
legislatures, though with the electorate restricted to the former
samurai and nobility. The Liberal Party was disbanded in 1884 (hence
“the remnants of ....”).

[^04-05-01]: Mishima, *Rokumeikan*, 11.

Kiyohara’s son Hisao describes him as “An impeccable idealist. A
figure like a leader of the French Revolution. A genuine liberal.
... A believer in Rousseau, a Japanese Jacobin, a man who doesn’t give
a damn about his life for liberty and equality ....”[^04-05-02]
Mishima may have modeled the character of Kiyohara on Taisuke Itagaki,
one of the founders of the real-life Liberal Party. With his
colleagues, Itagaki wrote a manifesto modeled on the U.S. Declaration
of Independence (“We, the thirty millions of people in Japan are all
equally endowed with certain definite rights ....”). Like Kiyohara,
Itagaki was the victim of an assassination attempt, in his case
unsuccessful, after which he allegedly cried, “Itagaki may die, but
liberty never!”

[^04-05-02]: Mishima, *Rokumeikan*, 14.

In the play, Kiyohara is less successful in his personal life: he
neglects Hisao in favor of his legitimate children and does not speak
to Asako in the twenty years after their affair. Though the meeting
with Asako rekindles her love for him, Hisao’s resentment continues
and drives him first to plan to kill his father and then die by
his father’s hand in a perverse act of revenge upon him.

Unlike the case of Akiko and Akira, there are no real parallels
between Ueda and Kiyohara. To the extent Ueda is characterized at all
(which is not much compared to even secondary characters like Kyoko or
Haruka), it is by explicit and implicit comparisons to Yasuko
Sugimoto.

Like Yasuko, Ueda is tall and handsome, and like Yasuko eminently
suitable for playing the part of a leading man. Also, like Yasuko
(whom Mr. Kagami nicknamed the “library maiden”), Ueda spends her time
reading in Fujigaya’s library and was discovered there by Haruka
acting out the parts of Kiyohara and Asako from *Rokumeikan* (*SBF*,
2:260). Unlike Yasuko, Ueda has long hair, but then Yasuko had longer
hair too (though not as long as Ueda’s) before she cut it off in an
attempt to emulate her sister Kazusa’s “tough personality” (2:112).

As Kiyohara, Ueda also speaks a line, “a helpless child resides within
me” (*SBF*, 3:102), that Yasuko had previously used in her letter to
Mr. Kagami (1:164). Presumably, the drama club was considering
*Rokumeikan* for the following year’s theater festival, and Yasuko had
read the play in preparation for playing Kiyohara. In any case,
Mr. Kagami caught the reference, as shown in his thoughts after the
play (3:107).

I suspect Takako Shimura introduced the character of Ueda primarily to
fill the slot left open by the departure of Yasuko as the “prince” of
Fujigaya. She acts opposite Kyoko as Yasuko did opposite Kawasaki in
volume 1 and, like Yasuko, inspires admiration and crushes in the
younger girls. She is this year’s star, as Yasuko was “last year’s
star” (*SBF*, 3:18). However, unlike Yasuko, Ueda doesn’t appear to
have any significant hang-ups, other than a bit of shyness. That makes
her a better friend for Akira, Kyoko, and Fumi, but it does tend to
make her somewhat bland and underdeveloped as a character.
