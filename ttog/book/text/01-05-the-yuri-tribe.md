---
title: "The Yuri Tribe"
---

## The Yuri Tribe

The last chapter discussed how Class S literature declined to near
extinction in the postwar period. That decline may have occurred
because the social context that had given rise to S relationships had,
for the most part, disappeared with the advent of coeducation,
Western-influenced dating behaviors, and “love marriages” as an
increasingly popular alternative to arranged marriages.

How then did things change so as to create an identifiable genre of
works featuring relationships between women, what we now know as
“yuri,” and a subgenre within the yuri genre of works like *Sweet Blue
Flowers* that feature relationships between schoolgirls?

As before, I point readers to the histories of yuri in
Friedman[^01-05-01] and Maser[^01-05-02] for a full treatment and
discuss only the trends of most interest to me, starting with the rise
of magazines featuring manga targeting the *shōjo* demographic of
girls and young women.

[^01-05-01]: Friedman, “On Defining Yuri.”

[^01-05-02]: Maser, “Beautiful and Innocent.”

The production of *shōjo* manga recapitulated the development of
prewar magazines featuring Class S literature: Initially, men were
featured prominently not only as *shōjo* magazine editors but also as
creators of a substantial fraction of *shōjo* magazine
content. However, over the 1960s, as a new generation of girls grew up
reading *shōjo* manga, and some tried their hand at writing it, almost
all *shōjo* manga came to be written by women.[^01-05-03]

[^01-05-03]: Dalma Kálovics, “The Missing Link of Shōjo Manga History: The Changes in 60s Shōjo Manga as Seen Through the Magazine *Shūkan Margaret*,” *Journal of Kyoto Seika University* 49 (2016), 11--13, [https://&#x200B;www&#x200B;.academia&#x200B;.edu&#x200B;/36310321&#x200B;/The&#x200B;_missing&#x200B;_link&#x200B;_of&#x200B;_sh&#x200B;%C5&#x200B;%8Djo&#x200B;_manga&#x200B;_history&#x200B;_the&#x200B;_changes&#x200B;_in&#x200B;_60s&#x200B;_sh&#x200B;%C5&#x200B;%8Djo&#x200B;_manga&#x200B;_as&#x200B;_seen&#x200B;_through&#x200B;_the&#x200B;_magazine&#x200B;_Sh&#x200B;%C5&#x200B;%ABkan&#x200B;_Margaret](https://www.academia.edu/36310321/The_missing_link_of_sh%C5%8Djo_manga_history_the_changes_in_60s_sh%C5%8Djo_manga_as_seen_through_the_magazine_Sh%C5%ABkan_Margaret).

Eventually, the production of *shōjo* manga assumed its present form:
edited primarily by older men---supplemented by a younger cadre of
women editors not on the career track---and created almost exclusively
by younger women.[^01-05-04] Magazines sought out these female artists
for their supposed superior knowledge of what girls would be
interested in and actively recruited them via “manga schools”
sponsored by the magazines themselves.[^01-05-05]

[^01-05-04]: Jennifer S. Prough, *Straight from the Heart: Gender, Intimacy, and the Cultural Production of Shōjo Manga* (Honolulu: University of Hawai‘i Press, 2011), 90--93.

[^01-05-05]: Prough, *Straight from the Heart*, 81--87.

During the 1960s, the content of *shōjo* manga also changed, with the
family dramas of the 1950s replaced with a broader range of stories,
including romances: stories based on Hollywood romances, romances
featuring boys and girls in America and other countries, and romances
involving Japanese schoolgirls and boys---in other words, stories
featuring relationships that might have arisen in the coeducational
school environments that most Japanese girls of that time
experienced.[^01-05-06]

[^01-05-06]: Kálovics, “The Missing Link of Shōjo Manga History,” 13--15.

In the early 1970s a new group of manga artists, the so-called Year
24 group, took the styles and conventions established in 1960s *shōjo*
manga and used them to produce innovative works that stretched genre
boundaries and introduced new themes. Many of these themes directly or
indirectly influenced the yuri genre.

The first was the revival of Class S themes of doomed love among
adolescent students, as seen in Moto Hagio’s *The Heart of
Thomas*.[^01-05-07] The twist was that instead of featuring girls, it
featured boys, and instead of being set in Japan, the action took
place in an imagined European setting.

[^01-05-07]: Moto Hagio, *The Heart of Thomas*, trans. Rachel Thorn (Seattle: Fantagraphics Books, 2012).

In writing her earlier story “November Gymnasium,” a predecessor to
*The Heart of Thomas*, Hagio did a draft of it as a story of love
between girls. However, she found substituting boys for girls more in
accord with the story she wanted to tell: “When I wrote it as a boys’
school story, everything fell into place smoothly. But when I wrote
the girls’ school version it came out sort of giggly. ... [That] sort
of nastiness distinctive to girls worked its way into the
story.”[^01-05-08]

[^01-05-08]: Moto Hagio, interview by Rachel Thorn, in Moto Hagio, *A Drunken Dream and Other Stories*, trans. Rachel Thorn (Seattle: Fantagraphics Books, 2010), xxi.

*The Heart of Thomas* was followed by other works in a similar vein,
including Keiko Takemiya’s *Kaze to ki no uta* (which has never
received an official English release). These eventually came to
constitute a new genre, “boy’s love” or “BL,” which became popular
among the adolescent girls who made up the core audience for *shōjo*
manga.

Even before *The Heart of Thomas* Ryoko Yamagishi had published her
manga *Shiroi heya no futari* (unreleased in English), also featuring
doomed love in a European setting but between two girls. This is
considered among the first, if not the first, yuri manga.[^01-05-09]
However *Shiroi heya no futari* did not immediately lead to a flood of
similar works, possibly due to what I’ve hypothesized: that the social
context of traditional Class S works was no longer present in postwar
Japan. From this point of view, most adolescent girls would likely
have been more interested in reading stories featuring romances
involving boys, including BL works, than stories featuring romances
between girls.

[^01-05-09]: Erica Friedman, review of *Shiroi heya no futari*, by Ryoko Yamagishi, *Okazu* (blog), June 3, 2004, [https://&#x200B;okazu&#x200B;.yuricon&#x200B;.com&#x200B;/2004&#x200B;/06&#x200B;/03&#x200B;/yuri&#x200B;-manga&#x200B;-shiroi&#x200B;-heya&#x200B;-no&#x200B;-futari](https://okazu.yuricon.com/2004/06/03/yuri-manga-shiroi-heya-no-futari).

This included reading stories about girls who appeared to be boys, or
vice versa, which brings me to other major themes influencing the yuri
genre: gender crossing, gender nonconformance, and transformations
more generally. These themes have relatively deep roots in Japanese
artistic culture---including the cross-dressing *onnagata* performers
in all-male kabuki performances and (more recently) the *otokoyaku* of
the all-female Takarazuka Revue---as well as in Japanese
life---witness the chronicling by the popular postwar “perverse press”
of the cross-dressing *danshō* sex workers or the effeminate *gei
bōi*.[^01-05-10]

[^01-05-10]: Mark McLelland, *Queer Japan from the Pacific War to the Internet Age* (Lanham, MD: Rowman &amp; Littlefield, 2005), chap. 2, Kindle.

Themes of gender crossing and gender nonconformance featured in such
manga as *Rose of Versailles* (recently released in
English)[^01-05-11] and *Dear Brother* (adapted as an anime
subsequently released in English),[^01-05-12] both by Riyoko Ikeda,
both featuring women presenting as male, and both containing at least
hints of romances between women.

[^01-05-11]: Riyoko Ikeda, *Rose of Versailles*, trans. Mori Morimoto, 5 vols. (Richmond Hill, ON: Udon Entertainment, 2019--21).

[^01-05-12]: *Dear Brother*, directed by Osamu Dezaki (1991--92; Altamonte Springs, FL: Discotek Media, 2021), Blu-ray Disc, 1080p HD.

However, the more impactful embodiment of these themes, at least in
the West, was Naoko Takeuchi’s popular 1990s manga *Pretty Guardian
Sailor Moon*[^01-05-13] and its anime adaptations. *Sailor Moon*
featured the “magical girl” transformations of Usagi Tsukino and her
friends (the “guardians” of the title), multiple instances of
gender-nonconforming or gender-ambiguous characters (for example, the
Sailor Starlights), and---most relevant in this context---the
relationship between Haruka Tenoh (Sailor Uranus) and her partner
Michiru Kaioh (Sailor Neptune).

[^01-05-13]: Naoko Takeuchi, *Pretty Guardian: Sailor Moon*, trans. William Flanagan, 12 vols. (New York: Kodansha, 2011--13).

*Sailor Moon* was followed by other works combining magical girl plots
with yuri elements, most notably *Revolutionary Girl Utena*, which
featured the pairing of prince-like Utena Tenjou and her “Rose Bride”
Anthy Himemiya.[^01-05-14] *Revolutionary Girl Utena*, directed by
*Sailor Moon* veteran Kunihiko Ikuhara, was more explicitly feminist
than its predecessor, featuring key themes I see echoed in *Sweet Blue
Flowers*: a critique of dominance hierarchies arising from patriarchy
and how they can warp relationships between women, and a search for
alternative relationships grounded in equality.[^01-05-15]

[^01-05-14]: *Revolutionary Girl Utena*, directed by Kunihiko Ikuhara (1997; Grimes, IA: Nozomi Entertainment, 2017), Blu-ray Disc, 1080p HD.

[^01-05-15]: There are other parallels between *Revolutionary Girl Utena* and *Sweet Blue Flowers*, including incorporating elements taken from the theater (including the Takarazuka Revue) and investigating the limitations of the “girl prince” archetype. Perhaps not coincidentally, Kunihiko Ikuhara also directed the opening sequence of the anime adaptation of *Sweet Blue Flowers*.

Moving beyond the realm of fiction and fantasy, the 1990s also saw a
marked increase in the public visibility of lesbians in Japan, part of
a “gay boom” of increased mainstream media coverage of gay
culture.[^01-05-16] This increased visibility coincided with the
publication of the first commercial magazines targeted at lesbians
themselves and (in those magazines) the publication of manga by
lesbians for lesbians, including Rica Takashima’s exploration of
lesbian life in Tokyo.[^01-05-17]

[^01-05-16]: McLelland, *Queer Japan*, chap. 5.

[^01-05-17]: Rica Takashima, *Tokyo Love ~ Rica ‘tte Kanji!?*, trans. Erin Subramanian and Erica Friedman (ALC Publishing, 2013), Kindle.

At the turn of the twenty-first century, these works and themes
combined to form a recognized genre, a genre acquiring the name “yuri”
(“lily”) by which we know it today.[^01-05-18] But for my purposes,
the most significant work was one that revived Class S tropes of
schoolgirl relationships at all-girls schools and without which *Sweet
Blue Flowers* likely would not exist. That work, *Maria Watches Over
Us*, deserves its own chapter.

[^01-05-18]: Erica Friedman, “Why We Call It ‘Yuri,’” Anime Feminist, August 9, 2017, [https://&#x200B;www&#x200B;.animefeminist&#x200B;.com&#x200B;/history&#x200B;-why&#x200B;-call&#x200B;-yuri](https://www.animefeminist.com/history-why-call-yuri).
