#!/bin/sh
# Make generic website.

echo '*** Making generic website ***'
cp _data/meta-web.yml _data/meta.yml
./run-linux.sh <<EOF
3

that-type-of-girl
x
x
EOF
(cd _site; rm -rf assets/js/mathjax; tar cvf ../_output/ttog-generic-web.tar assets book)
