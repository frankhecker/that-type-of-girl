---
title: "全て和モノ"
---

## 全て和モノ {#all-japanese}

本題（要するに『鹿鳴館』）に入る前に、藤が谷演劇祭で上演される他の題目について少しばかり考える時間を取らせていただきたい。まず第一に注目すべきは（和佐もチラシを読みながら語&#x2060;っているように）、全ての演目が日本の題材を扱&#x2060;った和モノであることであろう（『青い花』(5) p. 8 / *SBF*, 3:10）。
{:.first}

これは、（以前の節で触れていたように）「（舞台解釈の）超越さを仄めかす内容は最小限に抑えるように設計されている…(中略)…現代日本から時間と場所を隔離し、それゆえ現代日本社会への直接的批判を避けた作品」であると思われた前年度の演目とは対照的なものである。この年の作品はどれも、現代の日本を舞台にしてるわけではないが、『鹿鳴館』のような演目は『嵐が丘』よりも現代日本社会に関連していることは間違いない。

他の二作品はどうだろうか？『竹取物語』(*The Bamboo Cutter*) は、『かぐや姫の物語』(*The Tale of Princess Kaguya*) としても知られる、千年以上前の日本の昔話が原作となる、子供のいない老夫婦が竹の茎の中に赤ん坊を発見し、自分たちの子供として育てる物語である。かぐや姫は成長するにつれ、皇子をはじめとする求婚者に悩まされるが、全て断り、やがて月の民が迎えに来て、生まれ故郷へと戻る。

『竹取物語』は、一見、小学生にふさわしい可愛らしい物語に見える。しかし、キ&#x2060;ャロライン&#x2060;・ツ&#x2060;ァオが指摘するように、この物語は、娘の結婚を通して自分の社会的地位を追い求める「家父長的強迫観念」に駆られた父親を暗に批判しているとも解釈することができる。「もし、かぐや姫の父親が、かぐや姫の明らかに分かる心痛を見過ごしたりせず、あるいはかぐや姫の幸福に対し出し&#x2060;ゃば&#x2060;った真似をしたりしなければ、き&#x2060;っとかぐや姫はこの地球上で幸せに暮らしていただろう…(中略)…妻にしようとかぐや姫を追い求める男たちの欲から逃れて。」&#x2060;[^04-01-01]

[^04-01-01]: Caroline Cao, “The Patriarchal Pains of Womanhood in the Films of Studio Ghibli’s Isao Takahata,” Anime Feminist, January 25, 2019, [https://&#x200B;www&#x200B;.animefeminist&#x200B;.com&#x200B;/feature&#x200B;-the&#x200B;-patriarchal&#x200B;-pains&#x200B;-of&#x200B;-womanhood&#x200B;-in&#x200B;-the&#x200B;-films&#x200B;-of&#x200B;-studio&#x200B;-ghiblis&#x200B;-isao&#x200B;-takahata](https://www.animefeminist.com/feature-the-patriarchal-pains-of-womanhood-in-the-films-of-studio-ghiblis-isao-takahata).

『伊豆の踊子』(*The Izu Dancer*) は、日本の有名な作家、川端康成が1926年に発表した短編小説を原作とする、より現代的な物語である。欧米では『The Dancing Girl of Izu』としてより知られているこの物語は、1968年のノ&#x2060;ーベル文学賞受賞につなが&#x2060;った、川端康成初期の傑作である。

『伊豆の踊子』は、エドワ&#x2060;ード&#x2060;・サイデンステ&#x2060;ッカ&#x2060;ー&#x2060;[^04-01-02]、後にJ&#x2060;・マ&#x2060;ーテ&#x2060;ィン&#x2060;・ホルマンによ&#x2060;って英訳されている&#x2060;[^04-01-03]。（ホルマン訳の方が日本語に忠実だと言われているが、英語としてはサイデンステ&#x2060;ッカ&#x2060;ー訳の方が読みやすいと思う）。Wikipedia記事では、この物語は「若かりし頃の恋愛を叙情的に、かつ哀愁的に描いた作品」と評されているが、西洋人である私の目には、少しばかり不気味に映る。

[^04-01-02]: Yasunari Kawabata, “The Izu Dancer,” trans. Edward Seidensticker, in *The Izu Dancer, and Other Stories*, Yasunari Kawabata and Yasushi Inoue, trans. Edward Seidensticker and Leon Picon (Tokyo: Tuttle, 2011). Kindle.

[^04-01-03]: Yasunari Kawabata, “The Dancing Girl of Izu,” in *The Dancing Girl of Izu, and Other Stories*, 3--33, trans. J. Martin Holman (Washington, DC: Counterpoint, 1998).

なぜそう思うのだろうか？要約された筋書きを見てみよう（Answers.comの匿名投稿者より）：「語り手である19歳の青年は、東京の上流階級の学校から休暇でや&#x2060;ってきた内省的な学生であり、…(中略)…旅芸人一座の若い踊り子に出会い、夢中になる。最初は漠然としたエロテ&#x2060;ィ&#x2060;ックな魅力を感じていた。しかし、公衆浴場で彼女の裸を見たとき、彼女がまだ（13歳の）子供であり、純粋無垢であることに気が付く。このことで、彼女に対する感情が、兄のような愛すべき保護者的なものに変わる。彼は一家に受け入れられ、親しくなる。…(中略)…最終的に、青年と小さな踊り子は、また会うという約束を交わして別れる。しかし、我々読者は、この青年が悟&#x2060;っているように、それが決して起こらないことを理解している；人生における甘く優しい瞬間は過ぎ去り、彼らの感じている愛は成就不可能なものなのだ。」&#x2060;[^04-01-04]

[^04-01-04]: “What Is a Plot Summary of The Izu dancer’?,” Answers.com, accessed November 28, 2019, [https://&#x200B;www&#x200B;.answers&#x200B;.com&#x200B;/Q&#x200B;/What&#x200B;_is&#x200B;_a&#x200B;_plot&#x200B;_summary&#x200B;_of&#x200B;_The&#x200B;_Izu&#x200B;_dancer](https://www.answers.com/Q/What_is_a_plot_summary_of_The_Izu_dancer).

（サイデンステ&#x2060;ッカ&#x2060;ー版では青年の年齢が19歳、少女の年齢が13歳だが、ホルマン版では20歳、14歳とされている。これは、生まれたときから1歳で、正月に1歳ずつ年齢が上がるという日本の古い制度に従&#x2060;って書かれた年齢を、ホルマンが文字通りに翻訳しているからだと思われる。）

一方、評論家マ&#x2060;ーク&#x2060;・モリスによる、この物語は「浄化、純化に関するものであり、…(中略)…大人の女性の性欲を消し去り、処女性という存在し得ない白い空洞でそれを置き換えることによ&#x2060;って、解放、いわば歓喜にも近い衝動を生み出す叙事的なビジ&#x2060;ョン」を有するものだという意見に同意することもできなくはなく、価値ある文学作品とみなすこともできる&#x2060;[^04-01-05]。一方、純粋無垢であることが崇拝される、思春期前のいわゆる「俺の嫁」(waifu) が登場する多くのアニメや漫画に親しんでいる人たちにと&#x2060;っては、この物語は少し受け入れがたいかもしれない。

[^04-01-05]: Mark Morris, “Orphans,” review of *The Dancing Girl of Izu, and Other Stories*, by Yasunari Kawabata, trans. J. Martin Holman, *New York Times*, October 12, 1997, [https://&#x200B;archive&#x200B;.nytimes&#x200B;.com&#x200B;/www&#x200B;.nytimes&#x200B;.com&#x200B;/books&#x200B;/97&#x200B;/10&#x200B;/12&#x200B;/reviews&#x200B;/971012&#x200B;.12morrist&#x200B;.html](https://archive.nytimes.com/www.nytimes.com/books/97/10/12/reviews/971012.12morrist.html).

では、なぜ『伊豆の踊子』は、少なくとも六本の映画、三本のテレビドラマが制作され、そして（『青い花』では）堅苦しい女子校の中学生が演じるにふさわしいとみなされるような舞台化作品にまでなることができたのだろうか？中には、踊り子の少女の年齢を吊り上げることで、筋書きの暗示する所をすり替えているような脚色もある。例えば、1933年のサイレント映画版では、少女の年齢は語られないが、24歳の主演女優である田中絹代が未成年と見間違われることはまずないであろう&#x2060;[^04-01-06]。

[^04-01-06]: *The Dancing Girl of Izu*, directed by Heinosuke Gosho (Shochiku, 1933), 1 hr., 32 min., [https://&#x200B;www&#x200B;.youtube&#x200B;.com&#x200B;/watch&#x200B;?v&#x200B;=yd36RJ0nzdM](https://www.youtube.com/watch?v=yd36RJ0nzdM).

藤が谷での演劇が恐らくそうであるように、他の脚色では青年の年齢を引き下げている。アニメ版セ&#x2060;ーラ&#x2060;ーム&#x2060;ーンを見ている多くの人が、13歳の月野うさぎに大学生の彼氏がいることを気にしていないように、これは、気にしない人もいるかもしれない。

それよりも興味深い質問としては、志村貴子は気にしているのだろうか、という点があろう。これは、私には知る由もない。がしかし、これまで何度も書いてきたように、『青い花』は、年齢やその他の面で不平等な者同士の関係（千津とふみ、恭己とふみ、康と京子）よりも、対等な者同士の関係（あきらとふみ、織江と日向子）を暗黙的に支持しているように思われるのである。そういう視点から、志村が『伊豆の踊子』を引用したのは、若い少女と年上の男性に関して、現代の日本社会が今なおいくつかの問題を抱え続けていることを微かに仄めかしているのではないか、という私の解釈に、ご理解の程を示していただければ幸いに思う。
