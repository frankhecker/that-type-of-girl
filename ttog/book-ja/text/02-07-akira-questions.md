---
title: "あきらの質問"
---

## あきらの質問 {#akira-questions}

LGBTQIA、および、より発音のしやすいアナグラムであるQUILTBAGという頭字語は、「ゲイとレズビアン」というフレ&#x2060;ーズや、より一般的に使われている頭字語LGBTよりも包括的であることを意図して考案されたものだ。こうい&#x2060;った文字の積み重ねを揶揄することは簡単だが、これらの新語の背景には重要な意味がある：人々のアイデンテ&#x2060;ィテ&#x2060;ィや性的指向および感情の豊かな多様性は、制限されたラベルでは適切に描写できないのである。奥平あきらは、その典型的な例だろう。
{:.first}

万城目ふみと同じように、あきらの外見の印象は、内面の表現と必ずしも一致しない。一見すると、典型的な「元気娘」のステレオタイプに当てはまる：活発でエネルギ&#x2060;ッシ&#x2060;ュで、自信に満ちていて熱狂的というものだ。あきらの冷静さと実行力は、ふみにと&#x2060;って、涙を拭い、くじけずに人生を歩んでいくために必要不可欠な存在だ。

しかし同時にその一方、あきらは自分自身と自分の気持ち（例えばふみに対する気持ちなど）に関しては、それほど自信と確信を持ち合わせてはいない。ふみが杉本恭己と付き合&#x2060;っていることを告げた際、あきらは動揺する：「別に女の人を好きでも いんじ&#x2060;ゃないでし&#x2060;ょうか？ ……あたしの頭がシンプルすぎるのでし&#x2060;ょうか？」と自問するのである（『青い花』、(1) 144 / *SBF*, 1:144）。

物語のこの時点では、あきらが自分の性癖に疑問を抱いている（LGBTQIA の「Q」の、可能な意味の一つ（※訳注：QはQueerの他に、Questioningとも呼ばれる））というより―恭己の「女殺し」的なオ&#x2060;ーラを肌で感じてはいたものの（『青い花』(2) pp. 37--8 / *SBF*, 1:233--34）―異性愛という従来の語り口に対して疑問を抱いているのであろう。あきらは、藤が谷のような場所で語られる少女同士の友情については親しみがあ&#x2060;ったが―井汲京子が中学生から手紙を受け取&#x2060;った際、「ホントにそういうのあるんだ すごい～～」と驚いてはいたけれど（(1) p. 78 / 1:78）―今は、自分の大切な人の中で、その感情のリアルさに遭遇しているのである。

あきら自身が恋愛経験不足であることも、事態をより難解なものにしている。片思いの相手もいなければ、デ&#x2060;ートの類もしたことがないようだ。京子と澤乃井康が企画した「合コン」に行くことを楽しみにしているが、兄が付き添&#x2060;ったためか、はたまた他の理由か、そこでも何も起こらなか&#x2060;ったのである（『青い花』(1) pp. 97--8, (1) pp. 102--3 / *SBF*, 1:97--98, 1:102--3）。あきらの兄や京子から、康が自分に興味を持&#x2060;っていることを指摘されるも、「そういうの あたしはまだいいや&#x2060;って……」という反応が引き出されたのみであ&#x2060;った（(1) pp. 129--130 / 1:129--130）。

あきらにその覚悟はあるのだろうか？この物語の中で、どちらか一方とは&#x2060;っきり言うのはまだ早すぎる。今のところ、あきらがアセクシ&#x2060;ャルかアロマンチ&#x2060;ック（LGBTQIAの「A」の二つのあり得る意味。※訳注：他者に対して性的欲求（恋愛感情）を抱かないセクシ&#x2060;ャリテ&#x2060;ィ）であることを示唆する証拠は仄めかされている。残念ながら、あきらの兄のシスコンネタが、あきらのありのままの姿ではなく、何か暗い原因があることを暗示していて、この考えを曇らせてしま&#x2060;っているが…。一方、あきらを単なる友人以上の存在として見始めている（あるいは「初恋」の記憶からすると、再認識し始めている）ように思われるふみはどうなのだろうか。これらの疑問に対する答えは、今後の巻を見ていく必要があろう。
