---
title: "タイトルページ"
style: title-page
---

{% include metadata %}

{{ title }}
{:.title-page-title}

{{ subtitle }}
{:.title-page-subtitle}

{{ creator }}
{:.title-page-author}

自己出版
{:.title-page-publisher}
