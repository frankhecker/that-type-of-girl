---
title: "著作権等について"
style: copyright-page

# The Liquid tags here fetch metadata
# from this book's YML file in _data
---

{% include metadata %}

# 著作権等について {#copyright}
{:.non-printing}

{{ title }}：{{ subtitle }}\\
(*That Type of Girl: Notes on Takako Shimura’s* Sweet Blue Flowers)\\
{{ publisher }} (Frank Hecker), Ellicott City, MD 21042\\
©&nbsp;2022 {{ creator }}\\
日本語訳 ©&nbsp;2022 紺助\\
{{ rights }}\\
連絡先：frank@frankhecker.com\\
発行：{{ date }} 最終改訂：{{ modified }}

{% include identifiers scheme="ISBN" %}

表紙イラスト： オーラ・タラカノーヴァ
