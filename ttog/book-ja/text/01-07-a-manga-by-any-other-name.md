---
title: "漫画はどんな名で呼ばれようとも"
---

## 漫画はどんな名で呼ばれようとも {#a-manga-by-any-other-name}

なぜ『青い花』（洋題 Sweet Blue Flowers）は「甘く青い花」という名前なのだろうか？タイトルの末尾から始めて、逆順に見ていくとしよう。
{:first}

前述したように、英語版『Sweet Blue Flowers』の日本語原題は『青い花（Aoi hana）』である。Hanaは「flower」&#x2060;―つまり「flowers（花）」という意味だが、日本語には英語やその他の言語のような複数形が存在しない。志村が吉屋信子を名指しで挙げ、第一話のサブタイトルを「花物語」としたことからも、タイトルの「Hana」は、以前取り上げた短編集『花物語』を意識していることを示唆しているといえよう。

次に、「Aoi」だ。これは英語で「blue」という意味なので、「blue flower」あるいは「blue flowers」となる。この文脈において、青という色に何か意味はあるのだろうか？残念ながら、『花物語』の各話のタイトルを英語で記したリストが見つからないので、これが吉屋の特定の物語を指しているのかどうかは分からない。

何か他のものへの言及なのだろうか？西洋文学で青い花について最もよく知られている例は、18世紀のドイツのロマン派詩人&#x2060;・哲学者ノヴ&#x2060;ァ&#x2060;ーリス（ゲオルク&#x2060;・フ&#x2060;ィリ&#x2060;ップ&#x2060;・フリ&#x2060;ードリヒ&#x2060;・フライヘア&#x2060;・フ&#x2060;ォン&#x2060;・ハルデンベルク）の未完小説『Heinrich von Ofterdingen』にて言及されているものである。英訳タイトルは『Henry of Ofterdingen』だが―この疑問により密接な関連のある点として―日本語タイトルは『青い花』と訳されており、志村の作品と同名である。この作品は、ベ&#x2060;ッドに横たわる若き日のヘンリ&#x2060;ーが、見知らぬ旅人の語る物語のイメ&#x2060;ージに魅せられるシ&#x2060;ーンから始まる：「青い花をこの目に収めることを希求している。いつもそれが頭の中にあり、それ以外には考えることも文章として形にまとめることもままならないのだ。」&#x2060;[^01-07-01]

[^01-07-01]: Novalis, *Henry of Ofterdingen: A Romance*, trans. John Owen (Cambridge, MA: Cambridge Press, 1842; Project Gutenberg, 2010), chap. 1, [https://&#x200B;gutenberg&#x200B;.org&#x200B;/ebooks&#x200B;/31873](https://gutenberg.org/ebooks/31873).

ノヴ&#x2060;ァ&#x2060;ーリスの青い花（独：blaue Blume）には、神聖な意味合いと俗悪な意味合いとの両方が含まれる。一方、Wikipediaによると、この語は、「欲望、愛、そして無限で到達不可能なものを求める形而上学的な努力を意味する。希望と物事の美しさを象徴している」とある。その後、青い花は、ドイツ&#x2060;・ロマン主義運動全体のシンボルとな&#x2060;った。

しかし、この青い花は、架空のキ&#x2060;ャラクタ&#x2060;ーである若い少女マチルダの、ソフ&#x2060;ィ&#x2060;ー&#x2060;・フ&#x2060;ォン&#x2060;・キ&#x2060;ュ&#x2060;ーンをモチ&#x2060;ーフとした現実世界への化身という意味合いも含んでいる。ノヴ&#x2060;ァ&#x2060;ーリスは、ソフ&#x2060;ィ&#x2060;ーが12歳、彼が22歳のときに彼女と出会い、13歳の誕生日の直前に密かに婚約をした。しかし、結婚には至らなか&#x2060;った：ソフ&#x2060;ィ&#x2060;ーは二年後、15歳の誕生日を迎えた直後に病気で亡くな&#x2060;ってしま&#x2060;ったのである。

漫画の中で、ふみはあきらとの恋の芽生えを、「あまりにも小さなその花は  あまりにも小さすぎて  すぐそばにあるのにわからない  持て余してしまうかもしれない  そんな花......」と例えている（『青い花』(1) p. 187 / *SBF*, 1:187）。志村はドイツの小説家や詩人に親しみを持&#x2060;っているようなので（エピソ&#x2060;ードタイトルについての付録を参照）、彼女が作品タイトルを『青い花』にしたとき、ドイツ&#x2060;・ロマン派の「blaue Blume」や、特にノヴ&#x2060;ァ&#x2060;ーリスの小説を意識した可能性は十分あるだろう。しかし、私の考えでは、ヘンリ&#x2060;ーとマチルダの関係（およびノヴ&#x2060;ァ&#x2060;ーリスとソフ&#x2060;ィ&#x2060;ーの関係）に見られる年齢や身分の差は、『青い花』では否定されているように思われる。

「Sweet（甘い）」という言葉はどうだろうか？『青い花』がフランス語とスペイン語に翻訳されたとき、そのタイトルはそれぞれ「Fleurs bleues&#x2060;[^01-07-02]」「Flores azules&#x2060;[^01-07-03]」とそのまま直訳された。では、なぜ英語のタイトルはただの「Blue Flowers」ではなく、「Sweet Blue Flowers」なのだろうか？

[^01-07-02]: Takako Shimura, *Fleurs bleues*, trans. Satoko Inaba and Margot Maillac, 8 vols. (Paris: Kazé, 2009--15).

[^01-07-03]: Takako Shimura, *Flores azules*, trans. Ayako Koike, 8 vols. (Colombres, Spain: Milky Way Ediciones, 2015--16).

は&#x2060;っきりとは分からないが、『青い花』が全八巻で出版されたとき、日本語版で『Sweet Blue Flowers』が別タイトルとして使われていたことは確かに明言できる。（このタイトルは、『マンガ&#x2060;・エロテ&#x2060;ィクス&#x2060;・エフ』で連載が始ま&#x2060;ったときにも使われていたと思うが、確証はない）。このタイトルは、『青い花』の最初の英訳版が（部分的に）出版された際にも引き継がれ、その後VIZ Media版でも再び使用された。

最後に、このタイトルはどんな青い花を指しているのだろうか？青い花は、自然界にはあまり存在しないもので、この語が象徴的な意味を持つ理由の一つとな&#x2060;っている。（例えば、青いバラは天然には存在しないし、遺伝子操作で作ろうとしても一部しか成功していない）。

志村は、青い花の正体を、我々の想像に委ねようとしているのかもしれない：漫画のカラ&#x2060;ーペ&#x2060;ージで青い花を見つけたのは、第一巻冒頭の人物紹介にある七弁の青い花と、日本版第一巻の裏表紙にある四弁の花の、わずか二つだけであ&#x2060;った。

これはすなわち、このタイトルにふさわしい花の種類を自由に考えてみる余地が、我々自身に楽しみとして残されているということである。ユリは、日本語の「百合」という語が「ユリ」そのものであることおよびエスジ&#x2060;ャンルとの強い結びつきを持&#x2060;っていることからも、明らかな選択肢の一つであろう。しかし、本物のユリ（ユリ科の標準属）に、青い花は存在しない。これは、ひ&#x2060;ょ&#x2060;っとしたら志村のメ&#x2060;ッセ&#x2060;ージの一部なのではないだろうか：『青い花』がエスや百合という伝統的な物語にオマ&#x2060;ージ&#x2060;ュを捧げながらも、最終的にはそこから脱却するというメ&#x2060;ッセ&#x2060;ージの…。

その他可能性のあるチ&#x2060;ョイスとしては藤がある。藤は、紫色の花を咲かせるが、しばしば青みがか&#x2060;った色にも見えるのだ。「藤」という漢字は「藤が谷」にも、「藤崎」（藤が谷女学院がある鎌倉の隣の市）にも出てくる。「藤」は、あきらが藤が谷に入学したときのクラスの名前でもある（『青い花』(1) p. 26 / *SBF*, 1:26）。しかし、『青い花』はあきらの物語であると同時にふみの物語でもあり、ふみは別の学校に通&#x2060;っている。

結局、青い花の正体は、少なくとも英語の読者には謎のままである。この本の表紙を飾るものとして青い花のイラストを選んだが、それがどの種を描いたものなのか、私には分からない。私はこれらの花を、そして『青い花』を、他ならぬそれら自身独特の存在であると考えるようにしたいと思う。
