---
title: "新しい年、新しい子"
---

## 新しい年、新しい子 {#new-year-new-girl}

今回は『青い花』第二巻（オリジナル日本語版では第四巻）の後半へと入&#x2060;っていこう。ふみとあきらの高校1年生が終わり、2年生になる所だ。杉本恭己が舞台から退場し（前節で述べていた通り）、新たな補助的な脇役として、大野春花が登場する（『青い花』(4) p. 2 / *SBF*, 2:182）。
{:.first}

春花は、ふみとあきらという中心的な二人組の周りに興味深い脇役を据えるという、『青い花』の伝統を引き継いでいる。（しかし、これは普遍的なものではないわけだが：ふみの松岡の友人であるモギ&#x2060;ー、や&#x2060;っさん、ポンは相変わらず出番が少なく、キ&#x2060;ャラクタ&#x2060;ーも薄いままだ。）

春花は、物語の中でいくつかの非常に重要な役割を担&#x2060;っている。まず第一に、春花の個性的なキ&#x2060;ャラクタ&#x2060;ーと声の大きさは、それだけで面白い。また、特に藤が谷の小&#x2060;・中学校に通&#x2060;っておらず、高校編入組であるという側面から、藤が谷で起きていることをフレ&#x2060;ッシ&#x2060;ュな目で眺める視点というものも提供してくれるのだ。

この点およびその他の点（身長も含めて）において、春花はあきらに似ている。藤が谷での全てのことに対して目を丸くして感嘆の声を上げるその姿は、まるであきらの若い版であるかのようだ。実際に、学校へ続くトンネルに入り、「藤が谷のお嬢様」というあきらの言葉をほぼ一字一句同じように繰り返す春花の姿が、視覚的にも同じように捉えることができる（『青い花』(4) pp. 14--5 / *SBF*, 2:194--95）。「去年の奥平さんみたいな人がいるみたいよ」と京子は驚嘆の声を上げる（(4) p. 15 / 2:195）（※訳注：英語版では「SHE’S JUST LIKE YOU WERE!（あの子、以前のあなたみたいね！ ）」と感嘆の声を上げているものの、日本語版での京子はもう少し落ち着いているようである）。

あきらがいるのに、なぜあきらのクロ&#x2060;ーンが必要なのか、と思う人もいるかもしれない。最も単純な答えは、二年目のあきらは一年目のあきらとはだいぶ変わ&#x2060;っているということにある。あきらは、ふみの自分への興味にどぎまぎしており、この状況をどうしたらいいのか分からず、誰に向か&#x2060;ってアドバイスを尋ねたり親身にな&#x2060;って話を聞いてもらえばいいのかも分からず、どこか不安でほとんど我を失&#x2060;っているように見えるのだ。「ち&#x2060;ょ&#x2060;っとよそよそしいかんじ……」とふみはあきらに言う（『青い花』(4) p. 69 / *SBF*, 2:249）。

春花は、あきらの魅力的な特徴を読者に思い起こさせる：元気で衝動的な性格、正義感の強さ、自分が正しいと思うことをし&#x2060;っかりと通す発言や行動を厭わずにできる所などがそれだ。藤が谷に通う動機さえも、同じように天然&#x2060;っぽいものなのである：あきらはほとんど気まぐれなミ&#x2060;ーハ&#x2060;ー的感じで、一方春花は恭己への憧れから―恭己は藤が谷に通&#x2060;っているわけではなか&#x2060;ったことさえ知らずに―入学を決めたようだ（『青い花』(4) pp. 108--10 / *SBF*, 2:288--90）。年齢も違えば通&#x2060;っている学校も違うのに、ふみが春花と友達になることに何も驚きはないだろう。あきらに惹かれる部分の多くは、春花にも惹かれる部分にな&#x2060;っているのである。

春花もまた、ある意味、あきらと同じような自己分析をしている。あきらは、ふみの告白をき&#x2060;っかけに、ふみに対する自分の気持ちと、レズビアンの関係に対する自分の気持ちとを問い直していたように、春花もまた、姉のラブレタ&#x2060;ーを見つけたことで、身近な人を巻き込みながら、この問題について考えるようになる（『青い花』(4) p. 142 / *SBF* 2:322）。

また、春花は、大小様々な形で物語を駆動する推進力にもな&#x2060;っている。上田良子が図書館で本を読んでいるのを発見し、学園劇に出演するよう勧め、同じように出演しようかと考えているふみを（結局失敗したが）励ましたり助言を加えたりし、ふみがやめることを告げると、自分の強引さを詫びる（『青い花』(4) pp. 79--80, (4) p. 82, (4) p. 111, (4) p. 114, (4) pp. 134--6 / *SBF*, 2:259--60, 2:262, 2:291, 2:294, 2:314--16）。

最後に、春花の姉についての春花とふみとの会話（「うちの姉 女の人が好き&#x2060;っぽいんですよ」）、その暴露についてのふみの春花に対する反応、そしてその反応の不適切さについてのふみの後悔は、第二巻のクライマ&#x2060;ックスシ&#x2060;ーンである、ふみがあきらに自分の本当の気持ちを告白する場面を生み出している（『青い花』(4) pp. 148--51, (4) pp. 153--8 / *SBF*, 2:328--31, 2:333--38 ）。

このように、春花は非常に面白く、最大級に魅力的なキ&#x2060;ャラクタ&#x2060;ーである。英語版第二巻の終わりまでには、もう藤が谷で一目置かれる存在にな&#x2060;っており（「あんたすごいね 上級生の友達続々と」とあきらは語りかける）、前節で述べた「ヒエラルキ&#x2060;ーの逆転」というテ&#x2060;ーマをさらに強めている（『青い花』(4) p. 138 / *SBF*, 2:318）。第三巻での彼女のキ&#x2060;ャラクタ&#x2060;ー性がどうなるかはまだ分からないが、彼女の存在が第二巻のハイライトの一つであることは間違いない。
