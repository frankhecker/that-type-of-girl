---
title: "著者について"
---

## 著者について {#about-the-author}

著者が日本のものに興味を持つようにな&#x2060;ったき&#x2060;っかけは、何年も前に日本を訪れることにな&#x2060;った、仕事での出張であ&#x2060;った―偶然にもその旅では、将来『青い花』の舞台となる場所の近くで二週間を過ごしていた。漫画やアニメに興味を持&#x2060;ったのは、も&#x2060;っと最近のことである。
{:.first}

著者は、他の漫画やアニメ、あるいは日本の大衆文化全般に関する本は書いていないし、また書く予定もない。その他の記事については、[frankhecker.com][fh]で参照可能、またはTwitterの[@hecker][tw]をフ&#x2060;ォロ&#x2060;ーいただければと思う。Eメ&#x2060;ールでの連絡は、frank@frankhecker.comへどうぞ。

[fh]: https://frankhecker.com/
[tw]: https://twitter.com/hecker
