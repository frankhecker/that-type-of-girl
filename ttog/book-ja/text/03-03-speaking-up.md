---
title: "声を大にして"
---

## 声を大にして {#speaking-up}

前章で書いていたように、泣き虫であるにもかかわらず、「ふみの性格には鋼の芯がある」のだ。その鋼の芯は、和佐の結婚式の後、恭己が各務先生への気持ちから逃れるために、あきらや兄と一緒に江ノ島でふみに会おうとした場面で、最もは&#x2060;っきりと示されている（『青い花』(3) pp. 100--1 / *SBF*, 2:100--101）。
{:.first}

ふみは、恭己との過去の関わりや杉本家での和佐との出会いがあ&#x2060;ったにもかかわらず、藤が谷生でないことや恭己との別れなど様々な理由から、和佐の結婚式には出席しなか&#x2060;った。あきらは結婚式のことをふみに話していないようだし（「…て 言えないか」と京子は察する）、ふみもあきらの自宅に電話して、あきらの母によ&#x2060;って思い起こされるまで、す&#x2060;っかり忘れていたようである（『青い花』(3) p. 92, (3) pp. 76--7 *SBF*, 2:92, 2:76--77）．

ふみは、その場の思いつきで江ノ島に行くことを決める。最初の動機は、自分だけの時間を持ちたいというものだ&#x2060;ったようだ（『青い花』(3) pp. 90--1 / *SBF*, 2:90--91）。電話をかけた当初は遠慮していたものの、あきらが会いに来てくれることを喜んでいるようにも見える。しかし、恭己がついてきたこと、特に、みんなと一緒に残りたいと言うあきら兄とあきらとの争いに恭己自身が割&#x2060;って入&#x2060;った後に、事態は急展開する（(3) p. 100, (3) p. 106 / 2:100, 2:106）。

この一連の出来事は、日本の学校生活（そしてさらに一般的な日本人の生活）に特徴的な「先輩後輩」の力関係によ&#x2060;って引き起こされているように思われる。低学年の生徒は高学年の生徒に従い、学年が上がると、今度は自分が下の生徒から従われることになるシステムだ。

高校を舞台にした漫画やアニメの多く（『青い花』を含む）は、このような経過をたど&#x2060;って構成されている。新1年生が入学し、元1年生が2年生に進級して新1年生の先輩になり、元2年生が3年生に進級してヒエラルキ&#x2060;ーの頂点に立ち、3年生は社会に出て新たに従う先輩を持つ、という具合だ。学校の教室を描写する際は、生徒の学年が分かるように、外側に学年を示す教室札が吊り下げられるのが一般的で、見る者が生徒のヒエラルキ&#x2060;ーを把握できるようにな&#x2060;っているのである（※訳注：これは漫画的な記述に限&#x2060;った話ではなく、日本の学校では、現実的にそうな&#x2060;っていることが多い）。

物語のこの時点では、恭己は3年生、あきらとふみは1年生と、同じ高校内では最も大きい身分の差がある。恭己は先輩としての立場を利用して、まずあきらと兄とともに江ノ島へと向かい、次にあきらと兄による、兄がその場に残るか否かを巡る口論で兄が残る側に立ち（（なんであんたが主導権握&#x2060;ってんのさ…）とあきらは内心で腹を立てる）、最後に「じ&#x2060;ゃあ お姉さんも仲間に入れて」と提案する（『青い花』(3) p. 100, (3) pp. 106--7 / *SBF*, 2:100, 2:106--7）。

あきらはこの展開に明らかに不満そうだが、黙&#x2060;っている。その代わりに、実際に言葉にして反対を表明するのが、ふみだ&#x2060;ったのである：一言、「ダメです」と。ふみは恭己に「私が先輩とい&#x2060;っし&#x2060;ょに歩きたくないんです」と続け、「あ&#x2060;ーち&#x2060;ゃんを困らせないで下さい」と命令する（『青い花』(3) p. 108 / *SBF*, 2:108）。

あきらはシ&#x2060;ョ&#x2060;ックを受けるが、それはそうであろう。ふみの言葉は、標準的な先輩後輩の構図から著しく逸脱しているように思えるからだ。この後ふみは、恭己が仲直りしようとするのを拒否し（「ふみに会いたくな&#x2060;ったから」「私は会いたくなか&#x2060;ったです」）、そして最後に最上の侮辱を口にすることで、更に事態を悪化させていく：「も&#x2060;っと大人にな&#x2060;って下さい」と…（『青い花』(3) pp. 121--3 / *SBF*, 2:121--23）。

この後、ふみはこのやりとりを反省する（「勝手だな&#x2060;ぁ て」とあきらに尋ねる（※訳注：英語版では「勝手だ&#x2060;ったでし&#x2060;ょう？」という質問を投げる形だが、日本語版では特に問いかけるわけではなく、自分自身で納得している形である。））（青い花 (3) p. 128 / *SBF*, 2:128）。しかし、恭己に言&#x2060;った言葉は取り消すことができないし、志村の枠付けの中では、ふみの発言は間違いなく正しか&#x2060;ったと私は思&#x2060;っている。第一に、恭己は実際、ふみとよりを戻そうとして、先輩としての立場を悪用し、本来参加する権利のないプライベ&#x2060;ートな場に差し出がましくも現れていたということ。そして第二に、より重要なこととして、これらのシ&#x2060;ーンは、ふみと千津、京子と恭己など、これまでの本作の他の人間関係に対する志村の枠付けと、一貫したものとな&#x2060;っているからだ。もう一度私自身の意見を引用する：「『青い花』は、対等な関係を重視し、年齢やその他のヒエラルキ&#x2060;ーに応じた不平等な関係を暗黙の内に批判しているように見て取れる」。

ここでの違いというのは、そうすることで社会の調和を促すとされるヒエラルキ&#x2060;ーの中での悪い行為を批判することと、まさにヒエラルキ&#x2060;ーそのものの概念自体を批判することとの違いなのである。志村は後者を意図しているのではないかと思う：従来の伝統的なエスや百合作品に見られる支配的なヒエラルキ&#x2060;ー（それ自体が大きなヒエラルキ&#x2060;ーの中に組み込まれているヒエラルキ&#x2060;ー）の特徴を暗に批判し、個人主義や平等主義に基づいた新しい百合のモデルを提案しているのだと思う。

ふみは、『青い花』の世界において、このモデルを体現する第一人者である。しかし、これから見ていくように、彼女は唯一の存在ではなく、また最初の存在ですらないのだ。
