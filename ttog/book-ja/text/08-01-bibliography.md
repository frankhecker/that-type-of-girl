---
title: "参照文献"
---

## 参照文献 {#bibliography}

大学図書館を利用できない人のために、パブリ&#x2060;ックドメインやオ&#x2060;ープンアクセスの書籍や論文、また、完全版が無料で合法的にオンラインで閲覧可能な書籍や論文があれば、直接ダウンロ&#x2060;ードできるURLを掲載した。それ以外の論文については、DOI（デジタルオブジ&#x2060;ェクト識別子；インタ&#x2060;ーネ&#x2060;ット上の電子化コンテンツに恒久的に与えられる国際的な固有番号）のURLを記載している。
{:.first}

※訳注：日本語の原典や普及翻訳版が存在するものは、そちらの情報も併記した。

- Akasaka, Aka. *Kaguya-sama: Love Is War*. Translated by Emi
  Louie-Nishikawa. 22 vols. San Francisco: VIZ Media, 2018--.
  原典：赤坂アカ『かぐや様は告らせたい〜天才たちの恋愛頭脳戦〜』、東京：集英社、2015--。
- Ando, Shuntaro, Sosei Yamaguchi, Yuta Aoki, and Graham Thornicroft.
  “Review of Mental-Health-Related Stigma in Japan.” *Psychiatry and
  Clinical Neurosciences* 67, no. 7 (November 2013), 471--82.
  [https://&#x200B;onlinelibrary&#x200B;.wiley&#x200B;.com&#x200B;/doi&#x200B;/10&#x200B;.1111&#x200B;/pcn&#x200B;.12086](https://onlinelibrary.wiley.com/doi/10.1111/pcn.12086).
- Anime Feminist. “2022 Winter Premiere Digest.” January 14, 2022.
  [https://&#x200B;www&#x200B;.animefeminist&#x200B;.com&#x200B;/2022-winter-premiere-digest/](https://www.animefeminist.com/2022-winter-premiere-digest/).
- ---------. “About Us.”
  [https://&#x200B;www&#x200B;.animefeminist&#x200B;.com&#x200B;/about](https://www.animefeminist.com/about).
- Anonymous. “What is a Plot Summary of ‘The Izu dancer’?”
  Answers.com. Accessed November 28, 2019.
  [https://&#x200B;www&#x200B;.answers&#x200B;.com&#x200B;/Q&#x200B;/What&#x200B;_is&#x200B;_a&#x200B;_plot&#x200B;_summary&#x200B;_of&#x200B;_The&#x200B;_Izu&#x200B;_dancer](https://www.answers.com/Q/What_is_a_plot_summary_of_The_Izu_dancer).
- Aoki, Ei, dir. *Wandering Son*. Aniplex, 2011.
  [https://&#x200B;www&#x200B;.crunchyroll&#x200B;.com&#x200B;/hourou&#x200B;-musuko&#x200B;-wandering&#x200B;-son](https://www.crunchyroll.com/hourou-musuko-wandering-son).
  原典：あおきえい（監督）アニメ『放浪息子』、東京：アニプレ&#x2060;ックス、2011。[https://&#x200B;www&#x200B;.houroumusuko&#x200B;.jp/](https://www.houroumusuko.jp/)。
- Austen, Jane. *Mansfield Park*. London: 1814; Project Gutenberg, 1994.
  [https://&#x200B;gutenberg&#x200B;.org&#x200B;/ebooks&#x200B;/141](https://gutenberg.org/ebooks/141).
  邦訳：ジ&#x2060;ェイン&#x2060;・オ&#x2060;ーステ&#x2060;ィン（臼田昭訳） 『マンスフ&#x2060;ィ&#x2060;ールド&#x2060;・パ&#x2060;ーク』、東京：集英社、1996。
- ---------. *Pride and Prejudice*. London: 1813; Project Gutenberg, 2013.
  [https://&#x200B;gutenberg&#x200B;.org&#x200B;/ebooks&#x200B;/42671](https://gutenberg.org/ebooks/42671).
  邦訳：（富田彬訳）『高慢と偏見』、東京：岩波文庫、1950。
- Bacon, Alice Mabel. *Japanese Girls and Women*. Rev. ed. Boston:
  Houghton Mifflin, 1919.
  [https://&#x200B;archive&#x200B;.org&#x200B;/details&#x200B;/japanese&#x200B;girls&#x200B;wom&#x200B;00&#x200B;baco&#x200B;_2](https://archive.org/details/japanesegirlswom00baco_2).
- Bauman, Nicki. *The Holy Mother of Yuri* (blog).
  [https://&#x200B;yurimother&#x200B;.com](https://yurimother.com).
- ---------. “Yuri Is for Everyone: An Analysis of Yuri Demographics
  and Readership.” Anime Feminist, February 12, 2020.
  [https://&#x200B;www&#x200B;.animefeminist&#x200B;.com&#x200B;/yuri&#x200B;-is&#x200B;-for&#x200B;-everyone&#x200B;-an&#x200B;-analysis&#x200B;-of&#x200B;-yuri&#x200B;-demographics&#x200B;-and&#x200B;-readership](https://www.animefeminist.com/yuri-is-for-everyone-an-analysis-of-yuri-demographics-and-readership).
- Borker, Gorija. “Safety First: Perceived Risk of Street Harassment
  and Educational Choices of Women.” Job market paper, Department of
  Economics, Brown University, 2018.
  [https://&#x200B;data2x&#x200B;.org&#x200B;/wp&#x200B;-content&#x200B;/uploads&#x200B;/2019&#x200B;/11&#x200B;/Perceived&#x200B;Risk&#x200B;Street&#x200B;Harassment&#x200B;and&#x200B;Ed&#x200B;Choices&#x200B;of&#x200B;Women&#x200B;_Borker&#x200B;.pdf](https://data2x.org/wp-content/uploads/2019/11/PerceivedRiskStreetHarassmentandEdChoicesofWomen_Borker.pdf).
- Bowers, Faubion. “Politics and Love in Japan.” Review of *After the
  Banquet* by Yukio Mishima. *New York Times*, April 14, 1963.
  [https://&#x200B;archive&#x200B;.nytimes&#x200B;.com&#x200B;/www&#x200B;.nytimes&#x200B;.com&#x200B;/books&#x200B;/98&#x200B;/10&#x200B;/25&#x200B;/specials&#x200B;/mishima&#x200B;-banquet&#x200B;.html](https://archive.nytimes.com/www.nytimes.com/books/98/10/25/specials/mishima-banquet.html).
- Bridges, Rose. Review of *Sweet Blue Flowers*, vol. 1, by Takako
  Shimura. Anime News Network. October 20, 2017.
  [https://&#x200B;www&#x200B;.animenewsnetwork&#x200B;.com&#x200B;/review&#x200B;/sweet&#x200B;-blue&#x200B;-flowers&#x200B;-2&#x200B;-in&#x200B;-1&#x200B;-edition&#x200B;/gn&#x200B;-1&#x200B;/&#x200B;.122727](https://www.animenewsnetwork.com/review/sweet-blue-flowers-2-in-1-edition/gn-1/.122727).
- Brown, Ash. Review of *Sweet Blue Flowers*, vol. 1. *Experiments in
  Manga* (blog). October 27, 2017.
  [http://&#x200B;experimentsinmanga&#x200B;.mangabookshelf&#x200B;.com&#x200B;/2017&#x200B;/10&#x200B;/sweet&#x200B;-blue&#x200B;-flowers&#x200B;-omnibus&#x200B;-1](http://experimentsinmanga.mangabookshelf.com/2017/10/sweet-blue-flowers-omnibus-1).
- Burton, Margaret E. *The Education of Women in Japan*. New York:
  Fleming H. Revell, 1914.
  [https://&#x200B;archive&#x200B;.org&#x200B;/details&#x200B;/education&#x200B;women&#x200B;ja&#x200B;00&#x200B;burt&#x200B;uoft](https://archive.org/details/educationwomenja00burtuoft).
- Butler, Shane. “A Problem in Greek Ethics, 1867–2019: A History.”
  John Addington Symonds Project. Accessed February 13, 2022.
  [https://&#x200B;symondsproject&#x200B;.org&#x200B;/greek&#x200B;-ethics&#x200B;-history](https://symondsproject.org/greek-ethics-history).
- Canno. *Kiss and White Lily for My Dearest Girl*. Translated by
  Jocelyne Allen. 10 vols. New York: Yen Press, 2013--19.
  原典：缶乃『あの娘にキスと白百合を』、東京：KADOKAWA、2013--19。
- Cao, Caroline. “The Patriarchal Pains of Womanhood in the Films of
  Studio Ghibli’s Isao Takahata.” Anime Feminist. January 25, 2019.
  [https://&#x200B;www&#x200B;.animefeminist&#x200B;.com&#x200B;/feature&#x200B;-the&#x200B;-patriarchal&#x200B;-pains&#x200B;-of&#x200B;-womanhood&#x200B;-in&#x200B;-the&#x200B;-films&#x200B;-of&#x200B;-studio&#x200B;-ghiblis&#x200B;-isao&#x200B;-takahata](https://www.animefeminist.com/feature-the-patriarchal-pains-of-womanhood-in-the-films-of-studio-ghiblis-isao-takahata).
- Carpenter, Edward. *The Intermediate Sex: A Study of Some
  Transitional Types of Men and Women*. New York: Mitchell Kennerly, 1912.
  [https://&#x200B;www&#x200B;.google&#x200B;.com&#x200B;/books&#x200B;/edition&#x200B;/The_Intermediate_Sex&#x200B;/gbcNAAAAYAAJ](https://www.google.com/books/edition/The_Intermediate_Sex/gbcNAAAAYAAJ).
- Chalmers, Sharon. *Emerging Lesbian Voices from Japan*. London:
  RoutledgeCurzon, 2002.
- Chapman, David. “Geographies of Self and Other: Mapping Japan
  through the *Koseki*.” *Asia-Pacific Journal: Japan Focus* 9, no. 29
  (July 19, 2011).
  [https://&#x200B;apjjf&#x200B;.org&#x200B;/&#x200B;-David&#x200B;-Chapman&#x200B;/3565&#x200B;/article&#x200B;.pdf](https://apjjf.org/-David-Chapman/3565/article.pdf).
- Chapman, David, and Karl Jakob Krogness, ed. *Japan’s Household
  Registration System and Citizenship:* Koseki, *Identification, and
  Documentation*. London: Routledge, 2014.
- Charlebois, Justin. “Herbivore Masculinity as an Oppositional Form
  of Masculinity.” *Culture, Society &amp; Masculinities* 5,
  no. 1 (Spring 2013), 89--104.
- Cline, Alex. Review of *Sweet Blue Flowers*, vol. 1, by Takako
  Shimura. Adventures in Poor Taste. October 19, 2017.
  [https://&#x200B;aiptcomics&#x200B;.com&#x200B;/2017&#x200B;/10&#x200B;/19&#x200B;/sweet&#x200B;-blue&#x200B;-flowers&#x200B;-vol&#x200B;-1&#x200B;-review](https://aiptcomics.com/2017/10/19/sweet-blue-flowers-vol-1-review).
- Cook, Amelia. Review of *Sweet Blue Flowers*, vol. 1, by Takako
  Shimura. *Otaku USA*. December 9, 2017.
  [https://&#x200B;www&#x200B;.otakuusamagazine&#x200B;.com&#x200B;/sweet&#x200B;-blue&#x200B;-flowers&#x200B;-review](https://www.otakuusamagazine.com/sweet-blue-flowers-review).
- Dahlberg-Dodd, Hannah E. “Script Variation as Audience Design:
  Imagining Readership and Community in Japanese Yuri Comics.”
  *Language in Society* 49, no. 3 (2020), 357--78.
  [https://&#x200B;doi&#x200B;.org&#x200B;/10&#x200B;.1017&#x200B;/S0047404519000794](https://doi.org/10.1017/S0047404519000794).
- ---------. “Talking like a *Shōnen* Hero: Masculinity in Post-Bubble
  Era Japan through the Lens of *Boku* and *Ore*.” *Buckeye East Asian
  Linguistics* 3 (October 2018), 31--42.
  [https://&#x200B;kb&#x200B;.osu&#x200B;.edu&#x200B;/bitstream&#x200B;/handle&#x200B;/1811&#x200B;/86767&#x200B;/BEAL&#x200B;_v3&#x200B;_2018&#x200B;_Dahlberg&#x200B;-Dodd&#x200B;_31&#x200B;.pdf](https://kb.osu.edu/bitstream/handle/1811/86767/BEAL_v3_2018_Dahlberg-Dodd_31.pdf).
- Dargis, Melina. Review of *Sweet Blue Flowers*, vol. 2, by Takako
  Shimura. The Fandom Post. April 11, 2018.
  [https://&#x200B;www&#x200B;.fandompost&#x200B;.com&#x200B;/2018&#x200B;/04&#x200B;/11&#x200B;/sweet&#x200B;-blue&#x200B;-flowers&#x200B;-vol&#x200B;-02&#x200B;-manga&#x200B;-review](https://www.fandompost.com/2018/04/11/sweet-blue-flowers-vol-02-manga-review)
- ---------. Review of *Sweet Blue Flowers*, vol. 3, by Takako
  Shimura. The Fandom Post. April 11, 2018.
  [https://&#x200B;www&#x200B;.fandompost&#x200B;.com&#x200B;/2018&#x200B;/12&#x200B;/08&#x200B;/sweet&#x200B;-blue&#x200B;-flowers&#x200B;-vol&#x200B;-03&#x200B;-manga&#x200B;-review](https://www.fandompost.com/2018/12/08/sweet-blue-flowers-vol-03-manga-review).
- Deacon, Chris. “All the World’s a Stage: Herbivore Boys and the
  Performance of Masculinity in Contemporary Japan.” In *Manga Girl
  Seeks Herbivore Boy: Studying Japanese Gender at Cambridge*, edited
  by Brigitte Steger and Angelika Koch, 129--76. Berlin:
  LIT Verlag, 2013.
  [https://&#x200B;www&#x200B;.academia&#x200B;.edu&#x200B;/34610378&#x200B;/All&#x200B;_the&#x200B;_Worlds&#x200B;_a&#x200B;_Stage&#x200B;_Herbivore&#x200B;_Boys&#x200B;_and&#x200B;_the&#x200B;_Performance&#x200B;_of&#x200B;_Masculinity&#x200B;_in&#x200B;_Contemporary&#x200B;_Japan&#x200B;_in&#x200B;_Brigitte&#x200B;_Steger&#x200B;_and&#x200B;_Angelika&#x200B;_Koch&#x200B;_eds&#x200B;_Manga&#x200B;_Girl&#x200B;_Seeks&#x200B;_Herbivore&#x200B;_Boy&#x200B;_Studying&#x200B;_Japanese&#x200B;_Gender&#x200B;_at&#x200B;_Cambridge&#x200B;_LIT&#x200B;_Verlag&#x200B;_2013](https://www.academia.edu/34610378/All_the_Worlds_a_Stage_Herbivore_Boys_and_the_Performance_of_Masculinity_in_Contemporary_Japan_in_Brigitte_Steger_and_Angelika_Koch_eds_Manga_Girl_Seeks_Herbivore_Boy_Studying_Japanese_Gender_at_Cambridge_LIT_Verlag_2013).
- Dentsu. “First time poll categorizes straight respondents; analyzes
  their knowledge, awareness of LGBTQ+ matters---Most ‘knowledgeable
  but unconcerned’; do not think LGBTQ+ issues relate to them---.”
  April 8, 2021.
  [https://&#x200B;www&#x200B;.dentsu&#x200B;.co&#x200B;.jp&#x200B;/en&#x200B;/news&#x200B;/release&#x200B;/2021&#x200B;/0408&#x200B;-010371&#x200B;.html](https://www.dentsu.co.jp/en/news/release/2021/0408-010371.html).
  原典：電通 『LGBTQ+調査2020』、2021年4月8日。[https://&#x200B;www&#x200B;.dentsu&#x200B;.co&#x200B;.jp&#x200B;/news&#x200B;/release&#x200B;/2021&#x200B;/0408-010364&#x200B;.html](https://www.dentsu.co.jp/news/release/2021/0408-010364.html)。
- Dezaki, Osamu, dir. *Dear Brother*. 1991--92; Altamonte Springs,
  FL: Discotek Media, 2021. Blu-ray Disc, 1080p HD.
  原典：出崎統（監督）アニメ『おにいさまへ…』、東京：手塚プロダクシ&#x2060;ョン、1991--92。
- Dollase, Hiromi Tsuchiya. *Age of Shōjo: The Emergence, Evolution,
  and Power of Japanese Girls’ Magazine Fiction*. Albany: SUNY
  Press, 2019.
- ---------. “Yoshiya Nobuko’s ‘Yaneura no nishojo’: In
  Search of Literary Possibilities in ‘Shōjo’ Narratives.”
  *U.S.-Japan Women’s Journal*, English supplement, no. 20/21 (2001),
  151--178.
  [https://&#x200B;www&#x200B;.jstor&#x200B;.org&#x200B;/stable&#x200B;/42772176](https://www.jstor.org/stable/42772176).
- Douresseaux, Leroy. Review of *Sweet Blue Flowers*, vol. 1, by
  Takako Shimura. The Comic Book Bin. October 3, 2017.
  [http://&#x200B;www&#x200B;.comicbookbin&#x200B;.com&#x200B;/sweet&#x200B;blue&#x200B;flowers001&#x200B;.html](http://www.comicbookbin.com/sweetblueflowers001.html).
- ---------. Review of *Sweet Blue Flowers*, vol. 2, by Takako
  Shimura. The Comic Book Bin. January 8, 2018.
  [http://&#x200B;www&#x200B;.comicbookbin&#x200B;.com&#x200B;/sweetblueflowers002&#x200B;.html](http://www.comicbookbin.com/sweetblueflowers002.html).
- Duke, Benjamin. *The History of Japanese Education: Constructing the
  National School System, 1872--1890*. New Brunswick, NJ: Rutgers
  University Press, 2009.
- Dumas, Alexandre. *The Three Musketeers*. Translated by Richard
  Pevear. New York: Penguin Books, 2007. Kindle.
  邦訳：アレクサンドル&#x2060;・デ&#x2060;ュマ&#x2060;・ペ&#x2060;ール（生島遼一訳）『三銃士』、東京：岩波文庫、1947--48。
- Duyvis, Corinne. “#OwnVoices.” Accessed July 1, 2022.
  [https://&#x200B;www&#x200B;.corinneduyvis&#x200B;.net/ownvoices/](https://www.corinneduyvis.net/ownvoices/).
- *Eureka*, November 2017. Tokyo: Seidosha, 2017.
  原典：『ユリイカ 総特集＝志村貴子』（平成29年11月臨時増刊号）、東京：青土社、2017。[http://&#x200B;www&#x200B;.seidosha&#x200B;.co&#x200B;.jp&#x200B;/book&#x200B;/index&#x200B;.php&#x200B;?id=3091](http://www.seidosha.co.jp/book/index.php?id=3091)。
- “Essays.” Yuricon. Accessed February 14, 2022.
  [https://&#x200B;www&#x200B;.yuricon&#x200B;.com&#x200B;/essays](https://www.yuricon.com/essays).
- Evans, Alice. “How Did East Asia Overtake South Asia?” *The Great
  Gender Divergence* (blog). March 13, 2021.
  [https://&#x200B;www&#x200B;.draliceevans&#x200B;.com&#x200B;/post&#x200B;/how&#x200B;-did&#x200B;-east&#x200B;-asia&#x200B;-overtake&#x200B;-south&#x200B;-asia](https://www.draliceevans.com/post/how-did-east-asia-overtake-south-asia).
- EyeSpyeAlex \[Alexandra Nutting\]. Review of *Sweet Blue Flowers*,
  vol. 1, by Takako Shimura. The Geekly Grind. October 7, 2017.
  [http://&#x200B;www&#x200B;.thegeeklygrind&#x200B;.com&#x200B;/sweet&#x200B;-blue&#x200B;-flowers&#x200B;-part&#x200B;-one](http://www.thegeeklygrind.com/sweet-blue-flowers-part-one).
- ---------. Review of *Sweet Blue Flowers*, vol. 2, by Takako
  Shimura. The Geekly Grind. January 5, 2018.
  [http://&#x200B;www&#x200B;.thegeeklygrind&#x200B;.com&#x200B;/sweet&#x200B;-blue&#x200B;-flowers&#x200B;-part&#x200B;-two](http://www.thegeeklygrind.com/sweet-blue-flowers-part-two).
- ---------. Review of *Sweet Blue Flowers*, vol. 3, by Takako
  Shimura. The Geekly Grind. March 25, 2018.
  [http://&#x200B;www&#x200B;.thegeeklygrind&#x200B;.com&#x200B;/sweet&#x200B;-blue&#x200B;-flowers&#x200B;-volume&#x200B;-4&#x200B;-review](http://www.thegeeklygrind.com/sweet-blue-flowers-volume-4-review) \[sic\].
- ---------. Review of *Sweet Blue Flowers*, vol. 4, by Takako
  Shimura. The Geekly Grind. July 2, 2018.
  [http://&#x200B;www&#x200B;.thegeeklygrind&#x200B;.com&#x200B;/sweet&#x200B;-blue&#x200B;-flowers&#x200B;-volume&#x200B;-4&#x200B;-review&#x200B;-2](http://www.thegeeklygrind.com/sweet-blue-flowers-volume-4-review-2).
- Frederick, Sarah. “Not That Innocent: Nobuko Yoshiya’s Good Girls.”
  In *Bad Girls of Japan*, edited by Laura Miller and Jan Bardsley,
  65--79. New York: Palgrave Macmillan, 2005.
- ---------. Review of *Passionate Friendship: The Aesthetics of
  Girls’ Culture in Japan*, by Deborah Shamoon. *Mechademia*, October
  7, 2013.
  [https://&#x200B;www&#x200B;.mechademia&#x200B;.net&#x200B;/2013&#x200B;/10&#x200B;/07&#x200B;/book&#x200B;-review&#x200B;-passionate&#x200B;-friendship](https://www.mechademia.net/2013/10/07/book-review-passionate-friendship).
- ---------. Translator’s introduction to *Yellow Rose*, by Nokuko Yoshiya.
- ---------. *Turning Pages: Reading and Writing Women’s Magazines in
  Interwar Japan*. Honolulu: University of Hawai‘i Press, 2006.
- Freedman, Alisa. “Commuting Gazes: Schoolgirls, Salarymen, and
  Electric Trains in Tokyo.” *Journal of Transport History* 23, no. 1
  (March 2002), 23--36.
  [https://&#x200B;doi&#x200B;.org&#x200B;/10&#x200B;.7227&#x200B;/TJTH&#x200B;.23&#x200B;.1&#x200B;.4](https://doi.org/10.7227/TJTH.23.1.4).
- Friedman, Erica. *By Your Side: The First 100 Years of Yuri Manga
  and Anime*. Vista, CA: Journey Press, 2022.
- ---------. “Is Yuri Queer?” Anime Feminist. June 7, 2019.
  [https://&#x200B;www&#x200B;.animefeminist&#x200B;.com&#x200B;/feature&#x200B;-is&#x200B;-yuri&#x200B;-queer](https://www.animefeminist.com/feature-is-yuri-queer).
- ---------. “*Maria-sama ga miteru*: 20 Years of Watching Mary
  Watching Us.” *Okazu* (blog). January 28, 2018.
  [https://&#x200B;okazu&#x200B;.yuricon&#x200B;.com&#x200B;/2018&#x200B;/01&#x200B;/28&#x200B;/maria&#x200B;-sama&#x200B;-ga&#x200B;-miteru&#x200B;-20&#x200B;-years&#x200B;-of&#x200B;-watching&#x200B;-mary&#x200B;-watching&#x200B;-us](https://okazu.yuricon.com/2018/01/28/maria-sama-ga-miteru-20-years-of-watching-mary-watching-us).
- ---------, ed. *Okazu* (blog). [https://&#x200B;okazu&#x200B;.yuricon&#x200B;.com](https://okazu.yuricon.com).
- ---------. “On Defining Yuri.” *Transformative Works and Cultures* 24
  (2017).
  [https://&#x200B;journal&#x200B;.transformativeworks&#x200B;.org&#x200B;/index&#x200B;.php&#x200B;/twc&#x200B;/article&#x200B;/view&#x200B;/831&#x200B;/835](https://journal.transformativeworks.org/index.php/twc/article/view/831/835).
- ---------. “Overthinking Things 03/02/2011.” The Hooded Utilitarian.
  March 2, 2011.
  [https://&#x200B;www&#x200B;.hooded&#x200B;utilitarian&#x200B;.com&#x200B;/2011&#x200B;/03&#x200B;/overthinking&#x200B;-things&#x200B;-03022011](https://www.hoodedutilitarian.com/2011/03/overthinking-things-03022011).
- ---------. “Overthinking Things 04/03/2011: 40 Years of the Same Damn
  Story, Pt. 1.” The Hooded Utilitarian. April 3, 2011.
  [https://&#x200B;www&#x200B;.hoodedutilitarian&#x200B;.com&#x200B;/2011&#x200B;/04&#x200B;/overthinking&#x200B;-things&#x200B;-04032011](https://www.hoodedutilitarian.com/2011/04/overthinking-things-04032011).
- ---------. “Overthinking Things 05/03/2011: 40 Years of the Same
  Damn Story, Part 2.” The Hooded Utilitarian. May 2, 2011.
  [https://&#x200B;www&#x200B;.hoodedutilitarian&#x200B;.com&#x200B;/2011&#x200B;/05&#x200B;/21840](https://www.hoodedutilitarian.com/2011/05/21840).
- ---------.  “‘Own Voices’: Are There Queer Creators Creating Yuri?”
  YouTube video, 15:23. December 13, 2020.
  [https://&#x200B;www&#x200B;.youtube&#x200B;.com/watch?v=eqZeCMWDt08](https://www.youtube.com/watch?v=eqZeCMWDt08).
- ---------. Review of *Passionate Friendship: The Aesthetics of
  Girls’ Culture in Japan*, by Deborah Shamoon. *Okazu* (blog).
  February 6, 2014.
  [https://&#x200B;okazu&#x200B;.yuricon&#x200B;.com&#x200B;/2014&#x200B;/02&#x200B;/06&#x200B;/passionate&#x200B;-friendship&#x200B;-the&#x200B;-aesthetics&#x200B;-of&#x200B;-girls&#x200B;-culture&#x200B;-in&#x200B;-japan](https://okazu.yuricon.com/2014/02/06/passionate-friendship-the-aesthetics-of-girls-culture-in-japan).
- ---------. Review of *Shiroi heya no futari*, by Ryoko
  Yamagishi. *Okazu* (blog). June 3, 2004.
  [https://&#x200B;okazu&#x200B;.yuricon&#x200B;.com&#x200B;/2004&#x200B;/06&#x200B;/03&#x200B;/yuri&#x200B;-manga&#x200B;-shiroi&#x200B;-heya&#x200B;-no&#x200B;-futari](https://okazu.yuricon.com/2004/06/03/yuri-manga-shiroi-heya-no-futari).
- ---------. Review of *Sweet Blue Flowers*, disc 1. *Okazu*
  (blog). May 6, 2013.
  [https://&#x200B;okazu&#x200B;.yuricon&#x200B;.com&#x200B;/2013&#x200B;/05&#x200B;/06&#x200B;/yuri&#x200B;-anime&#x200B;-sweet&#x200B;-blue&#x200B;-flowers&#x200B;-aoi&#x200B;-hana&#x200B;-disk&#x200B;-1&#x200B;-english](https://okazu.yuricon.com/2013/05/06/yuri-anime-sweet-blue-flowers-aoi-hana-disk-1-english).
- ---------. Review of *Sweet Blue Flowers*, vol. 1, by Takako
  Shimura. *Okazu* (blog). October 4, 2017.
  [https://&#x200B;okazu&#x200B;.yuricon&#x200B;.com&#x200B;/2017&#x200B;/10&#x200B;/04&#x200B;/yuri&#x200B;-manga&#x200B;-sweet&#x200B;-blue&#x200B;-flowers&#x200B;-volume&#x200B;-1&#x200B;-english](https://okazu.yuricon.com/2017/10/04/yuri-manga-sweet-blue-flowers-volume-1-english).
- ---------. Review of *Sweet Blue Flowers*, vol. 2, by Takako Shimura.
  *Okazu* (blog). January 8, 2018.
  [https://&#x200B;okazu&#x200B;.yuricon&#x200B;.com&#x200B;/2018&#x200B;/01&#x200B;/08&#x200B;/yuri&#x200B;-manga&#x200B;-sweet&#x200B;-blue&#x200B;-flowers&#x200B;-volume&#x200B;-2&#x200B;-english](https://okazu.yuricon.com/2018/01/08/yuri-manga-sweet-blue-flowers-volume-2-english).
- ---------. Review of *Sweet Blue Flowers*, vol. 3, by Takako
  Shimura. *Okazu* (blog). April 11, 2018.
  [https://&#x200B;okazu&#x200B;.yuricon&#x200B;.com&#x200B;/2018&#x200B;/04&#x200B;/11&#x200B;/yuri&#x200B;-manga&#x200B;-sweet&#x200B;-blue&#x200B;-flowers&#x200B;-volume&#x200B;-3&#x200B;-english](https://okazu.yuricon.com/2018/04/11/yuri-manga-sweet-blue-flowers-volume-3-english).
- ---------. Review of *Sweet Blue Flowers*, vol. 4, by Takako
  Shimura. *Okazu* (blog). July 9, 2018.
  [https://&#x200B;okazu&#x200B;.yuricon&#x200B;.com&#x200B;/2018&#x200B;/07&#x200B;/09&#x200B;/yuri&#x200B;-manga&#x200B;-sweet&#x200B;-blue&#x200B;-flower&#x200B;-volume&#x200B;-4&#x200B;-english](https://okazu.yuricon.com/2018/07/09/yuri-manga-sweet-blue-flower-volume-4-english).
- ---------. Review of *Yagate kimi ni naru*, vol. 3, by Nio
  Nakatani. *Okazu* (blog). January 26, 2017.
  [https://&#x200B;okazu&#x200B;.yuricon&#x200B;.com&#x200B;/2017&#x200B;/01&#x200B;/26&#x200B;/yuri&#x200B;-manga&#x200B;-yagate&#x200B;-kimi&#x200B;-ni&#x200B;-naru&#x200B;-volume&#x200B;-3&#x200B;-&#x200B;%e3&#x200B;%82&#x200B;%84&#x200B;%e3&#x200B;%81&#x200B;%8c&#x200B;%e3&#x200B;%81&#x200B;%a6&#x200B;%e5&#x200B;%90&#x200B;%9b&#x200B;%e3&#x200B;%81&#x200B;%ab&#x200B;%e3&#x200B;%81&#x200B;%aa&#x200B;%e3&#x200B;%82&#x200B;%8b](https://okazu.yuricon.com/2017/01/26/yuri-manga-yagate-kimi-ni-naru-volume-3-%e3%82%84%e3%81%8c%e3%81%a6%e5%90%9b%e3%81%ab%e3%81%aa%e3%82%8b).
- ---------. Review of *Yaneura no nishojo*, by Nobuko Yoshiya.
  *Okazu* (blog). May 10, 2010.
  [https://&#x200B;okazu&#x200B;.yuricon&#x200B;.com&#x200B;/2010&#x200B;/05&#x200B;/09&#x200B;/yuri&#x200B;-novel&#x200B;-yaneura&#x200B;-no&#x200B;-nishojo](https://okazu.yuricon.com/2010/05/09/yuri-novel-yaneura-no-nishojo).
- ---------. “Why We Call It ‘Yuri.’” Anime Feminist.
  August 9, 2017.
  [https://&#x200B;www&#x200B;.animefeminist&#x200B;.com&#x200B;/history&#x200B;-why&#x200B;-call&#x200B;-yuri](https://www.animefeminist.com/history-why-call-yuri).
- ---------. “Yuri, 1919--2019, from Then to Now.” Anime Herald.
  February 6, 2019.
  [https://&#x200B;www&#x200B;.animeherald&#x200B;.com&#x200B;/2019&#x200B;/02&#x200B;/06&#x200B;/yuri&#x200B;-1919&#x200B;-2019&#x200B;-from&#x200B;-then&#x200B;-to&#x200B;-now](https://www.animeherald.com/2019/02/06/yuri-1919-2019-from-then-to-now).
- Fruchterman, Thomas M. J., and Edward M. Reingold. “Graph Drawing by
  Force-Directed Placement.” *Software: Practice and Experience* 21,
  no. 11 (November 1991), 1129--64.
  [https://&#x200B;doi&#x200B;.org&#x200B;/10&#x200B;.1002&#x200B;/spe&#x200B;.4380211102](https://doi.org/10.1002/spe.4380211102).
- Fujimoto, Yukari. “Where Is My Place in the World? Early Shōjo Manga
  Portrayals of Lesbianism.” Translated by Lucy Frazier. *Mechademia*
  9 (2014), 25--42.
  [https://&#x200B;doi&#x200B;.org&#x200B;/10&#x200B;.5749&#x200B;/mech&#x200B;.9&#x200B;.2014&#x200B;.0025](https://doi.org/10.5749/mech.9.2014.0025).
  原典：藤本由香里『私の居場所はどこにあるの? 少女マンガが映す心のかたち』、東京：朝日新聞出版、2008。
- Gaffney, Sean. Review of *Sweet Blue Flowers*, vol. 1, by Takako
  Shimura. *A Case Suitable for Treatment* (blog). September 30, 2017.
  [http://&#x200B;suitablefortreatment&#x200B;.mangabookshelf&#x200B;.com&#x200B;/2017&#x200B;/09&#x200B;/30&#x200B;/sweet&#x200B;-blue&#x200B;-flowers&#x200B;-omnibus&#x200B;-1](http://suitablefortreatment.mangabookshelf.com/2017/09/30/sweet-blue-flowers-omnibus-1).
- ---------. Review of *Sweet Blue Flowers*, vol. 2, by Takako
  Shimura. *A Case Suitable for Treatment* (blog). December 22, 2017.
  [http://&#x200B;suitablefortreatment&#x200B;.mangabookshelf&#x200B;.com&#x200B;/2017&#x200B;/12&#x200B;/22&#x200B;/sweet&#x200B;-blue&#x200B;-flowers&#x200B;-omnibus&#x200B;-2](http://suitablefortreatment.mangabookshelf.com/2017/12/22/sweet-blue-flowers-omnibus-2).
- ---------. Review of *Sweet Blue Flowers*, vol. 3, by Takako
  Shimura. *A Case Suitable for Treatment* (blog). March 20, 2018.
  [http://&#x200B;suitablefortreatment&#x200B;.mangabookshelf&#x200B;.com&#x200B;/2018&#x200B;/03&#x200B;/20&#x200B;/sweet&#x200B;-blue&#x200B;-flowers&#x200B;-omnibus&#x200B;-3](http://suitablefortreatment.mangabookshelf.com/2018/03/20/sweet-blue-flowers-omnibus-3).
- ---------. Review of *Sweet Blue Flowers*, vol. 4, by Takako
  Shimura. *A Case Suitable for Treatment* (blog). July 3, 2018.
  [http://&#x200B;suitablefortreatment&#x200B;.mangabookshelf&#x200B;.com&#x200B;/2018&#x200B;/07&#x200B;/03&#x200B;/sweet&#x200B;-blue&#x200B;-flowers&#x200B;-omnibus&#x200B;-4](http://suitablefortreatment.mangabookshelf.com/2018/07/03/sweet-blue-flowers-omnibus-4).
- Ghaznavi, Cyrus, Haruka Sakamoto, Shuhei Nomura, Anna Kubota,
  Daisuke Yoneoka, Kenji Shibuya, and Peter Ueda. “The Herbivore’s
  Dilemma: Trends in and Factors Associated with Heterosexual
  Relationship Status and Interest in Romantic Relationships among
  Young Adults in Japan---Analysis of National Surveys, 1987–2015.”
  *PLoS ONE* 15(11): e0241571.
  [https://&#x200B;journals&#x200B;.plos&#x200B;.org&#x200B;/plosone&#x200B;/article&#x200B;?id&#x200B;=10&#x200B;.1371&#x200B;/journal&#x200B;.pone&#x200B;.0241571](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0241571).
- Goldstein-Gidoni, Ofra. *Packaged Japaneseness: Weddings, Business,
  and Brides*. Honolulu: University of Hawai‘i Press, 1997.
- Gosho, Heinosuke, dir. *The Dancing Girl of Izu*. Shochiku, 1933. 1
  hr., 32 min.
  原典：五所平之助（監督）映画『伊豆の踊子』、東京：松竹、1933。
  [https://&#x200B;www&#x200B;.youtube&#x200B;.com&#x200B;/watch&#x200B;?v&#x200B;=yd36RJ0nzdM](https://www.youtube.com/watch?v=yd36RJ0nzdM)。
- Hagio, Moto. *The Heart of Thomas*. Translated by Rachel
  Thorn. Seattle: Fantagraphics Books, 2012.
  原典：萩尾望都『ト&#x2060;ーマの心臓』、東京：小学館、1975。
- Harano, Mami. “Anatomy of Mishima’s Most Successful Play
  *Rokumeikan*.” Master’s thesis, Portland State University, 2010.
  [https://&#x200B;pdxscholar&#x200B;.library&#x200B;.pdx&#x200B;.edu&#x200B;/cgi&#x200B;/viewcontent&#x200B;.cgi&#x200B;?article&#x200B;=1386&#x200B;&context&#x200B;=open&#x200B;_access&#x200B;_etds](https://pdxscholar.library.pdx.edu/cgi/viewcontent.cgi?article=1386&context=open_access_etds).
- Hazuki, Ruri. *Saturday: Introduction*. Gardena, CA: Lilyka, 2019.
  原典：綺月るり『Saturday』、2019。
  [https://&#x200B;twitter&#x200B;.com&#x200B;/i&#x200B;/events&#x200B;/108522&#x200B;654285&#x200B;7220096](https://twitter.com/i/events/1085226542857220096)。
- Hecker, Frank. “Relative Prominence of Characters and Their
  Relationships in Takako Shimura’s *Sweet Blue Flowers*.”
  RPubs.com. March 7, 2022.
  [https://&#x200B;rpubs&#x200B;.com&#x200B;/frankhecker&#x200B;/874648](https://rpubs.com/frankhecker/874648).
- Helen. Review of *Sweet Blue Flowers*, vol. 1, by Takako
  Shimura. TheOASG. December 7, 2017.
  [https://&#x200B;www&#x200B;.theoasg&#x200B;.com&#x200B;/reviews&#x200B;/manga&#x200B;/sweet&#x200B;-blue&#x200B;-flowers&#x200B;-volume&#x200B;-1&#x200B;-review](https://www.theoasg.com/reviews/manga/sweet-blue-flowers-volume-1-review).
- Hiener, Teresa A. “Shinto Wedding, Samurai Bride: Inventing
  Tradition and Fashioning Identity in the Rituals of Bridal Dress in
  Japan.” PhD diss., University of Pittsburgh, 1997.
- Hiramori, Daiki, and Saori Kamano. “Asking about Sexual Orientation
  and Gender Identity in Social Surveys in Japan: Findings from the
  Osaka City Residents’ Survey and Related Preparatory Studies.”
  &nbsp;*Journal of Population Problems* 76, no. 4 (December 2020), 443--66.
  [https://&#x200B;www&#x200B;.ipss&#x200B;.go&#x200B;.jp&#x200B;/syoushika&#x200B;/bunken&#x200B;/data&#x200B;/pdf&#x200B;/20760402&#x200B;.pdf](https://www.ipss.go.jp/syoushika/bunken/data/pdf/20760402.pdf).
  原典：平森大規&#x2060;・釜野さおり『性的指向と性自認のあり方を日本の量的調査でいかにとらえるか―大阪市民調査に向けた準備調査における項目の検討と本調査の結果）―』、東京：人口問題研究所 2021。[https://&#x200B;www&#x200B;.ipss&#x200B;.go&#x200B;.jp&#x200B;/syoushika&#x200B;/bunken&#x200B;/DATA&#x200B;/pdf&#x200B;/21770104&#x200B;.pdf](https://www.ipss.go.jp/syoushika/bunken/DATA/pdf/21770104.pdf)。
- Hodgin, Chuck. Review of *Sweet Blue Flowers*, vol. 1, by Takako
  Shimura. In “25 LGBTQAI+ Titles for Pride Month---and Onward,” by
  Kent Turner. *School Library Journal*. June 12, 2018.
  [https://&#x200B;www&#x200B;.slj&#x200B;.com&#x200B;/&#x200B;?detailStory&#x200B;=25&#x200B;-lgbtqai&#x200B;-titles&#x200B;-celebrate&#x200B;-pride](https://www.slj.com/?detailStory=25-lgbtqai-titles-celebrate-pride).
- Hong, Terry. Review of *Sweet Blue Flowers*, vol. 1, by Takako
  Shimura. *BookDragon* (blog). December 22, 2017.
  [https://&#x200B;smithsonianapa&#x200B;.org&#x200B;/bookdragon&#x200B;/sweet&#x200B;-blue&#x200B;-flowers&#x200B;-vol&#x200B;-1&#x200B;-takako&#x200B;-shimura&#x200B;-translated&#x200B;-adapted&#x200B;-john&#x200B;-werry](https://smithsonianapa.org/bookdragon/sweet-blue-flowers-vol-1-takako-shimura-translated-adapted-john-werry).
- Horii, Mitsutoshi, and Adam Burgess. “Constructing Sexual Risk:
  ‘Chikan,’ Collapsing Male Authority and the Emergence of Women-Only
  Train Carriages in Japan.” *Health, Risk &amp; Society* 14, no. 1
  (2012), 41--55.
  [https://&#x200B;doi&#x200B;.org&#x200B;/10&#x200B;.1080&#x200B;/13698575&#x200B;.2011&#x200B;.641523](https://doi.org/10.1080/13698575.2011.641523).
- Ikeda, Riyoko. *Rose of Versailles*. Translated by Mori Morimoto. 5
  vols. Richmond Hill, ON: Udon Entertainment, 2019--21.
  原典：池田理代子『ベルサイユのばら』、東京：集英社、1972--2018。
- Ikuhara, Kunihiko, dir. *Revolutionary Girl Utena*. 1997; Grimes, IA:
  Nozomi Entertainment, 2017. Blu-ray Disc, 1080p HD.
  原典：幾原邦彦（監督）『少女革命ウテナ』、東京：テレビ東京、1997。
- Iruma, Hitoma. *Bloom Into You: Regarding Saeki Sayaka*. Translated
  by Jan Cash and Vincent Castenada. 3 vols. Los Angeles: Seven Seas
  Entertainment, 2019--20.
  原典：入間人間『やがて君になる 佐伯沙弥香について』、東京：KADOKAWA〈電撃文庫〉、2018--20。
- Jaime. Review of *Even Though We’re Adults*, vol. 4, by Takako
  Shimura. *Yuri Stargirl* (blog). April 23, 2022.
  [https://&#x200B;www&#x200B;.yuristargirl&#x200B;.com/2022/04/even-though-were-adults-volume-4-manga.html](https://www.yuristargirl.com/2022/04/even-though-were-adults-volume-4-manga.html).
- ---------. Review of *Sweet Blue Flowers*, vol. 4, by Takako
  Shimura. *Yuri Stargirl* (blog). June 22, 2018.
  [https://&#x200B;www&#x200B;.yuristargirl&#x200B;.com&#x200B;/2018&#x200B;/06&#x200B;/sweet&#x200B;-blue&#x200B;-flowers&#x200B;-aoi&#x200B;-hana&#x200B;-vol&#x200B;-4&#x200B;.html](https://www.yuristargirl.com/2018/06/sweet-blue-flowers-aoi-hana-vol-4.html).
- Jansen, Marius B. *The Making of Modern Japan*. Cambridge, MA:
  Belknap Press, 2002.
- Kaiser, Vrai, Jacob Chapman, Cayla Coats, and Rachel Thorn. “Chatty
  AF 21: *Wandering Son* Retrospective (with Transcript).” Anime
  Feminist. September 3, 2017.
  [https://&#x200B;www&#x200B;.animefeminist&#x200B;.com&#x200B;/podcast-chatty-af-21-wandering-son-retrospective/](https://www.animefeminist.com/podcast-chatty-af-21-wandering-son-retrospective/).
- Kakefuda, Hiroko. *On Being a “Lesbian.”* Translated by Indiana Scarlet Brown. In “A Translation and
  Analysis of Japan’s Seminal Lesbian Studies Work,” 45--202. Masters
  thesis, University at Albany, State University of New York, 2018.
  [https://&#x200B;scholarsarchive&#x200B;.library&#x200B;.albany&#x200B;.edu&#x200B;/honors&#x200B;college&#x200B;_eas&#x200B;/3](https://scholarsarchive.library.albany.edu/honorscollege_eas/3).
  原典：掛札悠子『「レズビアン」である、ということ』、東京：河出書房新社、1992。
- Kálovics, Dalma. “The Missing Link of Shōjo Manga History: The
  Changes in 60s Shōjo Manga as Seen Through the Magazine *Shūkan
  Margaret*.” *Journal of Kyoto Seika University* 49 (2016), 3--22.
  [https://&#x200B;www&#x200B;.academia&#x200B;.edu&#x200B;/36310321&#x200B;/The&#x200B;_missing&#x200B;_link&#x200B;_of&#x200B;_sh&#x200B;%C5&#x200B;%8Djo&#x200B;_manga&#x200B;_history&#x200B;_the&#x200B;_changes&#x200B;_in&#x200B;_60s&#x200B;_sh&#x200B;%C5&#x200B;%8Djo&#x200B;_manga&#x200B;_as&#x200B;_seen&#x200B;_through&#x200B;_the&#x200B;_magazine&#x200B;_Sh&#x200B;%C5&#x200B;%ABkan&#x200B;_Margaret](https://www.academia.edu/36310321/The_missing_link_of_sh%C5%8Djo_manga_history_the_changes_in_60s_sh%C5%8Djo_manga_as_seen_through_the_magazine_Sh%C5%ABkan_Margaret).
- Kamano, Saori. “Entering the Lesbian World in Japan: Debut Stories.”
  *Journal of Lesbian Studies* 9, no. 1/2 (2005), 11--30.
  [https://&#x200B;doi&#x200B;.org&#x200B;/10&#x200B;.1300&#x200B;/J155v09n01&#x200B;_02](https://doi.org/10.1300/J155v09n01_02).
- Kamatani, Yuhki. *Our Dreams at Dusk: Shimanami Tasogare*.
  Translated by Jocelyne Allen. 4 vols. Los Angeles: Seven
  Seas Entertainment, 2019.
  原典：鎌谷悠希『しまなみ誰そ彼』、東京：小学館、2015--18。
- Karlin, Jason G. *Gender and Nation in Meiji Japan: Modernity, Loss,
  and the Doing of History*. Honolulu: University of Hawai‘i
  Press, 2014.
  [https://&#x200B;www&#x200B;.academia&#x200B;.edu&#x200B;/42197271&#x200B;/Gender&#x200B;_and&#x200B;_Nation&#x200B;_in&#x200B;_Meiji&#x200B;_Japan&#x200B;_Modernity&#x200B;_Loss&#x200B;_and&#x200B;_the&#x200B;_Doing&#x200B;_of&#x200B;_History](https://www.academia.edu/42197271/Gender_and_Nation_in_Meiji_Japan_Modernity_Loss_and_the_Doing_of_History).
- Kasai, Kenichi, dir. *Sweet Blue Flowers*. 2009; Grimes, IA: Lucky
  Penny Entertainment, 2013. DVD.
  原典：カサヰケンイチ（監督）アニメ『青い花』、東京：メデ&#x2060;ィアフ&#x2060;ァクトリ&#x2060;ー、2013。
- Katai, Tayama. “The Girl Watcher.” In *The Quilt and Other Stories
  by Tayama Katai*. Translated by Kenneth G. Henshall. Tokyo:
  University of Tokyo Press, 1981.
  原典：田山花袋『少女病』、東京：易風社、1908。[https://&#x200B;www&#x200B;.aozora&#x200B;.gr&#x200B;.jp&#x200B;/cards&#x200B;/000214&#x200B;/files&#x200B;/1098&#x200B;_42470&#x200B;.html](https://www.aozora.gr.jp/cards/000214/files/1098_42470.html)。
- Kawabata, Yasunari. “The Dancing Girl of Izu.” In *The Dancing Girl
  of Izu, and Other Stories*, 3--33. Translated by J. Martin Holman.
  Washington, DC: Counterpoint, 1998.
- ---------. “The Izu Dancer.” Translated by Edward Seidensticker. In
  *The Izu Dancer, and Other Stories*. Yasunari Kawabata and Yasushi
  Inoue. Translated by Edward Seidensticker and Leon Picon. Tokyo:
  Tuttle, 2011. Kindle.
  原典：川端康成『伊豆の踊子』、東京：新潮文庫、1926。
- Kawahara, Kazune. *My Love Story!!*. Vol. 1. Translated by Ysabet
  Reinhardt MacFarlane and JN Productions. San Francisco: Viz
  Media, 2014.
  原典：河原和音&#x2060;・アルコ『俺物語！！』集英社、2012--16。
- Kawai, Michi. *My Lantern*. 3rd ed. Tokyo: privately
  published, 1949.
  原典：河合道『わたしのランタ&#x2060;ーン』、東京：恵泉女学園&#x2060;・新教出版社、1968。
- Kimino, Sakurako. *Strawberry Panic*. Translated by Michelle
  Kobayashi and Anastasia Moreno. 3 vols. Los Angeles: Seven Seas
  Entertainment, 2008.
  原典：公野櫻子『ストロベリ&#x2060;ー&#x2060;・パニ&#x2060;ック！』、埼玉：メデ&#x2060;ィアワ&#x2060;ークス、2006。
- Kodama, Naoko. *I Married My Best Friend to Shut My Parents
  Up*. Translated by Amber Tamosaitis. Los Angeles: Seven Seas
  Entertainment, 2019.
  原典：コダマナオコ『親がうるさいので後輩(♀)と偽装結婚してみた。』、東京：一迅社、2018。
- Komori, Yuri. “Trends in Japanese First Names in the Twentieth
  Century: A Comparative Study.” *International Christian University
  Publications 3-A*, *Asian Cultural Studies* 28 (2002), 67--82.
  [https://&#x200B;icu&#x200B;.repo&#x200B;.nii&#x200B;.ac&#x200B;.jp&#x200B;/&#x200B;?action&#x200B;=repository&#x200B;_action&#x200B;_common&#x200B;_download&#x200B;&item&#x200B;_id&#x200B;=1637&#x200B;&item&#x200B;_no&#x200B;=1&#x200B;&attribute&#x200B;_id&#x200B;=18&#x200B;&file&#x200B;_no&#x200B;=1](https://icu.repo.nii.ac.jp/?action=repository_action_common_download&item_id=1637&item_no=1&attribute_id=18&file_no=1).
- Konayama, Kata. *Love Me for Who I Am*. Vol. 3. Translated by Amber
  Tamosaitis. Los Angeles: Seven Seas Entertainment, 2021.
  原典：粉山カタ『不可解なぼくのすべてを』(3)、東京：ジ&#x2060;ーオ&#x2060;ーテ&#x2060;ィ&#x2060;ー、2020。
- Kuno, Akiko. *Unexpected Destinations: The Poignant Story of Japan’s
  First Vassar Graduate*. Translated by Kirsten McIvor. Tokyo:
  Kodansha International, 1993.
  原典：久野明子『鹿鳴館の貴婦人 大山捨松―日本初の女子留学生』、東京：中央公論社、1988。
- Kurosawa, Akira, dir. *The Most Beautiful*. 1944; in *Eclipse Series
  23: The First Films of Akira Kurosawa (Sanshiro Sugata / The Most
  Beautiful / Sanshiro Sugata, Part Two / The Men Who Tread on the
  Tiger’s Tail)*; New York: Criterion Collection, 2010. 1 hr., 25 min. DVD.
  原典：黒澤明（監督）『一番美しく』、東京：東宝、1944。
- kyuuketsukirui \[pseud.\]. “Don’t Want to Know What I’ll Be without
  You.” Archive of Our Own. April 29, 2011.
  [https://&#x200B;archiveofourown&#x200B;.org&#x200B;/works&#x200B;/202164](https://archiveofourown.org/works/202164).
- “List of Manga Series by Volume Count.” Wikipedia. Last modified
  January 31, 2022.
  [https://&#x200B;en&#x200B;.wikipedia&#x200B;.org&#x200B;/wiki&#x200B;/List&#x200B;_of&#x200B;_manga&#x200B;_series&#x200B;_by&#x200B;_volume&#x200B;_count](https://en.wikipedia.org/wiki/List_of_manga_series_by_volume_count).
- livresdechevet \[pseud.\]. Review of *Sweet Blue Flowers*, vol. 1,
  by Takako Shimura. *More Bedside Books* (blog). Accessed February 12, 2022.
  [https://&#x200B;morebedsidebooks&#x200B;.tumblr&#x200B;.com&#x200B;/post&#x200B;/166815771350&#x200B;/sweet&#x200B;-blue&#x200B;-flowers&#x200B;-1&#x200B;-english&#x200B;-viz](https://morebedsidebooks.tumblr.com/post/166815771350/sweet-blue-flowers-1-english-viz).
- McLelland, Mark. *Love, Sex, and Democracy in Japan during the
  American Occupation*. New York: Palgrave Macmillan, 2012. Kindle.
- ---------. *Queer Japan from the Pacific War to the Internet
  Age*. Lanham, MD: Rowman &amp; Littlefield, 2005. Kindle.
- ---------. “The role of the ‘tōjisha’ in current debates about
  sexual minority rights in Japan,” 2009, 4--7,
  [https://&#x200B;ro&#x200B;.uow&#x200B;.edu&#x200B;.au&#x200B;/artspapers&#x200B;/206](https://ro.uow.edu.au/artspapers/206).
- McClelland, Mark, Katsuhiko Suganuma, and James Welker, eds. *Queer
  Voices from Japan: First-Person Narratives from Japan’s Sexual
  Minorities*. Lanham, MD: Lexington Books, 2007.
- Mackie, Vera. “Birth Registration and the Right to Have Rights: The
  Changing Family and the Unchanging *Koseki*.” In Chapman and
  Krogness, *Japan’s Household Registration System and Citizenship*,
  203--17.
- Maree, Claire. “Sexual Citizenship at the Intersections of
  Patriarchy and Heteronormativity: Same-Sex Partnerships and the
  *Koseki*.” In Chapman and Krogness, *Japan’s Household Registration
  System and Citizenship*, 187--202.
- Mars-Jones, Adam. *Noriko Smiling*. London: Notting Hill
  Editions, 2011.
- Maser, Verena. “Beautiful and Innocent: Female Same-Sex Intimacy in
  the Japanese Yuri Genre.” PhD diss., Universität Trier, 2015.
  [https://&#x200B;ubt&#x200B;.opus&#x200B;.hbz&#x200B;-nrw&#x200B;.de&#x200B;/frontdoor&#x200B;/index&#x200B;/index&#x200B;/docId&#x200B;/695](https://ubt.opus.hbz-nrw.de/frontdoor/index/index/docId/695).
- Matsushita, Yukihiro, and Toshiyuki Kato, dir. *Maria Watches Over
  Us*. 2004--9; Houston: Sentai Filmworks, 2020. Blu-ray Disc, 1080p HD.
  原典：ユキヒロマツシタ&#x2060;・加藤敏幸（監督）アニメ『マリア様がみてる』、東京：集英社&#x2060;・山百合会&#x2060;・テレビ東京、2004--9。
- Miman. *Yuri Is My Job!*. Translated by Diana Taylor. 9
  vols. New York: Kodansha, 2016--.
  原典：未幡『私の百合はお仕事です！』、東京：一迅社、2016--。
- Ministry of Education, Culture, Sports, Science, and
  Technology. “Overview.” MEXT website. Accessed January 2, 2022.
  [https://&#x200B;www&#x200B;.mext&#x200B;.go&#x200B;.jp&#x200B;/en&#x200B;/policy&#x200B;/education&#x200B;/overview&#x200B;/index&#x200B;.htm](https://www.mext.go.jp/en/policy/education/overview/index.htm).
- Mishima, Yukio. “The Rokumeikan: A Tragedy in Four Acts.” In *My
  Friend Hitler, and Other Plays of Yukio Mishima*, 4--54. Translated
  by Hiroaki Sato. New York: Columbia University Press, 2002.
  原典：三島由紀夫『鹿鳴館』、東京：東京創元社、1957。
- Morinaga, Milk. *Girl Friends*. Vol. 5. Translated by Anastasia
  Moreno. Los Angeles: Seven Seas Entertainment, 2017.
  原典：森永みるく『GIRL FRIENDS』(5) 双葉社、2010。
- ---------. *Hana &amp; Hina After School*. Vol. 3. Translated by
  Jennifer McKeon. Los Angeles: Seven Seas Entertainment, 2017.
  原典：『ハナとヒナは放課後』(3) 、東京：双葉社、2017。
- Morishima, Akiko. *The Conditions of Paradise*. Translated by Elina
  Ishikawa-Curran. Los Angeles: Seven Seas Entertainment, 2020.
  原典：森島明子『楽園の条件』、東京：一迅社、2007。
- Morris, Mark. “Orphans.” Review of *The Dancing Girl of Izu, and
  Other Stories*, by Yasunari Kawabata. *New York Times*,
  October 12, 1997.
  [https://&#x200B;archive&#x200B;.nytimes&#x200B;.com&#x200B;/www&#x200B;.nytimes&#x200B;.com&#x200B;/books&#x200B;/97&#x200B;/10&#x200B;/12&#x200B;/reviews&#x200B;/971012&#x200B;.12morrist&#x200B;.html](https://archive.nytimes.com/www.nytimes.com/books/97/10/12/reviews/971012.12morrist.html).
- Nagata, Kabi. *My Lesbian Experience with Loneliness*. Translated by
  Jocelyne Allen. Los Angeles, Seven Seas Entertainment, 2017.
  原典：永田カビ『さびしすぎてレズ風俗に行きましたレポ』、東京：イ&#x2060;ースト&#x2060;・プレス、2016。
- Nakatani, Nio. *Bloom Into You*. Translated by Jenni McKeon. 8
  vols. Los Angeles: Seven Seas Entertainment, 2017--20.
  原典：仲谷鳰『やがて君になる』、東京：KADOKAWA〈電撃コミ&#x2060;ックスNEXT〉、2015--19。
- National Institute of Population and Social Security
  Research. “Marriage Process and Fertility of Japanese Married
  Couples / Attitudes toward Marriage and Family among Japanese
  Singles: Highlights of the Survey Results on Married Couples/
  Singles.” Tokyo: National Institute of Population and Social
  Security Research, 2017.
  [https://&#x200B;www&#x200B;.ipss&#x200B;.go&#x200B;.jp&#x200B;/ps&#x200B;-doukou&#x200B;/e&#x200B;/doukou15&#x200B;/Nfs15R&#x200B;_points&#x200B;_eng&#x200B;.pdf](https://www.ipss.go.jp/ps-doukou/e/doukou15/Nfs15R_points_eng.pdf).
  原典：国立社会保障&#x2060;・人口問題研究所『現代日本の結婚と出産─ 第15回出生動向基本調査 （独身者調査ならびに夫婦調査）報告書─』2017。[https://&#x200B;www&#x200B;.ipss&#x200B;.go&#x200B;.jp&#x200B;/ps-doukou&#x200B;/j&#x200B;/doukou15&#x200B;/NFS15&#x200B;_reportALL&#x200B;.pdf](https://www.ipss.go.jp/ps-doukou/j/doukou15/NFS15_reportALL.pdf)。
- Nimura, Janice P. *Daughters of the Samurai: A Journey from East to
  West and Back*. New York: W. W. Norton, 2015. Kindle.
- Ninomiya, Shūhei. “The *Koseki* and Legal Gender Change.” Translated
  by Karl Jakob Krogness. In Chapman and Krogness, *Japan’s Household
  Registration System and Citizenship*, 169--86.
  原典：二宮周平『戸籍と人権』、大阪：部落解放&#x2060;・人権研究所、1995。
- Novalis. *Henry of Ofterdingen: A Romance*. Translated by John
  Owen. Cambridge, MA: Cambridge Press, 1842; Project Gutenberg, 2013.
  [https://&#x200B;gutenberg&#x200B;.org&#x200B;/ebooks&#x200B;/31873](https://gutenberg.org/ebooks/31873).
  邦訳：ノヴ&#x2060;ァ&#x2060;ーリス（田中克己訳）『青い花』、東京：第一書房、1936。
- Ozu, Yasujirō, dir. *Equinox Flower*. 1958; in *Eclipse Series 3:
  Late Ozu (Early Spring / Tokyo Twilight / Equinox Flower / Late
  Autumn / The End of Summer)*; New York: Criterion
  Collection, 2007. 1 hr., 58 min. DVD.
  原典：小津安二郎（監督）『彼岸花』、東京：松竹、1958。
- ---------. dir. *Late Autumn*. 1958; in *Eclipse Series 3: Late Ozu
  (Early Spring / Tokyo Twilight / Equinox Flower / Late Autumn / The
  End of Summer)*; New York: Criterion Collection, 2007. 2 hr., 8 min. DVD.
  原典：『秋日和』、東京：松竹、1958。
- ---------. dir. *Late Spring*. 1949; New York: Criterion
  Collection, 2012. 1 hr., 48 min. Blu-ray Disc, 1080p HD.
  原典：『晩春』、東京：松竹、1949。
- Pflugfelder, Gregory M. “‘S’ Is for Sister: School Girl Intimacy and
  ‘Same-Sex Love’ in Early Twentieth-Century Japan.” In *Gendering
  Modern Japanese History*, edited by Barbara Monoly and Kathleen Uno,
  133--90. Cambridge, MA: Harvard University Asia Center, 2005.
  [https://&#x200B;doi&#x200B;.org&#x200B;/10&#x200B;.1163&#x200B;/9781684174171&#x200B;_006](https://doi.org/10.1163/9781684174171_006).
- Prang, Margaret. *A Heart at Leisure from Itself: Caroline Macdonald
  of Japan*. Vancouver: UBC Press, 1995.
- “Private School Costs Triple Public Education Level through High
  School.” Nippon.com. October 4, 2018.
  [https://&#x200B;www&#x200B;.nippon&#x200B;.com&#x200B;/en&#x200B;/features&#x200B;/h00299](https://www.nippon.com/en/features/h00299).
- Prough, Jennifer S. *Straight from the Heart: Gender, Intimacy, and
  the Cultural Production of Shōjo Manga*. Honolulu: University of
  Hawai‘i Press, 2011.
- Pseudoerasmus \[ハンドルネ&#x2060;ーム\]. “Labour Repression and the Indo-Japanese
  Divergence.” *Pseudoerasmus* (blog). October 2, 2017.
  [https://&#x200B;pseudoerasmus&#x200B;.com&#x200B;/2017&#x200B;/10&#x200B;/02&#x200B;/ijd](https://pseudoerasmus.com/2017/10/02/ijd).
- Quinn, Josephine Crawley, and Christopher Brooke. “‘Affection in
  Education’: Edward Carpenter, John Addington Symonds, and the
  Politics of Greek Love.” In *Ideas of Education: Philosophy and
  Politics from Plato to Dewey*, edited by Christopher Brooke and
  Elizabeth Frazer, 252--66. London: Routledge, 2013.
- Rich, Adrienne. “Twenty-One Love Poems.” In *The Dream of a Common
  Language: Poems 1974--1977*. New York: W. W. Norton, 1978.
  [https://&#x200B;archive&#x200B;.org&#x200B;/details&#x200B;/dreamofcommonlan0000rich](https://archive.org/details/dream&#x200B;of&#x200B;common&#x200B;lan&#x200B;0000&#x200B;rich).
- Robertson, Jennifer. “The Politics of Androgyny in Japan: Sexuality
  and Subversion in the Theater and Beyond.” *American Ethnologist*
  19, no. 3 (August 1992), 419--42.
  [https://&#x200B;doi&#x200B;.org&#x200B;/10&#x200B;.1525&#x200B;/ae&#x200B;.1992&#x200B;.19&#x200B;.3&#x200B;.02a00010](https://doi.org/10.1525/ae.1992.19.3.02a00010).
- ---------. “Yoshiya Nobuko: Out and Outspoken in Practice and
  Prose.” In *Same‐Sex Cultures and Sexualities: An Anthropological
  Reader*, edited by Jennifer Robertson, 196--211. Malden, MA:
  Blackwell Publishing, 2005.
  [https://&#x200B;doi&#x200B;.org&#x200B;/10&#x200B;.1002&#x200B;/9780470775981&#x200B;.ch11](https://doi.org/10.1002/9780470775981.ch11).
- Satō, Takuya, dir. *Happy-Go-Lucky Days*. 2020; Houston: Sentai
  Filmworks, 2021. 55 min. Blu-ray Disc, 1080p HD.
  原典：佐藤卓哉（監督）劇場アニメ『どうにかなる日々』、東京：ポニ&#x2060;ーキ&#x2060;ャニオン、2020。
- Shamoon, Deborah. “Class S: Appropriation of ‘Lesbian’ Subculture in
  Modern Japanese Literature and New Wave Cinema.” *Cultural Studies*
  35, no. 1, 27--43.
  [https://&#x200B;doi&#x200B;.org&#x200B;/10&#x200B;.1080&#x200B;/09502386&#x200B;.2020&#x200B;.1844259](https://doi.org/10.1080/09502386.2020.1844259).
- ---------. *Passionate Friendship: The Aesthetics of Girls’ Culture
  in Japan*. Honolulu: University of Hawai‘i Press, 2012.
- Shimura, Takako. *Aoi hana*. 8 vols. Tokyo: Ohta Books, 2006--13.
  原典：志村貴子『青い花』全八巻、東京：太田出版、2004--13。
- ---------. *Awashima hyakkei*. 3 vols. Tokyo: Ōta Shuppan,
  2015--.
  原典：『淡島百景』、東京：太田出版、2015--。
- ---------. *Even Though We’re Adults*. Translated by Jocelyne
  Allen. 3 vols. Los Angeles: Seven Seas Entertainment, 2021--.
  原典：『おとなにな&#x2060;っても』、東京：講談社、2019--。
- ---------. *Fleurs bleues* \[フランス語版『青い花』\]. Translated by
  Satoko Inaba and Margot Maillac. 8 vols. Paris: Kazé, 2009--15.
- ---------. *Flores azules* \[スペイン語版『青い花』\]. Translated by
  Ayako Koike. 8 vols. Colombres, Spain: Milky Way Ediciones,
  2015--16.
- ---------. *Happy-Go-Lucky Days*. Translated by RReese. 2
  vols. Gardena, CA: Digital Manga Guild, 2013. Kindle.
  原典：『どうにかなる日々』、東京：太田出版、2002--04。
- ---------. *Sweet Blue Flowers* \[英語版『青い花』\]. Translated by
  John Werry. 4 vols. San Francisco: VIZ Media, 2017--18.
- ---------. *Sweet Blue Flowers* \[英語版『青い花』別版\]. Translated
  by Jeffrey Steven LeCroy. Vol. 1. Gardena, CA: Digital Manga, 2014.
  Kindle.
- ---------. *Wandering Son*. Translated by Rachel Thorn. 8
  vols. Seattle: Fantagraphics Books, 2011--.
  原典：『放浪息子』、東京：エンタ&#x2060;ーブレイン、2002--13。
- Sugimoto, Yoshio. *An Introduction to Japanese Society*. 5th
  ed. Cambridge: Cambridge University Press, 2020.
- Susan. Review of *Sweet Blue Flowers*, vol. 1, by Takako Shimura.
  Lesbrary. January 9, 2019.
  [https://&#x200B;lesbrary&#x200B;.com&#x200B;/susan-reviews-sweet-blue-flowers-by-takako-shimura/](https://lesbrary.com/susan-reviews-sweet-blue-flowers-by-takako-shimura/).
- ---------. Review of *Sweet Blue Flowers*, vol. 2, by Takako
  Shimura. Lesbrary. February 13, 2019.
  [https://&#x200B;lesbrary&#x200B;.com&#x200B;/susan-reviews-sweet-blue-flowers-volume-2-by-takako-shimura/](https://lesbrary.com/susan-reviews-sweet-blue-flowers-volume-2-by-takako-shimura/).
- Suzuki, Michiko. *Becoming Modern Women: Love and Female Identity in
  Prewar Japanese Literature and Culture*. Palo Alto: Stanford
  University Press, 2009.
- ---------. “The Translation of Edward Carpenter’s
  *Intermediate Sex* in Early Twentieth-Century Japan.” In *Sexology and
  Translation: Cultural and Scientific Encounters Across the Modern
  World*, edited by Heike Bauer, 197--215. Philadelphia: Temple
  University Press, 2015.
- Symonds, John Addington. *A Problem in Greek Ethics, being an
  Inquiry into the Phenomenon of Sexual Inversion, addressed
  especially to medical psychologists and jurists*. London:
  privately published, 1901.
  [https://&#x200B;archive&#x200B;.org&#x200B;/details&#x200B;/cu31924021844950](https://archive.org/details/cu31924021844950).
- ---------. *Letters of John Addington Symonds*.
  Vol. 3, *1885--1893*. Edited by Herbert M. Schueller and Robert
  L. Peters. Detroit: Wayne State University Press, 1969.
  [https://&#x200B;archive&#x200B;.org&#x200B;/details&#x200B;/letters&#x200B;of&#x200B;john&#x200B;add&#x200B;0003&#x200B;symo](https://archive.org/details/lettersofjohnadd0003symo).
- Takeuchi, Naoko. *Pretty Guardian: Sailor Moon*. Translated by
  William Flanagan. 12 vols. New York: Kodansha, 2011--13.
  原典：武内直子『美少女戦士セ&#x2060;ーラ&#x2060;ーム&#x2060;ーン』、東京：講談社、1992--97。
- Takashima, Hiromi. *Kase-san and Morning Glories*. Translated by
  Jocelyne Allen. Los Angeles: Seven Seas Entertainment, 2017.
  原典：高嶋ひろみ『あさがおと加瀬さん。』、東京：新書館、2012。
- ---------. *Kase-san and Yamada*. Translated by Jocelyne Allen. 2
  vols. Los Angeles: Seven Seas Entertainment, 2020--.
  原典：『山田と加瀬さん。』、東京：新書館、2020--。
- Takashima, Rica. *Tokyo Love ~ Rica ‘tte Kan ji!?* Translated by
  Erin Subramanian and Erica Friedman. ALC Publishing, 2013. Kindle.
- Thorn, Rachel. “Snips and Snails, Sugar and Spice: A Guide to
  Japanese Honorifics as Used in *Wandering Son*.” In Shimura,
  *Wandering Son*, 1:ii--iv.
- Trainor, Joseph C. *Educational Reform in Occupied Japan: Trainor’s
  Memoir*. Tokyo: Meisei University Press, 1983.
- Tsubaki, Izumi. *Monthly Girls’ Nozaki-kun*. Translated by Leighann
  Harvey. 13 vols. New York: Yen Press, 2015--.
  原典：椿いづみ『月刊少女野崎くん』、東京：スクウ&#x2060;ェア&#x2060;・エニ&#x2060;ックス、2015--。
- Tsurumi, E. Patricia. *Factory Girls: Women in the Thread Mills of
  Meiji Japan*. Princeton, NJ: Princeton University Press, 1990.
- ---------. “Yet to Be Heard: The Voices of Meiji Factory Women.”
  *Bulletin of Concerned Asian Scholars* 26, no. 4, 18--27.
  [https://&#x200B;doi&#x200B;.org&#x200B;/10&#x200B;.1080&#x200B;/14672715&#x200B;.1994&#x200B;.10416166](https://doi.org/10.1080/14672715.1994.10416166).
- Van Hecken, Joseph L. *The Catholic Church in Japan Since
  1859*. Translated by John Van Hoydonck. Tokyo: Herder
  Agency, 1960.
  [https://&#x200B;archive&#x200B;.org&#x200B;/details&#x200B;/catholic&#x200B;church&#x200B;in&#x200B;0000&#x200B;heck](https://archive.org/details/catholicchurchin0000heck).
- Welker, James. “From Women’s Liberation to Lesbian Feminism in
  Japan: *Rezubian Feminizumu* within and beyond the *Ūman Ribu*
  Movement in the 1970s and 1980s.” In *Rethinking Japanese
  Feminisms*, edited by Julia C. Bullock, Ayako Kano, and James
  Welker, 50--67. Honolulu: University of Hawai‘i Press, 2018.
  [https://&#x200B;www&#x200B;.jstor&#x200B;.org&#x200B;/stable&#x200B;/j&#x200B;.ctv3zp07j](https://www.jstor.org/stable/j.ctv3zp07j).
- White, Linda E. “Challenging the Heteronormative Family in the
  *Koseki*: Surname, Legitimacy, and Unmarried Mothers.” In Chapman
  and Krogness, *Japan’s Household Registration System and
  Citizenship*, 239--56.
- Yamada, Kana. “Now with a Legal Father, Saitama Man, 36, Ready to
  Start Own Life.” *Asahi Shimbun*, February 21, 2018.
  原典：山田佳奈『戸籍に亡き父の名を…婚外子の男性、死後認知認める判決』、東京：朝日新聞デジタル、2018年2月21日。[https://&#x200B;www&#x200B;.asahi&#x200B;.com&#x200B;/articles&#x200B;/A&#x200B;S&#x200B;L&#x200B;2&#x200B;N&#x200B;3&#x200B;V&#x200B;1&#x200B;P&#x200B;L&#x200B;2&#x200B;N&#x200B;P&#x200B;T&#x200B;F&#x200B;C&#x200B;0&#x200B;0&#x200B;4&#x200B;.html](https://www.asahi.com/articles/ASL2N3V1PL2NPTFC004.html)。
- Yoshiya, Nobuko. *Hana monogatari*. 2 vols. Tokyo: Kawade Shobō
  Shinsha, 2009。
  原典：吉屋信子『花物語』、東京：洛陽堂、1920.
- ---------. *Yaneura no nishojo*. Tokyo: Kokusho Kankōkai, 2003.
  原典：『屋根裏の二処女』、東京：洛陽堂、1920。
- ---------. *Yellow Rose*. 2nd ed. Translated by Sarah Frederick. Los
  Angeles: Expanded Editions, 2016. Kindle.
  原題：『黄薔薇』（本タイトルのみ、英語翻訳があり。日本語版原典は『花物語』に収録。）
{:.bibliography}
