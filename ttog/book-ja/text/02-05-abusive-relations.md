---
title: "虐待関係"
---

## 虐待関係 {#abusive-relations}

注意：本節には、児童性的虐待に関する議論が含まれる。

あきらの兄の悪行を知&#x2060;ったところで、今度はふみの従姉である千津が読者に第二弾を与える番だ。まず、これまでの話で提示された事実は以下の通り：ふみは現在15歳（この点は以前の議論を参照）である。千津は結婚を控えているので、高校を卒業し、少なくとも18歳、大学に進学していればそれ以上の年齢であることが予想される。千津は、（外見から判断して）ふみが15歳より若い時期に、ふみと性的関係を持ちたが&#x2060;っていた（そしてどうやら実際にや&#x2060;ったようだ）（『青い花』(1) p. 65 / *SBF*, 1:65）。
{:.first}

ふみは千津に対して純粋な気持ちを抱き、彼女の誘いに応じたようである。しかし、ふみの気持ちはともかく、私の考えでは、これは明らかに虐待のケ&#x2060;ースである：千津は、自分の目的のためにふみを利用し、結婚するときにはふみを捨てたのだ。あきらの兄と同様にこのエピソ&#x2060;ードについても、「女子学生百合」物語の中で一体何が行われているのか？…と問いかけたくなるといえよう。

不幸なことに、この千津との小話は、ふみがレズビアンにな&#x2060;ったのは子供の頃に「リクル&#x2060;ートされた」からだということを暗示しているように見えるかもしれない。この考え方は、レズビアンを子供を食い物にする者として中傷し、ふみのカミングアウトへの道のりの感情的なインパクトを弱めてしまう。これでは、ふみを、本来の彼女自身のアイデンテ&#x2060;ィテ&#x2060;ィを表現する主体性を持&#x2060;った人間としてではなく、ある役割に「仕立て上げ」て、過去のトラウマを乗り越えられない被害者だという考えを投げかけるものとな&#x2060;ってしま&#x2060;っている。

この文化圏の外から来た者として、私は志村貴子の考えを推測することしかできないが、それが彼女の意図であ&#x2060;ったかは疑問である。以下は、参考までに、私の推測である：

『青い花』は、『マンガ&#x2060;・エロテ&#x2060;ィクス&#x2060;・エフ』に連載されていたということを改めて思い出してほしい。志村（あるいは編集者）は、このような小ネタを盛り込むことで、同誌のタ&#x2060;ーゲ&#x2060;ットである大人の読者によりアピ&#x2060;ールできると考えたのかもしれない。

また、『マンガ&#x2060;・エロテ&#x2060;ィクス&#x2060;・エフ』やそれ以外に掲載された他の志村作品にも、この種の一般的に疑問視される内容が含まれている。例えば、『放浪息子』の読者は、大人のキ&#x2060;ャラクタ&#x2060;ーがトランスジ&#x2060;ェンダ&#x2060;ーの若者の股間を掴んで性器を調査するというシ&#x2060;ーンを思い浮かべるだろう&#x2060;[^02-05-01]。

[^02-05-01]: Shimura, *Wandering Son*, 2:100--101.

千津の小ネタは、物語に何か寄与している点はあるだろうか？一つには、その関係と結末を、ふみが杉本恭己とリバウンド交際関係（※訳注：恋愛関係が破局した後、その関係を未練なく過去のものとするために積極的に新しい交際を模索する期間の学術用語）を持つき&#x2060;っかけとして利用する、という目的があ&#x2060;ったのではないかと推測する。ふみの以前の関係が年齢の近い同級生であ&#x2060;ったとすると、あきらがふみの初恋の相手であり、さらにそれが（恐らく）真実の愛であるという物語を、より複雑にしてしまうであろう。また、千津との関係は、ふみの方があきらより性経験が豊富であることも意味する―今後の二人の関係展開に影響を与える要素だ。

最後に、志村が意図したかどうかは別として（していたかもしれないのではないかと推測するが）、千津とふみの関係は、年上の少女や女性が、年下の少女と関係を持つという、エスや百合物語の暗い面を映す鏡として機能している（例えば、吉屋信子の『黄薔薇』では、22歳の教師と17歳の生徒を取り上げている）&#x2060;[^02-05-02]。

[^02-05-02]: Yoshiya, *Yellow Rose*, chap. 2.

こうい&#x2060;った伝統的なパタ&#x2060;ーンは、一方のパ&#x2060;ートナ&#x2060;ーが他方のパ&#x2060;ートナ&#x2060;ーより年上で、（少なくとも社会的側面では）優位に立つという、年齢に基づく社会的ヒエラルキ&#x2060;ーを反映している。ペアの先輩側が卒業したり結婚したりすると、女性同士の親密な関係から離れ、男性支配および男性中心の世界で生きていくことになる。それに伴い、後輩側は、また新たな少女の先輩となり、終わりのないサイクルを繰り返すのだ。（例えば、『マリア様がみてる』で紹介していたス&#x2060;ール制度の仕組みを参照）。

一方『青い花』では、これまでの証拠に基づくと、対等な関係を重視し、年齢やその他のヒエラルキ&#x2060;ーに応じた不平等な関係を暗黙の内に批判しているように見て取れる。ふみとあきらの関係は、明らかにふみの先輩である恭己との関係よりも物語上重要なものとな&#x2060;っており、京子の恭己への片思いや恭己の各務先生への片思いが、不平等な関係に対する否定的な見方を更に強固なものとしている。このような観点から、千津とふみの関係は、古典的な百合のパタ&#x2060;ーンに内在し、かつ切り離せない潜在的な弊害の一例を読者に提供するものだといえよう。
