---
title: "付録4：レビュー"
---

## 付録4：レビュー {#appendix-4-reviews}

『青い花』に関する私のコメントが終わ&#x2060;った所で、今回は、他の人がどのような感想を持&#x2060;ったのかを見ていこう。この付録では、VIZ Media版の第一巻から第四巻までのレビ&#x2060;ュ&#x2060;ーを、動画レビ&#x2060;ュ&#x2060;ー並びにネ&#x2060;ット書店や書評サイトに投稿された読者レビ&#x2060;ュ&#x2060;ーを省いた、網羅的とはいえない形で掲載している。
{:.first}

（※訳注：以下本節の『青い花』は、特記する場合を除き、全てVIZ Media版（英語版）『Sweet Blue Flowers』を指す。）

### 第一巻 {#appendix-4-reviews-volume-1}

Anime News Network（ANN）のロ&#x2060;ーズ&#x2060;・ブリ&#x2060;ッジズのレビ&#x2060;ュ&#x2060;ー。ANNは最も著名なアニメニ&#x2060;ュ&#x2060;ースとレビ&#x2060;ュ&#x2060;ーサイトである；そしてかなりの数の漫画レビ&#x2060;ュ&#x2060;ーも行&#x2060;っている。ブリ&#x2060;ッジズは『青い花』に総合評価B+、スト&#x2060;ーリ&#x2060;ーB、芸術性Aの評価を付けた。「全体として、この作品は、他とは一線を画す百合漫画に飛び込む上で最適な作品だ。『青い花』は確かにこのジ&#x2060;ャンルの特徴を沢山備えているが、より現実的なものを求める人にも十分な噛み応えがあるといえる。」&#x2060;[^07-04-01]

[^07-04-01]: Rose Bridges, review of *Sweet Blue Flowers*, vol. 1, by Takako Shimura, Anime News Network, October 20, 2017, [https://&#x200B;www&#x200B;.animenewsnetwork&#x200B;.com&#x200B;/review&#x200B;/sweet&#x200B;-blue&#x200B;-flowers&#x200B;-2&#x200B;-in&#x200B;-1&#x200B;-edition&#x200B;/gn&#x200B;-1&#x200B;/&#x200B;.122727](https://www.animenewsnetwork.com/review/sweet-blue-flowers-2-in-1-edition/gn-1/.122727).

*Experiments in Manga*のア&#x2060;ッシ&#x2060;ュ&#x2060;・ブラウンのレビ&#x2060;ュ&#x2060;ー。志村の芸術性とその舞台演劇への関連性、そして登場人物の行動と相互作用の現実性などを強調した、概ね好意的なレビ&#x2060;ュ&#x2060;ーである。「『青い花』は素晴らしい作品だ。この漫画は、他の女性を愛する若い女性の経験をリアルに描いており、感情の共鳴が引き起こされる。」&#x2060;[^07-04-02]

[^07-04-02]: Ash Brown, review of *Sweet Blue Flowers*, vol. 1, by Takako Shimura, *Experiments in Manga* (blog), October 27, 2017, [http://&#x200B;experimentsinmanga&#x200B;.mangabookshelf&#x200B;.com&#x200B;/2017&#x200B;/10&#x200B;/sweet&#x200B;-blue&#x200B;-flowers&#x200B;-omnibus&#x200B;-1](http://experimentsinmanga.mangabookshelf.com/2017/10/sweet-blue-flowers-omnibus-1).

Adventures in Poor Tasteのアレ&#x2060;ックス&#x2060;・クラインのレビ&#x2060;ュ&#x2060;ー。ポ&#x2060;ップ&#x2060;・カルチ&#x2060;ャ&#x2060;ーに焦点を当てたウ&#x2060;ェブサイトで、概ね好意的なレビ&#x2060;ュ&#x2060;ーがなされている。クラインは登場人物とその動かし方を気に入&#x2060;っており、芸術性も際立&#x2060;っていると語&#x2060;った。批判としては、いくつかの場面で、誰が話しているのか、それが全体の時系列のどこに位置するのかが明確ではなか&#x2060;ったことを挙げている。「全体として、『青い花』第一巻は、この連載のスタ&#x2060;ートとしては堅実なものである。登場人物は好感が持てるし、上手く紹介されているし、絵も全体を通して美しい。とはいえ、この巻における感情的な場面は、どれもあまり印象には残らない。次巻を期待させ、興味を持たせるには十分だが、このままでは偉大な作品にまでは達しない。お勧めはするが、熱烈にお勧めまではしない。」&#x2060;[^07-04-03]

[^07-04-03]: Alex Cline, review of *Sweet Blue Flowers*, vol. 1.

*Otaku USA*のアメリア&#x2060;・ク&#x2060;ックによるレビ&#x2060;ュ&#x2060;ー。*Otaku USA*は、アニメや漫画を扱う、紙媒体かつオンライン雑誌である；またク&#x2060;ックはアニメ&#x2060;・フ&#x2060;ェミニストというサイトの創設者でもある。彼女のレビ&#x2060;ュ&#x2060;ーは好意的だ。特に、四人の主人公（あきら、ふみ、恭己、京子）の描写がリアルで雰囲気に富んでいると評価している。彼女は『青い花』を「オススメ」と評価した。「『青い花』 は、重要な形成期を経験している、複雑な若い女性たちの日常生活を（描いている）。400ペ&#x2060;ージのボリ&#x2060;ュ&#x2060;ームで、登場人物たちがハ&#x2060;ッピ&#x2060;ーエンドを迎えられるよう応援しながら読み終えることができる。」&#x2060;[^07-04-04]

[^07-04-04]: Amelia Cook, review of *Sweet Blue Flowers*, vol. 1, by Takako Shimura, *Otaku USA*, December 9, 2017, [https://&#x200B;www&#x200B;.otakuusamagazine&#x200B;.com&#x200B;/sweet&#x200B;-blue&#x200B;-flowers&#x200B;-review](https://www.otakuusamagazine.com/sweet-blue-flowers-review).

The Comic Book Binの、リロイ&#x2060;・ド&#x2060;ゥ&#x2060;ールソ&#x2060;ーによるレビ&#x2060;ュ&#x2060;ー。一般コミ&#x2060;ック枠の中で高い評価（10点満点で8点）。「百合と少女ロマンスのフ&#x2060;ァンは、『青い花』の香りを嗅ぎたくなるだろう。」&#x2060;[^07-04-05]

[^07-04-05]: Leroy Douresseaux, review of *Sweet Blue Flowers*, vol. 1, by Takako Shimura, The Comic Book Bin, October 3, 2017, [http://&#x200B;www&#x200B;.comicbookbin&#x200B;.com&#x200B;/sweetblueflowers001&#x200B;.html](http://www.comicbookbin.com/sweetblueflowers001.html).

The Geekly GrindのEyeSpyeAlex（アレクサンドラ&#x2060;・ナ&#x2060;ッテ&#x2060;ィング）によるレビ&#x2060;ュ&#x2060;ー。アニメ、漫画、ゲ&#x2060;ームに特化したサイトで、好意的な評価である。「一日の終わりに、『青い花』を本当に楽しんでいる。登場人物はリアルで、彼女らの人生には深みと複雑さを感じる。ビジ&#x2060;ュアルはもう少し印象的であ&#x2060;ってもいいと思うが、この漫画の地に足の着いた趣に合&#x2060;っている。」&#x2060;[^07-04-06]

[^07-04-06]: EyeSpyeAlex \[Alexandra Nutting\], review of *Sweet Blue Flowers*, vol. 1, by Takako Shimura, The Geekly Grind, October 7, 2017, [http://&#x200B;www&#x200B;.thegeeklygrind&#x200B;.com&#x200B;/sweet&#x200B;-blue&#x200B;-flowers&#x200B;-part&#x200B;-one](http://www.thegeeklygrind.com/sweet-blue-flowers-part-one).

*Okazu*のエリカ&#x2060;・フリ&#x2060;ードマンによるレビ&#x2060;ュ&#x2060;ー。フリ&#x2060;ードマンは、百合漫画とアニメの最も著名な推奨人&#x2060;・批評家の一人で、彼女のサイトは百合関連のニ&#x2060;ュ&#x2060;ースを扱う英語サイトとしては際立&#x2060;って最も権威のあるものである。彼女は、『青い花』日本語版のレビ&#x2060;ュ&#x2060;ーを投稿したこともある。このレビ&#x2060;ュ&#x2060;ーでは、第一巻を10点満点中総合8点、絵と登場人物を8点、スト&#x2060;ーリ&#x2060;ーと「百合」を7点と評価した。「オ&#x2060;ープニングとエンデ&#x2060;ィングは―個人的な意見では―非常に弱いと思うが、それ以外の部分は素晴らしい。驚くほどの深みと広がりがある。ふみとあきらを取り巻くキ&#x2060;ャラクタ&#x2060;ーも、二人に負けず劣らずよく練られていて、面白い。」&#x2060;[^07-04-07]

[^07-04-07]: Erica Friedman, review of *Sweet Blue Flowers*, vol. 1.

*A Case Suitable for Treatment*のシ&#x2060;ョ&#x2060;ーン&#x2060;・ガフニ&#x2060;ーによるレビ&#x2060;ュ&#x2060;ー。漫画に特化したサイトからの好意的なレビ&#x2060;ュ&#x2060;ーである。ガフニ&#x2060;ーは、『青い花』の公式翻訳完全版が出るのがかなり遅れたため、『やがて君になる』や『あの娘にキスと白百合を』とい&#x2060;った最近の作品と比べると、あまり特徴がない点は認めざるを得ないとしながらも、「『青い花』は、百合が好きな人も、志村貴子が好きな人も、絶対に手に取&#x2060;って読む価値のある作品だ。全4巻なので、本棚を圧迫することもないだろう。」と書いている。&#x2060;[^07-04-08]

[^07-04-08]: Sean Gaffney, review of *Sweet Blue Flowers*, vol. 1, by Takako Shimura, *A Case Suitable for Treatment* (blog), September 30, 2017, [http://&#x200B;suitablefortreatment&#x200B;.mangabookshelf&#x200B;.com&#x200B;/2017&#x200B;/09&#x200B;/30&#x200B;/sweet&#x200B;-blue&#x200B;-flowers&#x200B;-omnibus&#x200B;-1](http://suitablefortreatment.mangabookshelf.com/2017/09/30/sweet-blue-flowers-omnibus-1).

TheOASGのヘレンによるレビ&#x2060;ュ&#x2060;ー。このアニメと漫画のグル&#x2060;ープブログでは、百合作品のお約束の使用、ク&#x2060;ィアベイテ&#x2060;ィング（ク&#x2060;ィアという立場を利用して注目を浴びるなどの搾取）の可能性、そして恭己とふみの関係における恭己の家族の反応が非現実的であるという懸念も表されているものの、概ね好意的なレビ&#x2060;ュ&#x2060;ーである。ヘレンは『青い花』を5点満点中3点で評価した。「『青い花』は…(中略)…読者の視線のために作られたキ&#x2060;ャラクタ&#x2060;ーではなく、高校といういつだ&#x2060;って複雑すぎる世界に対処している本物のテ&#x2060;ィ&#x2060;ーンの女の子たちを描いており、登場人物を人間として扱&#x2060;っている。しかし、彼女たちが卒業するまでに何度失恋し、そして修復されていくかは、まだ分からない。」&#x2060;[^07-04-09]

[^07-04-09]: Helen, review of *Sweet Blue Flowers*, vol. 1, by Takako Shimura, TheOASG, December 7, 2017, [https://&#x200B;www&#x200B;.theoasg&#x200B;.com&#x200B;/reviews&#x200B;/manga&#x200B;/sweet&#x200B;-blue&#x200B;-flowers&#x200B;-volume&#x200B;-1&#x200B;-review](https://www.theoasg.com/reviews/manga/sweet-blue-flowers-volume-1-review).

*School Library Journal*のチ&#x2060;ャ&#x2060;ック&#x2060;・ホジンによるレビ&#x2060;ュ&#x2060;ー。二箇所のオンライン書店における『青い花』特設ペ&#x2060;ージで紹介されていた、非常に好意的なレビ&#x2060;ュ&#x2060;ーである。「志村による漫画作品の合本であるこの第一巻は、ゲイやバイセクシ&#x2060;ャルの若い女性たちの喜びや苦しみ、そして愛について、正直でかつ痛烈な物語を紡ぎ出している。…(中略)…百合（レズビアン&#x2060;・ロマンスに焦点を当てた漫画）フ&#x2060;ァンには間違いないのみならず、恋愛ものが好きな読者や一般の漫画愛好家にもお勧めできるほど強力な作品である。」&#x2060;[^07-04-10]

[^07-04-10]: &nbsp;Chuck Hodgin, review of *Sweet Blue Flowers*, vol. 1, by Takako Shimura, in Kent Turner, “25 LGBTQAI+ Titles for Pride Month---and Onward,” *School Library Journal*, June 12, 2018, [https://&#x200B;www&#x200B;.slj&#x200B;.com&#x200B;/&#x200B;?detailStory&#x200B;=25&#x200B;-lgbtqai&#x200B;-titles&#x200B;-celebrate&#x200B;-pride](https://www.slj.com/?detailStory=25-lgbtqai-titles-celebrate-pride).

*More Bedside Books*のlivresdechevetによるレビ&#x2060;ュ&#x2060;ー。翻訳の問題や以前刊行されていた第一巻のデジタル版からの変更点などを中心に、概ね好意的なレビ&#x2060;ュ&#x2060;ーとな&#x2060;っている。「要するに、『青い花』は、少女が少女に恋をし、成長していく姿を描いた不朽の名作であり、ついに英語で出版されることとな&#x2060;った。…(中略)…このジ&#x2060;ャンルや歴史に詳しい人もそうでない人も、テ&#x2060;ィ&#x2060;ーンエイジ&#x2060;ャ&#x2060;ーだけでなく年配の読者にも届くようなキ&#x2060;ャラクタ&#x2060;ーが登場する物語である。」&#x2060;[^07-04-11]

[^07-04-11]: &nbsp;livresdechevet \[pseud.\], review of *Sweet Blue Flowers*, vol. 1, by Takako Shimura, *More Bedside Books* (blog), accessed February 12, 2022, [https://&#x200B;morebedsidebooks&#x200B;.tumblr&#x200B;.com&#x200B;/post&#x200B;/166815771350&#x200B;/sweet&#x200B;-blue&#x200B;-flowers&#x200B;-1&#x200B;-english&#x200B;-viz](https://morebedsidebooks.tumblr.com/post/166815771350/sweet-blue-flowers-1-english-viz).

*BookDragon*のテリ&#x2060;ー&#x2060;・ホンによるレビ&#x2060;ュ&#x2060;ー。スミソニアン&#x2060;・アジア太平洋アメリカセンタ&#x2060;ー主催のブログで、高評価を記している。「せ&#x2060;っかちな読者のために、2009年に全11話のアニメ化が果たされた。紙面の上の華麗な絵を好む漫画純粋主義者にと&#x2060;っては、志村は決して期待を裏切らない。」&#x2060;[^07-04-12]

[^07-04-12]: &nbsp;Terry Hong, review of *Sweet Blue Flowers*, vol. 1, by Takako Shimura, *BookDragon* (blog), December 22, 2017, [https://&#x200B;smithsonianapa&#x200B;.org&#x200B;/bookdragon&#x200B;/sweet&#x200B;-blue&#x200B;-flowers&#x200B;-vol&#x200B;-1&#x200B;-takako&#x200B;-shimura&#x200B;-translated&#x200B;-adapted&#x200B;-john&#x200B;-werry](https://smithsonianapa.org/bookdragon/sweet-blue-flowers-vol-1-takako-shimura-translated-adapted-john-werry).

Lesbraryのスーザンによるレビ&#x2060;ュー。レズビアン文学に特化したサイトで、好意的な評価である。登場人物は見た目も行動もどこにでも居るテ&#x2060;ィーンエイジ&#x2060;ャーで、ふみと恭己のカミングアウトの場面は現実的でありかつ漫画では珍しいものだと賞賛している。「『青い花』は本当におすすめだ。可愛くて感情豊かで、私が漫画に期待しているよりも、ほんの少しリアルに10代の恋愛ドラマを描いている！」&#x2060;[^07-04-13]

[^07-04-13]: &nbsp;Susan, review of *Sweet Blue Flowers*, vol. 1, by Takako Shimura, Lesbrary, January 9, 2019, [https://&#x200B;lesbrary&#x200B;.com&#x200B;/susan-reviews-sweet-blue-flowers-by-takako-shimura/](https://lesbrary.com/susan-reviews-sweet-blue-flowers-by-takako-shimura/).

### 第二巻 {#appendix-4-reviews-volume-2}

The Fandom Postのメリ&#x2060;ーナ&#x2060;・ダ&#x2060;ーギスによるレビ&#x2060;ュ&#x2060;ー。ややぬるめの評価から好意的なレビ&#x2060;ュ&#x2060;ーまでを含み、彼女は、内容B、芸術性B、パ&#x2060;ッケ&#x2060;ージA、テキスト&#x2060;・翻訳Aという評価を下している。「この漫画の第二巻を読んでいる間、少しスト&#x2060;ーリ&#x2060;ーに迷い続けてしま&#x2060;った。場面のジ&#x2060;ャンプや、行間を読ませることが多か&#x2060;ったように思うが、私がそれを苦手としているのかもしれない。本筋は通&#x2060;っているのだが、これだけ登場人物が多いと、みんなの心の中を追うのが大変だ&#x2060;った。その雑然とした感じが、評価を少し下げた理由である。…(中略)…それでも、も&#x2060;っと良くできたかもしれないあらゆる点を超えて、この物語は、漫画だけでなく、読書全般のメインストリ&#x2060;ームに絶対に必要な物語である。」&#x2060;[^07-04-14]

[^07-04-14]: &nbsp;Melina Dargis, review of *Sweet Blue Flowers*, vol. 2, by Takako Shimura, The Fandom Post, April 11, 2018, [https://&#x200B;www&#x200B;.fandompost&#x200B;.com&#x200B;/2018&#x200B;/04&#x200B;/11&#x200B;/sweet&#x200B;-blue&#x200B;-flowers&#x200B;-vol&#x200B;-02&#x200B;-manga&#x200B;-review](https://www.fandompost.com/2018/04/11/sweet-blue-flowers-vol-02-manga-review).

The Comic Book Binの、リロイ&#x2060;・ド&#x2060;ゥ&#x2060;ールソ&#x2060;ーによる、好意的なレビ&#x2060;ュ&#x2060;ー：グレ&#x2060;ードA、10点満点中8点。&#x2060;[^07-04-15]

[^07-04-15]: &nbsp;Leroy Douresseaux, review of *Sweet Blue Flowers*, vol. 2, by Takako Shimura, The Comic Book Bin, January 8, 2018, [http://&#x200B;www&#x200B;.comicbookbin&#x200B;.com&#x200B;/sweetblueflowers002&#x200B;.html](http://www.comicbookbin.com/sweetblueflowers002.html).

The Geekly GrindのEyeSpyeAlex（アレクサンドラ&#x2060;・ナ&#x2060;ッテ&#x2060;ィング）によるレビ&#x2060;ュ&#x2060;ー。非常に好意的な評価で、第二巻は総合10点満点中9点、スト&#x2060;ーリ&#x2060;ー8.6点、芸術性9.5点、登場人物9点としている。「時々スト&#x2060;ーリ&#x2060;ー構成が不明瞭に思えたにもかかわらず、『青い花』を本当に楽しんでいる。各巻が通常の漫画より長いのは、続きが気にな&#x2060;って終わることが少ないことを意味し、とても良いことだと思う。柔らかい画風とシンプルな台詞が、この漫画に落ち着きを与えている。『青い花』は決してペ&#x2060;ージをめくる手が止まらなくなるようなスリラ&#x2060;ー作品ではないのだが、それこそが評価できる。リラ&#x2060;ックスして読める漫画があるのは喜ばしいことだ。」&#x2060;[^07-04-16]

[^07-04-16]: &nbsp;EyeSpyeAlex \[Alexandra Nutting\], review of *Sweet Blue Flowers*, vol. 2, by Takako Shimura, The Geekly Grind, January 5, 2018, [http://&#x200B;www&#x200B;.thegeeklygrind&#x200B;.com&#x200B;/sweet&#x200B;-blue&#x200B;-flowers&#x200B;-part&#x200B;-two](http://www.thegeeklygrind.com/sweet-blue-flowers-part-two).

*Okazu*のエリカ&#x2060;・フリ&#x2060;ードマンによるレビ&#x2060;ュ&#x2060;ー。フリ&#x2060;ードマンは、第二巻を10点満点中8点（一巻と同じ）、絵と登場人物を8点、スト&#x2060;ーリ&#x2060;ーを7点、「レズビアン」を4点と評価した。（フリ&#x2060;ードマンは、第一巻のときと同様、作品に「百合」の点数をつけるのが常である。恐らく、『青い花』は典型的な百合作品ではないと判断し、この項目を「レズビアン」に切り替えたのだろう。）「これは素晴らしい英語翻訳版であり、このクオリテ&#x2060;ィを保&#x2060;ったまま、続きが刊行されていくことを期待している。…(中略)…20世紀初頭の文学史の深みを持ちながら、現在にも通じるこの百合の『新古典』を、まだ手に取&#x2060;っていない方にはぜひお勧めしたい限りである。」&#x2060;[^07-04-17]

[^07-04-17]: &nbsp;Erica Friedman, review of *Sweet Blue Flowers*, vol. 2, by Takako Shimura, *Okazu* (blog), January 8, 2018, [https://&#x200B;okazu&#x200B;.yuricon&#x200B;.com&#x200B;/2018&#x200B;/01&#x200B;/08&#x200B;/yuri&#x200B;-manga&#x200B;-sweet&#x200B;-blue&#x200B;-flowers&#x200B;-volume&#x200B;-2&#x200B;-english](https://okazu.yuricon.com/2018/01/08/yuri-manga-sweet-blue-flowers-volume-2-english).

*A Case Suitable for Treatment*のシ&#x2060;ョ&#x2060;ーン&#x2060;・ガフニ&#x2060;ーによるレビ&#x2060;ュ&#x2060;ー。第一巻ほどではないが、概ね好意的なレビ&#x2060;ュ&#x2060;ーである。「『青い花』は良いシリ&#x2060;ーズだ。とはいえ、消耗するともいえるので、一気に読む―残り二冊の合本が出るのを待&#x2060;って―か、少量ずつ、例えば半分だけ読んでまた戻&#x2060;ってくるなどの形で楽しむのが良いのではないだろうか。また、ふみが好きな私にと&#x2060;っても、ふみはやや取り上げられすぎなきらいもある。（そして、公平に言えば、あきらの出番も多いが、そちらの方が若干ゴリ押し感は少ない。）」&#x2060;[^07-04-18]

[^07-04-18]: &nbsp;Sean Gaffney, review of *Sweet Blue Flowers*, vol. 2, by Takako Shimura, *A Case Suitable for Treatment* (blog), December 22, 2017, [http://&#x200B;suitablefortreatment&#x200B;.mangabookshelf&#x200B;.com&#x200B;/2017&#x200B;/12&#x200B;/22&#x200B;/sweet&#x200B;-blue&#x200B;-flowers&#x200B;-omnibus&#x200B;-2](http://suitablefortreatment.mangabookshelf.com/2017/12/22/sweet-blue-flowers-omnibus-2).

Lesbraryのスーザンによるレビ&#x2060;ュー。描かれている人間関係の多様性（「健全な関係と不健全な関係、気持ちが通じ合うこととそうでないこと」）を賞賛している、好意的な評価である。「まだ良いシリーズだし、次の展開がどうなるか本当に知りたい。彼女たち全員に幸せにな&#x2060;ってほしいから！」&#x2060;[^07-04-19]

[^07-04-19]: &nbsp;Susan, review of *Sweet Blue Flowers*, vol. 2, by Takako Shimura, Lesbrary, February 13, 2019, [https://&#x200B;lesbrary&#x200B;.com&#x200B;/susan-reviews-sweet-blue-flowers-volume-2-by-takako-shimura/](https://lesbrary.com/susan-reviews-sweet-blue-flowers-volume-2-by-takako-shimura/).

### 第三巻 {#appendix-4-reviews-volume-3}

The Fandom Postのメリ&#x2060;ーナ&#x2060;・ダ&#x2060;ーギスによるレビ&#x2060;ュ&#x2060;ー。ダ&#x2060;ーギスは第三巻の評価を、内容B、芸術性B-、パ&#x2060;ッケ&#x2060;ージA-、テキスト&#x2060;・翻訳をAとした。「レビ&#x2060;ュア&#x2060;ーの立場からすると、この物語は内容を要約するのが難しい。一つのエピソ&#x2060;ードに、数ペ&#x2060;ージの短いシ&#x2060;ーンが五つくらいは簡単に出てくる。これは、一つの章が特定の人物や状況に絞&#x2060;って描かれることが多い他の漫画とは大きく異なる。しかし、これは読む上では良いことだ。また、このスタイルの語り口の良さは、物語を読むというより、物語を見るような感覚になることにもある。加えて更に、スト&#x2060;ーリ&#x2060;ーを面白く、感動的で、より深くすることにもなるが、人によ&#x2060;っては気が散&#x2060;って混乱するかもしれない。」&#x2060;[^07-04-20]

[^07-04-20]: &nbsp;Melina Dargis, review of *Sweet Blue Flowers*, vol. 3, by Takako Shimura, The Fandom Post, April 11, 2018, [https://&#x200B;www&#x200B;.fandompost&#x200B;.com&#x200B;/2018&#x200B;/12&#x200B;/08&#x200B;/sweet&#x200B;-blue&#x200B;-flowers&#x200B;-vol&#x200B;-03&#x200B;-manga&#x200B;-review](https://www.fandompost.com/2018/12/08/sweet-blue-flowers-vol-03-manga-review).

The Geekly GrindのEyeSpyeAlex（アレクサンドラ&#x2060;・ナ&#x2060;ッテ&#x2060;ィング）によるレビ&#x2060;ュ&#x2060;ー。非常に好意的である：総合評価10点満点中9.1点、スト&#x2060;ーリ&#x2060;ー8.8点、芸術性9.2点、登場人物9.3点と評価。「面白くて、愛おしくて、ち&#x2060;ょ&#x2060;っとセクシ&#x2060;ー。それが『青い花』最新巻を表現する言葉だ。」&#x2060;[^07-04-21]

[^07-04-21]: &nbsp;EyeSpyeAlex \[Alexandra Nutting\], review of *Sweet Blue Flowers*, vol. 3, by Takako Shimura, The Geekly Grind, March 25, 2018, [http://&#x200B;www&#x200B;.thegeeklygrind&#x200B;.com&#x200B;/sweet&#x200B;-blue&#x200B;-flowers&#x200B;-volume&#x200B;-4&#x200B;-review](http://www.thegeeklygrind.com/sweet-blue-flowers-volume-4-review) \[sic\].

*Okazu*のエリカ&#x2060;・フリ&#x2060;ードマンによるレビ&#x2060;ュ&#x2060;ー。フリ&#x2060;ードマンは、第三巻を10点満点中8点（一、二巻と同じ点）、絵と登場人物は8点、スト&#x2060;ーリ&#x2060;ーは7点、「レズビアン」は6点、そして「サ&#x2060;ービス」に1点の評価。「この巻は、個人的に、Vizが全四巻として刊行するものの中では最も強いと思う。恋愛ものの要件を満たすために物語がそれ自身に戻る前に、若い女性たちの人間としての進歩が見て取れる。」&#x2060;[^07-04-22]

[^07-04-22]: &nbsp;Erica Friedman, review of *Sweet Blue Flowers*, vol. 3, by Takako Shimura, *Okazu* (blog), April 11, 2018, [https://&#x200B;okazu&#x200B;.yuricon&#x200B;.com&#x200B;/2018&#x200B;/04&#x200B;/11&#x200B;/yuri&#x200B;-manga&#x200B;-sweet&#x200B;-blue&#x200B;-flowers&#x200B;-volume&#x200B;-3&#x200B;-english](https://okazu.yuricon.com/2018/04/11/yuri-manga-sweet-blue-flowers-volume-3-english).

*A Case Suitable for Treatment*のシ&#x2060;ョ&#x2060;ーン&#x2060;・ガフニ&#x2060;ーによるレビ&#x2060;ュ&#x2060;ー。一部制限ありの、概ね好意的なレビ&#x2060;ュ&#x2060;ー。「『青い花』は、もちろん、これまでと同じような問題も抱えている。…(中略)…とはいえ、これはなお非常に良い巻であり、次の第四巻で終わると思われるので、もう一度若い恋の苦しみに浸るために、本書を手に入れない理由はないだろう。」&#x2060;[^07-04-23]

[^07-04-23]: &nbsp;Sean Gaffney, review of *Sweet Blue Flowers*, vol. 3, by Takako Shimura, *A Case Suitable for Treatment* (blog), March 20, 2018, [http://&#x200B;suitablefortreatment&#x200B;.mangabookshelf&#x200B;.com&#x200B;/2018&#x200B;/03&#x200B;/20&#x200B;/sweet&#x200B;-blue&#x200B;-flowers&#x200B;-omnibus&#x200B;-3](http://suitablefortreatment.mangabookshelf.com/2018/03/20/sweet-blue-flowers-omnibus-3).

### 第四巻 {#appendix-4-reviews-volume-4}

The Geekly GrindのEyeSpyeAlex（アレクサンドラ&#x2060;・ナ&#x2060;ッテ&#x2060;ィング）によるレビ&#x2060;ュ&#x2060;ー。非常に好意的なレビ&#x2060;ュ&#x2060;ーであ&#x2060;った：総合評価10点満点中9.5点、スト&#x2060;ーリ&#x2060;ー9.0点、芸術性9.5点、登場人物9.0点である。「最終回へと進む中において、『青い花』は完全に上手く決めた作品とな&#x2060;った。この最新巻は、ドラマと解決策のバランスが完璧だ&#x2060;った。テンポも素晴らしく、本当にず&#x2060;っと見ていられた。」&#x2060;[^07-04-24]

[^07-04-24]: &nbsp;EyeSpyeAlex \[Alexandra Nutting\], review of *Sweet Blue Flowers*, vol. 4, by Takako Shimura, The Geekly Grind, July 2, 2018, [http://&#x200B;www&#x200B;.thegeeklygrind&#x200B;.com&#x200B;/sweet&#x200B;-blue&#x200B;-flowers&#x200B;-volume&#x200B;-4&#x200B;-review&#x200B;-2](http://www.thegeeklygrind.com/sweet-blue-flowers-volume-4-review-2).

*Okazu*のエリカ&#x2060;・フリ&#x2060;ードマンによるレビ&#x2060;ュ&#x2060;ー。フリ&#x2060;ードマンは、第四巻を10点満点中総合9点（これまでの巻よりやや高い点数）、芸術性、登場人物、スト&#x2060;ーリ&#x2060;ーはどれも9点、「サ&#x2060;ービス」は3点、「LGBTQ」は10点と評価した。「『青い花』の凄い所―それは、2005年に連載を開始した所だ。13年前の作品である。10年以上前は、これは百合の道標であ&#x2060;った。2018年現在では、我々の今いる所に至る重要な足がかりとなり、ついに英語での決定版ができたことで、今まさに成熟したジ&#x2060;ャンルに進む瞬間を迎えているのである。」&#x2060;[^07-04-25]

[^07-04-25]: &nbsp;Erica Friedman, review of *Sweet Blue Flowers*, vol. 4.

*A Case Suitable for Treatment*のシ&#x2060;ョ&#x2060;ーン&#x2060;・ガフニ&#x2060;ーによるレビ&#x2060;ュ&#x2060;ー。概ね好意的なレビ&#x2060;ュ&#x2060;ーだが、若干の制限がある。「『青い花』最終巻はこの特別な作品の強みと弱みを全て見せつけている。…(中略)…個人的には、まさしくち&#x2060;ょうどいい長さで終わ&#x2060;ったと思う―あと3～4巻続けるのは、疲れ切&#x2060;ってしまうだろうから。『青い花』には甘くほろ苦い瞬間があ&#x2060;ったが、甘さが勝つこともあるのだと教えてくれる終わり方であ&#x2060;った。」&#x2060;[^07-04-26]

[^07-04-26]: &nbsp;Sean Gaffney, review of *Sweet Blue Flowers*, vol. 4, by Takako Shimura, *A Case Suitable for Treatment* (blog), July 3, 2018, [http://&#x200B;suitablefortreatment&#x200B;.mangabookshelf&#x200B;.com&#x200B;/2018&#x200B;/07&#x200B;/03&#x200B;/sweet&#x200B;-blue&#x200B;-flowers&#x200B;-omnibus&#x200B;-4](http://suitablefortreatment.mangabookshelf.com/2018/07/03/sweet-blue-flowers-omnibus-4).

*Yuri Stargirl*のハイメによるレビ&#x2060;ュ&#x2060;ー。翻訳と他言語化の問題点の指摘を含む、好意的なレビ&#x2060;ュ&#x2060;ーである。彼女は第四巻を、10点満点中8点（「強くオススメ」）、作品全体を10点満点中9点（「必須作品」）と評価した。「（『青い花』は）私の大好きなアニメの一つであるだけでなく、私の大好きな漫画の一つでもある。ついに完結し、おかげでとても良い週とな&#x2060;った。…(中略)…スト&#x2060;ーリ&#x2060;ーも、当然、素晴らしく…(後略)…。」&#x2060;[^07-04-27]

[^07-04-27]: &nbsp;Jaime, review of *Sweet Blue Flowers*, vol. 4, by Takako Shimura, *Yuri Stargirl* (blog), June 22, 2018, [https://&#x200B;www&#x200B;.yuristargirl&#x200B;.com&#x200B;/2018&#x200B;/06&#x200B;/sweet&#x200B;-blue&#x200B;-flowers&#x200B;-aoi&#x200B;-hana&#x200B;-vol&#x200B;-4&#x200B;.html](https://www.yuristargirl.com/2018/06/sweet-blue-flowers-aoi-hana-vol-4.html).
