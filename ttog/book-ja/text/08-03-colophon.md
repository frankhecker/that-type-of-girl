---
title: "奥付"
---

## 奥付 {#colophon}

本書は、Markdown言語の派生であるkramdown&#x2060;[^08-03-01]で書かれたプレ&#x2060;ーンテキストのソ&#x2060;ースフ&#x2060;ァイル群を対象に、Electric Bookのワ&#x2060;ークフロ&#x2060;ー&#x2060;[^08-03-02]によ&#x2060;って作成された。
{:.first}

[^08-03-01]: “kramdown,” Thomas Leitner, updated January 2019, [https://&#x200B;kramdown&#x200B;.gettalong&#x200B;.org](https://kramdown.gettalong.org).

[^08-03-02]: “The Electric Book Template,” Electric Book Works, accessed March 21, 2020, [https://&#x200B;electricbookworks&#x200B;.github&#x200B;.io&#x200B;/electric&#x200B;-book&#x200B;/index&#x200B;.html](https://electricbookworks.github.io/electric-book/index.html).

PDF版および印刷版のテキストとして、本文部分には源ノ明朝を&#x2060;[^08-03-03]、章題等には源暎ゴシ&#x2060;ックPを組み合わせて用いた&#x2060;[^08-03-04]。表紙テキストには、ヒラギノ明朝を用いた&#x2060;[^08-03-05]。

[^08-03-03]: 「源ノ明朝」, Adobe, [https://&#x200B;github&#x200B;.com&#x200B;/adobe-fonts&#x200B;/source-han-serif](https://github.com/adobe-fonts/source-han-serif).

[^08-03-04]: おたもん、「源暎ゴシ&#x2060;ック」, okoneya.jp, accessed May 29, 2022, [https://&#x200B;okoneya&#x200B;.jp&#x200B;/font&#x200B;/genei-gothic&#x200B;.html](https://okoneya.jp/font/genei-gothic.html).

[^08-03-05]: 「ヒラギノフ&#x2060;ォント」, SCREEN Graphic Solutions, accessed May 29, 2022, [https://&#x200B;www&#x200B;.screen-hiragino&#x200B;.jp](https://www.screen-hiragino.jp).

表紙画像は、オ&#x2060;ーラ&#x2060;・タラカノ&#x2060;ーヴ&#x2060;ァ&#x2060;[^08-03-06]のオリジナルイラストを、画像処理ソフトPixelmator Proを用いることで作成した&#x2060;[^08-03-07]。

[^08-03-06]: Ola Tarakanova, “Watercolor Blue Flowers Stock Illustration,” iStock by Getty Images, accessed December 24, 2021, [https://&#x200B;www&#x200B;.istockphoto&#x200B;.com&#x200B;/vector&#x200B;/watercolor&#x200B;-blue&#x200B;-flowers&#x200B;-gm1148509305&#x200B;-310182255](https://www.istockphoto.com/vector/watercolor-blue-flowers-gm1148509305-310182255).

[^08-03-07]: “Pixelmator Pro,” Pixelmator Team, accessed December 7, 2021, [https://&#x200B;www&#x200B;.pixelmator&#x200B;.com&#x200B;/pro](https://www.pixelmator.com/pro).

本書のテキスト部分を成すMarkdownフ&#x2060;ァイルは、PDFおよびEPUB出力フ&#x2060;ァイルの作成手順やその他のフ&#x2060;ァイルとともに、公開リポジトリ（[https://&#x200B;gitlab&#x200B;.com&#x200B;/frankhecker&#x200B;/that&#x200B;-type&#x200B;-of&#x200B;-girl](https://gitlab.com/frankhecker/that-type-of-girl)）で入手可能である。修正案については、リポジトリに対するissueとしてGitLab上へ投稿するか、著者にEメ&#x2060;ールでご連絡いただければ幸いに思う。
