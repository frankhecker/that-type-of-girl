---
title: "リジーちゃんとダーシーさん"
---

## リジーちゃんとダーシーさん {#lizzy-chan-and-darcy-san}

翻訳版漫画のレビ&#x2060;ュア&#x2060;ーは、その翻訳の質について語ることが義務づけられているようである。私は日本語を話すことも読むこともできないので、『青い花』の英訳版についての意見はほとんど意味がない。しかし、私が読んだ他の漫画と比べて気づいた点は、述べておく価値があるだろう。VIZ Media版の『青い花』は極めて欧米的なものにな&#x2060;っている：書き文字や効果音は英語のみに変換されており（他の多くの翻訳版では、その辺の日本語はそのまま表記し、併記する形で翻訳英語を加えているのとは違&#x2060;って）、一部の食べ物の名前を除いて、登場人物が日本語の語彙や表現を使うことが全くないのである。
{:.first}

その翻訳の特徴が最も強く出ている部分として、敬称が全く存在しないという点が挙げられる。『青い花』アニメの英語字幕では「A-chan（あ&#x2060;ーち&#x2060;ゃん）」「Fumi-chan（ふみち&#x2060;ゃん）」「Manjome-san（万城目さん）」「Sugimoto-senpai（杉本先輩）」などが使われているが、この翻訳版漫画では「Akira」「Fumi」「Manjome」などだけである。

漫画評論家&#x2060;・翻訳家のレイチ&#x2060;ェル&#x2060;・ソ&#x2060;ーンは、英語翻訳で無差別に敬称を使うことに反対している。例えば、志村貴子『放浪息子』の翻訳版第一巻では、次のように述べている：「正当な理由なく日本語の敬称を保持することは、私には、自称オタクに排他的なクラブの一員であることを感じさせるための虚飾にしか見えない…」&#x2060;[^01-08-01]。私はこれまで、翻訳で敬称を使うのは無害だと思&#x2060;っていたが、彼女の言い分も理解できるようにな&#x2060;ってきている。

[^01-08-01]: Rachel Thorn, “Snips and Snails, Sugar and Spice: A Guide to Japanese Honorifics as Used in *Wandering Son*,” in Shimura, *Wandering Son*, 1:iii.

フ&#x2060;ァンの希望としては、人間関係の機微を翻訳で表現してほしいというのが本音であろう。その場合、英語では、18～19世紀の英文学をたくさん読んだ人なら誰でも知&#x2060;っている、完璧な方法がある。例えば、ジ&#x2060;ェイン&#x2060;・オ&#x2060;ーステ&#x2060;ィンの『高慢と偏見』には、日本語の敬語の典型的な用法のほとんどに相当する英語が含まれている（「～先輩」は大きな例外であるが）：ミスタ&#x2060;ー&#x2060;・ダ&#x2060;ーシ&#x2060;ーはほとんど常に「ミスタ&#x2060;ー&#x2060;・ダ&#x2060;ーシ&#x2060;ー」であり、友人のミスタ&#x2060;ー&#x2060;・ビングリ&#x2060;ーは呼び捨ての「ダ&#x2060;ーシ&#x2060;ー」と呼び、下の名前である「フ&#x2060;ィ&#x2060;ッツウ&#x2060;ィリアム」という名で呼ばれることは一切ない。（我々読者は、小説のほぼ中盤、エリザベス&#x2060;・ベネ&#x2060;ットの叔母であるミセス&#x2060;・ガ&#x2060;ーデ&#x2060;ィナ&#x2060;ーが言及するまで、その名を知ることすらないのである）&#x2060;[^01-08-02]。

[^01-08-02]: Jane Austen, *Pride and Prejudice* (London: 1813; Project Gutenberg, 2013), vol. 1, chap. 3, vol. 2, chap. 2, [https://&#x200B;gutenberg&#x200B;.org&#x200B;/ebooks&#x200B;/42671](https://gutenberg.org/ebooks/42671).

同様に、エリザベス&#x2060;・ベネ&#x2060;ットは、時と場合によ&#x2060;って「ミス&#x2060;・ベネ&#x2060;ット」、「ミス&#x2060;・エリザベス&#x2060;・ベネ&#x2060;ット」、（思い込みの激しい尊大なミスタ&#x2060;ー&#x2060;・コリンズが使う）「ミス&#x2060;・エリザベス」、（エリザベスより育ちの良いライバル、ミス&#x2060;・ビングリ&#x2060;ーが見下すように使う）「ミス&#x2060;・イライザ&#x2060;・ベネ&#x2060;ット」、および（友人シ&#x2060;ャ&#x2060;ーロ&#x2060;ットと姉ジ&#x2060;ェ&#x2060;ーンが使う）「デ&#x2060;ィア&#x2060;・エリザ（可愛いエリザ）」や「デ&#x2060;ィアレスト&#x2060;・リジ&#x2060;ー（リジ&#x2060;ーち&#x2060;ゃん）」（これはあきらの「あ&#x2060;ーち&#x2060;ゃん」に似ている）など、様々に呼び分けられているのである。小説の結末でダ&#x2060;ーシ&#x2060;ーが呼び捨てで「エリザベス」と呼ぶのは、彼の愛情と二人の関係の深まりを示すものといえる。しかし、彼女にと&#x2060;って彼は最後まで「ミスタ&#x2060;ー&#x2060;・ダ&#x2060;ーシ&#x2060;ー」なのである&#x2060;[^01-08-03]。

[^01-08-03]: Austen, *Pride and Prejudice*, vol. 1, chap. 10, vol. 1, chap. 6, vol. 1, chap. 18, vol. 1, chap. 8, vol. 1, chap. 22, vol. 1, chap. 6, vol. 3, chap. 16.

なぜ漫画の翻訳者はこの戦略をとらないのだろうか？恐らく、現代英語のカジ&#x2060;ュアルな表現に慣れているフ&#x2060;ァンにと&#x2060;って、そのような言葉遣いは信じられないほど堅苦しいものだと感じられてしまうからではないだろうか。彼らは自分の漫画がリ&#x2060;ージ&#x2060;ェンシ&#x2060;ー小説（訳注：イギリスの摂政時代の舞台を中心としたラブロマンス）のように読まれることを望まず、自分が「さん」と「様」の違いを知&#x2060;っていることの満足感をより好むのだ。ジ&#x2060;ョン&#x2060;・ウ&#x2060;ェリ&#x2060;ー訳の『青い花』英語版では、登場人物がお互いに名前か苗字で呼び合&#x2060;ってフ&#x2060;ォ&#x2060;ーマルさの度合いを区別しており、ミスタ&#x2060;ー&#x2060;・カガミは常に「ミスタ&#x2060;ー&#x2060;・カガミ」である。しかし、それ以外は非オタクの読者に向けられている。この翻訳者の選択は、私が『青い花』のテ&#x2060;ーマの一つであると考えていることにも関係してくる：欧米のアニメ&#x2060;・漫画フ&#x2060;ァンは、年齢、階級、性別などのヒエラルキ&#x2060;ーが比較的見られない社会にたまたま住んでいるのだ&#x2060;―少なくとも歴史的な基準から見れば。つまり欧米のフ&#x2060;ァンは、まさに言語そのものがこうしたヒエラルキ&#x2060;ーを明示するような社会で日々毎日を過ごすことがどのようなものであるかを、知らないのである。

例えば、レイチ&#x2060;ェル&#x2060;・ソ&#x2060;ーンによれば、日本人は、必要に応じてどちらが年上かを相手に尋ねて確認し、正しい話し方を身につけるという。「日本には『brothers』『sisters』に当たる言葉はない：『兄』『弟』『姉』『妹』だけである。双子の場合でも、片方が恣意的に『年長者』と定義される。」&#x2060;[^01-08-04]

[^01-08-04]: Thorn, “Snips and Snails, Sugar and Spice,” 1:iii.

欧米のアニメ&#x2060;・漫画フ&#x2060;ァンの中で、年齢や年功序列で組織された社会に本当に住みたいと思う人がどれだけいるのだろうか&#x2060;―ち&#x2060;ょうど、例えば、現代のジ&#x2060;ェイン&#x2060;・オ&#x2060;ーステ&#x2060;ィンの読者の中に、「目下の者」に話す際および「目上の者」に従う際、誰もが厳格な表現規範を持つような階級別の社会に住みたいと思う人が、果たしてどれだけいるのだろうかと思う。

さらに、敬称の使用は、必ずしも無害な異国情緒（欧米のフ&#x2060;ァンがそう考えるかもしれない）や尊敬の念（日本や他のアジア諸国の人々がそう考えるかもしれない）ばかりであるとは限らない。しばしば、敬語はヒエラルキ&#x2060;ーを示すだけでなく、ヒエラルキ&#x2060;ーを強化する役割を果たすこともある。小津安二郎の映画『秋日和』では、若い女性が、友人の母親の情事に干渉していると非難して、三人の中年男性に立ち向かう。彼らは彼女に座るように奨めるが、彼女は立&#x2060;ったままでいることを主張する。その内の一人が「ゆりち&#x2060;ゃん」と呼びかけ優位に立とうとするが、彼女は怒&#x2060;って「百合子と呼んで」と言い返すのだ&#x2060;[^01-08-05]。

[^01-08-05]: *Late Autumn*, directed by Yasujirō Ozu, in *Eclipse Series 3: Late Ozu (Early Spring / Tokyo Twilight / Equinox Flower / Late Autumn / The End of Summer)* (1958; New York: Criterion Collection, 2007), 1:41:24.

そんなヒエラルキ&#x2060;ーを、『青い花』はどのように捉えているのだろうか？これについては後に更に触れていくが、この英訳の特徴にヒントがあるように思う。
