#!/bin/sh
# Make all JA generic versions.

# NOTE: Run install-dependencies.sh before running this script.
./convert-images-ja.sh
./make-generic-screen-ja.sh
./make-generic-epub-ja.sh
./make-generic-print-ja.sh
