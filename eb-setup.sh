#!/bin/bash
# eb-setup.sh: Adapted from Electric Book script ebw-usb-setup.sh.
# Distributed under the terms of the GNU General Public License v3.0.

# This script sets up the Electric Book tools on Ubuntu 18.04 LTS. It
# assumes a fresh Ubuntu 18.04 install. Anywhere else, use at your own
# risk. Tested on Ubuntu 18.04.6 LTS Desktop with a minimal install.

# User guidance
echo 'This process will set up the Electric Book workflow tools on Ubuntu.'
echo 'This may take a while. You should get some tea.'

# Get user info for Git config
echo '------------------------------------'
echo 'To configure Git version control, we need your name and email address.'
read -p "Enter your full name: " name
read -p "Enter your email address: " email

# Get user to set folder name for new project
echo '------------------------------------'
echo 'We will create a new Electric Book project to work in.'
read -p "Folder name for new project (no spaces or punctuation): " projectFolder
if [ "$projectFolder" == "" ]; then
	echo 'Using "new-electric-book" as the folder name.'
	projectFolder='new-electric-book'
fi

# Work in the parent directory of the directory the script is in.
script_path=$(realpath $0)
script_dir=$(dirname ${script_path})
parent_dir=$(dirname ${script_dir})
cd ${parent_dir}
echo "The folder will be created in $PWD"

# Upgrade base Ubuntu to latest patches.
echo '------------------------------------'
echo 'Upgrading Ubuntu to latest patches...'
sudo apt update -y
sudo apt upgrade -y

# Update apt repository index
echo '------------------------------------'
echo 'Updating packages...'
sudo add-apt-repository "deb http://archive.ubuntu.com/ubuntu $(lsb_release -sc) universe" -y
sudo apt update -y

# Set up Ruby and dependencies for native gems
echo '------------------------------------'
echo 'Installing Ruby and its dependencies...'
sudo apt install ruby -y
sudo apt install ruby-dev -y
sudo apt install make -y
sudo apt install gcc -y
sudo apt install build-essential -y

# Update gems
echo '------------------------------------'
echo 'Updating Ruby gems...'
sudo gem update --system 3.0.6 --no-ri --no-rdoc

# Install Jekyll
echo '------------------------------------'
echo 'Installing Jekyll...'
sudo gem install jekyll -v 4.2.2

# Install Bundler
echo '------------------------------------'
echo 'Installing Bundler...'
sudo gem install bundler -v 2.3.26

# Install and configure Git and Git GUI
# (Using specific PPA for latest version)
echo '------------------------------------'
echo 'Installing Git and gitg (a Git GUI)...'
sudo apt install git -y
sudo apt install gitg -y
git config --global user.name "$name"
git config --global user.email $email

# Install Prince
echo '------------------------------------'
echo 'Installing PrinceXML...'
sudo apt install gdebi -y
wget https://www.princexml.com/download/prince_14.2-1_ubuntu18.04_amd64.deb
sudo gdebi prince_14.2-1_ubuntu18.04_amd64.deb --non-interactive

# Install PhantomJS
# (Adapted from https://www.vultr.com/docs/how-to-install-phantomjs-on-ubuntu-16-04
# Another option: https://gist.github.com/telbiyski/ec56a92d7114b8631c906c18064ce620)
echo '------------------------------------'
echo 'Installing PhantomJS...'
sudo apt install chrpath libssl-dev libxft-dev libfreetype6-dev libfreetype6 libfontconfig1-dev libfontconfig1 -y
wget https://bitbucket.org/ariya/phantomjs/downloads/phantomjs-2.1.1-linux-x86_64.tar.bz2
sudo tar xvjf phantomjs-2.1.1-linux-x86_64.tar.bz2 -C /usr/local/share/
sudo ln -s /usr/local/share/phantomjs-2.1.1-linux-x86_64/bin/phantomjs /usr/local/bin/

# Install Pandoc
echo '------------------------------------'
echo 'Installing Pandoc...'
wget https://github.com/jgm/pandoc/releases/download/2.5/pandoc-2.5-1-amd64.deb
sudo dpkg -i pandoc-2.5-1-amd64.deb

# Install Node
echo '------------------------------------'
echo 'Installing Node...'
sudo snap install node --classic --channel=10

# Install Gulp (updated to fix permissions problem with npm)
echo '------------------------------------'
echo 'Installing Gulp...'
sudo npm install --global gulp-cli --unsafe-perm=true --allow-root

# Install GraphicsMagick
# (Thanks https://gist.github.com/witooh/089eeac4165dfb5ccf3d)
echo '------------------------------------'
echo 'Installing GraphicsMagick...'
sudo apt install software-properties-common -y
sudo add-apt-repository ppa:rwky/graphicsmagick -y
sudo apt update
sudo apt install graphicsmagick -y

# Install Emacs (substituted for VS Code)
echo '------------------------------------'
echo 'Installing Emacs...'
sudo apt install emacs25-nox -y

# Install better PDF viewer and make it default
# (Because sometimes evince has 'permission denied' issues.)
echo '------------------------------------'
echo 'Installing xPDF...'
sudo apt install xpdf -y
sudo sed -i '/pdf/s/evince./xpdf./g' /etc/gnome/defaults.list

# Install an epub reader (substitute Foliate for Calibre)
echo '------------------------------------'
echo 'Installing Foliate...'
sudo snap install foliate

# Install an epub validator
echo '------------------------------------'
echo 'Installing EPUBCheck...'
wget https://github.com/w3c/epubcheck/releases/download/v4.2.6/epubcheck-4.2.6.zip --directory-prefix /tmp
(cd /usr/local/bin; sudo unzip /tmp/epubcheck-4.2.6.zip)
sudo apt install openjdk-11-jre-headless -y

# Clone the Electric Book template
echo '------------------------------------'
echo 'Cloning the electric-book template repository...'
git clone https://github.com/electricbookworks/electric-book.git

# Use the Electric Book template version as of 2022-03-06.
echo '------------------------------------'
echo 'Checking out Electric Book version as of 2022-03-06...'
cd electric-book
git checkout 38799cc52ceb2c738980dc1fd6831817a9195e8b
cd ..

# Allow run-linux.sh to execute
echo '------------------------------------'
echo 'Giving the Electric Book run script permission to execute...'
cd electric-book
chmod +x run-linux.sh
cd ..

# Install EB template dependencies
echo '------------------------------------'
echo 'Installing the Electric Book template dependencies...'
cd electric-book
bundle update
bundle install
sudo chown -R $USER:$(id -gn $USER) ~/.config ~/.npm
npm install
cd ..

# Rename template and reset git history for a new project
echo '------------------------------------'
echo 'Creating a new, blank project from the template...'
mv electric-book $projectFolder
cd $projectFolder
rm -rf .git
git init
git add .
git commit -m "New project created"
cd ..

# Check installations
echo '------------------------------------'
echo 'Done. Listing installed versions:'
echo '------------------------------------'
echo 'Ruby version installed?' && ruby --version
echo '------------------------------------'
echo 'Jekyll version installed?' && jekyll --version
echo '------------------------------------'
echo 'Bundler version installed?' && bundler --version
echo '------------------------------------'
echo 'Git version installed?' && git --version
git config --list
echo '------------------------------------'
echo 'gitg version installed?' && gitg --version
echo '------------------------------------'
echo 'PrinceXML version installed?' && prince --version
echo '------------------------------------'
echo 'PhantomJS version installed?' && phantomjs --version
echo '------------------------------------'
echo 'Pandoc version installed?' && pandoc --version
echo '------------------------------------'
echo 'Node version installed?' && node --version
echo '------------------------------------'
echo 'Gulp version installed?' && gulp --version
echo '------------------------------------'
echo 'GraphicsMagick version installed?' && gm -version
echo '------------------------------------'
echo 'Emacs version installed?' && emacs --version
echo '------------------------------------'
echo 'XPDF version installed?' && xpdf -v
echo '------------------------------------'
echo 'Foliate version installed?' && foliate --version

installError=0
function isInstalled {
	if hash $1 2>/dev/null; then
		echo "$1 is installed."
	else
		echo "$1 is not installed, sorry."
		installError=1
	fi
}

# Check that everything is installed
echo '------------------------------------'
echo 'Done. Checking installs:'
isInstalled ruby
isInstalled jekyll
isInstalled bundler
isInstalled git
isInstalled gitg
isInstalled prince
isInstalled phantomjs
isInstalled pandoc
isInstalled node
isInstalled gulp
isInstalled gm
isInstalled emacs
isInstalled xpdf
isInstalled foliate

if [ $installError == 1 ]; then
	echo '------------------------------------'
	echo 'There were errors installing tools, sorry.'
	echo 'Please check the logs above to find what went wrong.'
else
	echo '------------------------------------'
	echo 'Everything is installed, the Electric Book tools are ready to use.'
fi
