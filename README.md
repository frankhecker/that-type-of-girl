# That Type of Girl

This repository contains text and images for the book *That Type of
Girl: Notes on Takako Shimura’s* Sweet Blue Flowers, as well as for
the book’s Japanese translation, 『そっち系のひと：志村貴子「青い花」
に関する考察』.

The contents of the repository can be used to create a version of the
book in three different formats:

- A PDF file suitable for reading online with a standard PDF reader
  utility such as `xpdf` for Ubuntu. This version includes a (generic)
  book cover and working links.
- An EPUB file suitable for reading online with a standard EPUB reader
  utility such as Foliate for Ubuntu. This version also includes a
  (generic) book cover and working links.
- A PDF file suitable for printing in a 6-inch by 9-inch format. This
  version does not include a cover, and links do not work.
- A web version suitable for hosting on a web server. This version is
  available for the English edition only.

Note that the covers for the online PDF, EPUB, and web versions do not
include the watercolor illustration of blue flowers present on the
versions available from frankhecker.com. See the section on licensing
and redistribution below for more information.

The repository also contains the RMarkdown file and unabridged
character index used to create the separate document “Relative
Prominence of Characters and Their Relationships in Takako Shimura’s
*Sweet Blue Flowers*.”

## Contributing

If you have suggestions for changes to the book, please submit them as
issues in GitLab against this repository (if you have a userid on
gitlab.com) or just send them to me via email at
frank@frankhecker.com.

I am particularly interested in reports of typographical, grammatical,
or other errors. You’re also welcome to suggest other changes, for
example, to improve the writing or expand on particular points.

If you want to distribute a version of the book, with or without your
changes, please see the section below on licensing and redistribution.

## Building the book

### Dependencies

The book is built using the [Electric Books Jekyll template][eb]
created by [Electric Book Works][ebw] running on Ubuntu 18.04 Desktop
for 64-bit PC (AMD64). (Note that the current version of this software
will not run on later versions of Ubuntu.)

[eb]: https://electricbookworks.github.io/electric-book/
[ebw]: https://electricbookworks.com/

The script `eb-setup.sh` installs all other needed software and
prepares a working Electric Book project directory in which to build
PDF and EPUB versions.

The script `ttog-setup.sh` copies over the files for the book itself
and makes a few changes to the Electric Book template to fix various
issues.

### Installing the Electric Book system

For best results, start with a clean install of Ubuntu Desktop 18.04
LTS. If you are installing from CD or DVD, or are creating a virtual
machine using VMware Fusion or some other virtualization environment,
you can start with the [ubuntu-18.04.6.desktop-amd64.iso][u18] ISO
file. You need install only the minimal version.

[u18]: https://releases.ubuntu.com/18.04/ubuntu-18.04.6-desktop-amd64.iso

NOTE: For the remaining steps you must have sudo access from whatever
userid you use. If you are installing Ubuntu from scratch, use the
userid you created during the installation process.

First, create a suitable directory in which to work. I suggest
`Documents/Source` under your home directory; the instructions below
assume the use of that directory. All other directories will be
created under this source directory.

    $ cd ~/Documents
    $ mkdir Source

Go into the source directory, download the zip file containing the
book’s git repository and extract the files:

    $ cd ~/Documents/Source
    $ wget https://gitlab.com/frankhecker/that-type-of-girl/-/archive/main/that-type-of-girl-main.zip
    $ unzip that-type-of-girl-main.zip

This will create the directory `that-type-of-girl-main` in the source
directory.

Go into the directory `that-type-of-girl-main` and run the
`eb-setup.sh` script. This script will update Ubuntu to the latest
18.04 LTS patches, install the Electric Book code and all of its
prerequisites, and install some other useful utilities.

    $ cd ~/Documents/Source/that-type-of-girl-main
    $ ./eb-setup.sh

NOTE: The script will prompt you for the name of a project directory.
This directory can be named anything *except*
`that-type-of-girl-main`. You can hit return to accept the default
name of `new-electric-book`.

The script will also prompt for your name and email address in order
to initialize the git configuration, and when needed will prompt for
your password in order to complete installation of some files.

The script takes a while to run, so be patient, especially when it
appears to be hung. It should take less than half an hour to complete
on a modern laptop with a high-speed Internet connection. Running the
script will produce some warnings about out-of-date software and
security vulnerabilities, but it should complete successfully with all
software installed.

### Installing the book files

At this point the Electric Book directory is ready for use, but does
not have any actual book files. To load the Electric Book directory
with the files and scripts for *That Type of Girl*, go into the
directory `that-type-of-girl-main` and run the script `ttog-setup.sh`.

    $ cd ~/Documents/Source/that-type-of-girl-main
    $ ./ttog-setup.sh

NOTE: The script will prompt you for the name of a project directory.
This is the same directory name you entered when running the
`eb-setup.sh` script. If you chose the default name of
`new-electric-book`, just hit return to use that name.

### Building the book

The Electric Book directory is now ready for you to build the book.
(The examples below assume that you named the Electric Book directory
`new-electric-book`.)

You must first install the various dependencies for the Electric Book
template code. This script need only be run once.

    $ cd ~/Documents/Source/new-electric-book
    $ ./install-dependencies.sh

If you want to build all versions at once, you can use the
`make-generic-all.sh` script.

    $ cd ~/Documents/Source/new-electric-book
    $ ./make-generic-all.sh

(To build all versions for the Japanese translation, run
`make-generic-all-ja.sh` instead.)

You can also build particular versions. To do so, you must convert the
image files from their original format to the formats needed for the
various output types. (This script is run automatically when you use
the `make-generic-all.sh` script.)

    $ ./convert-images.sh

(The corresponding script for the Japanese version is
`convert-images-ja.sh`.)

You can then build a version of the book using one of the following
scripts:

- `make-generic-epub.sh` (generic EPUB).
- `make-generic-print.sh` (PDF for printing).
- `make-generic-screen.sf` (PDF for online reading).
- `make-generic-web.sf` (web version).

For example, if you want to create a PDF file for online reading, run
the script `make-screen-generic.sh`.

    $ ./make-generic-screen.sh

(The corresponding script for the Japanese version is
`make-generic-screen-ja.sh`.)

Assuming the script runs without errors, it will produce a file
`book-screen.pdf` (or `book-screen-ja.pdf` for the Japanese version)
in the `_output` subdirectory of the Electric Book directory.

Note that you will see various errors when opening the PDF files with
`xpdf`, or opening the epub file with `foliate`. These messages are
not due to the files themselves, and can be ignored.

You can also build book files using the `run-linux.sh` script. (This
script is invoked by the scripts discussed above.) There are three
steps to doing this:

1. Run the `run-linux.sh` script and select option 9 to install or
   update various code dependencies for the Electric Book template.
   You must run this option at least once, before doing anything else.
2. Run the `run-linux.sh` script and select option 7 to convert images
   in the `book/images/_source` directory for use in the various
   output formats. You must run this option at least once, before
   doing anything else.
3. Run the `run-linux.sh` script and select the appropriate option to
   build the particular format you’re interested in: option 1 for a
   PDF file for printing, option 2 for a PDF file for online reading,
   option 3 for a web version (created as a tar file), and option 4
   for an EPUB file.

(When using the `run-linux.sh` script to build the Japanese versions,
enter `book-ja` for the name of the book’s subdirectory.)

Note that the PDF output files will have a watermark in the
upper-righthand corner of the first page. This is produced by the
trial version of the Prince software installed by the `eb-setup.sh`
script. The watermark will not be present when using a licensed copy
of Prince.

## Licensing and redistribution

Both the Electric Book template and the text of the book itself are
available under licenses that permit you to redistribute this book, in
whole or in part, with or without modifications. However there are
other elements of the book that have various restrictions, as
described below.

### Licensing terms for the Electric Book template

The code for the Electric Book template is distributed under the terms
of the [GNU General Public License 3.0][gpl]. Although the repository
for *That Type of Girl* does not contain a complete copy of the
Electric Book code, it does contain the following files that are
derived from Electric Book code:

[gpl]: https://www.gnu.org/licenses/gpl.html

- `eb-setup.sh`
- `_config.yml`
- `index.md`
- `_data/meta-epub-generic.yml`
- `_data/meta-print-generic.yml`
- `_data/meta-screen-generic.yml`

Since they are derivative works of GPL-licensed files, these files are
also distributed under the terms of the GNU GPL 3.0.

### License terms for the text of the book

The complete text of the book is in the Markdown files `*.md` in the
subdirectory `book/text`. All such files are distributed under the
terms of [Creative Commons Attribution-Sharealike (CC-BY-SA) 4.0
license][cc].

[cc]: http://creativecommons.org/licenses/by-sa/4.0/legalcode

Note that the license permits you to distribute modified versions of
the text, with material extracted, deleted, added or changed. Such a
derivative work could simply have corrections for typos or dead links,
be a complete translation of the book into another language, or
anything in between. Whatever the case, the license generally requires
that you distribute such modified versions using the same license
terms under which you received it, or terms compatible with those
terms.

Thus you would normally distribute the modified text under the
CC-BY-SA license. However, you may instead distribute your modified
version under the terms of the GNU GPL 3.0, i.e., the same license
used by the Electric Book code. See the discussion of [compatible
licenses][compat] on the Creative Commons site.

[compat]: https://creativecommons.org/share-your-work/licensing-considerations/compatible-licenses

There are two other conditions that are not part of the license terms,
but that I ask you to follow as a matter of common courtesy to me as
the original author of the book:

- Do not change the text to make it appear as if I am making arguments
  or offering opinions not in the original book.
- If you add material that goes beyond what is included in the
  original book---for example, making arguments or offering opinions
  of your own---please make it clear in the text that such arguments
  or opinions are yours, not mine.

### Licensing of images included with the book

There are three images associated with the book and included in at
least some output formats:

- `cover.jpg` (cover page, not included in the print version).
- `japan-arranged-marriages-vs-love-marriages.png` (included in the
  chapter “The Decline of S”).
- `sbf-social-graph.png` (included in Appendix 2, “Character
  Relationships”).

(The corresponding files for the Japanese translation are
`cover-ja.jpg`, `japan-arranged-marriages-vs-love-marriages-ja.png`,
and `sbf-social-graph-ja.png`.

The English and Japanese cover pages included in the public repository
are generic versions that do not include the [watercolor illustration
of blue flowers][ola] created by Ola Tarakanova. The terms of the
[iStock content license agreement][istock] under which I obtained that
illustration permit me to use it in works created and distributed by
me, but do not permit me to sublicense it to others.

[ola]: https://www.istockphoto.com/vector/watercolor-blue-flowers-gm1148509305-310182255
[istock]: https://www.istockphoto.com/legal/license-agreement

If you wish to distribute a version of the book with the original
cover illustration, you must obtain a license by purchasing the rights
to use the illustration. (Fortunately, the cost to do this is
relatively low, only USD 12 at the time of writing.) If you have a
valid license to the illustration and wish to create a modified cover,
contact me at frank@frankhecker.com and I will provide you a copy of a
Pixelmator Pro or Adobe Photoshop file that you can use to create a
new cover.

The graph of arranged marriages vs. love marriages is from a
[document][] produced by the [National Institute of Population and
Social Security Research][ipss], attached to the Japanese Ministry of
Health, Labor and Welfare. The document, and the graph within it, are
copyrighted works of the Japanese government.

[document]: http://www.ipss.go.jp/ps-doukou/e/doukou15/Nfs15R_points_eng.pdf
[ipss]: https://www.ipss.go.jp/pr-ad/e/eng/index.html

In including the illustration in the book I am exercising my fair use
rights under US law in order to reproduce the graph for purposes of
commentary and criticism. If you live outside the US, then your rights
under local law may be different.

The *Sweet Blue Flowers* social graph is from my own analysis. As
noted in the [analysis document][sbfsg], I have made the entire
document, including all included graphs, available for unrestricted
use, distribution and modification under the terms of the [Creative
Commons CC0 1.0 Universal (CC0 1.0) Public Domain Dedication][cc0].
In other words, I’ve placed the document and its graphs in the public
domain, to the extent that I can do so.

[sbfsg]: https://rpubs.com/frankhecker/874648
[cc0]: https://creativecommons.org/publicdomain/zero/1.0/

### Use of ISBNs associated with the book

The copies of the configuration files in the repository (e.g.,
`_data/meta-pdf.yml`) include dummy values for the book ISBNs. If you
wish to distribute a modified version of the book commercially (for
example, through Amazon Kindle Direct Publishing or a similar
service), you will need to acquire your own ISBNs, either from the
distribution service or from whatever organization is authorized to
assign ISBNs in your country.

Do *not* re-use for your own versions of the book any of the ISBNs
included in the versions of the book that I distribute through my
website, Amazon, or other services. Those ISBNs are assigned to me as
the publisher, and no one else is authorized to use them.

## Recreating the *Sweet Blue Flowers* social graph

The subdirectory `analysis` contains two files that I used to create
the *Sweet Blue Flowers* social graph included in Appendix 2 of the
book:

- `sbf-social-graph.Rmd`. The RMarkdown source for the analysis.
- `char-index.md`. The unabridged version of the character index
  Markdown file used to create Appendix 1.

If you would like to reproduce or extend the analysis, you can use the
[RStudio Desktop][] software for Windows, macOS, or Linux, or the
[RStudio Cloud][] service.

[RStudio Desktop]: https://www.rstudio.com/products/rstudio/
[RStudio Cloud]: https://rstudio.cloud/

I personally recommend the RStudio Cloud service, which has a generous
free tier. You will need to set up Python as described in the analysis
document, and install any R packages not already installed.
