#!/bin/bash

# This script sets up the TTOG book files to be built. It assumes that
# the eb-setup.sh script has already been run to create an Electric
# Book project directory in the parent directory of the directory in
# which this script is located.

# User guidance
echo 'This process will copy the TTOG book files to the EB project directory'

# Get user to set folder name for new project
echo '------------------------------------'
echo 'You must have an already-created new Electric Book project.'
read -p "Folder name used for project (no spaces or punctuation): " projectFolder
if [ "$projectFolder" == "" ]; then
	echo 'Using "new-electric-book" as the folder name.'
	projectFolder='new-electric-book'
fi

# Check to verify that the parent directory exists and is in the
# parent directory of the directory the script is in.
script_path=$(realpath $0)
ttog_dir=$(dirname ${script_path})
parent_dir=$(dirname ${ttog_dir})
if [ ! -d ${parent_dir}/${projectFolder} ]
then
    echo "The folder ${projectFolder} does not exist in ${parent_dir}"
    exit 1
fi
eb_dir=${parent_dir}/${projectFolder}

# Replace example book files with TTOG book files.
echo '------------------------------------'
echo 'Deleting example book files...'
rm -f ${eb_dir}/_config.yml
rm -f ${eb_dir}/index.md
rm -f ${eb_dir}/_data/meta.yml
rm -f ${eb_dir}/book/text/*.md
for d in app epub print-pdf screen-pdf _source web
do
    rm -rf ${eb_dir}/book/images/${d}/*
done
rm -rf ${eb_dir}/_site/*

echo '------------------------------------'
echo 'Copying TTOG book files...'
cp ${ttog_dir}/ttog/_config.yml ${eb_dir}/
cp ${ttog_dir}/ttog/index.md ${eb_dir}/
cp ${ttog_dir}/ttog/_data/meta-*.yml ${eb_dir}/_data/
cp ${ttog_dir}/ttog/book/text/* ${eb_dir}/book/text/
cp ${ttog_dir}/ttog/book/images/_source/* ${eb_dir}/book/images/_source/

echo '------------------------------------'
echo 'Copying TTOG (JA) book files...'
mkdir -p ${eb_dir}/book-ja/text
mkdir -p ${eb_dir}/book-ja/images/_source
mkdir -p ${eb_dir}/book-ja/styles
mkdir -p ${eb_dir}/book-ja/fonts
cp ${ttog_dir}/ttog/book-ja/text/* ${eb_dir}/book-ja/text/
cp ${ttog_dir}/ttog/book-ja/images/_source/* ${eb_dir}/book-ja/images/_source/
cp ${ttog_dir}/ttog/book-ja/styles/* ${eb_dir}/book-ja/styles/
cp ${ttog_dir}/ttog/book-ja/fonts/* ${eb_dir}/book-ja/fonts/
cp ${ttog_dir}/ttog/book-ja/index.md ${eb_dir}/book-ja/
cp ${ttog_dir}/ttog/book-ja/package.opf ${eb_dir}/book-ja/
cp ${ttog_dir}/ttog/book-ja/toc.ncx ${eb_dir}/book-ja/

# Make changes to EB files to fix various issues.
echo '------------------------------------'
echo 'Modifying run-linux.sh script to use epubcheck 4.2.6...'
sed -i -e 's/epubcheck-4.2.2/epubcheck-4.2.6/' ${eb_dir}/run-linux.sh

echo '------------------------------------'
echo 'Modifying run-linux.sh script to not run web server...'
sed -i -e 's/jekyll serve/jekyll build/' ${eb_dir}/run-linux.sh

echo '------------------------------------'
echo 'Modifying _epub-base-typography.scss to fix Calibre issue...'
grep -q '// overflow-x:' ${eb_dir}/_sass/template/partials/_epub-base-typography.scss
if [ $? -ne 0 ]
then
    sed -i -e 's:overflow-x:// overflow-x:' \
	${eb_dir}/_sass/template/partials/_epub-base-typography.scss
fi

echo '------------------------------------'
echo 'Modifying _print-page-setup-lightning-source.scss to fix Prince issue...'
sed -i -e 's/\-center/-middle/' \
    ${eb_dir}/_sass/template/partials/_print-page-setup-lightning-source.scss

echo '------------------------------------'
echo 'Modifying _epub-cover.scss to fix display of cover image for ADE...'
grep -q 'height: 100vh; // ADE fix' ${eb_dir}/_sass/template/partials/_epub-cover.scss
if [ $? -ne 0 ]
then
    sed -i -e '/div.cover/a\' \
	-e '\t\theight: 100vh; // ADE fix' \
	${eb_dir}/_sass/template/partials/_epub-cover.scss
fi

echo '------------------------------------'
echo 'Modifying print-pdf.scss for KDP and D2D...'
grep -q '$print-page-setup-lightning-source' ${eb_dir}/book/styles/print-pdf.scss
if [ $? -ne 0 ]
then
    sed -i -e '/^\$edition-suffix/a\' \
	-e '$print-page-setup-lightning-source: true;' \
	${eb_dir}/book/styles/print-pdf.scss
fi

echo '------------------------------------'
echo 'Changing accent color for web version...'
grep -q '$color-accent: $template-blue' ${eb_dir}/_sass/template/web.scss
if [ $? -ne 0 ]
then
    sed -i -e '/^\$color-accent/s/\$template-green/$template-blue/' \
        ${eb_dir}/_sass/template/web.scss
fi

echo '------------------------------------'
echo 'Display images as smaller size on web version...'
grep -q 'sizes="auto"' ${eb_dir}/_includes/image
if [ $? -eq 0 ]
then
    sed -i -e 's/sizes="auto"/sizes="66vw"/' \
        ${eb_dir}/_includes/image
fi

echo '------------------------------------'
echo 'Replacing locales.yml file to define JA locale...'
cp ${ttog_dir}/ttog/_data/locales.yml ${eb_dir}/_data/

echo '------------------------------------'
echo 'Replacing nav.yml file to change About reference in web version...'
cp ${ttog_dir}/ttog/_data/nav.yml ${eb_dir}/_data/

echo '------------------------------------'
echo 'Replacing search include file to disable web search box...'
cp ${ttog_dir}/ttog/_includes/search ${eb_dir}/_includes/

# Copy over scripts to build EPUB, print PDF, and screen PDF.
echo '------------------------------------'
echo 'Copying TTOG make scripts...'
cp ${ttog_dir}/ttog/install-dependencies.sh  ${eb_dir}/
cp ${ttog_dir}/ttog/convert-images*.sh       ${eb_dir}/
cp ${ttog_dir}/ttog/make-generic-*.sh        ${eb_dir}/
chmod +x ${eb_dir}/install-dependencies.sh
chmod +x ${eb_dir}/convert-images*.sh
chmod +x ${eb_dir}/make-generic-*.sh
