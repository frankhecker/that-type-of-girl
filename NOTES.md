# Additional Notes

This is an informal set of notes meant to supplement the README
file. It includes notes relevant to the Japanese translation of _That
Type of Girl_, as well as to any future translations into other
languages.

## Japanese translation notes

The following are summaries of the decisions made regarding the
Japanese translation, based on correspondence between Frank Hecker and
Konsuke, the EN-to-JA translator.

*Translation of the book’s title.* The original title “That Type of
Girl” was taken from Fumi’s statement to her friends on page 3:309 of
_Sweet Blue Flowers_: “I *am* that type of girl” (in response to a
question from Pon).  The original Japanese phrase is 「そっち系のひと」,
which is gender-neutral; it can be translated as “that type of
person”.  We decided that fidelity to the manga was more important
than having the word “girl” be in the title of the Japanese
translation.

Translation of the book’s subtitle. The word “notes” in the book’s
subtitles (“Notes on Takako Shimura’s _Sweet Blue Flowers_” was
intended to convey the idea that the chapters of the book were for the
most part relatively informal musings not worthy of the word “essay”.

A literal translation of “notes” into Japanese does not convey the
same sense. We therefore settled on the subtitle 「志村貴子『青い花』
に関する考察」. In place of the word “notes” this uses a word that can
be translated as “investigation” or “study”.

*Transliteration of Western names.* Western personal names are
referenced in the book credits (i.e., Frank Hecker, the author and
publisher, and Ola Tarakanova, the artist who created the original
illustration of flowers used on the cover), in the text of the book
(e.g., scholars whose works are quoted), and in the endnotes (the
authors of the works cited). We decided to transliterate Western names
to katakana in the book credits and within the text, but to retain the
original names in the endnotes. (See also the note below on source
citations.)

Source citations. There are many books, papers, and other sources
cited in the endnotes and in the bibliography. We decided to retain
all citations as is, with no translation or transliteration of
personal names and titles of works, in order to reflect the actual
sources consulted and make it easier for readers to do Internet
searches for the works cited. The only exceptions are for citations to
works in Japanese and additional comments included within the
endnotes; these have been translated into Japanese.

In-text citations. For the convenience of readers not having access to
(or not being able to read) the English edition of *Sweet Blue
Flowers*, we include in the in-text citations volume and page numbers
for both the original Japanese edition of 『青い花』 and the English
edition of *Sweet Blue Flowers*.

Formatting of such in-text citations is as shown below, with page
numbers in the English edition converted to page numbers in the
Japanese edition:

    （『青い花』(3) p. 97, (4) pp. 32--3 / *SBF*, 2:97, 2:112--13）

Except for 『青い花』 and the enclosing fullwidth left and right
parentheses (U+FF08 and U+FF09 respectively), all other characters in
the in-text citation are in the basic Latin block (U+0000 through
U+007F).

In page ranges for the Japanese edition, the second number includes
only changed digits from the initial page number. Page ranges for the
English edition are in the format recommended by the Chicago Manual of
Style.

Note that there is *not* a space between ‘』’ and the first volume
number. All other elements of the in-text citation have standard
spaces (U+0020) between them, and lines may be broken at those points.

*Fonts.* In order to be able to freely distribute the book under the
CC BY-SA license and allow others to freely create their own PDF
versions, we need to use only fonts available under a license that
allows unrestricted use and redistribution for both noncommercial
*and* commercial use. Unfortunately there is not a wide variety of
freely-licensed Japanese fonts, and some of those are of lesser
quality.

Fortunately we were able to find a suitable font for the text of the
book: [GenEi Gothic][geg], a modified version of [Adobe Source Han
Sans][han]. We use this (in regular and bold weights) for both body
text and display text (e.g., chapter titles).

[geg]: https://okoneya.jp/font/genei-gothic.html
[han]: https://fonts.adobe.com/fonts/source-han-sans-japanese

The text on the cover of the book is instead set using the Hiragino
Micho typeface. This is a high-quality typeface (roughly equivalent to
a Western serif typeface) distributed by Apple with macOS. Since the
cover image is distributed only as a JPG file, there is no need to
redistribute the Hiragino Mincho font files and thus no issues
regarding licensing.

*Line breaking.* Japanese publishers use two general sets of criteria
for deciding where to break lines in Japanese text, sometimes referred
to as “lax” criteria and “strict” criteria. For more information see
the W3C “[Requirements for Japanese Text Layout][w3c]”, in particular
[section 3.17][s317], and the “[Japanese Typesetting Expressions
Manual][ebpaj]” published by the [Digital Publishers Federation of
Japan][dpfoj], in particular section 3-18.

[w3c]: https://www.w3.org/TR/jlreq/?lang=en
[s317]: https://www.w3.org/TR/jlreq/?lang=en#characters_not_starting_a_line
[ebpaj]: http://www.ebpaj.jp/images/kumihan-en.pdf
[dpfoj]: http://www.ebpaj.jp/

Line breaking according to the strict criteria likely cannot be done
without either hand-coding line-break points or using text-processing
software specially designed for Japanese text and using a Japanese
line-breaking dictionary. We therefore decided to adopt the lax
criteria, with the additional requirement that lines *not* be broken
before endnote superscripts and the long vowel ‘ー’ (katakana-hiragana
prolonged sound mark, U+30FC).

To prevent line breaks at unwanted points, we use the Unicode [word
joiner][wj] character (U+2060), inserted into Markdown text as
`&#x2060;`. To allow line breaks where they would otherwise not be
allowed, we use the Unicode [zero-width space][zwsp] (U+200B),
inserted into Markdown text as `&#x200B`. This is used, for example,
to allow line breaking in long URLs according to the [rules
recommended by the Chicago Manual of Style][cmos].

[wj]: https://en.wikipedia.org/wiki/Word_joiner
[zwsp]: https://en.wikipedia.org/wiki/Zero-width_space
[cmos]: https://www.chicagomanualofstyle.org/qanda/data/faq/topics/URLs/faq0008.html
