# Changelog

## [1.0.1] - 2022-03-14
### Changed
- Remove brand names of bookstores from "Appendix 4: Reviews".

## [1.0.0] - 2022-03-12
### Added
- Release first edition.
